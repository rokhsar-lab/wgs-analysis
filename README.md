
[TOC]

# **Introduction** #

The `run-wgs-preprocessing` code is a Bash script for fire-and-forget clean-up
of raw [FASTQ](http://en.wikipedia.org/wiki/FASTQ_format) files. It requires 
access to a cluster (i.e., UGE/SGE or SLURM) or a single machine with many cores. 
The pipeline makes the following standard procedure for handling WGS data easy:

1. Adaptor trimming
2. Base quality trimming
3. Read alignment
4. BAM generation
5. Duplicate marking
6. Generate basic run statistics
7. Cleanup of intermediate files


# **Getting Started** #

## Installation ##
Successfully running any of the included pipeline scripts requires a successful
install of various dependencies, please see the complete **Install** section
below for a list of dependencies and detailed installation instructions.


## Usage ##
To dump a short list of available options, type:

```bash
run-wgs-preprocessing -h
```

To obtain a descriptive list of available options and their defaults, type:

```bash
run-wgs-preprocessing --help
```

An example command-line looks like:

```bash
cd /abs/path/to/LIBRARY1_RUN1

nohup run-wgs-preprocessing \
  --config your.config \
  --cleanup \
  &> run.log &
```

**NOTE**: It is important to use the correct flowcell ID and lane values, as both
are combined with the library name to produce unique read-group identifiers 
for each run.


## Directory Structure ##
It is essential to create a new project sub-directory for each library x
sequencing run x lane, as the pipeline uses touch files to manage progress and
running multiple instances of the pipeline in the same directory will cause 
failure. A recommended directory structure is hierarchically arranged by
sample name, then library and sequencing run. A sample with multiple libraries
sequenced on multiple lanes might look like:

```bash
SAMPLE_NAME/
   LIBRARY1_L001/
       LIBRARY1_L001.fofn
       LIBRARY1_index_L001_R1_NNN.fastq.gz
       LIBRARY1_index_L001_R2_NNN.fastq.gz
       [...]
   LIBRARY1_L002/
       LIBRARY1_L002.fofn
       LIBRARY1_index_L002_R1_NNN.fastq.gz
       LIBRARY1_index_L002_R2_NNN.fastq.gz
       [...]
   LIBRARY2_L001/
       LIBRARY2_L001.fofn
       LIBRARY2_index_L001_R1_NNN.fastq.gz
       LIBRARY2_index_L001_R2_NNN.fastq.gz
       [...]
   [...]
```

## FOFN Files ##
The pipeline works most efficiently on many FASTQ files in parallel over a
cluster (when available) or by leveraging multiple CPUs; therefore, it is highly
recommended that the analyst breaks up his/her large FASTQ files into multiple,
smaller sub-parts (4 million reads per file is recommended). To accomodate large
datasets with many sub-part FASTQ files, a FOFN file is required for each
run of the pipeline to track which data are available to it. The FOFN file
includes _full_ paths to the raw FASTQ files (uncompressed or gzip'd) and lists
one sub-part file per line. A FOFN file for single-read data or collated/
interleaved paired-end data has the following format:

```bash
/abs/path/to/read_001.fastq.gz<\n>
/abs/path/to/read_002.fastq.gz<\n>
[...]
```


A typical FOFN file for end-separated/pre-uncollated paired-end data lists
both read-end 1 (R1) and read-end 2 (R2) on a single line, as seen in this
example:

```bash
/abs/path/to/read_R1_001.fastq.gz<\t>/abs/path/to/read_R2_001.fastq.gz<\n>
/abs/path/to/read_R1_002.fastq.gz<\t>/abs/path/to/read_R2_002.fastq.gz<\n>
[...]
```

Where `<\t>` and `<\n>` represent tab and newline characters, respectively.
See `examples/PE.fofn` and `examples/PEcollated.fofn` for toy examples.


## Config Files ##
This pipeline relies on a config file (see `examples/wgs.config`) for properly
setting up a run. It uses Bash syntax, and so spaces *must* not be used when
assigning values:

```bash
OPTION_NAME="value"    # correct
OPTION_NAME = "value"  # WRONG
```

For boolean-type options, leave false values as undefined. Any other value may
be evaluated as true:

```bash
BOOL_OPTION=;   # means 'false'
BOOL_OPTION=0;  # may be evaluated as 'true'
```

General run-global options are:

* `WORKDIR`: File path. The run-specific directory. Created if not pre-existing.

* `REFERENCE_FASTA`: File path. The path to the reference genome [FASTA](https://en.wikipedia.org/wiki/FASTA_format) file.

* `ADAPTER_FASTA`: File path. The path to any adapter sequences used for library prep in FASTA format.

* `READS_FOFN`: File path. A "File-Of-File-Names" containing two columns (for paired-end reads with end-specific files) or one column (for single-read sequencing or interleaved/collated paired-end runs) of file names to sequencing read FASTQ files. If inputting collated paired-end data, set the `READS_COLLATED` option to `1`. See the **FOFN Files** section above for more info about the file format.

* `SAMPLE_ID`: String. The sample name. No spaces allowed. 

* `LIBRARY_ID`: String. The sequencing library name. No spaces allowed.

* `LIBRARY_INDEX`: String. The nucleotide sequencing index (if applicable).

* `FLOWCELL_ID`: String. The sequencing flowcell ID string.

* `FLOWCELL_LANE`: Integer. The sequencing lane number.

* `SEQUENCING_CENTER`: String. The name of the sequencing center that generated the run.

* `SEQUENCING_PLATFORM`: Eunumerative. Sequencing type; may be one of: `CAPILLARY`, `DNBSEQ`, `HELICOS`, `ILLUMINA`, `IONTORRENT`, `LS454`, `ONT` (Oxford Nanopore), `PACBIO` (Pacific Biosciences), or `SOLID`.

* `READS_PAIRED`: Boolean. Set to `1` for paired-end reads, or unset for false.

* `READS_COLLATED`: Boolean. Set to `1` for collated paired-end reads, or unset for false.

* `PROPER_PAIRS_ONLY`: Boolean. Set to `1` to filter for properly-paired aligned reads, or unset for false.

* `USE_LEGACY_BWA`: Boolean. Set to `1` to use `bwa aln` and `bwa sampe` (or `bwa samse` for single reads), unset for false.


There are multiple stages that the pipeline runs, and config file parameters for
both the tool(s) executed at each stage and how to allocate compute resources for
them. For each stage, config options ending in `*_SUBMIT_OPTIONS` refer to the
`qbatch submit` options used to run a process and `*_EXE_OPTIONS` refer to the
software being executed at that stage (e.g., `ALIGN_SUBMIT_OPTIONS` and
`ALIGN_EXE_OPTIONS`). 

For possible options to `qbatch submit` or a specific stage, please refer to their
respective usage pages. Stages are:

* `REFIDX`: Configures `bwa index`, see `man bwa` for possible options.

* `INPUT`: Configures job resources for splitting collated FASTQ files into end-specific files. Only `INPUT_SUBMIT_OPTIONS` is applicable. 

* `ADAPTER_TRIM`: Configures `fastq-mcf`, see `fastq-mcf --help` for possible options.

* `ALIGN`: Configures either `bwa aln` or `bwa mem`, depending on whether the `$USE_LEGACY_BWA` option is enabled. See their respective `man` pages for options.

* `FIXMATE`: Configures `samtools fixmate`. The `-m` option is always enabled, see `man samtools` for additional options.

* `SORT`: Configures `samtools sort`, see `man samtools` for possible options.

* `MERGE`: Configures `samtools merge`, see `man samtools` for possible options.

* `MARKDUP`: Configures `samtools markdup`, see `man samtools` for possible options.

* `FILTER`: Configures `samtools view`, see `man samtools` for possible options.

* `INDEX`: Configures `samtools index`, see `man samtools` for possible options.

* `STATS`: Configures `samtools stats`, see `man samtools` for possible options.


# **Install** #

## 1. Perl modules ##

Install or update the Perl module dependencies listed below.
For example, to install the `Data::Dumper` module, do:

```bash
perl -MCPAN -e 'install "Data::Dumper"'
```

#### Perl Module Dependencies ####

* `Data::Dumper`
* `English`
* `Env`
* `File::Basename`
* `FileHandle`
* `File::Path`
* `File::Spec`
* `File::Temp`
* `File::Which`
* `Getopt::Long`
* `Getopt::Std`
* `IO::Compress::Gzip`
* `IO::Uncompress::Gunzip`
* `IO::Handle`
* `POSIX`
* `strict`
* `threads`


## 2. This repository ##

The user can obtain a clone of this repository by invoking the
following commands (The user may be prompted for a password,
hit the Enter key to continue):

```bash
git clone --recursive https://bredeson@bitbucket.org/rokhsar-lab/wgs-analysis.git

# for a globally-accessible install:
make install -C wgs-analysis

# for a user-specific install:
make install -C wgs-analysis PREFIX=/path/to/install/destination
```

The above will download and compile all software dependencies (excluding
the Perl module dependencies listed above). In many cases, the third-party
software dependencies may already be installed in the user's system or,
alternatively, may be installed via easier means (i.e., via `conda`). If
this is the case, the minimal new dependencies requiring installation are
the Perl libraries. These may be installed seperately:

```bash
make install-libraries -C wgs-analysis PREFIX=/path/to/install/destination
make install-scripts -C wgs-analysis PREFIX=/path/to/install/destination

source wgs-analysis/activate
```

## 3. Configure your enironment ##

### 3.1 Quickly and temporarily ###
An `activate` file is created in the repository when `make install` is executed.
Run one of the following commands to add to your environment all the required
paths to use the tools included in `wgs-analysis`:
```bash
source wgs-analysis/activate

# or
. wgs-analysis/activate
```

### 3.2 Persistently ###
Alternatively, the user may include the full paths in their `~/.bashrc` file
(create one if it does not yet exist):

```bash
export PATH="/path/to/wgs-analysis/bin:$PATH";
export MANPATH="/path/to/wgs-analysis/share/man:$MANPATH";
export INCLUDE_PATH="/path/to/wgs-analysis/include:$INCLUDE_PATH";
export LIBRARY_PATH="/path/to/wgs-analysis/lib:$LIBRARY_PATH";
export LD_LIBRARY_PATH="/path/to/wgs-analysis/lib:$LD_LIBRARY_PATH";
export PERL5LIB="/path/to/wgs-analysis/lib:$PERL5LIB";
```

## 4. External Dependencies ##

### Data QC and Alignment  ###
The dependencies installed above with `make install` include
the following third-party software:

* **ea-utils** v1.1.2+ <http://code.google.com/p/ea-utils/wiki/FastqMcf> 
* **BWA** 0.5.9-r16+ (not 0.7.6a, <2.0) <https://github.com/lh3/bwa>
* **HTSlib** <https://github.com/samtools/htslib>
* **SAMtools** 1.0+ <https://github.com/samtools/samtools>
* **Tabix** <http://sourceforge.net/projects/samtools/files/tabix>
* **SeqTk** <https://github.com/lh3/seqtk>
* **Sickle** <https://github.com/najoshi/sickle>
* **Valkyr** <https://bitbucket.org/bredeson/valkyr>

### Miscellaneous ###
The following dependencies are NOT installed via `make install` above
and must be installed separately.

* **Perl** v5.10.1+ <http://www.activestate.com/activeperl/downloads>
* Job Schedulers supported:
    * **SGE** (or derivatives) <http://en.wikipedia.org/wiki/Oracle_Grid_Engine>
    * **SLURM** <https://slurm.schedmd.com/documentation.html>


# **Citing this work** #

If you use this code in your research, please cite the following publication:

Bredeson JV, Lyons JB, Prochnik SE, Wu GA, Ha CM, Edsinger-Gonzales E, Grimwood J, Schmutz J,
Rabbi IY, Egesi C, Nauluvula P, Lebot V, Ndunguru J, Mkamilo G, Bart RS, Setter TL, Gleadow RM,
Kulakow P, Ferguson ME, Rounsley S, Rokhsar DS. Sequencing wild and cultivated cassava and
related species reveals extensive interspecific hybridization and genetic diversity.
*Nat Biotechnol*. 2016 May;34(5):562-70. doi:10.1038/nbt.3535.


# **Bugs and Issues** #

## Known Issues ##

After quality filtering, poor quality sequencing runs may produce empty files.
These will often cause the pipeline to hang at the `adapter_trim` step. Should this
happen, and you can verify that this is why the pipeline failed, creating the
touch file manually may be necessary (e.g. `touch adapter_trim.complete`).

Some difficulty is known running the pipeline in non-Bash shells.

## Reporting ##
Any comments and bugs should be directed to the author.

## Contact ##

Jessen V. Bredeson <jessenbredeson@berkeley.edu>

