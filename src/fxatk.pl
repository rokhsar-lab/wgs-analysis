
$PACKAGE = '__PACKAGE_NAME__';
$VERSION = '__PACKAGE_VERSION__';
$CONTACT = '__PACKAGE_CONTACT__';
$PURPOSE = 'Tools for working with fasta files';

require 5.010;

use strict;
use warnings;
use English;

use constant {
    #faidx
    NAME  => 0, LENGTH => 1, OFFSET => 2, LINE_LEN => 3, LINE_BLEN => 4,
    #gff3
    SEQID => 0, SOURCE => 1, TYPE  => 2, RSTART => 3, REND => 4,
    SCORE => 5, STRAND => 6, PHASE => 7, ATTR   => 8, ITER => 9,
    #sequence
    DESC  => 1, SEQ   => 2, QUAL  => 3
};
use POSIX qw(ceil);
use Getopt::Std;
use File::Basename qw(basename);
use vars qw($PROGRAM $PACKAGE $PURPOSE $VERSION $CONTACT
            $DEBUG $TYPE %FUNCTION $CMD @BUFFER %CODON);

BEGIN {
    $PROGRAM = basename($PROGRAM_NAME);
    $DEBUG = 0;
    %CODON = 
        ('TTT' => 'F', 'TTC' => 'F', 'TTA' => 'L', 'TTG' => 'L',
         'TCT' => 'S', 'TCC' => 'S', 'TCA' => 'S', 'TCG' => 'S',
         'TAT' => 'Y', 'TAC' => 'Y', 'TAA' => '*', 'TAG' => '*',
         'TGT' => 'C', 'TGC' => 'C', 'TGA' => '*', 'TGG' => 'W',
         'CTT' => 'L', 'CTC' => 'L', 'CTA' => 'L', 'CTG' => 'L',
         'CCT' => 'P', 'CCC' => 'P', 'CCA' => 'P', 'CCG' => 'P',
         'CAT' => 'H', 'CAC' => 'H', 'CAA' => 'Q', 'CAG' => 'Q',
         'CGT' => 'R', 'CGC' => 'R', 'CGA' => 'R', 'CGG' => 'R',
         'ATT' => 'I', 'ATC' => 'I', 'ATA' => 'I', 'ATG' => 'M',
         'ACT' => 'T', 'ACC' => 'T', 'ACA' => 'T', 'ACG' => 'T',
         'AAT' => 'N', 'AAC' => 'N', 'AAA' => 'K', 'AAG' => 'K',
         'AGT' => 'S', 'AGC' => 'S', 'AGA' => 'R', 'AGG' => 'R',
         'GTT' => 'V', 'GTC' => 'V', 'GTA' => 'V', 'GTG' => 'V',
         'GCT' => 'A', 'GCC' => 'A', 'GCA' => 'A', 'GCG' => 'A',
         'GAT' => 'D', 'GAC' => 'D', 'GAA' => 'E', 'GAG' => 'E',
         'GGT' => 'G', 'GGC' => 'G', 'GGA' => 'G', 'GGG' => 'G' );
}

#----------------------------------------------------------------#
#                              SUBS                              #
#----------------------------------------------------------------#

# could add -q to clean, which would accept fna.qual files and standardize
# the per-base qual widths to two chars wide (white-space padded)


sub __FUNC__ { 
    my $lvl = shift || 0;
    return((reverse(split '::',(caller($lvl+1))[3]))[0]) 
}

sub stack {
    my ($type,@msg) = @_;
    my $lvl = 2;
    my $rtn = __FUNC__($lvl);
    print(STDERR "[$PROGRAM $rtn]", '_'x(77-length($PROGRAM.$rtn)),"\n");
    print(STDERR join('',$type,@msg),"\n");
    while (caller($lvl)) {
        my @i = caller($lvl++);
        print(STDERR join(' ',$i[1],'sub',$i[3],'line',$i[2]),"\n");
    }
    print(STDERR '_'x80,"\n\n");
}

sub throw {
    if ($DEBUG) { 
        &stack('EXCEPTION: ',@_);
    } else {
        printf(STDERR "[%s %s] %s\n",$PROGRAM,__FUNC__(1),join('',@_));
    }
    exit(1);
}

sub warn {
    if ($DEBUG) { 
        &stack('WARNING: ',@_);
    } else {
        printf(STDERR "[%s %s] %s\n",$PROGRAM,__FUNC__(1),join('',@_));
    }
}

sub debug {
    printf(STDERR "[%s %s] %s\n",$PROGRAM,__FUNC__(1),join('',@_)) if $DEBUG;
}

sub FUNC_USAGE {
    my $usage = uc(__FUNC__(2));
    $usage .= '_USAGE';
    eval $usage; # kinda kludgy, but still require strict refs
    print STDERR $@;
    exit(1);
}

sub char {
    my $val = shift // &FUNC_USAGE;
    $val =~ /^[!-~][ !-~]*$/ && return $val;
    &warn('Not a valid, printable string');
    &FUNC_USAGE;
}

sub uint {
    my $val = shift // &FUNC_USAGE;
    $val =~ /^[0-9]+$/ && return $val;
    &warn("Not of unsigned int type: $val");
    &FUNC_USAGE;
}

sub sint {
    my $val = shift // &FUNC_USAGE;
    $val =~ /^[-+]?[0-9]+$/ && return $val;
    &warn("Not of signed int type: $val");
    &FUNC_USAGE;
}

sub ufloat {
    my $val = shift // &FUNC_USAGE;
    $val =~ /^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/ && return $val;
    &warn("Not of unsigned float type: $val");
    &FUNC_USAGE;
}

sub sfloat {
    my $val = shift // &FUNC_USAGE;
    $val =~ /^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/ && return $val;
    &warn("Not of signed float type: $val");
    &FUNC_USAGE; 
}

sub regx {
    my $val = shift // &FUNC_USAGE;
    if (ref($val) =~ /Regexp/) { return $val }
    if (ref(\$val) eq 'SCALAR' && eval {'' =~ /$val/; 1}) {
	return $val;
    } else { 
	&warn("Not a valid regexp: $val");
	&FUNC_USAGE; 
    }
}

sub file {
    my $val = shift // return 0;
    my $op  = shift || '';

    -f $val || return 0;
    if ($op =~ /S/i) { -s $val || &warn("$val is empty.") }
    if ($op =~ /R/i) { -r $val || &warn("$val is not readable.") }
    return 1;
}

sub fai {
    my $fa = shift // return;

    my ($fafai,$fai) = ($fa,$fa);
    $fafai .= '.fai';
    $fai =~ s/\.[^.]+/.fai/;  

    if (-f $fafai && -s $fafai && -r $fafai) {
	return $fafai;
    } elsif (-f $fai && -s $fai && -r $fai ) {
	return $fai;
    } else { return }
}

sub read_list {
    my ($file,$field) = @_;
    &file($file,'r') || &throw(($file//'List file'),' not a regular file.');

    my $data = {};
    open(LIST,$file) || &throw("Cannot open $file for reading.");
    while (local $_ = <LIST>) {
        m/^#|^\s*$/ && next;
	chomp;
	my @F = split /\t/;
	$data->{$F[0]} = $field ? $F[$field] : 1;
    }
    close(LIST);
    return $data;
}

sub read_coordinates {
    my $file = shift;
    &file($file,'r') || &throw(($file//'Coord. file'),' not a regular file.');

    my $data = {};
    open(LIST,$file) || &throw("Cannot open $file for reading.");
    while (local $_ = <LIST>) {
        m/^#|^\s*$/ && next;
	chomp;
	my @F = split /\t/;
	$F[0] =~ /^[!-)+-<>-~][!-~]*$/ || 
	    &throw("Invalid identifier (line $.).");
	$data->{$F[0]}->{'start'} = $F[1]||1;
	$data->{$F[0]}->{'stop'}  = $F[2]||0;
    }
    close(LIST);
    return $data;
}

sub read_faidx {
    my $file = shift;
    my $prfx = $file;
    
    # try to detect all valid .fai file names
    $prfx =~ s/\.f(ast|n)?a$//;
    if (-f "$file.fai") {
	$file = "$file.fai";
    } elsif (-f "$prfx.fai") {
	$file = "$prfx.fai";
    } else {
	$file = undef;
    }


    &file($file,'rs') || &throw(($file//'faidx'),' not readable or is empty.');

    my $data = {};
    open(FAI,$file) || &throw("Cannot open $file for reading.");
    while (local $_ = <FAI>) {
        m/^#|^\s*$/ && next;
        m/([!-)+-<>-~][!-~]*)\t(\d+)\t(\d+)/ || 
            &throw("Invalid identifier or insufficient data (line $.).");
        exists $data->{$1} && 
            &warn("Duplicate entry detected in $file (line $.).");
        $data->{$1}->{'length'} = $2;
	$data->{$1}->{'pos'} = $3;
    }
    close(FAI);
    return $data;
}

sub read_fasta {
    my $FH = shift;
    ref($FH) eq 'GLOB' || 
	&throw("Invalid input: Must pass a GLOB for reading.");
    
    local $_ = &nextline($FH) // return;
    m/^>/ || &throw("Missing expected sequence header (line $.)");
    my (@fa) = m/^>\s*(\S*)(.*)/;
    
    $fa[SEQ] = '';
    while ($_ = &nextline($FH)) { # "\n" evaluates true
	m/^\s*$/ && next;
	if (/^>/) { &pushback($_); last }
	chomp;
	$fa[SEQ] .= $_;
    }
    
    return(\@fa)
}

sub pull_fasta {
    my $pos = shift;
    defined($pos) || 
	&throw('Invalid input: uint position expected at ',__FUNC__);
    seek(FA,$pos,0) || &throw("Could not seek() to positon $pos");

    my $seqstr = '';
    while (local $_ = <FA>) {
        m/^>/ && last;
        $seqstr .= $_;
    }
    $seqstr =~ s/\s//g;
    return $seqstr;
}
	
sub rvcmp {
    my $str = shift;
    defined($str) && ref(\$str) eq 'SCALAR' || 
	&throw('Invalid input: Sequence string expected at ',__FUNC__);
    $str =~ /[0-9]/ && &throw('Invalid input: Numeric passed to ',__FUNC__);
    $str =~ tr/atugcrymkATUGCRYMK/taacgyrkmTAACGYRKM/;
    return scalar(reverse($str));
}

sub nextline {
    my $FH = shift;
    ref($FH) eq 'GLOB' || &throw('Invalid input: GLOB expected at ',__FUNC__);
    return(scalar(@BUFFER) ? shift @BUFFER : <$FH>);
}

sub pushback {
    unshift @BUFFER,@_ if @_;
    return scalar(@_);
}

sub CLEAN_USAGE {
    die "Usage: $PROGRAM clean [-o <out.fasta>] <-|in.fasta> [...]\n";
}

sub clean {
    my (%argv,$outfile);
    getopts('Xho:',\%argv) && @ARGV || &CLEAN_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'h') { &CLEAN_USAGE }
	elsif ($o eq 'X') { $DEBUG   = 1 }
	elsif ($o eq 'o') { $outfile = &char($argv{$o}) } 
    }
    open(STDOUT,">$outfile") || &throw("Cannot open $outfile for writing.")
	if defined $outfile;
    
    while (my $fa = shift @ARGV) {
	$fa eq '-' || &file($fa,'r') || &throw("Cannot open $fa for reading.");
	open(FA,$fa) || &throw("Cannot open $fa for reading");
	while (my $fasta = &read_fasta(\*FA)) {
	    $fasta->[2] =~ s/[^a-zA-Z*]//g;

	    print join("\n",'>'.$fasta->[SEQID].$fasta->[DESC],
		       $fasta->[SEQ] =~ /(.{1,60})/g,'');
	}
	close(FA);
    }
    close(STDOUT);
    return 1;
}

sub REDEF_USAGE {
    die "Usage: $PROGRAM redef [-o <out.fasta>] <in.def> <-|in.fasta> [...]\n";
}

sub redef {
    my (%argv,$outfile);
    getopts('Xho:',\%argv) && @ARGV >= 2 || &REDEF_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'h') { &REDEF_USAGE }
	elsif ($o eq 'X') { $DEBUG   = 1 }
	elsif ($o eq 'o') { $outfile = &char($argv{$o}) } 
    }
    open(STDOUT,">$outfile") || &throw("Cannot open $outfile for writing.")
        if defined $outfile;

    my $newdef = &read_list(shift @ARGV,1);
    while (my $fa = shift @ARGV) {
	$fa eq '-' || &file($fa,'r') || &throw("Cannot open $fa for reading.");
	open(FA,$fa) || &throw("Cannot open $fa for reading");
	while (my $fasta = &read_fasta(\*FA)) {
	    if ($newdef->{$fasta->[SEQID]}) {		
		if ($newdef->{$fasta->[SEQID]} =~ /^>/) {
		    $fasta->[SEQID] = $newdef->{$fasta->[SEQID]};
		    $fasta->[SEQID] =~ s/^>//;
		    $fasta->[DESC] = '';
		} else {
		    $fasta->[DESC] = $newdef->{$fasta->[SEQID]};
		}
	    }
	    print join("\n",'>'.$fasta->[SEQID].$fasta->[DESC],
		       $fasta->[SEQ] =~ /(.{1,60})/g,'');
	}
	close(FA);
    }
    close(STDOUT);
    return 1;
}

sub FAIDX_USAGE {
    die "Usage: $PROGRAM faidx <in.fasta>\n";
}

sub faidx {
    my %argv;
    getopts('Xh',\%argv) && @ARGV == 1 || &FAIDX_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'h') { &FAIDX_USAGE }
	elsif ($o eq 'X') { $DEBUG   = 1 }
    }

    for my $fa (@ARGV) {
	&file($fa,'rs') && open(FA,$fa) || 
	    &throw("Cannot open $fa for reading.");
	
	my @INIT = (undef,0,0,0,0); 
	my ($o,@FAI) = (0,@INIT);

	local $_  = '';	
	do {$_ = <FA>; $o += length} while !/\S+/;
	m/^>\s*(\S+)/ || &throw('File not in fasta format.');
	@FAI[NAME,OFFSET] = ($1,$o);
	
        open(FAI,">$fa.fai") || &throw("Cannot open $fa.fai for writing.");
	while (<FA>) {
	    m/^>/ && &throw("No sequence string found at (line $.).");
	    my $seq = $_;
	    chomp;
	    $FAI[LINE_BLEN] = length($seq);
	    $FAI[LINE_LEN]  = length;
	    $o += $FAI[LINE_BLEN];
	    my ($i,$l) = (0,$FAI[LINE_BLEN]);
	    while (<FA>) {
		$o += length;
		m/^>/ && last;
		$seq .= $_;
		$l  = length;
		$i++;
	    }
	    # now make sure our sequence is flush (ignoring last line)
	    length($seq) - $l == $i*$FAI[LINE_BLEN] ||
		&throw("Different line length in sequence '",($FAI[NAME]||''),"'.");
	    
	    $seq =~ s/\s+//g;
	    $FAI[LENGTH] = length($seq);
	    print FAI join("\t",@FAI),"\n";
	    
	    (defined && m/^>\s*(\S+)/) || eof(FA) ||
		&throw("No seqeunce ID detected (line $.)");
	    @FAI = @INIT; # reset
	    @FAI[NAME,OFFSET] = ($1,$o);
	}
	close(FAI);
	close(FA);
    }
    return 1;
}

sub STATS_USAGE {
    die qq/
Usage:   $PROGRAM stats [options] <-|in.fasta> [...]

Options: -o FILE  Write chrom stats to FILE [stdout]
         -l FILE  Write summary stats to FILE [stderr]
         -g INT   Expected genome size [null]


/;

}

sub stats {
    my (%argv,$outfile,$logfile,$gsize);
    getopts('Xho:l:g:',\%argv) && @ARGV || &STATS_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'h') { &STATS_USAGE }
	elsif ($o eq 'X') { $DEBUG   = 1 }
	elsif ($o eq 'o') { $outfile = &char($argv{$o}) }
	elsif ($o eq 'l') { $logfile = &char($argv{$o}) }
	elsif ($o eq 'g') { $gsize   = &uint($argv{$o}) }
    }
    open(STDOUT,">$outfile") || &throw("Cannot open $outfile for writing.")
        if defined $outfile;
    open(STDERR,">$logfile") || &throw("Cannot open $logfile for writing.")
        if defined $logfile;
    
    my %stat = map {$_ => 0} qw(MAX TOTNUM TOTLEN MEAN STDDEV N50);
    $stat{'MIN'} = 9**9**9;

    my ($h,$l,$n,$hdr,$seq) = (0,0,0,0,'');
    my (%bases,%length,%stats,@data,%char,%lens,$type,@alph);
    while (my $fa = shift @ARGV) {
	$fa eq '-' || &file($fa,'r') || &throw("Cannot open $fa for reading.");
	open(FA,$fa) || &throw("Cannot open $fa for reading.");
	print "#$fa\n" if $hdr;
	while (my $fasta = &read_fasta(\*FA)) {
	    $fasta->[SEQ] =~ s/[^a-zA-Z]//g;
	    if (!@alph) {
		my $C = substr($fasta->[SEQ],0,100);
		my $c = $C =~ s/([^atucgnATUCGN])/$1/g;
		($type,@alph) = (($c/length($C)) >= 0.35)
		    ? ('amino acid',split('','ACDEFGHIKLMNPQRSTVWXY')) 
		    : ('nucleotide',split('','ATUCGN'));
		printf(STDERR "[%s %s] $fa auto-detected as $type.\n",
		       $PROGRAM,__FUNC__);
	    }
	    
	    my $length = length($fasta->[SEQ]);
	    $stat{'TOTLEN'} += $length;
	    $stat{'TOTNUM'}++;
	    $stat{'MIN'} = $length if $length < $stat{'MIN'};
	    $stat{'MAX'} = $length if $length > $stat{'MAX'};
	    $lens{$length}++;
	    my $fxa = uc $fasta->[SEQ];

	    my %scaf;
	    @scaf{@alph} = split('','0' x scalar(@alph));
	    for(my $i = 0; $i < $length; $i++) {
		my $c = substr($fxa,$i,1); # 'c' for char
		$data[$i]{'TOTAL'}++;
		$data[$i]{$c}++;
		$char{$c}++;
		$scaf{$c}++;
	    }
	    if (!$hdr) {
		print join("\t",'#seqid','length',@alph),"\n";
		print "#$fa\n";
		$hdr=1;
	    }
	    print join("\t",$fasta->[SEQID],$length,@scaf{@alph}),"\n";
	}
	close(FA);
    }
    close(STDOUT);
    
    printf(STDERR "[%s %s] Calculated %s\n",$PROGRAM,__FUNC__,scalar localtime);
    # calc N50 and L50
    my $len = 0;
    $stat{'MEAN'} = $stat{'TOTLEN'}/$stat{'TOTNUM'};
    my @sorted = sort {$b <=> $a} keys %lens;
    for my $l (@sorted) {
	$stat{'STDDEV'} += $lens{$l}*(($stat{'MEAN'} - $l)**2);
	if ($len <= $stat{'TOTLEN'}/2) {
	    $stat{'L50'} = $l;
	    $stat{'N50'}++;
	}
	if ($gsize && ($len <= ($gsize/2))) {
	    $stat{'LG50'} = $l;
	    $stat{'NG50'}++;
	}
	$len += $lens{$l}*$l;
    }
    $stat{'STDDEV'} = sqrt($stat{'STDDEV'}/$stat{'TOTNUM'});
    $stat{'MEDIAN'} = (scalar(@sorted) % 2 || scalar(@sorted) < 2)
	? $sorted[$#sorted/2] 
	: ($sorted[($#sorted-1)/2] + $sorted[($#sorted+1)/2])/2;

    # print base percents
    print(STDERR "SEQUENCE COMPOSITION:\n");
    my @char = sort keys %char;
    while (@char) {
	my @c = splice(@char,0,10);
	my ($hstr,$cstr) = ('','');
	while (my $c = shift @c) {
	    $hstr .= sprintf('%7s',$c);
	    $cstr .= sprintf('%7.2f',100*$char{$c}/$stat{'TOTLEN'});
	}
	print(STDERR join("\n",$hstr,$cstr,'',''));
    }
    print(STDERR "SEQUENCE METRICS:\n");
    my @order = qw(TOTLEN TOTNUM MEAN MEDIAN STDDEV MIN MAX N50 L50 NG50 LG50);
    my ($w) = sort {$b <=> $a} map{length(sprintf('%.2f',$stat{$_}))} keys %stat;
    $w += 2;
    while (@order) {
	my @o = splice(@order,0,int(80/$w));
	my ($hstr,$ostr) = ('','');
	while (my $o = shift @o) {
	    $hstr .= sprintf('%'.$w.'s',$o||0);
	    $ostr .= sprintf('%'.$w.'.2f',$stat{$o}||0);
	}
	print(STDERR join("\n",$hstr,$ostr,'',''));
    }
    close(STDERR);
    return 1;
}   

sub SUBSEQ_USAGE {
    die "Usage: $PROGRAM subseq [-o <out.fasta>] <in.coord.list> ".
	"<-|in.fasta> [...]\n";
}

sub subseq {
    &warn("This utility contains known bugs that have not yet been fixed, it is recommended to use Heng Li's seqtk for this function.");
    exit(1);

    my (%argv,$outfile);
    getopts('Xho:',\%argv) && @ARGV >= 2 || &SUBSEQ_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'h') { &SUBSEQ_USAGE }
	elsif ($o eq 'X') { $DEBUG   = 1 }
	elsif ($o eq 'o') { $outfile = &char($argv{$o}) }
    }
    open(STDOUT,">$outfile") || &throw("Cannot open $outfile for writing.")
        if defined $outfile;

    my $coord = &read_coordinates(shift @ARGV);
    while (my $fa = shift @ARGV) {
	$fa eq '-' || &file($fa) || &throw("Cannot open $fa for reading.");
        open(FA,$fa) || &throw("Cannot open $fa for reading.");
        while (my $fasta = &read_fasta(\*FA)) {
	    if (!$coord->{$fasta->[SEQID]}) {
		$coord->{$fasta->[SEQID]}->{'start'} = 1;
		$coord->{$fasta->[SEQID]}->{'stop'}  = length($fasta->[SEQ]);
	    } else {
		$coord->{$fasta->[SEQID]}->{'stop'} ||= length($fasta->[SEQ]);
	    }
	    
	    $fasta->[SEQ] = substr($fasta->[SEQ],
				   $coord->{$fasta->[SEQID]}->{'start'} - 1,
				   $coord->{$fasta->[SEQID]}->{'stop'}  
				 - $coord->{$fasta->[SEQID]}->{'start'} + 1);
	    
	    print join("\n",'>'.$fasta->[SEQID].$fasta->[DESC],
		       $fasta->[SEQ] =~ /(.{1,60})/g,'');
	}
	close(FA);
    }
    
    close(STDOUT);
    return 1;
}	

sub MASK2BED_USAGE {
    die "Usage: $PROGRAM [-H] <in.fasta> [<out.bed>]\n";
}

sub mask2bed {
    my (%argv,$hardmasked);
    getopts('XhH',\%argv) && @ARGV || &MASK2BED_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'h') { &MASK2BED_USAGE }
	elsif ($o eq 'X') { $DEBUG  = 1 }
	elsif ($o eq 'H') { $hardmasked = 1 }
    }
    my $infile  = shift @ARGV;
    my $outfile = @ARGV ? shift @ARGV 
	: do { ($_ = $infile) =~ s/\.f(n|ast)?a/.bed/; $_ };

    $infile eq '-' || &file($infile) || &throw("Cannot open $infile for reading.");
    open(FA,$infile) || &throw("Cannot open $infile for reading.");
    open(BED,">$outfile") || &throw("Cannot open $outfile for writing.");
    while (my $fasta = &read_fasta(\*FA)) {
	$fasta->[SEQ] =~ s/[[:lower:]]/N/g if ! $hardmasked;
	while ($fasta->[SEQ] =~ /(N+)/g) {
	    print BED join("\t",$fasta->[SEQID],$-[0],$-[0]+length($1)),"\n";
	}
    }
    close(BED);
    close(FA);
    
    return 1;
}

sub CASE_USAGE {
    die qq/
Usage:   $PROGRAM case [options] <-|in.fasta> [...]

Options: -o FILE   Write output to FILE [stdout]
         -a AA|NT  Force alphabet (see Notes) [auto-detect]
         -l        Lower-case all sequence
         -u        Upper-case all sequence
         -x        Filter masked (lower-cased) sequence
         -c        Use only cannonical NT\/AA code characters
         -h        This help message

Notes:

  1. Unless '-a' is explicitly specified, the sequence type is guessed by 
     examining the composition of the first 100 characters. 'Amino-acid' 
     is selected if non-ATUCGN characters exceed 35\% of the sequence.

  2. If both '-u' or '-l' are specified along with '-x', the sequence is 
     filtered prior to case-change.

  3. '-c' replaces non-cannonical charcters with either an 'X' or 'N' 
     depending on sequence type.


/;
}

sub case {
    my (%argv,$outfile);
    my ($lc,$uc,$filter,$canonical,$alphabet) = (0,0,0,0,0);
    getopts('Xho:a:clux',\%argv) && @ARGV || &CASE_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'h') { &CASE_USAGE }
	elsif ($o eq 'a') { ($alphabet)=$argv{$o}=~/^(A|N)/i || &CASE_USAGE }
	elsif ($o eq 'o') { $outfile = &char($argv{$o}) }
	elsif ($o eq 'X') { $DEBUG  = 1 }
	elsif ($o eq 'x') { $filter = 1 }
	elsif ($o eq 'l') { $lc = 1 }
	elsif ($o eq 'u') { $uc = 1 }
	elsif ($o eq 'c') { $canonical = 1 }
    }
    open(STDOUT,">$outfile") || &throw("Cannot open $outfile for writing.")
        if defined $outfile;

    while (my $fa = shift @ARGV) {
	$fa eq '-' || &file($fa) || &throw("Cannot open $fa for reading.");
	open(FA,$fa) || &throw("Cannot open $fa for reading");
	my $fchar = $alphabet =~ /^A/i ? 'X' : 'N';
	while (my $fasta = &read_fasta(\*FA)) {
	    $fasta->[2] =~ s/[^a-zA-Z*]//g;
	    
	    if (! $alphabet) {
		my $C = substr($fasta->[SEQ],0,100);
		my $c = $C =~ s/([^atucgnATUCGN])/$1/g;
		$alphabet = ($c/length($C)) >= 0.35 ?'amino acid':'nucleotide';
		printf(STDERR "[%s %s] %s auto-detected as %s\n",
		    $PROGRAM,__FUNC__,$fa,$alphabet);
		$fchar = $alphabet =~ /^A/i ? 'X' : 'N';
	    }
	    
	    if ($canonical) {
		my $can = $alphabet =~ /^A/i 
		    ? 'acdefghiklmnpqrstvwyACDEFGHIKLMNPQRSTVWY' 
		    : 'atucgATUCG';
		$fasta->[SEQ] =~ s/[^$can]/$fchar/g;
	    }
	    $fasta->[SEQ] =~ s/[a-z]/$fchar/g if $filter;
	    $fasta->[SEQ] = $lc ? lc($fasta->[SEQ]) : uc($fasta->[SEQ]) 
		if $lc || $uc;
	    
	    print join("\n",'>'.$fasta->[SEQID].$fasta->[DESC],
		       $fasta->[SEQ] =~ /(.{1,60})/g,'');
	}
	close(FA);
    }
    close(STDOUT);
    return 1;
}

sub SPLIT_USAGE {
    die qq/
Usage:   $PROGRAM split [options] <in.fasta> <out.prefix>

Options: -f INT   Max number of files to write to [2]
         -e INT   Max number of entries per file [null]
         -h       This help message


/;
}

sub split {
    my (%argv,$outfile);
    my ($nFiles,$nEntry) = (2,0);
    getopts('Xh:f:e:',\%argv) && @ARGV == 2 || &SPLIT_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'h') { &SPLIT_USAGE }
	elsif ($o eq 'X') { $DEBUG  = 1 }
	elsif ($o eq 'f') { $nFiles = &uint($argv{$o})||2 }
	elsif ($o eq 'e') { $nEntry = &uint($argv{$o}) }
    }
    
    my $fa  = &char($ARGV[0]);
    my $po  = &char(pop @ARGV);
    &file($fa,'rs') && -r $fa && -s $fa ||
	&throw("Cannot open $fa for reading.");

    my $fai = &fai($fa);
    if (! defined($fai)) {
	printf(STDERR "[%s %s] Indexing...\n",$PROGRAM,__FUNC__); 
	&faidx || &throw("$fa could not be (faidx) indexed.");
	$fai = $fa.'.fai';
    }
    my $nSeqs = scalar(keys %{&read_faidx($fa)});
    # can add -b ("balanced" by AAs/NTs) as a swtich...
    ($nFiles,$nEntry) = ($nEntry != 0) 
        ? (POSIX::ceil($nSeqs/$nEntry),$nEntry)
	: ($nFiles,POSIX::ceil($nSeqs/$nFiles));

    # our infile is a fasta, now open output files on demand
    my ($f,$n,$e,@fh) = (0,0,9**9**9,());
    for (my $i = 1; $i <= $nFiles; $i++) {
	push @fh,sprintf('%s.%04s.fa',$po,$i);
    }
    open(FA,$fa) || &throw("Cannot open $fa for reading.");
    printf(STDERR "[%s %s] Writing to %u files (%u\/file)...\n",
	   $PROGRAM,__FUNC__,$nFiles,$nEntry);
    while (my $fasta = &read_fasta(\*FA)) {
	$n == $nSeqs && last;
	if ($e >= $nEntry) {
	    close(FH);
	    open(FH,'>'.$fh[$f++]) || 
		&throw("Cannot open $fh[$f] for writing.");
	    $e = 0;
	}
	print FH join("\n",'>'.$fasta->[SEQID].$fasta->[DESC],
		      $fasta->[SEQ] =~ /(.{1,60})/g,'');
	$e++;
	$n++;
    }
    close(FA);
    close(FH);
    
    return 1;
}

sub GFF3_USAGE {
    die qq/
Usage:   $PROGRAM gff3 [options] <in.fasta> [...]

Options: -o FILE    Write output to FILE [stdout]
         -f         Include fasta sequence at end of file
         -i INT     Initialize the ID tag count with INT [0]   
         -s STRING  Set the 'source' field value ['GENOME']
         -t STRING  Set the 'type' field value ['chromosome']
         -h         This help message


/;
}

sub gff3 {
    my (%argv,$outfile);
    my ($pfasta,$iter,$source,$type) = (0,0,'GENOME','chromosome');
    getopts('Xfho:i:t:s:',\%argv) && @ARGV || &GFF3_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'h') { &GFF3_USAGE }
	elsif ($o eq 'X') { $DEBUG  = 1 }
	elsif ($o eq 'f') { $pfasta = 1 }
	elsif ($o eq 's') { $source = &char($argv{$o})||'.' }
	elsif ($o eq 't') { $type   = &char($argv{$o})||'.' }
	elsif ($o eq 'o') { $outfile= &char($argv{$o}) }
	elsif ($o eq 'i') { $iter = &uint($argv{$o}) }
	
    }
    open(STDOUT,">$outfile") || &throw("Cannot open $outfile for writing.")
        if defined $outfile;

    my %tagcode;
    my %escape =
	('"' => '%22', '#' => '%23', '%' => '%25', "'" => '%27',
	 '(' => '%28', ')' => '%29', ',' => '%44', '/' => '%2F',
	 ';' => '%3B', '<' => '%3C', '=' => '%3D', '>' => '%3E',
	 '[' => '%5B', "\\"=> '%5C', ']' => '%5D', '`' => '%60',
	 '{' => '%7B', '}' => '%7D', '~' => '%7E', '&' => '%26',
	 ' ' => '%20', "\t"=> '%09', "\n"=> '%0A', "\r"=> '%0D');

    $tagcode{'Name'} = 
	sub {
	    my $GFF = shift || return;
	    my $name = $GFF->[SEQID];
	    $name =~ s/([,=;"\t\n\r])/$escape{$1}/g;
	    return $name;
        };
    $tagcode{'ID'} = 
	sub {
	    my $GFF = shift || return;
	    return sprintf('%09d',$GFF->[ITER]);
        };

    my @INIT = ('.',$source,$type,'.','.','.','+','.','.');
    print "\#\#gff-version 3\n";
    for (my $i = 0; $i < @ARGV; $i++) {
	&file($ARGV[$i],'r') &&	open(FA,$ARGV[$i]) || 
	    &throw("Cannot open $ARGV[$i] for reading"); 
	while (my $fasta = &read_fasta(\*FA)) {
	    $fasta->[SEQ] =~ s/[^a-zA-Z*]//g;
	    
	    my @GFF; @GFF[SEQID..ATTR] = @INIT;
	    $GFF[SEQID] = $fasta->[SEQID];
	    $GFF[SEQID] =~ s/([^a-zA-Z0-9.:*$@!+_?|\-\^])/$escape{$1}/g;
	    @GFF[RSTART,REND] = (1,length($fasta->[SEQ]));
	    $GFF[RSTART] < $GFF[REND] || 
		&throw($GFF[SEQID]," lacks sequence string!"); 
	    $GFF[ITER] = $iter++;
	    my @tagPairs;
	    for my $tag (sort keys %tagcode) {
		my $value = &{$tagcode{$tag}}(\@GFF) || next;
		push @tagPairs, $tag.'='.$value;
	    }
	    $GFF[ATTR] = join(';',@tagPairs) || '.';
	    print join("\t",@GFF[SEQID..ATTR]),"\n";
	}
	close(FA);
    }

    if (! $pfasta) {close(STDOUT); return 1;}

    print "\#\#FASTA\n";
    while (my $fa = shift @ARGV) {
	open(FA,$fa) || &throw("Cannot open $fa for reading");
	while (my $fasta = &read_fasta(\*FA)) {
	    print join("\n",'>'.$fasta->[SEQID].$fasta->[DESC],
		       $fasta->[SEQ] =~ /(.{1,60})/g,'');
	}
	close(FA);
    }
    close(STDOUT);
    return 1;
}

sub GET_USAGE {
    die qq/
Usage:   $PROGRAM get [options] <in.fasta> [...]

Options: -o FILE  Write output to FILE [stdout]
         -e INT   Get sequences with length equal to INT
         -g INT   Get sequences with length greater than INT
         -l INT   Get sequences with length less than INT
         -i FILE  Include sequences listed in FILE
         -x FILE  Exclude sequences listed in FILE
         -r STR   Get sequences with STR in defline
         -R STR   Exclude sequences with STR in defline
         -w       Force -r and -R arguments to match whole words 
         -S       Get the shortest sequences
         -L       Get the longest sequences
         -U       Consider the union set [intersection]
         -h       This help message

Notes: 

  1. '-S' and '-L' are exempt from being conditioned by the union or 
     intersecting sets.


/;
}

sub get {
    my ($regin,$regex,$incl,$excl) = ('','',{},{});
    my (%argv,$outfile,$shortest,$longest,$union,$gt,$eq,$lt,$word);
    getopts('SLXho:e:g:l:i:x:r:R:Uw',\%argv) && @ARGV || &GET_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'h') { &GET_USAGE }
	elsif ($o eq 'X') { $DEBUG  = 1 }
	elsif ($o eq 'o') { $outfile = &char($argv{$o}) }
	elsif ($o eq 'i') { $incl = &read_list($argv{$o}) }
	elsif ($o eq 'x') { $excl = &read_list($argv{$o}) }
	elsif ($o eq 'e') { $eq = &uint($argv{$o}) }
	elsif ($o eq 'g') { $gt = &uint($argv{$o}) }
	elsif ($o eq 'l') { $lt = &uint($argv{$o}) }
	elsif ($o eq 'r') { $regin = &regx($argv{$o}) }
	elsif ($o eq 'R') { $regex = &regx($argv{$o}) }
	elsif ($o eq 'w') { $word  = 1 }
	elsif ($o eq 'L') { $longest  = 1 }
	elsif ($o eq 'S') { $shortest = 1 }
	elsif ($o eq 'U') { $union = 1 }
    }
    open(STDOUT,">$outfile") || &throw("Cannot open $outfile for writing.")
        if defined $outfile;
    
    if ($regin) {
	$regin = $word ? qr/\b$regin\b/ : qr/$regin/;
    }
    if ($regex) {
	$regex = $word ? qr/\b$regex\b/ : qr/$regex/;
    }

    my (%min,%max,%T);
    my ($min,$max) = (9**9**9,-9**9**9);
    my %test = (
	'g' => sub {my $fasta = shift; return length($fasta->[SEQ]) > $gt },
	'e' => sub {my $fasta = shift; return length($fasta->[SEQ]) == $eq },
	'l' => sub {my $fasta = shift; return length($fasta->[SEQ]) < $lt },
	'r' => sub {my $fasta = shift; return(($fasta->[SEQID].$fasta->[DESC]) =~ $regin || 0) },
	'R' => sub {my $fasta = shift; return(($fasta->[SEQID].$fasta->[DESC]) !~ $regex || 0) },
	'i' => sub {my $fasta = shift; return($incl->{$fasta->[SEQID]}||0) },
	'x' => sub {my $fasta = shift; return(!$excl->{$fasta->[SEQID]}) },
	'S' => sub {my $fasta = shift; length($fasta->[SEQ]) <= $min || return 0; 
		    if ($min == length($fasta->[SEQ])) {
			$min{$fasta->[SEQID].$fasta->[DESC]} = $fasta->[SEQ]
	            } else {
			$min = length($fasta->[SEQ]); 
			%min = ($fasta->[SEQID].$fasta->[DESC] => $fasta->[SEQ])} return 0 },
	'L' => sub {my $fasta = shift; length($fasta->[SEQ]) >= $max || return 0;
                    if ($max == length($fasta->[SEQ])) {
			$max{$fasta->[SEQID].$fasta->[DESC]} = $fasta->[SEQ]
                    } else {
			$max = length($fasta->[SEQ]); 
			%max = ($fasta->[SEQID].$fasta->[DESC] => $fasta->[SEQ])} return 0 },
	);

    for my $d (keys %argv) {
	exists($test{$d}) && exists($argv{$d}) || next;
	$T{$d} = $test{$d};
    }
    %T || &throw("Must specify a selection method");
    while (my $fa = shift @ARGV) {
	$fa eq '-' || &file($fa) || &throw("Cannot open $fa for reading.");
	open(FA,$fa) || &throw("Cannot open $fa for reading");
	while (my $fasta = &read_fasta(\*FA)) {
	    my $success = 0;
	    for my $t (keys %T) {
		$success += &{$T{$t}}($fasta);
	    }
	    if ($union && $success) { # examine union set
		print join("\n",'>'.$fasta->[SEQID].$fasta->[DESC],
			   $fasta->[SEQ] =~ /(.{1,60})/g,'');
		delete($min{$fasta->[SEQID].$fasta->[DESC]})
		    if $shortest && exists $min{$fasta->[SEQID].$fasta->[DESC]};
		delete($max{$fasta->[SEQID].$fasta->[DESC]})
		    if $longest && exists $max{$fasta->[SEQID].$fasta->[DESC]};
	    } elsif (!$union && $success) { # examine intersecting set
		my $expect = scalar(keys(%T));
		# make max and min exempt from expected
		$expect-- if $shortest;
		$expect-- if $longest;
		print join("\n",'>'.$fasta->[SEQID].$fasta->[DESC],
			   $fasta->[SEQ] =~ /(.{1,60})/g,'')
		    if $success == $expect;
		delete($min{$fasta->[SEQID].$fasta->[DESC]})
		    if $shortest && exists $min{$fasta->[SEQID].$fasta->[DESC]};
		delete($max{$fasta->[SEQID].$fasta->[DESC]})
		    if $longest && exists $max{$fasta->[SEQID].$fasta->[DESC]};
	    }
	}
	close(FA);
    }
    my %nr = map{$_ => 1} keys %min, keys %max;
    for my $def (sort keys %nr) {
	my $seq = $min{$def} || $max{$def} || next;
	print join("\n",'>'.$def,($seq =~ /(.{1,60})/g),'');
    }
    close(STDOUT);
    return 1;
}

sub SORT_USAGE {
    die qq/
Usage:   $PROGRAM sort [options] <in.fasta> <out.fasta>

Options: -n      Sort on sequence name [length]
         -d      Sort descending
         -h      This help message


/;
}

sub sort {
    my (%argv,$name,$desc);
    getopts('Xhnd',\%argv) && @ARGV == 2 || &SORT_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'h') { &SORT_USAGE }
	elsif ($o eq 'X') { $DEBUG = 1 }
	elsif ($o eq 'n') { $name  = 1 }
	elsif ($o eq 'd') { $desc  = 1 }
    }
    
    my $fa = &char($ARGV[0]);
    my $po = &char(pop(@ARGV));
    open(PO,'>'.$po) || &throw("Cannot open $po for writing.");
    &file($fa,'f') && -r $fa && -s $fa ||
	&throw("Cannot open $fa for reading."); 

    my $fai = &fai($fa);
    if (! defined($fai)) {
	printf(STDERR "[%s %s] Indexing...\n",$PROGRAM,__FUNC__); 
	&faidx || &throw("$fa could not be (faidx) indexed.");
	$fai = $fa.'.fai';
    }	
    open(FA,$fa) || &throw("Cannot open $fa for reading.");
    
    # do a fast C-based lexical sort (via Schwartzian Transform)
    printf(STDERR "[%s %s] Sorting...\n",$PROGRAM,__FUNC__);
    my $idx = &read_faidx($fa);
    my @sorted = $name 
        ? map {$_->[1]}
               CORE::sort {$a->[0] cmp $b->[0]}
                   map {(my $t = $_) =~ s/(\d+)/sprintf"%015.6d",$1/ge; [$t,$_]} keys %$idx
        : map {substr($_,15)}
	       CORE::sort 
		   map {sprintf('%015s%s',$idx->{$_}->{'length'},$_)} keys %$idx;
    @sorted = reverse @sorted if $desc;

    for my $id (@sorted) {
	my $seq = &pull_fasta($idx->{$id}->{'pos'}) || 
	    &throw("Sequence get for $id failed!");
	length($seq) == $idx->{$id}->{'length'} || 
	    &throw("Different length in sequence '$seq', ",
		   length($seq),' != ',$idx->{$id}->{'length'});
	print PO join("\n",'>'.$id,($seq =~ /(.{1,60})/g),'');
    }
    
    close(FA);
    close(PO);
    return 1;
}

sub RVCOMP_USAGE {
die qq/
Usage:   $PROGRAM rvcomp [options] <in.fasta> [...]

Options: -o FILE  Write output to FILE [stdout]
         -l FILE  Reverse compl. only the sequences listed in FILE
         -q       Force qual format
         -h       This help message


/;
}

sub rvcomp {
    my (%argv,$outfile,$qualfmt,$list);
    getopts('Xhql:o:',\%argv) && @ARGV || &RVCOMP_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'h') { &RVCOMP_USAGE }
	elsif ($o eq 'X') { $DEBUG = 1 }
	elsif ($o eq 'q') { $qualfmt = 1 }
	elsif ($o eq 'o') { $outfile = &char($argv{$o}) }
	elsif ($o eq 'l') { $list = &read_list($argv{$o}) }
    }
    open(STDOUT,">$outfile") || &throw("Cannot open $outfile for writing.")
        if defined $outfile;

    while (my $fa = shift @ARGV) {
	$fa eq '-' || &file($fa) || &throw("Cannot open $fa for reading.");
        open(FA,$fa) || &throw("Cannot open $fa for reading");
        while (my $fasta = &read_fasta(\*FA)) {
	    if ($qualfmt && $fasta->[SEQ] =~ /^[0-9]+(?:\s+[0-9]+)*$/) {
		my @seq;
		if (!defined($list) ||
		    (defined($list) && $list->{$fasta->[SEQID]})) {
		    @seq = reverse(split(' ',$fasta->[SEQ]));
		} else {
		    @seq = split(' ',$fasta->[SEQ]);
		}
		print '>'.$fasta->[SEQID].$fasta->[DESC],"\n";
		while (@seq) {
		    my @subseq = splice(@seq,0,60);
		    print join(' ',@subseq),"\n";
		}
	    } else {
		if (!defined($list) || 
		    (defined($list) && $list->{$fasta->[SEQID]})) {
		    $fasta->[SEQ] = &rvcmp($fasta->[SEQ]);
		}
		print join("\n",'>'.$fasta->[SEQID].$fasta->[DESC],
			   $fasta->[SEQ] =~ /(.{1,60})/g,'');	    
	    }
	}
	close(FA);
    }
    close(STDOUT);
    return 1;
}

sub SAMPLE_USAGE {
die qq/
Usage:   $PROGRAM sample [options] <in.fasta>

Options: -o FILE   Write output to FILE [stdout]
         -p FLOAT  Sample FLOAT percent sequences [10]
         -n INT    Sample INT sequences from file
         -h        This help message


/;
}

sub sample {
    my $p = 10;
    my (%argv,$outfile,$n);
    getopts('Xho:p:n:',\%argv) && @ARGV == 1 || &SORT_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'h') { &SORT_USAGE }
	elsif ($o eq 'X') { $DEBUG = 1 }
	elsif ($o eq 'o') { $outfile = &char($argv{$o}) }
	elsif ($o eq 'p') { $p = &ufloat($argv{$o}) }
	elsif ($o eq 'n') { $n = &uint($argv{$o}) }
    }
    defined($p) || defined($n) || &throw("Must specify '-n' or '-p'");
    open(STDOUT,">$outfile") || &throw("Cannot open $outfile for writing.")
        if defined $outfile;

    my $fa = $ARGV[0];
    &file($fa,'rs') && -r $fa && -s $fa || 
	&throw("Cannot open $fa for reading!");

    my $fai = &fai($fa);
    if (! defined($fai)) {
	printf(STDERR "[%s %s] Indexing...\n",$PROGRAM,__FUNC__); 
	&faidx || &throw("$fa could not be (faidx) indexed.");
	$fai = $fa.'.fai';
    }    
    my $idx = &read_faidx($fai);

    if ($p) { $p /= 100 }
    ($p,$n) = $n 
	? ($n/scalar(keys(%$idx)),$n)
	: ($p,int($p * scalar(keys(%$idx))));
    $n <= scalar(keys %$idx) || 
	&throw("Sample size ($n) exceeds population size (",
	       scalar(keys %$idx),')');

    open(FA,$fa) || &throw("Cannot open $fa for reading!");
    my $i = 0;
    for my $id (keys %$idx) {
	$i < $n && rand() <= $p || next;
	my $seq = &pull_fasta($idx->{$id}->{'pos'}) || 
	    &throw("Sequence get for $id failed!");
	length($seq) == $idx->{$id}->{'length'} || 
	    &throw("Different length in sequence '$seq', ",
		   length($seq),' != ',$idx->{$id}->{'length'});
	
	print join("\n",'>'.$id,($seq =~ /(.{1,60})/g),'');
	$i++;
    }
    
    close(FA);
    close(STDOUT);
    return 1;
}

sub TRANSLATE_USAGE {
die qq/
Usage:   $PROGRAM translate [options] <-|in.fasta>

Options: -o FILE   Write output to FILE [stdout]
         -l INT    Minimum AA length to output [50]
         -6        Translate all 6 frames
         -O        Split into ORFs
         -s        Require translations to start with a Met
         -h        This help message


/;
}

sub translate {
    my $min_l = 50;
    my (%argv,$outfile,$six,$met,$orfs);
    getopts('Xho:6Oml:',\%argv) && @ARGV == 1 || &TRANSLATE_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'h') { &TRANSLATE_USAGE }
	elsif ($o eq 'X') { $DEBUG = 1 }
	elsif ($o eq '6') { $six  = 1 }
	elsif ($o eq 'O') { $orfs = 1 }
	elsif ($o eq 'm') { $met  = 1 }
	elsif ($o eq 'o') { $outfile = &char($argv{$o}) }
	elsif ($o eq 'l') { $min_l = &uint($argv{$o})||50 }
    }
    open(STDOUT,">$outfile") || &throw("Cannot open $outfile for writing.")
        if defined $outfile;

    my $frame = $six ? 3 : 1;
    while (my $fa = shift @ARGV) {
	$fa eq '-' || &file($fa) || &throw("Cannot open $fa for reading.");
        open(FA,$fa) || &throw("Cannot open $fa for reading");
        while (my $fasta = &read_fasta(\*FA)) {
	    my $nt = uc $fasta->[2];
            $nt =~ s/[^A-Z*]//g;
	    my ($sixframes,$c,$d) = ($six,1,'+');
	    for(my $f = 0; $f < $frame; ++$f) {
		my $l = length($fasta->[2]) - $f;
		$l = $l - ($l % 3);
		my @aa  = ('');
		for (my $i = $f; $i < $l; $i += 3) {
		    $aa[0] .= ($CODON{substr($nt,$i,3)}||'X');
		}
		if ($orfs) { @aa = split(/\*/,shift @aa) }
		if ($met) {
		    my ($i,$j);
		    for ($j = 0; $j < @aa; ++$j) {
			for ($i = 0; $i < length($aa[$j]); ++$i) {
			    if (substr($aa[$j],$i,1) eq 'M') { last }
			}
			$aa[$j] = substr($aa[$j],$i);
		    }
		}
		while (my $prot = shift @aa) {
		    length($prot) < $min_l && next;
		    print join("\n",'>'.$fasta->[SEQID].'.'.$c++.
			       ' frame='.$d.($f+1),$prot =~ /(.{1,60})/g,'');
		}
		if ($sixframes && $f == 2) {
		    $nt = &rvcmp($nt);  
		    $sixframes = 0; 
		    $f = -1; 
		    $d = '-';
		}
	    }
	}
	close(FA);
    }
    close(STDOUT);
    return 1;
}

sub CUTN_USAGE {
    die qq/
Usage:   $PROGRAM cutN [options] <in.fasta>

Options: -o FILE  Write output to FILE [stdout]
         -n INT   Min gap length to report [1]
         -g       Write a BED file of gap positions


/;
}

sub cutN {
    my %argv;
    my $mingap = 1;
    my $outbed = 0;
    my $outfil = undef;
    getopts('gn:o:',\%argv) && @ARGV == 1 || CUTN_USAGE;
    for my $o (keys(%argv)) {
	if    ($o eq 'n') { $mingap = &uint($argv{'n'}) }
	elsif ($o eq 'o') { $outfil = $argv{$o} }
	elsif ($o eq 'g') { $outbed = 1 }
    }
    open(FA,$ARGV[0]) || &throw("Cannot open $ARGV[0] for reading: $!.");
    open(STDOUT, ">$outfil") || &throw("Cannot open $outfil for writing: $!.")
	if defined $outfil;

    my $i = 0;
    my $NUCLEOTIDE = qr/[nN]+/;
    while (my $fa  = &read_fasta(\*FA)) {
	my $gapstart;
	my $ntseen = 0;
	my $gapend = 0;
	my $seqstr = uc $fa->[2];
	for($i = 0; $i < length($seqstr); ++$i) {
	    if (substr($seqstr,$i,1) eq 'N') {
		if (!$outbed && !defined($gapstart) && $ntseen) {
		    print(join("\n",sprintf(">%s:%u-%u",$fa->[0],$gapend+1,$i),
			       substr($seqstr,$gapend,$i-$gapend) =~ /(.{1,60})/g,''));
		}
		$gapstart //= $i;
		$gapend = $i+1;
	    } else {
		if ( $outbed && $gapstart) {
		    print(join("\t",$fa->[0],$gapstart,$i),"\n") 
			if $i-$gapstart >= $mingap;
		}
		$gapstart = undef;
		$ntseen = 1;
	    }
	}
	if (!$outbed && !defined($gapstart)) {
	    print(join("\n",sprintf(">%s:%u-%u",$fa->[0],$gapend+1,$i),
		       substr($seqstr,$gapend,$i-$gapend) =~ /(.{1,60})/g,''));
	}
    }
    close(STDOUT);
    close(FA);
}


sub HELP_MESSAGE {
die qq/
Program: $PROGRAM ($PURPOSE)
Version: $PACKAGE $VERSION
Contact: $CONTACT

Usage:   $PROGRAM <command> [options]

Command: clean      Remove non-[a-zA-Z*] chars
         sort       Sort fasta by name or size
         case       Change case or filter masked sequence
         faidx      Index fasta file
         cutN       Perform operations on gap sequence
         split      Split a fasta file into multiple files
         gff3       Convert to gff3 format
         stats      Get basic statistics about sequences
         rvcomp     Reverse-complement sequences
         subseq     Extract sub-sequence
         sample     Take a sample of fasta entries
         get        Filter sequences by criteria
         redef      Re-name sequences
         mask2bed   Extract a BED file of masked sequence
         translate  Translate nt sequence to aa

/;
}

sub main {
    %FUNCTION = (
	'clean'   => \&clean,   'faidx'  => \&faidx,
	'gff3'    => \&gff3,    'split'  => \&split,
	'case'    => \&case,    'sort'   => \&sort,
	'stats'   => \&stats,   'rvcomp' => \&rvcomp,
	'get'     => \&get,     'sample' => \&sample,
	'subseq'  => \&subseq,  'redef'  => \&redef,
	'mask2bed'=> \&mask2bed,'translate' => \&translate,
	'cutN'    => \&cutN
	);
    $CMD = shift @ARGV;
    $CMD && exists($FUNCTION{$CMD}) || &HELP_MESSAGE;
    &{$FUNCTION{$CMD}} || &throw("$CMD unexpectedly aborted.");
    return 0;
}

exit(main());


__END__
