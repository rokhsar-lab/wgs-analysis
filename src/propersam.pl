
$PACKAGE = '__PACKAGE_NAME__';
$VERSION = '__PACKAGE_VERSION__';
$CONTACT = '__PACKAGE_CONTACT__';
$PURPOSE = 'Filter a SAM file for proper pairs';

use strict;
use warnings;

use Getopt::Std;
use File::Basename;

use vars qw($PROGRAM $PACKAGE $PURPOSE $VERSION $CONTACT $SAMHEADER $NULLENTRY $SECONDARY);
use constant {
    QNAME => 0, FLAG  => 1, RNAME => 2, POS   => 3,
    MAPQ  => 4, CIGAR => 5, RNEXT => 6, PNEXT => 7,
    TLEN  => 8, QSEQ  => 9, QUAL  => 10,TAG   => 11,
    END1  => 0, END2  => 1,
        
};


BEGIN {
    $PROGRAM = basename($0);
    $SAMHEADER = qr/^\@/;
    $NULLENTRY = qr/^\s*$/;
    $SECONDARY = 0x100 | 0x800;
}

sub parseTags {
    my %tags = map {m/^([a-zA-Z][0-9a-zA-Z]):[AifZHB]:([^\t]*)$/} @_;
    return \%tags;
}

sub HELP_MESSAGE {
die qq/
Program: $PROGRAM ($PURPOSE)
Version: $PACKAGE $VERSION
Contact: $CONTACT

Usage: $PROGRAM [-Q INT] <-|in.sam>


/;
}

main: {
    our $opt_Q = 0;
    getopts('Q:') && @ARGV == 1 || &HELP_MESSAGE;

    my $end1;
    my $end2;
    while (my $entry = <>) {
	if ($entry =~ $NULLENTRY) { next }
	if ($entry =~ $SAMHEADER) { print($entry); next }
	chomp($entry);

	my @entry  = split(/\t/,$entry);

	$entry[FLAG] += 0; # coerce string -> numeric

	# annoying special handling of secondary alignments:
	if ($entry[FLAG] & $SECONDARY) {
	    $entry[$#entry+1] = "ZM:i:$entry[MAPQ]";
	    $entry[MAPQ] = 0;
	    print( join("\t",@entry),"\n");
	    next;
	}

	if (!$end1) {
	    $end1 = \@entry; next;
	} else {
	    $end2 = \@entry;
	    
	    if ($end1->[QNAME] ne $end2->[QNAME]) {
		die "Unmatched pair end detected (SAM may be unsorted): $end1->[QNAME]\n";
	    }

	    if ((~$end1->[FLAG] & 0x2) || (($end1->[MAPQ] < $opt_Q) && ($end2->[MAPQ] < $opt_Q))) {
		$end1->[$#{$end1}+1] = "ZM:i:$end1->[MAPQ]";
		$end2->[$#{$end2}+1] = "ZM:i:$end2->[MAPQ]";
		$end1->[MAPQ] = $end2->[MAPQ] = 0;
	    }
	    print( join("\t",@$end1),"\n");
	    print( join("\t",@$end2),"\n");
	    
	    $end1 = undef;
	    $end2 = undef;
	}
    }
    
    exit(0);
}
