
import os
import sys
import time
import pysam
import getopt
from scipy.stats import binom

__authors__ = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Filter genotypes by depth and ref-allele balance'

num = len
het_threshold = {}
hom_threshold = {}

def notNone(value):
    return value is not None


def version(exitcode=0, stream=sys.stderr):
    stream.write("%s %s\n" % (__program__, __version__))
    sys.exit(exitcode)


def usage(message=None, exitcode=1, stream=sys.stderr):
    message = '' if message is None else "ERROR: %s\n\n\n" % message
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("\n")
    stream.write("Usage: %s [options] <in.vcf> <out.vcf>\n" % (
        os.path.basename(sys.argv[0])))
    stream.write("\n")
    stream.write("Options:\n")
    #            0        10        20        30        40        50        60        70        80
    #            |----+----|----+----|----+----|----+----|----+----|----+----|----+----|----+----|
    stream.write("  -a,--het-alpha <ufloat>\n")
    stream.write("      Binomial test significance alpha for deviation from expected allele\n")
    stream.write("      balance value for heterozygous (het) loci. Pass a value of 0 to disable\n")
    stream.write("      (range: [0.0, 1.0]) [0.0001]\n")
    stream.write("\n")
    stream.write("  -A,--hom-alpha <ufloat>\n")
    stream.write("      Binomial test significance alpha for deviation from expected allele\n")
    stream.write("      balance value for homozygous (hom) loci. Pass a value of 0 to disable\n")
    stream.write("      (range: [0.0, 1.0]) [0.0001]\n")
    stream.write("\n")
    stream.write("  -b,--min-het-ab <ufloat>\n")
    stream.write("      Lower-tail max deviation from expected allele balance for het loci\n")
    stream.write("      allele balance < {-e} - {-b} are filtered (range: [0.0, 1.0]) [0.2]\n")
    stream.write("\n")
    stream.write("  -B,--max-het-ab <ufloat>\n")
    stream.write("      Upper-tail max deviation from expected allele balance for het loci\n")
    stream.write("      allele balance > {-e} + {-B} are filtered (range: [0.0, 1.0]) [0.2]\n")
    stream.write("\n")
    stream.write("  -d,--min-depth <uint>\n")
    stream.write("      Minimum depth of a locus to consider it confident/callable [0]\n")
    stream.write("\n")
    stream.write("  -D,--max-depth <uint>\n")
    stream.write("      Maximum depth of a locus to consider it confident/callable [inf]\n")
    stream.write("\n")    
    stream.write("  -e,--exp-het-ab <ufloat>\n")
    stream.write("      Expected allele balance for het loci (range: [0.0, 1.0]) [0.5]\n")
    stream.write("\n")
    stream.write("  -E,--exp-hom-ab <ufloat>\n")
    stream.write("      Expected allele balance for hom loci (range: [0.0, 1.0]) [0.01]\n")
    stream.write("\n")
    stream.write("  -H,--max-hom-ab <ufloat>\n")
    stream.write("      Max allele balance for hom loci. Hom genotypes with supporting allele\n")
    stream.write("      balance < 1 - {-H} are filtered (range: [0.0, 1.0]) [0.1]\n")
    stream.write("\n")
    stream.write("  -s,--sample <str>\n")
    stream.write("      Name of sample to apply filters to and write to output VCF (all other\n")
    stream.write("      samples will be omitted).\n")
    stream.write("\n")
    stream.write("  -v,--version\n")
    stream.write("      Write the version info to screen and exit\n")
    stream.write("\n")
    stream.write("  -h,--help\n")
    stream.write("      This usage message\n")
    #            0        10        20        30        40        50        60        70        80
    #            |----+----|----+----|----+----|----+----|----+----|----+----|----+----|----+----|
    stream.write("\n") 
    stream.write("\n%s" % message)
    sys.exit(exitcode)


    
def main(argv):
    short_opts = 'a:A:b:B:d:D:e:E:h:H:s:v'
    long_opts = (
        'het-alpha=',
        'hom-alpha=',
        'min-het-ab=',
        'max-het-ab=',
        'max-hom-ab=',
        'min-depth=',
        'max-depth=',
        'exp-het-ab=',
        'exp-hom-ab=',
        'sample=',
        'version',
        'help'
    )
    try:
        options, arguments = getopt.getopt(argv, short_opts, long_opts)
    except getopt.GetoptError as message:
        usage(message)

    het_alpha = 0.0001
    hom_alpha = 0.0001
    min_depth = 0
    max_depth = float('inf')
    min_het_ab = 0.2
    max_het_ab = 0.2
    max_hom_ab = 0.1
    exp_het_ab = 0.50
    exp_hom_ab = 0.01
    sample_name = None
    try:
        for flag, value in options:
            if   flag in ('-h','--help'):    usage(exitcode=0)
            elif flag in ('-v','--version'): version(exitcode=0)
            elif flag in ('-a','--het-alpha'):  het_alpha = float(value)
            elif flag in ('-A','--hom-alpha'):  hom_alpha = float(value)
            elif flag in ('-b','--min-het-ab'): min_het_ab = float(value)
            elif flag in ('-B','--max-het-ab'): max_het_ab = float(value)
            elif flag in ('-d','--min-depth'):  min_depth  = float(value)
            elif flag in ('-D','--max-depth'):  max_depth  = float(value)            
            elif flag in ('-e','--exp-het-ab'): exp_het_ab = float(value)
            elif flag in ('-E','--exp-hom-ab'): exp_hom_ab = float(value)
            elif flag in ('-H','--max-hom-ab'): max_hom_ab = float(value)
            elif flag in ('-s','--sample'): sample_name = value
    except ValueError:
        usage("Invalid argument to '%s' option." % flag)

    if num(arguments) != 2:
        usage('Unexpected number of arguments')
    
    iVCF = pysam.VariantFile(arguments[0], mode='r')
    iVCF.header.add_meta(
        __pkgname__ + "Command",
        value="""<ID=%s,CommandLine="%s",Version="%s",Date="%s">""" % (
            __program__,
            ' '.join(sys.argv),
            __version__,
            time.strftime("%B %d, %y %X %p %Z", time.localtime())
        )
    )
    
    if sample_name is not None:
        if sample_name in iVCF.header.samples:
            iVCF.subset_samples((sample_name,))
        else:
            usage("Sample ID not detected in VCF: %r" % sample_name)
    elif num(iVCF.header.samples) != 1:
        usage("Loci with unexpected number of Sample fields detected, expected 1\n")

    oVCF = pysam.VariantFile(arguments[1], mode='w', header=iVCF.header)
        
    warn_noAD_loci = 0
    warn_null_loci = 0
    for locus in iVCF:
        sample = locus.samples[0]
        if num(sample.allele_indices) != 2:
            warn_null_loci += 1
            continue
        
        if 'AD' not in sample:
            sample.allele_indices = (None, None)
            warn_noAD_loci += 1

        if sample.allele_indices[0] == sample.allele_indices[1]:
            # homozygous or no-call
            if sample.allele_indices[0] is None:
                pass
            else:
                total_depth = sum(filter(notNone, sample['AD']))
                if min_depth <= total_depth <= max_depth:
                    allele_balance = 1.0 - float(sample['AD'][sample.allele_indices[0]]) / max(1, total_depth)
                
                    if total_depth not in hom_threshold:
                        if hom_alpha > 0:
                            hom_threshold[total_depth] = float(binom.isf(hom_alpha, total_depth, exp_hom_ab)) / max(1, total_depth)
                        else:
                            hom_threshold[total_depth] = 1.0

                    if allele_balance > max_hom_ab or \
                       allele_balance > hom_threshold[total_depth]:
                        sample.allele_indices = (None, None)
                else:
                    sample.allele_indices = (None, None)
                    
        elif sample.allele_indices[0] is None or \
             sample.allele_indices[1] is None:
            # hemizygous
            if sample.allele_indices[0] is None:
                allele_depth = sample['AD'][sample.allele_indices[1]]
            else:
                allele_depth = sample['AD'][sample.allele_indices[0]]
            total_depth = sum(filter(notNone, sample['AD']))
            if min_depth <= total_depth <= max_depth:
                allele_balance = 1.0 - float(allele_depth) / max(1, total_depth)
            
                if total_depth not in hom_threshold:
                    if hom_alpha > 0:
                        hom_threshold[total_depth] = float(binom.isf(hom_alpha, total_depth, exp_hom_ab)) / max(1, total_depth)
                    else:
                        hom_threshold[total_depth] = 1.0
                
                if allele_balance > max_hom_ab or \
                   allele_balance > hom_threshold[total_depth]:
                    sample.allele_indices = (None, None)
            else:
                sample.allele_indices = (None, None)
        else:
            # heterozygous
            total_depth = sample['AD'][sample.allele_indices[0]] + sample['AD'][sample.allele_indices[1]]
            if min_depth <= total_depth <= max_depth:
                allele_balance = float(sample['AD'][sample.allele_indices[0]]) / max(1, total_depth)

                if total_depth not in het_threshold:
                    if het_alpha > 0:
                        het_threshold[total_depth] = 1.0 - float(binom.isf(het_alpha, total_depth, exp_het_ab)) / max(1, total_depth)  # upper => lower tail
                    else:
                        het_threshold[total_depth] = 0.0

                if allele_balance < (exp_het_ab - min_het_ab):
                    sample.allele_indices = (None, None)

                if allele_balance > (exp_het_ab + max_het_ab):
                    sample.allele_indices = (None, None)                
                
                if allele_balance > exp_het_ab:
                    allele_balance = 1.0 - allele_balance

                if allele_balance < het_threshold[total_depth]:
                    sample.allele_indices = (None, None)
            else:
                sample.allele_indices = (None, None)

        oVCF.write(locus)
        
    if warn_null_loci:
        sys.stderr.write("WARNING: Non-diploid Sample-GT field detected, skipped %d loci\n" % warn_null_loci)
    if warn_noAD_loci:
        sys.stderr.write("WARNING: Loci lacking Sample-AD field detected, skipped %d loci\n" % warn_noAD_loci)
        
    iVCF.close()
    oVCF.close()

main(sys.argv[1:])

