
PACKAGE = '__PACKAGE_NAME__'
VERSION = '__PACKAGE_VERSION__'
CONTACT = '__PACKAGE_CONTACT__'
PURPOSE = 'Plot Meraculous assembler k-mer histograms'

args = commandArgs(TRUE);

if ((length(args) != 3) & (length(args) != 4)) {
    cat("\n", file=stderr())
    cat("Program: plot-kmer-stats (",PURPOSE,")\n", file=stderr(), sep="")
    cat("Version:",PACKAGE,VERSION,"\n", file=stderr())
    cat("Contact:",CONTACT,"\n\n", file=stderr())
    cat('Usage: plot-kmer-stats <in.frq> <in.kha> <out.pdf> [min.depth]\n', file=stderr());
    cat("\n\n", file=stderr())
    q(status=1);
    #   |----+----|----+----|----+----|----+----|----+----|----+----|----+----|----+----|
    #   0        10        20        30        40        50        60        70        80
}

FRQ = read.table(args[1], header=FALSE);
#FRQ = read.table(args[1], header=T);
KHA = read.table(args[2], header=FALSE);

colnames(FRQ) = c('depth','counts');
colnames(KHA) = c('counts','frac');

D.mn = 3;
if (length(args) == 4) {
   D.mn = as.numeric(args[4]);
}

Sum  = sum(as.numeric(FRQ$counts));
d.mn = min(FRQ$depth[which(FRQ$depth > 0)]); # skip 0's
i.mn = which(FRQ$depth == d.mn)[1];

if (is.na(i.mn)) {
   stop('No data with depth greater than zero.');
}

count = FRQ$counts[i.mn];
for (i in i.mn:length(FRQ$depth)) {
    count = mean(FRQ$counts[i+c(0:4)]);
    if (count <= FRQ$counts[i.mn]) {
       i.mn = i;
    } else {
       break;
   }
}
d.mn = FRQ$depth[i.mn];


i.mx  = i.mn
area  = 0;
total = sum(as.numeric(FRQ$counts[i.mn:length(FRQ$counts)]));
for (i in i.mn:length(FRQ$counts)) {
    area = area + FRQ$counts[i];
    if ((area/total) > 0.97) {
       break;
    }
    i.mx = i;
}

i.pk = i.mn;
C.pk = count;
for (i in i.mn:i.mx) {
    count = mean(FRQ$counts[i+c(0:4)]);
    if (count > C.pk) {
       C.pk = count;
       i.pk = i;
    }
}
i.pk = i.pk + 2;
D.mn = max(D.mn,d.mn);
D.mx = FRQ$depth[i.mx];
D.pk = FRQ$depth[i.pk];
C.pk = FRQ$counts[i.pk];

C.lb = c();
C.tk = c();
C.dv = c(0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1);
for (i in 0:round(log10(max(KHA$counts)),0)) {
    C.tk = append(C.tk, (10**i) * C.dv);
    C.lb = append(C.lb, (10**i) * C.dv[10]);
}

pdf(args[3], width=14);
par(mfrow=c(1,2));
plot(FRQ$depth,FRQ$counts/Sum,   xlim=c(0,D.mx), ylim=c(0,1.15*C.pk/Sum), main='Genomic depth', xlab='k-mer depth', ylab='Fraction of distinct k-mers', type='l', col='blue', lwd=2.5);
plot(log10(KHA$counts),KHA$frac, xlim=c(log10(range(C.tk))), ylim=c(0,1), main='Assemblable genome', xlab='k-mer copy number', ylab='Cumulative fraction of distinct k-mers', type='l', lwd=2.5, col='blue', axes=F); 

param = par();
rect(param$usr[1],param$usr[3],param$usr[2],param$usr[4], border='black', lwd=1.5);
axis(1, at=log10(C.tk), labels=NA)
axis(1, tick=F, at=log10(C.lb), labels=C.lb);
axis(2);

invisible(dev.off());
