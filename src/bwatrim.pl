
# AUTHOR = Jessen V. Bredeson
my $PACKAGE = '__PACKAGE_NAME__';
my $VERSION = '__PACKAGE_VERSION__';
my $CONTACT = '__PACKAGE_CONTACT__';
my $PURPOSE = "Trim fastq reads a la Heng Li\'s BWA";

require 5.010;

use strict;
use warnings;
use English;

use Getopt::Std;
use FileHandle;
use File::Spec;
use File::Basename qw(basename);

use constant {H1=>0, S=>1, H2=>2, Q=>3};
use vars qw($PROGRAM $MODULATOR $DEBUG %COMPRESSION
            $LOADED $BWA_MIN_RDLEN @IN @IN_TYPE $OUT_TYPE);

BEGIN {
    $DEBUG = 0;
    $LOADED = 0;
    $PROGRAM = basename($PROGRAM_NAME);
    $OUT_TYPE = 1;
    $MODULATOR = 0;
    $BWA_MIN_RDLEN = 0;
    %COMPRESSION = (
	0 => ['FileHandle','FileHandle'],
	1 => ['FileHandle','FileHandle'],
	2 => ['IO::Compress::Gzip','IO::Uncompress::Gunzip'],
	4 => ['IO::Compress::Bzip2','IO::Uncompress::Bunzip2']
    );
}


sub __FUNC__ { 
    my $lvl = shift || 0;
    return((reverse(split '::',(caller($lvl+1))[3]))[0]) 
}

sub stack {
    my ($type,@msg) = @_;
    my $lvl = 2;
    my $rtn = __FUNC__($lvl);
    print(STDERR "[$PROGRAM $rtn]", '_'x(77-length($PROGRAM.$rtn)),"\n");
    print(STDERR join('',$type,@msg),"\n");
    while (caller($lvl)) {
        my @i = caller($lvl++);
        print(STDERR join(' ',$i[1],'sub',$i[3],'line',$i[2]),"\n");
    }
    print(STDERR '_'x80,"\n\n");
}

sub throw {
    if ($DEBUG) { 
        &stack('EXCEPTION: ',@_);
    } else {
        printf(STDERR "[%s %s] %s\n",$PROGRAM,__FUNC__(1),join('',@_));
    }
    exit(1);
}

sub warn {
    if ($DEBUG) { 
        &stack('WARNING: ',@_);
    } else {
        printf(STDERR "[%s %s] %s\n",$PROGRAM,__FUNC__(1),join('',@_));
    }
}

sub debug {
    printf(STDERR "[%s %s] %s\n",$PROGRAM,__FUNC__(1),join('',@_)) if $DEBUG;
}

sub char {
    my $val = shift // &HELP_MESSAGE;
    $val =~ /^[!-~][ !-~]*$/ && return $val;
    &warn('Not a valid, printable string');
    &HELP_MESSAGE;
}

sub uint {
    my $val = shift // &HELP_MESSAGE;
    $val =~ /^[0-9]+$/ && return $val;
    &warn("Not of unsigned int type: $val");
    &HELP_MESSAGE;
}

sub file {
    my $val = shift // return 0;
    my $op  = shift || '';

    -f $val || return 0;
    if ($op =~ /S/i) { -s $val || &warn("$val is empty.") }
    if ($op =~ /R/i) { -r $val || &warn("$val is not readable.") }
    return $val;
}

sub fq_open {
    my ($file1,$file2) = @_;

    my $fh1 = $COMPRESSION{$IN_TYPE[0]}->[1]->new($file1) || 
    &throw("Cannot open $file1 for reading");
    &debug("$file1 opened");
    my $fh2 = $fh1;
    if (defined($file2) &&
	File::Spec->rel2abs($file1) ne File::Spec->rel2abs($file2)) {
        $fh2 = $COMPRESSION{$IN_TYPE[1]}->[1]->new($file2) || 
	&throw("Cannot open $file2 for reading");
	&debug("$file2 opened");
    }

    return($fh1,$fh2);
}

sub read_fq {
    my $mod = $MODULATOR;
    $MODULATOR = !$MODULATOR;
    return &__get_fq($IN[$mod]);
}

sub __get_fq {
    my $fh = shift;
    my @entry;
    $entry[0] = $fh->getline // return; # allow catching invalid fastq formats
    $entry[0] && $entry[0] =~ /^\@/ || 
        &throw("Fastq header expected, line ",$fh->input_line_number);
    $entry[1] = $fh->getline;
    $entry[2] = $fh->getline;
    $entry[3] = $fh->getline // 
        &throw("Unexpected EOF, line ",$fh->input_line_number);
    $entry[2] && $entry[2] =~ /^\+/ || 
        &throw("Must use Illumina 4-line fastq format");

    return(\@entry);
}

sub core {
    my ($prefix,$end1,$end2,$qthr,$qset,$out_type,$min_l,$max_e) = @_;

    my $fp = []; 
    my @fc = (0,0,0); my ($f0,$f1,$f2);
    my ($failed,$nCasavaFail,$nTrimFail,$ntotal) = (0,0,0,0);
    my ($qoff,$qq) = $qset & 0x1 ? (64,'B') : (33,'#');
    my ($e0,$e1,$e2) = (0,0,0);
    my @e = $out_type & 0x2 ? ($out_type & 0x4 
        ? (\$e0,\$e1,\$e2) : (\$e0,\$e1,\$e1)) : (\$e0,\$e0,\$e0);

    my ($f_mode,$f_suff) 
	=  $OUT_TYPE & 0x4 ? ('','.fastq.bz2')
	: ($OUT_TYPE & 0x2 ? ('','.fastq.gz')
	: ('>','.fastq')); # FileHandle 

    (@IN) = &fq_open($end1,$end2);

    printf(STDERR "[%s] Starting %s\n",$PROGRAM,scalar(localtime));
    while (my $fq = &read_fq()) {
	$ntotal++;
	if (($qset & 0x4) && $fq->[H1] =~ m/^\@\S+\s+[12]:Y:\d+:[ATGCN]*$/) {
	    $nCasavaFail++;
	    $out_type & 0x2 || next; # PE
	    $failed |= (!$MODULATOR)+1;
	} elsif ($qthr) {
	    chomp($fq->[Q]);
	    my ($a,$l) = (0,length($fq->[Q]));
	    my ($max_a,$max_l) = ($a,$l);
	    my @qstr = unpack('C*',$fq->[Q]);
	    while ($l > $BWA_MIN_RDLEN) { 
		$a += $qthr - ($qstr[$l-1] - $qoff);
		if ($a > $max_a) {
		    $max_a = $a;
		    $max_l = $l;
		}
		$l--;
	    }
	    if ($max_l < $min_l) {
		$nTrimFail++;
		$out_type & 0x2 || next; # PE
		$failed |= (!$MODULATOR)+1;
	    } else {
		$fq->[S]  = (substr($fq->[S],0,$max_l)||'N')."\n";
		$fq->[Q]  = (substr($fq->[Q],0,$max_l)||$qq);
		$fq->[Q]  =~ tr/\100-\176/\041-\117/ if $qset & 0x2;
		$fq->[Q] .= "\n";
	    }
	}

	if (${$e[0]} <= 0) {
	    $f0 && $f0->close; ${$e[0]} = $max_e;
	    $f0 = $COMPRESSION{$OUT_TYPE}->[0]->new(
		sprintf("%s%s_0_%04s%s",$f_mode,$prefix,++$fc[0],$f_suff)) ||
		&throw("Cannot open $prefix files for writing");
	}
	if (!($out_type & 0x2)) { $f0->print(@$fq); ${$e[0]}--; next } # SE
	if (${$e[1]} <= 0) { # PE
	    $f1 && $f1->close; ${$e[1]} = $max_e;
	    $f1 = $COMPRESSION{$OUT_TYPE}->[0]->new(
		sprintf("%s%s_1_%04s%s",$f_mode,$prefix,++$fc[1],$f_suff)) ||
		&throw("Cannot open $prefix files for writing");

	    if ($out_type & 0x4) { # PE-split
	    $f2 && $f2->close; ${$e[2]} = $max_e;
	    $f2 = $COMPRESSION{$OUT_TYPE}->[0]->new(
		sprintf("%s%s_2_%04s%s",$f_mode,$prefix,$fc[1],$f_suff)) ||
		&throw("Cannot open $prefix files for writing");
	    } else { $e[2] = \$e1; $f2 = $f1 } # PE-interleaved
	    &debug("Opening file set ",$fc[1]);
	}
	if (@$fp) {
	    if ($failed & 3) {
		if (!($failed & 0x1)) { $f0->print(@$fp); ${$e[0]}-- }
		if (!($failed & 0x2)) { $f0->print(@$fq); ${$e[0]}-- }
		$failed = 0;
	    } else {
		$f1->print(@$fp); ${$e[1]}--;
		$f2->print(@$fq); ${$e[2]}--;
	    }
	    $fp = [];
	} else { $fp = $fq }
    }
    map {$_ && $_->close} (@IN,$f0,$f1,$f2);
    printf(STDERR "[%s] CASAVA-filtered reads: %u \/ %u (%.4f)\n",
	   $PROGRAM,$nCasavaFail,$ntotal,($nCasavaFail/($ntotal||1)));
    printf(STDERR "[%s] Length-filtered reads: %u \/ %u (%.4f)\n",
	   $PROGRAM,$nTrimFail,$ntotal,($nTrimFail/($ntotal||1)));
    printf(STDERR "[%s] Finished %s\n",$PROGRAM,scalar(localtime));

    return 1;    
}

sub eval_dep {
    my $file = shift;
    
    my ($type,$requested) = ($file =~ /\.(?:t?bz2?|bzip2)$/ ? ('bzip2',0x4) 
	: ($file =~ /[.\-](?:Z|g?z)$|_z$/ ? ('gzip',0x2) : ('FileHandle',0x1)));
    
    if (!($requested & $LOADED)) {
	for my $module (@{$COMPRESSION{$requested}}) {
	    my $loaded = File::Spec->catfile(split('::',$module.'.pm'));
	    eval {
		require $loaded;
	    }; if ($EVAL_ERROR) {
		&throw("$type not accessible on this system, ",
		       "check install of module \'$module\'");
	    }
	    &debug($module,' loaded');
	}
	$LOADED |= $requested;
    }
    return($requested);
}

sub HELP_MESSAGE {
die qq/
Program: $PROGRAM ($PURPOSE)
Version: $PACKAGE $VERSION
Contact: $CONTACT

Usage:   $PROGRAM [options] -1 <in.fastq> <out.prefix>

Options: -2 FILE  Read in right-end paired-end fastq FILE [null]
         -B       Write compressed output as bzip2
         -C       Write compressed output as gzip
         -I       Assume Illumina quality (ASCII+64) offset
         -l INT   Minimum acceptable clipped sequence length [35]
         -M INT   Maximum number of entries per file [none]
         -Q       Filter out reads failing CASAVA 1.8+ quality filter
         -q INT   Quality threshold [20]
         -S       Apply strict BWA trimming behaviour
         -s       Split end-pairs into end-specific files (require -2)
         -t       Transform qualities from Illumina to ASCII+33 offset
                  (force -I)
         -h       This help message

Notes: 
  
  1. Sequences must be in the 4-line Illumina fastq format. Currently
     gzip- and bzip2-compressed fastq files are accepted.

  2. ASCII+33 base quality offset is assumed by default.

  3. Specify the same input FILE for both '-1' and '-2' if FILE is an
     interleaved\/collated paired-end file (both pairs in same file).

  4. Sequences trimmed below the value passed via '-l' are silently 
     discarded. If '-2' is used, the good end of a failed end-pair is 
     ouputted to an '_0.fastq' appended file.

  5. Enabling '-s' requires '-2' and forces its accompanying behaviour. 
     Left- and right-ended files get '_1.fastq' and '_2.fastq' appended
     (respectively) to out.prefix

  6. Setting '-l' overrides '-S'

/;
}

sub main {
    my %argv;
    
    my ($qthr,$qset,$out_type,$min_l,$max_e) = (20,0,0,35,9**9**9);
    getopts('1:2:BCISM:QXl:q:st',\%argv) && @ARGV == 1 || &HELP_MESSAGE;    
    for my $o (sort keys %argv) {
	if    ($o eq 'h') { &HELP_MESSAGE }
	elsif ($o eq 'X') { $DEBUG = 1 }
	elsif ($o eq 'I') { $qset |= 0x1 }
	elsif ($o eq 'Q') { $qset |= 0x4 }
	elsif ($o eq 'S') { $BWA_MIN_RDLEN = 35; $min_l = 1 }
	elsif ($o eq 'l') { $min_l = &uint($argv{$o}) }
	elsif ($o eq 'q') { $qthr  = &uint($argv{$o}) }
	elsif ($o eq 'M') { $max_e = &uint($argv{$o})||9**9**9 }
        elsif ($o eq 'B') { $OUT_TYPE = &eval_dep('.bz2') }
	elsif ($o eq 'C') { $OUT_TYPE = &eval_dep('.gz')  }
	elsif ($o eq 't') { $qset & 0x1 || print(STDERR "[$PROGRAM] Setting '-I'...\n");$qset |= 3 }
	elsif ($o eq 's') { $out_type ^ 3 && &throw("'-s' requires PE input.");
			    $out_type |= 0x4 }
	elsif ($o =~ /^[12]$/) { &file($argv{$o},'rs'); $IN_TYPE[$o-1] = &eval_dep($argv{$o}); 
			    $out_type |= $o }
    }

    $out_type & 0x1 || &HELP_MESSAGE;
    &core(&char($ARGV[0]),$argv{'1'},$argv{'2'},$qthr,$qset,$out_type,$min_l,$max_e);
    return(0);
}


exit(&main);
