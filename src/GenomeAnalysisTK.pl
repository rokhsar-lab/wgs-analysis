
my $PACKAGE = '__PACKAGE_NAME__';
my $VERSION = '__PACKAGE_VERSION__';
my $CONTACT = '__PACKAGE_CONTACT__';
my $PURPOSE = 'GATK command-line wrapper';

use strict;
use warnings;

use File::Spec;
use File::Temp;
use File::Basename;
use EnvironmentModules;

use vars qw($PROGRAM $JARNAME);
BEGIN {
    $PROGRAM = basename($0);
    $JARNAME = 'GenomeAnalysisTK.jar';
}

sub WRAPPER_VERSION {
    die "$PROGRAM $VERSION\n";
}

sub WRAPPER_USAGE {
    die qq/
Program: $PROGRAM ($PURPOSE)
Version: $PACKAGE $VERSION
Contact: $CONTACT

Usage:   $PROGRAM [options] [-T] <GATK.tool> [GATK.options]

Options: --module-version VSTR  Load VSTR version of GATK
         --memory INT           Set the max java heapsize [512M]
         --wrapper-help         See this help page
         --wrapper-version      See wrapper version
         --tmp-dir STR          Set the tmpdir for writing [\$TMPDIR]
         --help                 See GATK help page
         --version              See GATK version

/;
}

sub which {
    my $executable = shift //
        die("Must pass an executable name to search PATH\n");
    
    $executable .= $^O =~ /mswin/i ? '.exe' : '';
    
    my $exepath;
    for my $path (File::Spec->path()) {
        my $path = File::Spec->catfile($path,$executable);
        if (-x $path) { $exepath = $path; last }
    }   
    $exepath // die("Must add $executable to your PATH\n");
    
    return $exepath;
}


sub extractOptionValues {
    my $options = shift;
    my %values;
    for my $flag (keys %$options) {
	for (my $i = 0; $i < @ARGV; ++$i) {
	    if ($ARGV[$i] =~ /$flag/) {
		if ($options->{$flag}) {
		    $values{$flag} = $ARGV[$i+1];
		    splice(@ARGV,$i,2);
		    last;
		} else {
		    $values{$flag} = 1;
		    splice(@ARGV,$i,1);
		}
	    }
	}
    }
    
    return \%values;
}


sub buildCommand {
    my $java    = shift;
    my $gatk    = shift;
    my $memory  = '512M';

    # we must extract memory option, as it is passed into the wrapper,
    # but may be mixed in among GATK's args:
    my $opt = extractOptionValues({
	'memory'          => 1,
	'tmp-dir'         => 1,
	'wrapper-help'    => 0,
	'wrapper-version' => 0,
    });
    if ($opt->{'wrapper-help'}) {
	WRAPPER_USAGE;
    } elsif ($opt->{'wrapper-version'}) {
	WRAPPER_VERSION;
    } elsif (defined($opt->{'memory'})) {
	$memory = $opt->{'memory'};
    }

	
    # allow samtools-like command-line interface, tool (passed via -T)
    # must be passed as first (non-wrapper) argument:

    my $tmpdir  = length($opt->{'tmp-dir'}) 
	? File::Spec->rel2abs($opt->{'tmp-dir'}) 
	: File::Spec->tmpdir();
    my $command = "$java -Djava.io.tmpdir=".File::Temp->newdir(File::Spec->catfile($tmpdir,"GenomeAnalysisTK-$$-XXXXXXXXXXXXXXXXX"), 'CLEANUP' => 1, 'UNLINK' => 1);
    if (@ARGV) {
	if (defined($ARGV[0]) && $ARGV[0] =~ /^[^-]/) {
	    unshift(@ARGV,'-T');
	}
    } else {
	push(@ARGV,'--help');
    }

    $command .= ' -Xmx'.$memory if $memory;
    $command .= join(' ','','-jar',$gatk,@ARGV);

    return $command;
}


sub main {
    my $opt = extractOptionValues({'module-version'=>1});

    module('load oracle-jdk');
    module('load GATK'.(defined($opt->{'module-version'}) ? '/'.$opt->{'module-version'} : ''));

    # if 'which' can find it, it's executable
    my $java = which('java'); 
    my $gatk = dirname(which('GenomeAnalysisTK'));
    if (! defined($java)) {
	die "[$PROGRAM] Cannot load Java from env.\n";
    }
    
    if (! defined($gatk)) {
	die "[$PROGRAM] Cannot load GATK from env.\n";
    }
    
    my $jar = File::Spec->catfile($gatk,$JARNAME);
    if (! -f $jar) { 
	die "[$PROGRAM] jar-file does not exist: $jar\n";
    }


    system( buildCommand($java,$jar) );
    
    return($?);
}

exit main();
