
my $PACKAGE = '__PACKAGE_NAME__';
my $VERSION = '__PACKAGE_VERSION__';
my $CONTACT = '__PACKAGE_CONTACT__';
my $PURPOSE = 'Filter blastN alignments';

use strict;
use warnings;

use Getopt::Std;
use File::Basename;

use vars qw($PROGRAM);

BEGIN {
    $PROGRAM = basename($0);
}

my %argv;
my $endedness = 0;
@ARGV == 3 || die qq/
Program: $PROGRAM ($PURPOSE)
Version: $PACKAGE $VERSION
Contact: $CONTACT

Usage: $PROGRAM <blast.tsv> <min.read.len> <max.edits>

Notes: blast.tsv is produced by: 
  blastn -outfmt '6 qseqid qstart qend sseqid \
    length mismatch gaps qlen slen'

/;

my $max = pop(@ARGV) || 0;
my $min = pop(@ARGV) || 1;

# if ($argv{'1'}) { $endedness |= 0x1 }
# if ($argv{'2'}) { $endedness |= 0x2 }
# if ($endedness == 3) { $endedness  = 0x4 }

print("# 0x1: length(end-1) >= $min\n");
print("# 0x2: length(end-2) >= $min\n");
while (<>) {
    m/^#|^\s*$/ && next;
    
    my @F = split(/\t/);

    $F[1]--;
    $F[4] = ($F[4] - $F[5] - $F[6]);

    my $end = 0;
    if ($F[1] >= $min) { $end |= 0x1 }
    if ($F[7]-$F[2] >= $min) { $end |= 0x2 }
    
    printf("%s\t%u\t%u\t%s\t%u\t+\n",@F[0..3],$end);
	#if (($end & $endedness) && ($F[8]-$F[4] <= $max));
}
