
my $PACKAGE = '__PACKAGE_NAME__';
my $VERSION = '__PACKAGE_VERSION__';
my $CONTACT = '__PACKAGE_CONTACT__';
my $PURPOSE = 'Utils for working with BED files';

require 5.010;

use strict;
use warnings;
use English;

use Data::Dumper;

use Getopt::Std;
use File::Basename;
use Valkyr::Error qw(throw warn debug);
use Valkyr::Math::Utils 'max';
use Valkyr::Data::Type::Assert qw(is_file is_uint is_sint is_string);
use Valkyr::FileUtil::IO;
use Valkyr::FileUtil::Fasta qw(readFaidx);

use vars qw($PROGRAM $CMD $CHAIN);
use constant {CHROM=>0, RSTART=>1, REND=>2, NAME => 3, SCORE => 4, STRAND => 5};

BEGIN {
    $PROGRAM = basename($0); 
    $CHAIN = 1 
}



sub BED2CHAIN_USAGE {
    die qq/
Usage: $PROGRAM bed2chain <in.bed> <target.fasta>

Notes: 

   Assumes the query is the new sequence (BED 'name' field) and the target 
   the old ('chr', 'start', and 'end' BED fields).

/;
}

sub bed2chain {
    my %argv;
    getopts('h',\%argv) && !$argv{'h'} && @ARGV == 2 || &BED2CHAIN_USAGE;
    
    warn("This code is experimental and was written for a very specific purpose.");

    my $fdi = open(shift(@ARGV),READ);
    my $fai = readFaidx(shift(@ARGV));

    my @bed;
    my $prevchr;
    my $prevnam;
    my $len = 0;
    $CHAIN = 1;
    while (local $_ = $fdi->getline()) {
	m/^#|^\s*$/ && next;
	chomp();

	my @BED = split(/\t/);
	
	if (defined($prevchr) && $prevchr ne $BED[CHROM]) {
	    print chainMatched(\@bed,$fai,$len);
	    @bed = ();
	}
	push(@bed,\@BED);
	
	$prevchr = $BED[CHROM];
	$prevnam = $BED[NAME];
	$len     = $BED[REND];
    }
    if (@bed) {
	print chainMatched(\@bed,$fai,$len);
    }
    if (keys(%$fai)) {
	print chainUnmatched($fai);
    }

    return 1;
}
	  
sub chainUnmatched {
    my $fai = shift;

    my @chain;
    for my $q (sort keys %$fai) {
	my $qlen = $fai->{$q}->{'length'};
	push(@chain,join(' ','chain',999,$q,$qlen,'+',0,$qlen,
			 $q,$qlen,'+',0,$qlen,$CHAIN++));
	push(@chain,$qlen,'');
    }
    return join("\n",@chain,'');   
}

  
sub chainMatched {
    my $bed  = shift;
    my $fai  = shift;
    my $rlen = shift;

    my @chain;
    for my $b (@$bed) {
	my $qlen = $b->[REND] - $b->[RSTART];
	push(@chain,join(' ','chain',999,$b->[CHROM],$rlen,'+',$b->[RSTART],$b->[REND],
			 $b->[NAME],$qlen,($b->[STRAND] eq '-' ? $b->[STRAND] : '+'),0,$qlen,$CHAIN++));
	push(@chain,$qlen,'');
	$fai->{$b->[NAME]} && delete($fai->{$b->[NAME]});
	$fai->{$b->[CHROM]} && delete($fai->{$b->[CHROM]});
    }
    return join("\n",@chain,'');	    
}


sub SITE2BED_USAGE {
    die "Usage: $PROGRAM site2bed [-d INT] [-01Ah] <-|sites.tsv>\n";
}


sub site2bed {
    my %argv = ('o' => STDIO, 'd' => 1);
    my ($start,$lastpos,$lastchr);
    getopts('h01d:o:A',\%argv) && !$argv{'h'} && @ARGV || &SITE2BED_USAGE;
    
    my $dist = is_uint($argv{'d'},\&SITE2BED_USAGE);
    my $fdi  = open(shift(@ARGV),READ);
    my $fdo  = open($argv{'o'},WRITE);
    
    my ($chr,$pos,@rest);
    if ($argv{'0'} || $argv{'1'}) { $dist = -1 };
    while (local $_ = $fdi->getline()) {
	m/^#|^\s*$/ && next;
	chomp();
	

	($chr,$pos,@rest) = split(/\t/);
	$lastchr //= is_string($chr);
	$lastpos //= is_uint($pos);
	$start   //= is_uint($pos);
	if (($lastchr ne is_string($chr)) || 
	    (is_uint($pos) - $lastpos > $dist) || $fdi->eof()) {
	    if ($argv{'0'}) { $start = 1 }
	    if ($argv{'1'}) { 
		$lastchr = $chr;
		$lastpos = $start = $pos;
	    }
	    print($fdo join("\t",$lastchr,$start-1,$lastpos));
	    print($fdo join("\t",'',@rest)) if $argv{'A'} && @rest;
	    print($fdo "\n");
	    $start = $pos;
	}

	$lastpos = $pos;
	$lastchr = $chr;
	@rest = ();
    }

    $fdo->close();
    $fdi->close();

    return 1;
}


sub EXPAND_USAGE {
    die "Usage: $PROGRAM expand [-l INT] [-r INT] <in.bed>\n";
}


sub expand {
    local $_ = '';
    my %argv = ('l' =>0, 'r' => 0);
    getopts('l:r:',\%argv) && @ARGV || &EXPAND_USAGE;
    is_sint($argv{'l'},\&EXPAND_USAGE); 
    is_sint($argv{'r'},\&EXPAND_USAGE);

    while (<>) {		 
	m/^#|^\s*$/ && next;
	chomp;
	
	my @BED = split /\t/;
	$BED[RSTART] -= $argv{'l'};
	$BED[REND] += $argv{'r'};
	$BED[RSTART] = $BED[RSTART] < 0 ? 0 : $BED[RSTART];
	print join("\t",@BED),"\n";
    }
    return 1;
}


sub readList {
    my $file  = is_file(shift,'fr');
    my $field = is_uint(shift);

    my %data;
    my $list = open($file,READ);
    while (local $_ = $list->getline()) {
        m/^#|^\s*$/ && next;
	chomp();
        my @F = split(/\t/);

        $data{ is_string($F[0]) } = $field ? $F[$field] : 1;
    }
    $list->close();

    return \%data;
}

sub readChain {
    my $file  = is_file(shift,'fr');

    # chain 0.1642 scaffold08420 174146 - 0 174146 chromosomeI 20421248 + 0 174146 1

    my %data;
    my $list = open($file,READ);
    while (local $_ = $list->getline()) {
        m/^chain/ || next; chomp();
	
        my @F = split(/\s+/);

	# chain files in half-open interval convention, convert
	# range end coordinates to 0-based
	$data{ is_string($F[2]) } = {
	    'tname'   => is_string($F[2]),
	    'tlength' => is_uint($F[3]),
	    'tstrand' => is_string($F[4]),
	    'tstart'  => is_uint($F[5]),
	    'tend'    => is_uint($F[6]),
	    'qname'   => is_string($F[7]),
	    'qlength' => is_uint($F[8]),
	    'qstrand' => is_string($F[9]),
	    'qstart'  => is_uint($F[10]),
	    'qend'    => is_uint($F[11])
	};
    }
    $list->close();

    return \%data;
}


sub uvec {
    return is_string(shift) eq '-' ? -1 : 1;
}
	
sub strand {
    return is_sint(shift) < 0 ? '-' : '+';
}

sub orient { # only to be used with half-open intervals
    my $start  = is_uint(shift);
    my $end    = is_uint(shift);
    my $strand = is_string(shift);

    if ($strand eq '-') {
	return( $start < $end ? ($end,$start) : ($start,$end) );

    } else {
	return( $start,$end );
    }
}


sub remapCoord {
    my $tstrand = is_string(shift);
    my $tstart  = is_uint(shift);
    my $sstart  = is_uint(shift);
    my $qstart  = is_uint(shift);
    my $base1   = is_uint(shift||0);
    
    return(
	$tstart + uvec($tstrand) * abs($qstart - $sstart)
	# + ($base1 ? uvec($tstrand) : 0)
    );
}


sub LIFT_USAGE {
    die qq/
Usage:   $PROGRAM lift [options] <in.coord> <in.chain>

Options: -o FILE  Write output to FILE [stdout]
         -0       Point-coord File is 0-based
         -p       in.coord is point-coord file [BED]
         -q       in.coord contains features as query [target]
         -h       This help message

Notes: 

   When '-p' is in effect, coordinates in in.coord and their corresponding 
   entries in in.chain are assumed to have the same strandedness relative
   to each other (coordinate 1 is on positive strand).


/;
}


sub lift {
    warn("TODO: fix to adhere to .chain format spec.");
    my %argv = ('o' => STDIO, 0 => 0, 'p' => 0);
    getopts('pq0o:',\%argv) && !$argv{'h'} && @ARGV == 2 || &LIFT_USAGE;
    
    my $file  = open(shift(@ARGV));
    my $chain = readChain(shift(@ARGV),$argv{'q'});
    my $fdo   = open($argv{'o'},WRITE);

    my ($source,$target) = $argv{'q'} ? ('q','t') : ('t','q');
    for my $contig (keys(%$chain)) {
	my $c = $chain->{$contig};

	# if both are negative, they are effectivly '+' wrt one another
	if ($c->{$source.'strand'} eq $c->{$target.'strand'}) {
	    $c->{$source.'strand'} =  $c->{$target.'strand'} = '+';
	}
	($c->{$source.'start'},$c->{$source.'end'}) 
	    = orient($c->{$source.'start'},
		     $c->{$source.'end'},
		     $c->{$source.'strand'});
	($c->{$target.'start'},$c->{$target.'end'}) 
	    = orient($c->{$target.'start'},
		     $c->{$target.'end'},
		     $c->{$target.'strand'});
    }

    while (local $_ = $file->getline()) {
	m/^#|^\s*$/ && next; chomp();
	
	my @BED = split(/\t/);
	if (! $chain->{$BED[CHROM]}) {
	    throw('Contig missing chain entry: ',$BED[CHROM]);
	}

	my $c = $chain->{$BED[CHROM]};
	if (is_sint($BED[RSTART]) < 0) {
	    throw(sprintf("Coordinate out of bounds (%s:%s) ",@BED[CHROM,RSTART]));
	}
	if ($argv{'p'}) {
	    #if (!$argv{'0'}) { $BED[RSTART]-- }
	    if ($BED[RSTART] < 0) {
		throw(sprintf("Coordinate out of bounds, perhaps you forgot ".
			      "to enable the '-0' option? (%s:%s).",@BED[CHROM,RSTART]));
	    }
	    ($BED[RSTART]) = remapCoord(
		$c->{$target.'strand'},
		$c->{$target.'start'},
		$c->{$source.'start'},
		$BED[RSTART],
		!$argv{'0'}
		);

	} else {
	    $BED[NAME]   //= '.';
	    $BED[SCORE]  //=  0 ;
	    $BED[STRAND] //= '+';
	    
	    @BED[RSTART,REND] = orient(@BED[RSTART,REND,STRAND]);
	    $BED[RSTART] = remapCoord(
		$c->{$target.'strand'},
		$c->{$target.'start'},
		$c->{$source.'start'},
		$BED[RSTART]
		);
	    $BED[REND]   = remapCoord(
		$c->{$target.'strand'},
		$c->{$target.'start'},
		$c->{$source.'start'},
		$BED[REND]
		);
	    
	    @BED[RSTART,REND] = sort {$a <=> $b} @BED[RSTART,REND];
	    $BED[STRAND]      = strand(uvec($BED[STRAND]) *
				       uvec($c->{$source.'strand'}) *
				       uvec($c->{$target.'strand'}));
	}
	$BED[CHROM] = $c->{$target.'name'};

	print($fdo join("\t",@BED),"\n");
    }

    return 1;
}

sub LEN_USAGE {
    die qq/
Usage:   $PROGRAM len [options] <in.bed>

Options: -o FILE  Write output to FILE
         -d       Calc. median summary statistic
         -u       Calc. mean summary statistic
         -s       Calc. sum of all values
         -m       Calc. min value
         -M       Calc. max value
         -r       Calc. range (same as -mM)


/;
}

sub len {
    my %argv;
    getopts('usdmMro:',\%argv) && @ARGV == 1 || &LEN_USAGE;
	
    if ($argv{'r'}) { $argv{'m'} = $argv{'M'} = 1 }

    my $fdi = open(shift(@ARGV),READ);
    my $fdo = open($argv{'o'},WRITE) if exists($argv{'o'});

    my @l;
    my $sum  = 0;
    my $count = 0;
    while (local $_ = $fdi->getline()) {
	m/^#|^\s*$/ && next;
	chomp();

	my @F = split(/\t/);
	my $l = $F[2]-$F[1];

	push(@F,$l);
	push(@l,$l);

	print($fdo join("\t",@F),"\n") if exists($argv{'o'});

	$sum += $l;
	$count++;
    }
    @l   = sort {$a <=> $b} @l;
    
    my $mu  = $sum / ($count || 1);
    my $mid = $l[0.5*@l];
    
    my @dat = ();
    if ($argv{'u'}) { push(@dat, $mu) }
    if ($argv{'d'}) { push(@dat, $mid) }
    if ($argv{'s'}) { push(@dat, $sum) }
    if ($argv{'m'}) { push(@dat, $l[0]) }
    if ($argv{'M'}) { push(@dat, $l[$#l]) }    
    if (@dat) { print('# ',join("\t",@dat),"\n") }

    return 1;
}

sub catint {
    @ARGV == 2 || die sprintf("Usage: %s <in.bed> <genome.len>\n",basename($0));
    
    my $fdi = open(shift(@ARGV),READ);
    my $ref = open(shift(@ARGV),READ);
    my $fdo = open(STDIO,WRITE);

    my %offset;
    my $offset = 0;
    while (local $_ = $ref->getline()) {
	m/^#|^\s*$/ && next;
	chomp();

	my @ref = split(/\t/);

	$offset{$ref[0]} = is_uint($offset);

	$offset += $ref[1];
    }

    while (local $_ = $fdi->getline()) {
	m/^#|^\s*$/ && next;
	chomp();
	
	my @bed = split(/\t/);
	
	if (! exists($offset{$bed[0]})) {
	    throw("BED contig not found in reference: $bed[0]");

	}
	print($fdo join("\t",'genome',
			is_uint($bed[1])+$offset{$bed[0]},
			is_uint($bed[2])+$offset{$bed[0]},
			$bed[0],
			@bed[3..$#bed]
	      ),"\n");
    }
    close($fdo);
    close($ref);
    close($fdi);
	

    return 1;
}

sub HELP_MESSAGE {
    die qq/
Program: $PROGRAM ($PURPOSE)
Version: $PACKAGE $VERSION
Contact: $CONTACT

Usage:   $PROGRAM <command> [options]

Command: expand     Expand existing range
         site2bed   Convert contiguous sites to bed
         bed2chain  Convert a bed file to chain format
         catint     Concatenate intervals into a single coordinate system 
         lift       Map (lift) one coordinate system to another
         len        Compute length of BED intervals


/;
}


main: {
    my %FUNCTION = (
	'site2bed'  => \&site2bed,  'expand' => \&expand,
	'bed2chain' => \&bed2chain, 'lift'   => \&lift,
	'catint'    => \&catint,    'len'    => \&len,
	);
    
    if ($ARGV[0] && $FUNCTION{$ARGV[0]}) {
	$CMD = shift @ARGV;
	exit( ! &{$FUNCTION{$CMD}} );

    } else {
	&HELP_MESSAGE;
    }
}	

