
PACKAGE = '__PACKAGE_NAME__'
VERSION = '__PACKAGE_VERSION__'
CONTACT = '__PACKAGE_CONTACT__'
PURPOSE = 'Plot SNV rate profile'

args = commandArgs(TRUE);

if (length(args) < 2) {
    cat("\n", file=stderr());
    cat("Program: plot-snv-rate-profile (",PURPOSE,")\n", file=stderr(), sep="")
    cat("Version:",PACKAGE,VERSION,"\n", file=stderr())
    cat("Contact:",CONTACT,"\n\n", file=stderr())
    cat("Usage: plot-snv-rate-profile <genome.fasta> <snvrate.conf> [Y.max]\n\n", file=stderr());
    cat("Notes:\n\n", file=stderr());
    cat("   snvrate.conf takes the form:\n", file=stderr());
    cat("   [sample-name:]<sample.rate>[,depth.bed]\n", file=stderr());
    cat("\n\n", file=stderr());
    quit(status=1);
}


ymx = -1;
if (length(args) > 2) {
   ymx = as.numeric(args[3]);
}

chrom.sizes = read.table(paste(args[1],'fai',sep='.'), header=F);
chrom.names = chrom.sizes[,1]
chrom.sizes = chrom.sizes[,2]
names(chrom.sizes) = chrom.names

pdf.width = 3
pdf.height = ceiling(length(chrom.names) / 3)

input.list = read.table(args[2], stringsAsFactors=FALSE, header=FALSE);


for (i in seq(length(input.list[,1]))) {
    inputs = strsplit(as.character(input.list[i,1]), ':')[[1]];
    sample = toString(i)
    if (length(inputs) == 2) {
       sample = inputs[1];
       inputs = inputs[2];
    }
    cno = 3;
    Dep = data.frame();
    inputs = strsplit(inputs[1], ',')[[1]];
    if (length(inputs) == 2) {
        Dep = as.data.frame(read.table(inputs[2], na.strings='.'));
	cno = length(Dep);
	nas = which(is.na(Dep[,cno]));
	if (length(nas) > 0) {
	    Dep[nas,cno] = rep(0, length(nas));
	}
	Dep[,cno] = as.integer(Dep[,cno]);
	inputs = inputs[1];
    }
    Het = read.table(inputs, stringsAsFactors=FALSE, header=TRUE);

    cat("Plotting ",sample,"\n", sep='');
    pdf(paste(sample,'profile','pdf', sep='.'), width=7*pdf.width, height=7*pdf.height);
    par(mfrow=c(pdf.height,pdf.width), las=1);

    if (ymx < 0) {
        ymx = max(Het$HET,Het$HOM);
    }
    cno = length(Dep);
    for (chrom.name in chrom.names) {
        chrom.size = chrom.sizes[chrom.name]
        dat  = Het[which(Het$CHROM == chrom.name),]


	dep = data.frame();
	if (cno >= 3) {
	    dep = Dep[which(Dep[,1] == chrom.name),];
	} else {
	    V2  = dat$START;
	    V5  = rep(0,length(dat$START));
	    dep = data.frame(V2,V5);
	    cno = length(dep);
        }
	dmx = max(0,dep[,cno]);
	# dmx = max(0,Dep[,cno]);

	# c("Heterozygous","Homozygous"), col=c("#000000","#00688B")
	plot(dat$START,dat$HET, col="#000000", type='l', bty='l', lwd=2, xlim=c(0,chrom.size), ylim=c(0,ymx), xlab='Position', ylab='', main=chrom.name); #, xaxp=c(0,chrom.size,as.integer(chrom.size/2e6)), cex.axis=1);
    	lines(dat$START,dat$HOM, col="#00688B", lwd=2);
	lines(dep$V2,ymx*dep[,cno]/dmx,col="grey50", lwd=2);

    }
    invisible(dev.off());
}




    
