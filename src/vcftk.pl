
$PURPOSE = 'Utilities for working with VCF files';
$PACKAGE = '__PACKAGE_NAME__';
$VERSION = '__PACKAGE_VERSION__';
$CONTACT = '__PACKAGE_CONTACT__';

require 5.010;

use strict;
use warnings;
no  warnings 'recursion';
use English;

use Getopt::Std;
use File::Spec;
use File::Basename 'basename';
use Valkyr::Error qw(throw debug warn report);
use Valkyr::Error::Exception qw(ValueError DeprecationWarning AssertionError);
use Valkyr::Util::Token qw(__PROGRAM__ __FUNCTION__);
use Valkyr::Math::Utils qw(argmax argmin max min sum log);
use Valkyr::Math::Units::HumanReadable 'expand';

use Valkyr::FileUtil::IO;
use Valkyr::FileUtil::VCF 
    qw(:standard NULL_FIELD NULL_GT MAX_GQ GT_FIELD_SEP BREAKEND_MSG BREAKEND_PAT
       ALLELE_FIELD_SEP PASS_FILTER_PAT GT_GROUP_PAT GT_PHASED NULL_ALLELE);
use Valkyr::FileUtil::Fasta qw(readFaidx faidxName);
use Valkyr::FileUtil::Counter 
    qw(progressReport progressCounter configCounter resetCounter);

use Valkyr::Data::Type::Test;
use Valkyr::Data::Type::Assert 
    qw(is_uint is_sint is_ufloat is_sfloat is_file is_subclass is_coderef
       is_string is_regex is_arrayref is_hashref is_stringref is_bool is_char);

use vars qw($PROGRAM $PACKAGE $PURPOSE $VERSION $CONTACT $DEBUG $PRINT_LAB);

BEGIN { $PROGRAM = basename(__PROGRAM__) }

use constant { # 32-bit masks
    NONREF =>  0xFFFFFFFF,
    CHROM1 => ~0xAAAAAAAA, CHROM2 =>  0xAAAAAAAA,
    ALLEL1 => ~0xCCCCCCCC, ALLEL2 =>  0xCCCCCCCC,
    SEQID  => 0, SOURCE => 1, TYPE   => 2, RSTART => 3, REND   => 4, 
    SCORE  => 5, STRAND => 6, PHASE  => 7, ATTR   => 8,
    inf    => 9_223_372_036_854_775_808
};


sub basefunc {
    is_string(shift) =~ /(\w+)$/;
    return $1 || 'function';
}


sub load {
    my $module = shift || 
	throw('Module name expected at ',__FUNCTION__,'().');
    
    local $EVAL_ERROR = undef;
    
    eval "require $module";
    throw("Module not accessible, check ",
	  "install of module \'$module\'.") if $EVAL_ERROR;
    
    return 1;
}


sub which {
    my $executable = shift //
        throw("Must pass an executable name to search PATH");
    
    $executable .= $^O =~ /mswin/i ? '.exe' : '';
    
    my $path;
    my $exepath;
    for $path (File::Spec->path()) {
        $path = File::Spec->catfile($path,$executable);
        if (Valkyr::Data::Type::Test::is_file($path, 'fx')) { 
	    $exepath = $path; 
	    last;
	}
    }   
    if (! defined($exepath)) {
	throw("Must add $executable to your PATH");
    }
    
    return $exepath;
}


sub escChar {
    my $string = is_string(shift);

    $string =~ s/([\040-\044\072-\074\076-\100\133-\136\140\173-\176])/_/g;

    return $string;
}


sub float2phred {
    my $float = is_ufloat(shift); 
    my $e_min = is_ufloat(shift // '1e-309'); 
    # the limit of perl's numeric precision on Debian x86_64 is '1e-309'
    
    if ($float =~ /^0\.?0*$/) { $float = $e_min }
    my @E = split(/[eE]/,$float);
    
    return(-10 * (log(10,$E[0]||1) + ($E[1]||0)));
}


sub sort_naturally {
    return map {$_->[1]}
           CORE::sort {$a->[0] cmp $b->[0]}
           map {my $key = uc($_);
	        $key =~ s/(\d+)/sprintf('%020d',$1)/ge; [$key,$_]} @_;
}


sub sort_naturally2 {
    return map {[$_->[1], $_->[2]]}
           CORE::sort {($a->[0] cmp $b->[0]) || ($a->[2] <=> $b->[2])}
           map {my $key = uc($_->[0]);
	        $key =~ s/(\d+)/sprintf('%020d',$1)/ge; [$key,$_->[0],$_->[1]]} @_;
}


sub phred2float {
    my $phred = is_ufloat(shift);
    return 10**($phred / -10);
}


sub as_sbitA {
    my $allele = is_string(shift);

    return $allele eq NULL_ALLELE ? 0 : (1 << is_uint($allele));
}


sub as_aidxB {
    my $bit = is_uint(shift);
    
    return $bit ? numShifts($bit) : NULL_ALLELE;
}


sub addFILTER {
    my $tag = is_string(shift);
    
    my %filter;
    if ($tag !~ PASS_FILTER_PAT) {
	%filter = map{$_=>1} split(/,/,$tag);
    }
    map {$filter{is_string($_)}=1} @_;

    return join(',',sort keys(%filter));
}


sub addFT {
    my $tag = is_string(shift);
    
    my %filter;
    if ($tag !~ PASS_FILTER_PAT) {
	%filter = map{$_=>1} split(/;/,$tag);
    }
    map {$filter{is_string($_)}=1} @_;

    if (%filter) {
	return join(';',sort keys(%filter));
    } else {
	return 'PASS';
    }
}


sub readList {
    my $file  = is_file(shift,'f');
    my $field = is_uint(shift // 0);
    
    my $data = {};
    
    my $list = open($file,READ);
    while (defined( local $_ = $list->getline() )) {
        m/^#|^\s*$/ && next;
	chomp();
        my @F = split(/\t/);
        $data->{$F[0]} = $F[$field];
    }
    $list->close();

    return $data;
}


sub readOrderedList {
    my $file  = is_file(shift,'f');
    my $field = is_uint(shift // 0);
    
    my %seen;
    my @data;
    
    my $list = open($file,READ);
    while (defined( local $_ = $list->getline() )) {
        m/^#|^\s*$/ && next;
	chomp();
        my @F = split(/\t/);
	
	if ($seen{$F[$field]}) {
	    warn(sprintf('Duplicate entry detected (%s, line %s).',
			 $file,$list->input_line_number()));
	}

	push(@data,$F[$field]);
	$seen{$F[$field]}++;
    }
    $list->close();

    return \@data;
}


sub readBED {
    my $file  = is_file(shift,'f');

    my $prev_chr = '';
    my $prev_pos = -1;

    my @data;
    my $list = open($file,READ);
    while (local $_ = $list->getline()) {
        m/^#|^\s*$/ && next;
	chomp();

        my @bed = split(/\t/);

	if ($bed[0] eq $prev_chr && $bed[1] < $prev_pos) {
	    throw("Input BED must be reference-order sorted: $file.");
	}
        #push @data,[
	#    do{(my $t = join(':',@F[0..2])) =~ s/(\d+)/sprintf"%015.6d",$1/ge; $t},
	push(@data,
	     {
		 'CHROM'  => is_string($bed[0]),
		 'START'  => is_uint($bed[1]),
		 'END'    => is_uint($bed[2]),
		 'NAME'   => is_string($bed[3]//$bed[0]),
		 'SCORE'  => is_sfloat($bed[4]||0),
		 'STRAND' => is_string($bed[5]||'+')
	     });
	#];
	$prev_chr = $bed[0];
	$prev_pos = $bed[1];
    }
    close($list);
    
    return \@data;
    # return [map {$_->[1]} CORE::sort {$a->[0] cmp $b->[0]} @data];
}

sub readVCF {
    my $file = is_file(shift,'f');

    local $_ = '';
    my $prev_chr = '';
    my $prev_pos = -1;

    my @data;
    my $list = open($file,READ);
    while ($_ = $list->getline()) {
	if(/^\S+/) { last }
    }
    if (!/^##fileformat=VCF/) {
	throw("Missing VCF header or not a VCF file: $file");
    }
    while ($_ = $list->getline()) {
        m/^#|^\s*$/ && next;
	chomp();

        my @vcf = split(/\t/);

	if ($vcf[0] eq $prev_chr && $vcf[1] < $prev_pos) {
	    throw("Input VCF must be reference-order sorted: $file.");
	}

        push(@data,
	     {
		 'CHROM'  => is_string($vcf[0]), 
		 'START'  => is_uint($vcf[1])-1, # POS
		 'END'    => is_uint($vcf[1]),   # POS
		 'NAME'   =>(is_string($vcf[2]) eq NULL_FIELD ? "$vcf[0]:$vcf[1]" : $vcf[2]),
		 'SCORE'  => is_uint($vcf[5]),   # QUAL
		 'STRAND' => '+'		 # always
	     });

	$prev_chr = $vcf[0];
	$prev_pos = $vcf[1];
    }
    close($list);
    
    return \@data;
}

sub readIntervals {
    my $file = is_file(shift,'f');

    my $prev_chr = '';
    my $prev_pos = -1;

    my @data;
    my $list = open($file,READ);
    while (local $_ = $list->getline()) {
        m/^#|^\s*$/ && next;
	chomp();
	
	if (/^([^:\-]+):(\d+)(?:\-(\d+))?/) { # CHROM:START[-END]
	    if ($1 eq $prev_chr && $2 < $prev_pos) {
		throw("Input intervals must be reference-order sorted: $file.");
	    }
	    push(@data,
		 {
		     'CHROM'  => $1,
		     'START'  => $2-1,
		     'END'    =>($3||$2),
		     'NAME'   => $_,
		     'SCORE'  => 0,
		     'STRAND' => '+'
		});

	    $prev_chr = $1;
	    $prev_pos = $2;

	} else {
	    throw("Invalid intervals format: [$_]");
	}
    }
    close($list);
    
    return \@data;
}   


sub readRNG {
    my $file = is_file(shift,'f');
    
    if ($file =~ /vcf$/) {
	report('Reading VCF file');
	return readVCF($file);

    } elsif ($file =~ /bed$/) {
	report('Reading BED file');
	return readBED($file);

    } elsif ($file =~ /intervals$/) { 
	report('Reading Intervals file');
	return readIntervals($file);

    } else {
	throw("Unsupported interval format: $file");
    }
}


sub intersectsInterval {
    my $featr  = is_arrayref(shift);
    my $index  = is_hashref(shift);
    my $contig = is_string(shift);
    my $coord  = is_uint(shift);
    my $cache  = shift;
    my $pntr   = is_uint(shift || 0);

    # chrom sweep alg requiring half-open intervals

    my $contig1 = is_uint($index->{$contig});
    if (!defined($contig1)) { 
	throw AssertionError("Missing Meta ##contig entry in VCF file: $contig.");
    }
    while (@$featr && $pntr < @$featr) {
	my $ftrange = is_hashref($featr->[$pntr]);
	my $contig2 = is_uint($index->{is_string($ftrange->{'CHROM'})});

	is_uint($ftrange->{'START'}); is_uint($ftrange->{'END'});
	if (!defined($contig2)) {
	    throw AssertionError("Missing Meta ##contig entry in BED file: $ftrange->{'CHROM'}.");
	}
	if ($contig1 == $contig2) {
	    if ($coord <= is_uint($ftrange->{'START'})) {
		return 0;
	    } elsif ($ftrange->{'START'} < $coord && $coord <= $ftrange->{'END'}) {
		if ($cache) {
		    push(@$cache,$ftrange);
		    intersectsInterval($featr,$index,$contig,$coord,$cache,$pntr+1);
		}
		return 1;
	    } elsif ($pntr) {
		return 1
	    }
	} elsif ($contig2 > $contig1) {
	    return 0;
	}
	$pntr || shift(@$featr);
    }

    return 0;
}


sub bitcount {   
    my $array = is_uint(shift); 
    my $count = 0; 
    while ($array) { 
	$array &= ($array - 1);
	$count++;  
    } 
    return $count; 
}


sub asdCore {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $fdi = is_subclass(shift,'IO::Handle');
    my $fai = is_hashref(shift);
    my $inc = is_arrayref(shift);
    my $exc = is_arrayref(shift);
    my $opt = is_uint(shift);

    my ($META,$HEADER) = getMeta($fdi);
    
    my $REQD  = ['FT'];
    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - (FORMAT+1));
    
    my %sample;
    my @sample;
    if (! %$fai) { $fai = $META->{'contig'} || {} }    
    if (! @$inc) { @$inc = @{$HEADER}[(FORMAT+1)..($width-1)] }
    for my $id (@$inc) {
	if (! $FIELD{$id}) {
	    throw("Sample not defined in VCF header: $id");
	} elsif ($FIELD{$id} <= FORMAT) {
	    throw("Invalid sample name: $id");
	} else {
	    $sample{$id} = $FIELD{$id};
	}
    }
    for my $id (@$exc) {
	if (! $FIELD{$id}) {
	    throw("Sample not defined in VCF header: $id");
	} elsif ($FIELD{$id} <= FORMAT) {
	    throw("Invalid sample name: $id");
	} elsif (defined($sample{$id})) {
	    delete($sample{$id});
	}
    }
    @sample = sort keys(%sample);
    
    
    throw(sprintf('At least two samples required by %s()',
		  __FUNCTION__)) if (@sample < 2);
    
    
    
    # Fills lower triangle of matrix:
    my $n = 0;
    my @INIT;
    for(my $j = 1; $j < @sample; ++$j) {
	for(my $i = 0; $i < $j; ++$i) {
	    $n = $j*($j-1)/2+$i;
	    $INIT[$n] = 0;
	}
    }
    my @A = @INIT;
    my @N = @INIT;
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) { # core loop
	progressCounter();
	
	if ($vcf->[ALT] =~ /\,/) { next } # multi-allelic
	if ($vcf->[ALT] eq NULL_FIELD) { next }
	if ($vcf->[FILTER] !~ PASS_FILTER_PAT) { next }

	my @gt;
	my $FORMAT = getFORMAT($vcf->[FORMAT],$REQD);
	for(my $j  = 0; $j < @sample; ++$j) {
	    my @s  = getSample($vcf->[$FIELD{$sample[$j]}],$FORMAT);
	    if ($s[$FORMAT->{'FT'}] =~ PASS_FILTER_PAT &&
		$s[$FORMAT->{'GT'}] =~ GT_GROUP_PAT) { 
		$gt[$j] = (as_sbitA($1) | as_sbitA($2));
		
	    } else {
		$gt[$j] = 0;
	    }
	    if (!$gt[$j]) {
		next;
	    }

	    for(my $i = 0; $i < $j; ++$i) {
		my $k = $j*($j-1)/2+$i;
		my $s = ($gt[$i] & $gt[$j]);
		my $a = ($gt[$i] | $gt[$j]);
		# my $u = ($gt[$i] | $gt[$j]) & ~$s;

		if (!$gt[$i]) {
		    next;
		} elsif ($opt & 0x4) {
		    if ($gt[$i] != $gt[$j]) {
			$A[$k] += 1.0
		    }
		    $N[$k]++;
		} elsif ($opt & 0x1) {
		    if ($s == 0) {
			$A[$k] += 1.0
		    }
		    $N[$k]++;
		} else {
		    if ($s == 0) {
			# no alleles shared
			$A[$k] += 1.0;
			$N[$k]++;

		    } elsif ($s == $a) {
			# all alleles shared
			$A[$k] += 0.0;
			$N[$k]++;
			
		    } elsif ($s) {
			$A[$k] += 0.5;
			$N[$k]++;
		    }
		}
	    }
	}
    }
    progressReport();

    # TODO: calc significance from expected values:
    #  https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2869072/
    
    if ($opt & 0x2) {  # write as dense matrix
	my $sep = '';
	print($fdo join("\t", @sample),"\n");
	for(my $j = 0; $j < @sample; ++$j) {
	    $sep = '';
	    for(my $i = 0; $i < @sample; ++$i) {
		if ($j == $i) {
		    printf($fdo "%s%.05f", $sep, 0);
		} else {
		    my $k = $j*($j-1)/2+$i;
		    if ($j < $i) {
			$k = $i*($i-1)/2+$j;
		    }
		    printf($fdo "%s%.05f", $sep, ($A[$k]||0)/($N[$k]||1));
		}
		$sep = "\t";
	    }
	    printf($fdo "\n")
	}
	
    } else {
	# Output lower triangle of matrix as pairwise list:
	print($fdo join("\t", qw(INDV1 INDV2 SEGSITES ASD)),"\n");
	for(my $j  = 0; $j < @sample; ++$j) {
	    for(my $i = 0; $i < $j; ++$i) {
		my $k = $j*($j-1)/2+$i;
		printf($fdo "%s\t%s\t%u\t%.05f\n", $sample[$j], $sample[$i], $N[$k], ($A[$k]||0)/($N[$k]||1));
	    }
	}
    }
    return 1;
}
	

sub ASD_USAGE {
    die qq/
Usage:   $PROGRAM asd [options] <multi_sample.vcf>

Options: -o FILE   Write output to FILE [stdout]
         -b        Count presence\/absence of shared alleles
         -d        Write output as square matrix [pairwise]
         -g        Count presence\/absence of share genotypes
         -i FILE   Include samples listed in FILE only [null]
         -x FILE   Exclude samples listed in FILE [null]
         -R FILE   faidx indexed reference sequence [null]
         -h        This help message

Notes:   Requires at least two genotype\/sample fields.


/;
}


sub asd {
    my $option = 0;
    my $outfile = STDIO;
    my ($exclude,$include,$reffile,%argv);
    my $command = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('bdghXx:i:o:R:',\%argv) && !$argv{'h'} && @ARGV == 1 || &ASD_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq 'b') { $option |= 0x1 }
	elsif ($o eq 'd') { $option |= 0x2 }
	elsif ($o eq 'g') { $option |= 0x4 }
	elsif ($o eq 'o') { $outfile = is_string($argv{$o},\&ASD_USAGE) }
	elsif ($o eq 'R') { $reffile = is_string($argv{$o},\&ASD_USAGE) }
	elsif ($o eq 'i') { $include = is_string($argv{$o},\&ASD_USAGE) }
	elsif ($o eq 'x') { $exclude = is_string($argv{$o},\&ASD_USAGE) }
    }
    
    report('Starting ',scalar(localtime));
    report('Command-line: ',$command);

    my $infile = shift(@ARGV);
    my $prefix = shift(@ARGV);

    my $fdi  = open($infile,READ);
    my $fdo  = open($outfile,WRITE);
    my $fai  = defined($reffile) ? readFaidx($reffile) : {};
    $include = defined($include) ? readOrderedList(split(':',$include)) : [];
    $exclude = defined($exclude) ? readOrderedList(split(':',$exclude)) : [];
    
    asdCore($fdo,$fdi,$fai,$include,$exclude,$option);
    
    close($fdo);
    close($fdi);
    
    report('Finished ',scalar(localtime));
    
    return 1;
}


sub DstCore {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $fds = is_subclass(shift,'IO::Handle');
    my $fdi = is_subclass(shift,'IO::Handle');
    my $fai = is_hashref(shift);
    my $inc = is_arrayref(shift);
    my $exc = is_arrayref(shift);
    my $Wsz = is_uint(shift) || inf;
    my $opt = is_uint(shift);

    my ($META,$HEADER) = getMeta($fdi);
    
    my $REQD  = ['FT'];
    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - (FORMAT+1));
    
    my %sample;
    my @sample;
    if (! %$fai) { $fai = $META->{'contig'} || {} }    
    if (! @$inc) { @$inc = @{$HEADER}[(FORMAT+1)..($width-1)] }
    for my $id (@$inc) {
	if (! $FIELD{$id}) {
	    throw("Sample not defined in VCF header: $id");
	} elsif ($FIELD{$id} <= FORMAT) {
	    throw("Invalid sample name: $id");
	} else {
	    $sample{$id} = $FIELD{$id};
	}
    }
    for my $id (@$exc) {
	if (! $FIELD{$id}) {
	    throw("Sample not defined in VCF header: $id");
	} elsif ($FIELD{$id} <= FORMAT) {
	    throw("Invalid sample name: $id");
	} elsif (defined($sample{$id})) {
	    delete($sample{$id});
	}
    }
    @sample = sort keys(%sample);
    
    
    throw(sprintf('At least two samples required by %s()',
		  __FUNCTION__)) if (@sample < 2);
    

    
    my $n = 0;
    my @INIT;
    my $prevchr;
    my $prevbin;
    for(my $j = 1; $j < @sample; ++$j) {
	for(my $i = 0; $i < $j; ++$i) {
	    $n = $j*($j-1)/2+$i;
	    printf($fdo "## %s\t%s\t%u\n",$sample[$j],$sample[$i],$n);
	    $INIT[$n] = 0;
	}
    }
    print($fdo join("\t",'#CHROM','START','END',(0..$n)),"\n");

    
    my @HH = @INIT; my @hh = @INIT;
    my @FD = @INIT; my @fd = @INIT;
    my @SS = @INIT; my @ss = @INIT;
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) { # core loop
	progressCounter();
	
	if ($vcf->[ALT] =~ /\,/) { next } # multi-allelic
	if ($vcf->[ALT] eq NULL_FIELD) { next }
	if ($vcf->[FILTER] !~ PASS_FILTER_PAT) { next }

       	my $currbin = int($vcf->[POS] / $Wsz);

	$prevbin //= $currbin;
	$prevchr //= $vcf->[CHROM];
	if ((0x1 & ~$opt) && (($vcf->[CHROM] ne $prevchr) || ($currbin != $prevbin))) {
	    print($fdo join("\t",$prevchr,$prevbin*$Wsz,min(($prevbin+1)*$Wsz,$fai->{$prevchr}->{'length'})));
	    map  { printf($fdo "\t%.05f", 0.5 + ($fd[$_] - 0.5*$hh[$_]) / (($ss[$_]+$fd[$_])||1)) } 0..$n;
	    print($fdo "\n");
	    
	    @hh = @INIT;
	    @fd = @INIT;
	    @ss = @INIT;
	}
	

	my @gt;
	my $FORMAT = getFORMAT($vcf->[FORMAT],$REQD);
	for(my $j  = 0; $j < @sample; ++$j) {
	    my @s  = getSample($vcf->[$FIELD{$sample[$j]}],$FORMAT);
	    if ($s[$FORMAT->{'FT'}] =~ PASS_FILTER_PAT &&
		$s[$FORMAT->{'GT'}] =~ GT_GROUP_PAT) { 
		$gt[$j] = (as_sbitA($1) | as_sbitA($2));
		
	    } else {
		$gt[$j] = 0;
	    }
	    if (!$gt[$j]) { next }

	    for(my $i = 0; $i < $j; ++$i) {
		my $k = $j*($j-1)/2+$i;
		my $s = ($gt[$i] & $gt[$j]);
		my $u = ($gt[$i] | $gt[$j]) & ~$s;

		if    (!$gt[$i]) {         1 }                                  # missing data
		elsif ( $s == 0) { $FD[$k]++; $fd[$k]++ }	                # fixed-diff
		elsif ( $s == 3) { $SS[$k]++; $ss[$k]++; $HH[$k]++; $hh[$k]++ } # het-het
		elsif ( $u  & 3) { $SS[$k]++; $ss[$k]++ }                       # other seg type
	    }
	}
	$prevchr = $vcf->[CHROM];
	$prevbin = $currbin;
    }
    if (sum(@ss,@fd)) {
	print($fdo join("\t",$prevchr,$prevbin*$Wsz,min(($prevbin+1)*$Wsz,$fai->{$prevchr}->{'length'})));
	map  { printf($fdo "\t%.05f", 0.5 + ($fd[$_] - 0.5*$hh[$_]) / (($ss[$_]+$fd[$_])||1)) } 0..$n;
	print($fdo "\n");
    }
    progressReport();

    print($fds join("\t", qw(INDV1 INDV2 AAaa AaAa SEGSITE DST)),"\n");
    for(my $j  = 0; $j < @sample; ++$j) {
	for(my $i = 0; $i < $j; ++$i) {
	    my $k = $j*($j-1)/2+$i;
	    printf($fds "%s\t%s\t%u\t%u\t%u\t%.05f\n", $sample[$j],$sample[$i],$FD[$k],$HH[$k],$SS[$k], 0.5 + ($FD[$k] - 0.5*$HH[$k]) / (($SS[$k]+$FD[$k])||1));
	}
    }

    
    return 1;
}
	

sub DST_USAGE {
    die qq/
Usage:   $PROGRAM Dst [options] <multi_sample.vcf> <out.prefix>

Options: -i FILE   Include samples listed in FILE only [null]
         -x FILE   Exclude samples listed in FILE [null]
         -R FILE   faidx indexed reference sequence [null]      
         -S        Write Dst summary file only
         -w INT    Set bin to INT bp wide (0 for no binning) [1000]
         -h        This help message

Notes:   Requires at least two genotype\/sample fields.


/;
}


sub Dst {
    my $option  = 0;
    my $winsize = 1000;
    my ($exclude,$include,$reffile,%argv);
    my $command = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('Xhx:i:w:R:S',\%argv) && !$argv{'h'} && @ARGV == 2 || &DST_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq 'S') { $option |= 0x1 }
	elsif ($o eq 'R') { $reffile = is_string($argv{$o},\&DST_USAGE) }
	elsif ($o eq 'i') { $include = is_string($argv{$o},\&DST_USAGE) }
	elsif ($o eq 'x') { $exclude = is_string($argv{$o},\&DST_USAGE) }
	elsif ($o eq 'w') { $winsize = is_uint(expand($argv{$o}),\&DST_USAGE) }
    }
    if ($winsize == 0) {
	$winsize = 2**32-1;
    }

    report('Starting ',scalar(localtime));
    report('Command-line: ',$command);

    my $infile = shift(@ARGV);
    my $prefix = shift(@ARGV);

    my $fdi  = open($infile,READ);
    my $fdo  = open("$prefix.Dst",WRITE);
    my $fds  = open("$prefix.Dst.summary",WRITE);
    my $fai  = defined($reffile) ? readFaidx($reffile) : {};
    $include = defined($include) ? readOrderedList(split(':',$include)) : [];
    $exclude = defined($exclude) ? readOrderedList(split(':',$exclude)) : [];
    
    DstCore($fdo,$fds,$fdi,$fai,$include,$exclude,$winsize,$option);
    
    close($fdo);
    close($fdi);
    
    report('Finished ',scalar(localtime));
    
    return 1;
}


sub raxbinCore {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $fdi = is_subclass(shift,'IO::Handle');
    my $ql  = is_ufloat(shift);
    my $gq  = is_uint(shift);
    my $dp  = is_uint(shift);

    my ($META,$HEADER) = getMeta($fdi);

    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - ($FIELD{'INFO'}+2));

    throw(sprintf('At least two samples required by %s()',
		  __FUNCTION__)) if ($nGeno < 2);

    my $c = 0;
    my $R = ['FT','GQ','DP'];
    my @B = map {[]} ($FIELD{'FORMAT'}+1)..$#{$HEADER};

    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) {
	progressCounter();

	$vcf->[QUAL] >= $ql || next;
	
	my %FORMAT = %{scalar( getFORMAT($vcf->[FORMAT],$R) )};
	for(my $s = 0; $s < @{$vcf} - (FORMAT+1); ++$s) {
	    my @g = getSample($vcf->[$s+FORMAT+1],\%FORMAT);
	    if ($g[$FORMAT{'GT'}] =~ /\./) {
		$B[$s]->[$c] = '?';

	    } elsif ($g[$FORMAT{'FT'}] !~ PASS_FILTER_PAT) {
		$B[$s]->[$c] = '?';

	    } elsif (($g[$FORMAT{'GQ'}] ne NULL_FIELD && $g[$FORMAT{'GQ'}] < $gq) ||
		     ($g[$FORMAT{'GQ'}] ne NULL_FIELD && $g[$FORMAT{'DP'}] < $dp)) { 
		# uncertain
		$B[$s]->[$c] = '?';
	    } elsif ($g[$FORMAT{'GT'}] =~ /^(\d+)[|\/](\d+)?/) { 
		$B[$s]->[$c] = ($1||$2) ? 1 : 0;
	    } else {
		# dont know what it is
		$B[$s]->[$c] = '?';
	    }
	}
	$c++;
    }
    progressReport();

    print($fdo join(' ',$nGeno,$c),"\n");
    for(my $i = 0; $i < $nGeno; ++$i) {
	print($fdo $HEADER->[$i+FORMAT+1],"\t",join('',@{$B[$i]}),"\n");
    }
    return 1;
}


sub RAXBIN_USAGE {
    die qq/
Usage:   $PROGRAM raxbin [options] <multi_sample.vcf>

Options: -o FILE   Write output to FILE [stdout]
         -d INT    Min. indiv. DP threshold [4] 
         -g INT    Min. indiv. GQ threshold [0]
         -q FLOAT  Min. QUAL [0] 
         -h        This help message

Notes:   Requires at least one genotype\/sample field.


/;
}


sub raxbin {
    my %argv;
    my ($ql,$gq,$dp,$outfile) = (0,0,4,STDIO);
    my $commandline  = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('Xhg:q:o:d:',\%argv) && !$argv{'h'} && @ARGV == 1 || &RAXBIN_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq 'g') { $gq = is_uint($argv{$o},\&RAXBIN_USAGE) }
	elsif ($o eq 'd') { $dp = is_uint($argv{$o},\&RAXBIN_USAGE) }
	elsif ($o eq 'q') { $ql = is_ufloat($argv{$o},\&RAXBIN_USAGE) }
	elsif ($o eq 'o') { $outfile = is_string($argv{$o},\&RAXBIN_USAGE) }
    }

    report('Starting ',scalar(localtime));
    report('Command-line: ',$commandline);

    my $fdi = open(shift(@ARGV),READ);
    my $fdo = open($outfile,WRITE);

    raxbinCore($fdo,$fdi,$ql,$gq,$dp);

    $fdo->close();
    $fdi->close();

    report('Finished ',scalar(localtime));

    return 1;
}


sub log10_multinomial {
    if (@_ < 2) {
	throw('Two or more arguments required at ',__FUNCTION__,'().');
    }
    my $N = is_uint(shift);
    my @n = @_;
    
    if ($N < 0) {
	throw('First argument must be a positive int');
    }

    $N ||= 1;
    my $logM = sum(map {log(10,$_)} 1..$N);

    for(my $i = 0; $i < @n; ++$i) {
        is_uint($n[$i]);
        $n[$i] ||=1; # 0! == 1!
        # log(10,x!) = sum(log(10,x_i)); [1..x]
        $logM -= sum(map {log(10,$_)} 1..$n[$i]);
    }

    return $logM;
}


sub hw_fisher_exact {
    my $f = is_hashref(shift);
    my $g = is_arrayref(shift);
    my $x = is_hashref(shift||{});

    my (@genotypes,@alleles);
    my ($N,$nHets,$nAlleles) = (0,0,0);    
    for(my $i = 0; $i < @$g; ++$i) {
	my @g = split(':',$g->[$i]);
	if ($x->{$i}) { next }
	if (exists($f->{'GT'}) && $g[$f->{'GT'}] =~ /^(\d+)[\/|](\d+)/) {
	    $genotypes[($2*($2+1)/2)+$1]++;
	    $alleles[$1]++;
	    $alleles[$2]++;
	    $nHets++ if $2 != $1;
	    $N += 2;
	}
    }
    $nAlleles = scalar(@alleles);
    $nAlleles > 0 || return 0;
    for(my $i = 0; $i < ($nAlleles * ($nAlleles + 1) / 2); ++$i) {
	$genotypes[$i] ||= 0;
    }

    my $logP = log10_multinomial($N,@genotypes) 
	     - log10_multinomial(2*$N,@alleles) 
	     + $nHets * log(10,2);

    return 10*$logP; 
}


sub hwexactCore{
    my $fdo  = is_subclass(shift,'IO::Handle');
    my $fdi  = is_subclass(shift,'IO::Handle');
    my $excl = is_hashref(shift || {});
    my $cmd  = is_string(shift || '');

    warn(DeprecationWarning('Please use vcftools or GATK to perform this operation.'));

    my ($META,$HEADER) = getMeta($fdi);

    my (%G,%X);
    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - ($FIELD{'INFO'}+2));

    throw(sprintf('At least ten sample required by %s()',
		  __FUNCTION__)) if ($nGeno < 10);

    addMeta($META,'VCFtk',
        {
	    '_ORDER_' => [qw(ID Version Date CommandlineOptions)],
	    'ID'      => 'hwexact',
	    'Version' => $VERSION,
	    'Date'    => '"'.scalar(localtime).'"',
	    'CommandlineOptions' => '"'.$cmd.'"'
	});


    print($fdo setMeta($META,$HEADER),"\n");

    for(my $i = 0; $i < $nGeno; ++$i) {
	if ($excl->{$HEADER->[$i]}) { $X{$i}++ }
    }
    # start core loop
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) { 
	progressCounter();

	my %FORMAT = %{scalar( getFORMAT($vcf->[FORMAT]) )};
	my $phredP = hw_fisher_exact(\%FORMAT,[ @{$vcf}[(FORMAT+1)..$#{$vcf}] ],\%X);
	
	print($fdo "$phredP\n");
    }
    progressReport();
    
    return 1;
}


sub HWEXACT_USAGE {
    die qq/
Usage:   $PROGRAM hwexact [options] <in.vcf>

Options: -o FILE   Write output to FILE [stdout]
         -x FILE   Exclude samples listed in FILE [null]
         -h        This help message

Notes:   Requires at least one genotype\/sample field.


/;
}


sub hwexact {
    my (%argv,$excfile,$exclude);
    my $outfile = STDIO;
    my $commandline = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('Xho:',\%argv) && !$argv{'h'} && @ARGV == 1 || HWEXACT_USAGE;
    for my $o (keys %argv) {
        if    ($o eq 'X') { $Valkyr::Error::DEBUG   = 1 }
        elsif ($o eq 'o') { $outfile = is_string($argv{$o},\&HWEXACT_USAGE) }
	elsif ($o eq 'x') { $excfile = is_string($argv{$o},\&HWEXACT_USAGE) }
    }


    report('Starting ',scalar(localtime));
    report('Command-line: ',$commandline);

    my $fdi = open(shift(@ARGV),READ);
    my $fdo = open($outfile,WRITE);
    my $exc = defined($excfile) ? readList($excfile) : {};
    
    hwexactCore($fdo,$fdi,$exc);
    
    $fdo->close();
    $fdi->close();

    report('Finished ',scalar(localtime));

    return 1;
}


sub esc {
    my $val = is_string(shift);
    $val =~ s/\./_/g;
    return $val;
}


sub pcthex {
    return uc(sprintf('%%%x',ord(shift)));
}


sub gvfCore {
    my $fdo     = is_subclass(shift,'IO::Handle');
    my $fdi     = is_subclass(shift,'IO::Handle');
    my $passsrc = is_string(shift);
    my $failsrc = is_string(shift);
    my $iter    = is_uint(shift);
    my $opts    = is_uint(shift);
    my $pragma  = is_hashref(shift);

    my %PRAGMA = (
        'r' => 'reference-fasta',
        'v' => 'file-version',
        'd' => 'file-date',
        'p' => 'population',
        's' => 'sex',
        'c' => 'technology-platform-class',
        'n' => 'technology-platform-name',
        'V' => 'technology-platform-version',
        'M' => 'technology-platform-machine-id',
        'l' => 'technology-platform-read-length',
        'T' => 'technology-platform-read-type',
        'I' => 'technology-platform-read-pair-span',
        'a' => 'technology-platform-average-coverage',
        'E' => 'sequencing-scope',
        'm' => 'capture-method',
        'R' => 'capture-regions',
        'A' => 'sequence-alignment',
        'C' => 'variant-calling',
        'D' => 'sample-description',
        'G' => 'genomic-source',
        'F' => 'pragma'
        );

    # qr/([;=&,])/;
    my $ATTRPAT = qr/([\000-\037\046\054\073\075\177])/; 
    # qr/([^a-zA-Z0-9.:^*$@!+_?\-|])/;
    my $NAMEPAT = qr/([\000-\040\042\043\045-\051\054\057\073-\076\133-\135\140\173-\177])/; 
    my @STDATTR = qw(ID Name Alias Reference_seq Variant_seq Start_range 
                     End_range Genotype Zygosity Individual Variant_reads 
                     Total_reads Total_freq Variant_freq validated);

    my $prevchr;
    my $region  = {};
    my $warned  =  0;
    my (%G,%P1,%P2,%T);
    my ($META,$HEADER) = getMeta($fdi);

    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - ($FIELD{'INFO'}+2));
    my $REQD  = ['AD','FT'];

    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    if (exists($META->{'contig'})) {
	$region = $META->{'contig'};
    }
    # print pragma
    print($fdo "##gff-version 3\n");
    print($fdo "##gvf-version 1.06\n");
    for my $p (qw(r v d p s c n V M l T I a E m R A C D G F)) {
        exists($pragma->{$p}) && exists($PRAGMA{$p}) || next;
        printf($fdo "##%s %s\n",$PRAGMA{$p},$pragma->{$p});
    }
    if ($nGeno == 1) {
        printf($fdo "##individual-id %s\n",$HEADER->[ $#{$HEADER} ]);
    } elsif ($nGeno > 1) {
        printf($fdo "##multi-individual %s\n",
	       join(',',@{$HEADER}[(FORMAT+1)..$#{$HEADER}]));
    }
    # start core loop
    while (my $vcf = getEntry($fdi,$width)) { 
	progressCounter();

        my ($f,$i,$AC) = (0,0,0);
        my (@GVF,@indiv,%freq,@GT,@ZY,@vDP,@tDP);

        # filters
        if ($vcf->[ALT] eq NULL_FIELD) { next }
        if ($vcf->[ALT] =~ BREAKEND_PAT) { 
            warn(sprintf(BREAKEND_MSG,@{$vcf}[CHROM,POS]));
	    next;  
        }
	
        # fill in what info we currently have into a GVF line
        my $L = length( $vcf->[REF] );
        @GVF[ SEQID, RSTART, SCORE..ATTR ]
            = ( @{$vcf}[CHROM,POS,QUAL], '+', '.', {});
	@GVF[ SOURCE, REND ] 
	    = ($passsrc, $vcf->[POS] + $L - 1);
        
	if ($vcf->[FILTER] !~ PASS_FILTER_PAT) {
	    ($opts & 0x8) || next;
	    $GVF[ATTR]->{'filters'} = join(',',split(';',$vcf->[FILTER]));
	    $GVF[SOURCE] = $failsrc;
	}

        my $indel = 0;
        my @alleles = ( $vcf->[REF], split(',',$vcf->[ALT]) );
        for(my $i = 1; $i < @alleles; ++$i) {
            my $l = length( $alleles[$i] );
            if ($l < $L) { $indel |= 0x1 }
            if ($l > $L) { $indel |= 0x2 }
        }
        # get into the nitty-gritty of how Variant_seq and Reference_seq tag
        # data is formatted to represent all genotypes (see spec).  The 
        # Reference_seq attribute is optional, so omit it in cases where
        # there may be multiple possible values for the Reference_seq 
        # attribute ('insertion', 'deletion' and 'indel')
        if ($indel & 3) {
            if ($indel == 3) {
                $GVF[TYPE] = 'indel';
            } elsif ($indel & 0x1) {
                $GVF[TYPE] = 'deletion';
            } elsif ($indel & 0x2) {
                $GVF[TYPE] = 'insertion';
            } else { 
                throw(sprintf('Assertion failure: TYPE != INDEL (%s:%u).',
			      @{$vcf}[CHROM,POS])); 
            }
        } else {
            $GVF[TYPE] = ($L == 1) ? 'SNV' : 'sequence_alteration';
        }
        # if a sequence_alteration is too long to be represented in Gbrowser,
        # abbreviate with '~[NUM]' notation
        my %alleles;
        my $index = 0;
        for(my $i = 0; $i < @alleles; ++$i) {
            my $l = length( $alleles[$i] );
            if ($l > 100) { $alleles[$i] = "~$l" }
            $alleles{$alleles[$i]} //= $index++;
        }
	
	# Are there any filtered individuals or missing data? If so, add types
        # to accounting of genotypes 
        my ($missing_data,$lowQual_data,$hemizyg_data) = (0,0,0);
	if ($nGeno > 0) {
	    my %FORMAT = %{scalar( getFORMAT($vcf->[FORMAT],$REQD) )};
	    
	    for(my $i = FORMAT+1; $i < @{$vcf}; ++$i) {
		my @g = getSample($vcf->[$i],\%FORMAT);

		if ($g[$FORMAT{'FT'}] !~ PASS_FILTER_PAT) { $lowQual_data = 1 }

		my @a = split(GT_FIELD_SEP, @g[$FORMAT{'GT'}] );
		my $ploidy = scalar(@a);
		if    ($ploidy  <  2 ) { $hemizyg_data = 1 }
		elsif (grep {/\./} @a) { $missing_data = 1 }
		elsif ($ploidy  >  2 ) {
		    $warned || warn("A ploidy of $ploidy is not ",
				    "supported and may result in errors.");
		    $warned = 1;
		}
	    }
	    if ($lowQual_data) { push( @alleles,'^' ); $alleles{'^'} = $index++ }
	    if ($missing_data) { push( @alleles,'.' ); $alleles{'.'} = $index++ }
	    if ($hemizyg_data) { push( @alleles,'!' ); $alleles{'!'} = $index++ }
	    
	    # Loop over genotype fields and stuff data into attribute fields.
	    # A line like this $alleles{$alleles[$a[0]]} converts from old
	    # VCF genotype numbering system to new GVF numbering system,
	    # which should be identical in most cases
	    %freq = map {$_ => 0} 0..$#alleles;
	    for(my $i = FORMAT+1; $i < @{$vcf}; ++$i) { # s = sample
		my @g = getSample($vcf->[$i],\%FORMAT);
		my @a = split(GT_FIELD_SEP,$g[$FORMAT{'GT'}]);
		
		my ($ploidy,$has_nonref) = (scalar(@a),0);
		$AC += $ploidy;
		if (grep {/\./} @a) { # missing data
		    # possible more generalized form:
		    push( @GT,join(':',map{$freq{$alleles{$_}}++;$alleles{$_}} @a) );
		    push( @ZY,'.' );
		} elsif ($g[$FORMAT{'FT'}] !~ PASS_FILTER_PAT) { # lowQual
		    push( @GT,$alleles{'^'} );
		    push( @ZY,'.' );
		    $freq{ $alleles{'^'} } += $ploidy;
		    if ($has_nonref || ($opts & 0x2)) { 
			push( @vDP,'.' ); push( @tDP,'.' );
		    }
		    next;
		} elsif ($ploidy < 2) { # hemizygous
		    push( @GT,$a[0].':!' );
		    push( @ZY,'hemizygous' );
		    $freq{ $a[0] }++;
		    $has_nonref = $a[0];
		} else {
		    my @A;
		    my $zyg = 'homozygous';
		    for(my $a = 0; $a < @a; ++$a) {
			if ($a[$a] != $a[0]) { $zyg = 'heterozygous' }
			if ($a[$a] != 0) { $has_nonref = 1 }
			$freq{$a[$a]}++;
		    }
		    push( @GT,join(':',@a) );
		    push( @ZY,$zyg );
		    # check for non-ref, as a single occurance genotype may 
		    # have been filtered out
		}
		if ($has_nonref) { push( @indiv,$i-(FORMAT+1) ) }
		if ($has_nonref || ($opts & 0x2)) { 
		    if (defined( $g[$FORMAT{'AD'}] )) { # GATK v1.4+
			my @dp = split(',', $g[$FORMAT{'AD'}]);
			push( @vDP,join(':',@dp) );
			push( @tDP,sum(map {s/^\.$/0/;$_} @dp) );
		    } else {
			push( @vDP,'.' );
			push( @tDP,'.' );
		    }
		}                
	    }
	}
        # fill required ATTRs
        $GVF[ATTR]->{'ID'} = sprintf('%09s',$iter++);
        $GVF[ATTR]->{'Reference_seq'} = $vcf->[REF];
        $GVF[ATTR]->{'Variant_seq'}   = join(',',@alleles);
        # fill optional ATTRs
        if (@GT) {
            $GVF[ATTR]->{'Genotype'}      = join(',',@GT );
            $GVF[ATTR]->{'Zygosity'}      = join(',',@ZY ) if $opts & 0x4;
            $GVF[ATTR]->{'Individual'}    = join(',',@indiv) if @indiv;
            $GVF[ATTR]->{'Variant_reads'} = join(',',@vDP) if @vDP;
            $GVF[ATTR]->{'Total_reads'}   = join(',',@tDP) if @tDP;
            $GVF[ATTR]->{'Variant_freq'}  = join(',',map {sprintf('%.2f',$freq{$_}/($AC||1))} 0..$#alleles);
        }
	if ($opts & 0x1) {
	    my $INFO = getINFO($vcf->[INFO]);
            for my $tag (keys %$INFO) {
                my $attr = lc $tag;
                $GVF[ATTR]->{$attr} = $INFO->{$tag} // 1; # not all attrs are key=value
		$GVF[ATTR]->{$attr} =~ s/$ATTRPAT/pcthex($1)/ge;
            }
        }
        if ($vcf->[ID] ne NULL_FIELD()) {
            my @names = map {s/$ATTRPAT/pcthex($1)/ge;$_} split(',',$vcf->[ID]);
            $GVF[ATTR]->{'Name'}  = $names[0];
            $GVF[ATTR]->{'Alias'} = join(',Alias=',@names[1..$#names])
		if @names > 1;
	}
	my @attr;
	for my $attr (@STDATTR, keys(%{$GVF[ATTR]})) {
	    exists($GVF[ATTR]->{$attr}) || next;
	    push( @attr,$attr.'='.$GVF[ATTR]->{$attr} );
	    delete($GVF[ATTR]->{$attr});
        }
        $GVF[ATTR] = join(';',@attr);
        @GVF[SEQID,SOURCE] = map {s/$NAMEPAT/pcthex($1)/ge;$_} @GVF[SEQID,SOURCE];
        
	if (defined($prevchr) && 
	    $prevchr ne $vcf->[CHROM] || eof($fdi)) {
	    printf($fdo "###\n");
	}
	if ((!defined($prevchr) || $prevchr ne $vcf->[CHROM]) &&
	    (defined($region->{$vcf->[CHROM]}) && 
	     defined($region->{$vcf->[CHROM]}->{'length'}))) {
	    printf($fdo "##sequence-region %s 1 %u\n",
		   $vcf->[CHROM],$region->{$vcf->[CHROM]}->{'length'})
	}
        print($fdo join("\t",@GVF),"\n");
	$prevchr = $vcf->[CHROM];
    }
    progressReport();

    return 1;
}


sub GVF_USAGE {
    die qq/
Usage:   $PROGRAM gvf [options] <-|in.vcf>

Options: -o FILE   Write output to FILE [stdout]
         -i INT    Initialize the ID attr iterator with INT [0]
         -S STR    Set PASS position 'source' field to STR [VCF]
         -f STR    Set filtered position 'source' field to STR [filtered]
         -O        Output filtered sites
         -e        Output VCF INFO annotations in GVF attr field
         -j        Output info for homozygous-reference individuals
         -z        Output Zygosity attr
         -h        This help message

Pragmas: -A STR    sequence-alignment description (free text)
         -C STR    variant-calling description (free text)
         -D STR    sample-description (free text)
         -E STR    sequencing-scope (whole_genome|whole_exome|targeted_capture)
         -F STR    generic pragma (see spec)
         -G STR    genomic-source (prenatal|somatic|germline)
         -I INT    technology-platform-read-pair-span
         -M STR    technology-platform-machine-id
         -R STR    Set capture-regions BED-file path to STR
         -T STR    technology-platform-read-type (fragment|pair)
         -V STR    technology-platform-version
         -a INT    technology-platform-average-coverage
         -c STR    technology-platform-class (see spec)
         -d STR    file-date (YYYY-MM-DD)
         -l STR    technology-platform-read-length
         -m STR    capture-method (see spec)
         -n STR    technology-platform-name (see spec)
         -p STR    population pragma ID
         -r STR    Set reference-fasta pragma path to STR
         -s STR    Set the sex pragma (male|female)
         -v FLOAT  file-version

Notes: 

  1. This script expects diploid individuals and may break if individuals
     are of any other ploidy.
 
  2. GVF spec: http:\/\/www.sequenceontology.org\/resources\/gvf.html\#gvf_pragmas 

  3. The pragmas above are not printed unless their values are explicitly 
     defined by the user. 

  4. The possible enumerative-\/set-values for the restricted pragma are not
     enforced by this script and users are therefore discourged from 
     deviating from the current spec, as compatibility with processes using
     the pragmas may become compromised.


/;
}


sub gvf {
    my (%argv,%pragma);
    my $commandline = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    my ($passsrc,$failsrc,$outfile,$iter,$opts) = ('VCF','filtered',STDIO,0,0);
    getopts('XhejzOo:i:S:r:v:d:p:s:c:n:V:M:l:T:I:a:E:m:R:A:C:D:G:F:f:',\%argv) && 
        !$argv{'h'} && @ARGV == 1 || GVF_USAGE;
    for my $o (keys %argv) {
        if    ($o eq 'e') { $opts   |= 0x1 }
	elsif ($o eq 'j') { $opts   |= 0x2 }
	elsif ($o eq 'z') { $opts   |= 0x4 }
	elsif ($o eq 'O') { $opts   |= 0x8 }
        elsif ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
        elsif ($o eq 'i') { $iter    = is_uint($argv{$o},\&GVF_USAGE) }
        elsif ($o eq 'o') { $outfile = is_string($argv{$o},\&GVF_USAGE) }
        elsif ($o eq 'S') { $passsrc = is_string($argv{$o},\&GVF_USAGE)||'.' }
        elsif ($o eq 'f') { $failsrc = is_string($argv{$o},\&GVF_USAGE)||'.' }
        elsif ($o =~ /[aI]/) { $pragma{$o} = is_uint($argv{$o},\&GVF_USAGE) }
        elsif ($o =~ /[v]/)  { $pragma{$o} = is_ufloat($argv{$o},\&GVF_USAGE) }
        elsif ($o =~ /[cdlmnprsACDEFGMRTV]/) { 
	    $pragma{$o} = is_string($argv{$o},\&GVF_USAGE) }
    }

    report('Starting ',scalar(localtime));
    report('Command-line: ',$commandline);

    my $fdi = open(shift(@ARGV),READ);
    my $fdo = open($outfile,WRITE);

    gvfCore($fdo,$fdi,$passsrc,$failsrc,$iter,$opts,\%pragma);

    $fdo->close();
    $fdi->close();

    report('Finished ',scalar(localtime));

    return 1;
}


sub mainparams {
    my %params = @_;
    
    return qq|
#define MAXPOPS   $params{k}      // (int) number of populations assumed
#define BURNIN    $params{b}   // (int) length of burnin period
#define NUMREPS   $params{r}   // (int) number of MCMC reps after burnin
#define INFILE   $params{o}   // (str) name of input data file
#define OUTFILE  $params{o}.str  //(str) name of output data file
#define NUMINDS  $params{N}    // (int) number of diploid individuals in data file
#define NUMLOCI  $params{M}    // (int) number of loci in data file
#define PLOIDY       2    // (int) ploidy of data
#define MISSING     $params{m}    // (int) value given to missing genotype data
#define ONEROWPERIND $params{s}    // (B) store data for individuals in a single line
#define LABEL     1     // (B) Input file contains individual labels
#define POPDATA   0     // (B) Input file contains a population identifier
#define POPFLAG   0     // (B) Input file contains a flag which says 
#define LOCDATA   0     // (B) Input file contains a location identifier
#define PHENOTYPE 0     // (B) Input file contains phenotype information
#define EXTRACOLS 0     // (int) Number of additional columns of data 
#define MARKERNAMES      0  // (B) data file contains row of marker names
#define RECESSIVEALLELES 0  // (B) data file contains dominant markers (eg AFLPs)
#define MAPDISTANCES     0  // (B) data file contains row of map distances 
#define PHASED           0 // (B) Data are in correct phase (relevant for linkage model only)
#define PHASEINFO        0 // (B) the data for each individual contains a line
#define MARKOVPHASE      0 // (B) the phase info follows a Markov model.
#define NOTAMBIGUOUS  -999 // (int) for use in some analyses of polyploid data
|;
}


sub structureCore {
    my $fdo   = is_subclass(shift,'IO::Handle');
    my $fdi   = is_subclass(shift,'IO::Handle');
    my $param = is_hashref(shift);


    my (@fh1,@fh2);
    my $num_markers = 0;
    my ($META,$HEADER) = getMeta($fdi);
    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - (INFO+2));
    my $REQD  = ['FT'];

    throw(sprintf('At least one sample required by %s()',
		  __FUNCTION__)) if ($nGeno < 1);


    my $dir = File::Temp->newdir('vcftk-structure_XXXXXXXXX', 
				 'CLEANUP'    => !$Valkyr::Error::DEBUG);

    debug($dir);
    for (my $i = 0; $i < $nGeno; ++$i) {
	$fh1[$i] = File::Temp->new('TEMPLATE' => escChar($HEADER->[$i]).'_XXXXXXXX',
				   'DIR'      => $dir,
	                           'UNLINK'   => !$Valkyr::Error::DEBUG ) ||
	    throw("Cannot open tempfile (in '$dir') for writing.");
	$fh2[$i] = $param->{'s'} ? $fh1[$i] : 
                   File::Temp->new('TEMPLATE' => escChar($HEADER->[$i]).'_XXXXXXXX',
				   'DIR'      => $dir,
	                           'UNLINK'   => !$Valkyr::Error::DEBUG ) ||
	    throw("Cannot open tempfile (in '$dir') for writing.");
	$fh1[$i]->print($HEADER->[$i+FORMAT+1]);
	$fh2[$i]->print($HEADER->[$i+FORMAT+1]) if !$param->{'s'};
    }
    
    report('Scattering...');
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) {
	progressCounter();

	if ($vcf->[FILTER] !~ PASS_FILTER_PAT) { next }
	if ($vcf->[ALT] eq NULL_FIELD()) { next }
	if ($vcf->[ALT] =~ /,/) { 
	    warn("Multi-allelic site ($vcf->[CHROM]:$vcf->[POS]), skipping.");
	    next;
	}
	
	my %FORMAT = %{scalar( getFORMAT($vcf->[FORMAT],$REQD) )};
        for(my $i = 0; $i < @$vcf - (FORMAT+1); ++$i) {
            my @g = getSample($vcf->[$i+FORMAT+1],\%FORMAT);
	    
	    if ($g[$FORMAT{'FT'}] !~ PASS_FILTER_PAT) {
		$fh1[$i]->print("\t",$param->{'m'});
		$fh2[$i]->print("\t",$param->{'m'});
		
	    } elsif ($g[$FORMAT{'GT'}] =~ GT_GROUP_PAT) {
		if ($1 eq NULL_ALLELE || $2 eq NULL_ALLELE) {
		    $fh1[$i]->print("\t",$param->{'m'});
		    $fh2[$i]->print("\t",$param->{'m'});
		} else {
		    $fh1[$i]->print("\t$1");
		    $fh2[$i]->print("\t$2");
		}
	    } else {
		throw("Assertion failure: Sample-GT field does not exist: ",
		      $HEADER->[$i+FORMAT+1]);
	    }
	}
	$num_markers++;
    }
    progressReport();
    resetCounter();
    
    $param->{'M'} = $num_markers;
    $param->{'N'} = $nGeno;


    report('Gathering...');
    configCounter({'prefix' => 'Samples processed: '});
    map {$_->seek(0,0) || throw('Could not seek()')} @fh1;
    map {$_->seek(0,0) || throw('Could not seek()')} @fh2 if !$param->{'s'};
    for (my $i = 0; $i < min(scalar(@fh1),scalar(@fh2)); ++$i) {
	progressCounter();
	print($fdo $fh1[$i]->getlines(),"\n");
	print($fdo $fh2[$i]->getlines(),"\n");
	close($fh1[$i]);
	close($fh2[$i]) if !$param->{'s'};
    }
    progressReport();

    return 1;
}


sub STRUCTURE_USAGE {
    die qq/
Usage:   $PROGRAM structure [options] <in.vcf> <out.txt>

Options: -m INT   Code missing data values as INT [-9]
         -k INT   Max populations [2]
         -b INT   Num. burn-in repetitions [10k]
         -r INT   Num. MCMC repetitions [20k]
         -s       Print individual genotype data on same line
         -h       This help message

Notes:   Requires at least one genotype\/sample field.


/;
}


sub structure {
    load('File::Temp');

    my %argv;
    my %param = ('s'=>0, 'm'=>-9, 'k'=>2, 'b'=>10000, 'r'=>20000,
	         'N'=>0, 'M'=> 0, );  
    my $commandline = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('Xhsm:k:b:r:',\%argv) && !$argv{'h'} && @ARGV == 2 || STRUCTURE_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq 's') { $param{$o} = 1 }
	elsif ($o eq 'm') { $param{$o} = is_sint($argv{$o},\&STRUCTURE_USAGE) }
	elsif ($o eq 'k') { $param{$o} = is_uint($argv{$o},\&STRUCTURE_USAGE) }
	elsif ($o eq 'b') { $param{$o} = is_uint(expand($argv{$o}),\&STRUCTURE_USAGE) }
	elsif ($o eq 'r') { $param{$o} = is_uint(expand($argv{$o}),\&STRUCTURE_USAGE) }
    }

    report('Starting ',scalar(localtime));
    report('Command-line: ',$commandline);

    my $infile  = $param{'i'} = shift(@ARGV);
    my $outfile = $param{'o'} = shift(@ARGV);

    my $fdi = open($infile,READ);
    my $mp  = open('mainparams',WRITE);
    my $fdo = open($outfile,WRITE,NOSTREAM);

    structureCore($fdo,$fdi,\%param);

    print($mp mainparams(%param));

    close($fdo); 
    close($fdi);
    close($mp);

    report('Finished ',scalar(localtime));

    return 1;
}


sub frappeConfig {
    my $infile = is_string(shift);
    my %params = @_;
    
    return qq/
GenotypeFile   = "$infile" ## Mandatory genotype file name
MaxIter        =  $params{r}   ## maximum number of EM iterations
K              =  $params{k}   ## Number of ancestral individuals
M              =  $params{M}   ## Number of markers
I              =  $params{N}   ## Number of individuals;
step           =  $params{s}   ## Define either step or Nout
Nout           =  $params{n}
IndividualFile = "simIndfile.txt"  ### Optional file 
printP         = 1     ## optional 
threshold      =  $params{c}  ## optional convergence threshold
/;
}


sub frappeCore {
    my $fdo   = is_subclass(shift,'IO::Handle');
    my $fdi   = is_subclass(shift,'IO::Handle');
    my $param = is_hashref(shift);
    

    my @fh;
    my %seen;
    my $fam   = 1;
    my $num_markers = 0;
    my ($META,$HEADER) = getMeta($fdi);
    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - ($FIELD{'INFO'}+2));
    my $REQD  = ['FT'];

    throw(sprintf('At least one sample required by %s()',
		  __FUNCTION__)) if ($nGeno < 1);



    my $dir = File::Temp->newdir('vcftk-frappe_XXXXXXXXX', 
				 'CLEANUP'    => !$Valkyr::Error::DEBUG);
    debug($dir);
    for(my $i = 0; $i < $nGeno; ++$i) {
	my $n = substr($HEADER->[$i+FORMAT+1],0,25);
	if ($seen{$n}) {
	    warn("Duplicate sample ID detected: $n");
	}
	$fh[$i] = File::Temp->new('TEMPLATE' => escChar($HEADER->[$i+FORMAT+1]).'_XXXXXXXX',
				  'DIR'      => $dir,
	                          'UNLINK'   => !$Valkyr::Error::DEBUG ) ||
	    throw("Cannot open tempfile (in '$dir') for writing.");
	
	$fh[$i]->print(join(' ',$param->{'P'}||$fam++,$n,0,0,0,0));
	$seen{$n}++;
    }
    
    report('Scattering...');
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) {
	progressCounter();

	if ($vcf->[FILTER] !~ PASS_FILTER_PAT) { next }
	if ($vcf->[ALT] eq NULL_FIELD()) { next }
	if ($vcf->[ALT] =~ /,/) { 
	    warn("Multi-allelic site ($vcf->[CHROM]:$vcf->[POS]), skipping.");
	    next;
	}
	
	my %FORMAT = %{scalar( getFORMAT($vcf->[FORMAT],$REQD) )};
        for(my $i = 0; $i < @$vcf - (FORMAT+1); ++$i) {
            my @g = getSample($vcf->[$i+FORMAT+1],\%FORMAT);

            if ($g[$FORMAT{'FT'}] !~ PASS_FILTER_PAT) {
		$fh[$i]->print(' ',as_sbitA(NULL_ALLELE),' ',as_sbitA(NULL_ALLELE));

	    } elsif ($g[$FORMAT{'GT'}] =~ GT_GROUP_PAT) {
		$fh[$i]->print(' ',as_sbitA($1),' ',as_sbitA($2));

	    } else {
		throw("Assertion failure: Sample-GT field does not exist: ",
		      $HEADER->[$i+FORMAT+1]);
	    }
	}
	$num_markers++;
    }
    progressReport();
    resetCounter();
    
    $param->{'M'} = $num_markers;
    $param->{'N'} = $nGeno;
    

    report('Gathering...');
    configCounter({'prefix' => 'Samples processed: '});
    map {$_->seek(0,0) || throw('Could not seek()')} @fh;
    for (my $i = 0; $i < @fh; ++$i) {
	progressCounter();
	print($fdo $fh[$i]->getlines(),"\n");
	close($fh[$i]);
    }
    progressReport();

    return 1;
}


sub FRAPPE_USAGE {
    die qq/
Usage:   $PROGRAM frappe [options] <in.vcf> <out.prefix>

Options: -P STR   Population ID [null]
         -c INT   Convergence threshold [10k]
         -k INT   Max populations [2]
         -r INT   Num. EM repetitions [10k]
         -s INT   Num. Step [200]
         -n INT   Num. Nout [0]
         -h       This help message

Notes:

  1. Requires at least one genotype\/sample field.

  2. Sample names are assumed to be 25 characters
     or fewer, otherwise they are truncated.


/;
}


sub frappe {
    load('File::Temp');

    my %argv;
    my %param = ('k' => 2, 'r' => 10000, 'c' => 10000, 's' => 0, 'n' => 0, 'P' => '');
    my $commandline = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('Xhk:s:n:P:r:c:',\%argv) && !$argv{'h'} && @ARGV == 2 || FRAPPE_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq 'k') { $param{$o} = is_uint($argv{$o},\&FRAPPE_USAGE) }
	elsif ($o eq 's') { $param{$o} = is_uint($argv{$o},\&FRAPPE_USAGE) }
	elsif ($o eq 'n') { $param{$o} = is_uint($argv{$o},\&FRAPPE_USAGE) }
	elsif ($o eq 'P') { $param{$o} = is_string($argv{$o},\&FRAPPE_USAGE) }
	elsif ($o eq 'r') { $param{$o} = is_uint(expand($argv{$o}),\&FRAPPE_USAGE) }
	elsif ($o eq 'c') { $param{$o} = is_uint(expand($argv{$o}),\&FRAPPE_USAGE) }
    }
    if (length($param{'P'}) > 25) { 
	throw('Population ID must be fewer than 26 characters.');
    }
    if (!($param{'s'} + $param{'n'})) { 
	throw('Must define either step or Nout parameter');
    } elsif ($param{'s'} * $param{'n'}) { 
	throw('Cannot define both step and Nout parameters');
    } elsif ($param{'r'} < $param{'n'}) {
	throw('Nout parameter must be less than max EM iterations');
    } elsif ($param{'r'} < $param{'s'}) {
	throw('step parameter must be less than max EM iterations');
    }

    report('Starting ',scalar(localtime));
    report('Command-line: ',$commandline);

    my $infile = shift(@ARGV);
    my $prefix = shift(@ARGV);

    my $fdi = open($infile,READ);
    my $cfg = open("$prefix.txt",WRITE);
    my $fdo = open("$prefix.ped",WRITE,NOSTREAM);

    frappeCore($fdo,$fdi,\%param);

    print($cfg frappeConfig("$prefix.ped",%param));

    close($fdo);
    close($fdi);
    close($cfg);

    report('Finished ',scalar(localtime));

    return 1;
}


sub print_bins {
    my $fho   = is_subclass(shift,'IO::Handle');
    my $lines = is_arrayref(shift);
    for my $line (@$lines) {
	$line // next;
	print($fho join("\t",@$line),"\n");
    }
    return 1;
}


sub binvarCore {
    my $fdo   = is_subclass(shift,'IO::Handle');
    my $fdi   = is_subclass(shift,'IO::Handle');
    my $field = is_string(shift);
    my $bsize = is_uint(shift);
    my $optml = is_sint(shift);
    my $numrc = is_uint(shift);
    my $info  = is_uint(shift);
    my $cmd   = is_string(shift || '');

    my ($META,$HEADER) = getMeta($fdi);
    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};

    addMeta($META,'VCFtk',
        {
	    '_ORDER_' => [qw(ID Version Date CommandlineOptions)],
	    'ID'      => 'binvar',
	    'Version' => $VERSION,
	    'Date'    => '"'.scalar(localtime).'"',
	    'CommandlineOptions' => '"'.$cmd.'"'
	});


    print($fdo setMeta($META,$HEADER),"\n");

    my $cmp = $numrc  
	? sub {eval{$_[0] <=> $_[1]} ||0}
        : sub {eval{$_[0] cmp $_[1]} ||0};

    my (@S,@V);
    my $prevchr = undef;
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) { 
	progressCounter();

	my $bin = int($vcf->[POS]/$bsize);
	if (defined($prevchr) && $prevchr ne $vcf->[CHROM]) {
	    print_bins($fdo,\@V);
	    $prevchr = $vcf->[CHROM];
	    @S = ();
	    @V = ();
	}
	$S[$bin] ||= $optml < 0 
	    ? ($numrc ?  9**9**9 : '~') 
	    : ($numrc ? -9**9**9 : ' ');
	$V[$bin] //= $vcf;
	if ($info) {
	    my $INFO = getINFO($vcf->[INFO]);
	    if (defined($INFO->{$field}) && 
		$cmp->($INFO->{$field},$S[$bin]) == $optml) {
		$S[$bin] = $INFO->{$field};
		$V[$bin] = $vcf;
	    }
	} elsif ($FIELD{$field} && 
		 $cmp->($vcf->[$field],$S[$bin]) == $optml) {
	    $S[$bin] = $vcf->[$field];
	    $V[$bin] = $vcf;
	}
	$prevchr = $vcf->[CHROM];
    }
    progressReport();
    print_bins($fdo,\@V);

    return 1;
}


sub BINVAR_USAGE {
    die qq/
Usage:   $PROGRAM binvar [options] <in.vcf>

Options: -o FILE  Write output to FILE [stdout]
         -f STR   Optimize STR field [QUAL]
         -b INT   Bin size\/width [1000]
         -I       Field is an INFO key=value pair
         -n       Optimize numerically
         -m       Optimize for the min. value [max]
         -h       This help message

Notes:   Requires at least one genotype\/sample field.


/;
}


sub binvar {
    my %argv;
    my ($bsize,$outfile,$isINFO,$optiml,$numeric,$field) 
	= (1000,STDIO,0,1,0,'QUAL');
    my $commandline = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('XhmnIo:b:f:',\%argv) && !$argv{'h'} && @ARGV == 1 || &BINVAR_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq 'm') { $optiml  = -1 }
	elsif ($o eq 'n') { $numeric =  1 }
	elsif ($o eq 'I') { $isINFO  =  1 }
	elsif ($o eq 'f') { $field   = is_string($argv{$o},\&BINVAR_USAGE) }
	elsif ($o eq 'o') { $outfile = is_string($argv{$o},\&BINVAR_USAGE) }
	elsif ($o eq 'b') { $bsize   = is_uint(expand($argv{$o}),\&BINVAR_USAGE) }
    }

    report('Starting ',scalar(localtime));
    report('Command-line: ',$commandline);
    
    my $fdi = open(shift(@ARGV),READ);
    my $fdo = open($outfile,WRITE);
    
    binCore($fdo,$fdi,$field,$bsize,$optiml,$numeric,$isINFO,$commandline);
    
    $fdo->close(); 
    $fdi->close();

    report('Finished ',scalar(localtime));

    return 1;
}


sub is_sample {
    my $min   = is_uint(shift);
    my $max   = is_uint(shift);
    my $str   = is_string(shift);
    my $usage = is_coderef(shift);
    
    if ($max  < $min) {
	throw("Invalid range of required samples: [$min,$max]");
    }
    
    my @sample  = split(',',$str);
    my $redund  = indexArray(qw(CHROM POS ID REF ALT QUAL FILTER INFO FORMAT));
    if ($min   <= @sample && @sample <= $max) {
	for (my $i = 0; $i < @sample; ++$i) {
	    is_string($sample[$i],$usage);
	    if (exists($redund->{$sample[$i]})) {
		throw("Invalid sample ID: Reserved field ($sample[$i]).");
	    }
	}
	return @sample;

    } else {
	if ($min == $max) {
	    report("Must provide sample IDs for exactly $min samples.");
	} else {
	    report("Must provide sample IDs for between $min ",
		   "to $max samples.")
	}
	&$usage; 
	exit(1);
    }
}


sub jmlocCore {
    my $fdo   = is_subclass(shift,'IO::Handle');
    my $fdi   = is_subclass(shift,'IO::Handle');
    my $P0IDs = is_arrayref(shift);
    my $opt   = is_uint(shift);
    my $pop   = is_string(shift);
    my $RMPAT = shift;

    my ($META,$HEADER) = getMeta($fdi);

    my $nloci = 0;
    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - ($FIELD{'INFO'}+2));

    if (! ($opt & 0x1)) {
	throw(sprintf('Parental and offspring samples required by %s().',
		      __FUNCTION__)) if ($nGeno < 3);
	
	if (!defined($P0IDs->[0]) || !exists($FIELD{$P0IDs->[0]})) {
	    throw("Parent ID ($P0IDs->[0]) not found in VCF.");
	}
	if (!defined($P0IDs->[1]) || !exists($FIELD{$P0IDs->[1]})) {
	    throw("Parent ID ($P0IDs->[1]) not found in VCF.");
	}
    }

    my $num_totl = 0;
    my $num_site = 0;
    report('Counting loci...');

  LOC: # we need to know the number of loci:
    while (my $vcf = getEntry($fdi,$width)) {
	$num_totl++;
	
        if ($vcf->[ALT] eq NULL_FIELD) { next }
        if ($vcf->[ALT] =~ BREAKEND_PAT ) { 
            warn(sprintf(BREAKEND_MSG,@{$vcf}[CHROM,POS]));
	    next;
        }

	my @P0GTs;
	my %FORMAT = %{scalar( getFORMAT($vcf->[FORMAT]) )};
	if ($opt & 0x1) {
	    my $I  = getINFO( $vcf->[INFO] );
	    if (! defined($I->{'P0GT'}) || ! exists($I->{'P0PHASED'})) { 
		warn(sprintf('Locus missing P0GT or P0PHASED INFO sub-field(s),'.
		     ' skipping (%s:%u).',@{$vcf}[CHROM,POS]));
		next;
	    }
	    @P0GTs = split(',',$I->{'P0GT'});

	} else {
	    @P0GTs = @{$vcf}[ @FIELD{@$P0IDs} ];
	}
	if (@P0GTs!= 2) {
	    throw('Two parental genotypes required.');
	}

	my $het    = 0;
	for(my $i  = 0; $i < @P0GTs; ++$i) { 
	    my @g  = getSample($P0GTs[$i],\%FORMAT);
	    if ($g[$FORMAT{'GT'}] =~ /^(\d+)[\/|](\d+)/ && $1 != $2) {
		$het |= ($i+1);
	    }
	}
    	if ($het & 3) { $nloci++ }
    }
    seek($fdi,0,0) || throw('Failed to seek().');

    report('Input samples: ',$nGeno||0);
    report('Total sites: ',$nloci||0);
    report(sprintf('Encoding sites: %u / %u (%.4f)',
		   $nloci,$num_totl,($nloci||0)/($num_totl||1)));


    # print meta info
    print($fdo "name = $pop\n");
    print($fdo "popt = CP\n");
    print($fdo "nloc = $nloci\n");
    print($fdo "nind = $nGeno\n\n");

    my $warned = 0;

    # start core loop
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
  SITE:
    while (my $vcf = getEntry($fdi,$width)) {
	progressCounter();

        # filters
        if ($vcf->[ALT] eq NULL_FIELD) { next }
        if ($vcf->[ALT] =~ BREAKEND_PAT) { next }

	my $locus_id = $vcf->[CHROM].':'.$vcf->[POS];

	if ($RMPAT) { $locus_id =~ s/$RMPAT// }
	if (!($warned & 0x1) && length($locus_id) > 20) {
	    warn("Locus ID '$locus_id' greater than 20 chars ",
		 "in length, consider re-running with '-r' option.");
	    $warned |= 0x1;
	}
		
	# what segregation encoding should we use?
	# <abxcd> het for four observed alleles
	# <efxeg> het for three observed alleles
	# <hkxhk> het for two observed alleles
	# <lmxll> het for first parent, two alleles
	# <nnxnp> het for second parent, two alleles

	my @P0GTs;
	my @alleles;
	my @G = (0,0);
	my $het    = 0;
	my $bits   = 0;
	my $seg    = '';
	my $class  = '';
	my %FORMAT = %{scalar( getFORMAT($vcf->[FORMAT]) )};
	if ($opt & 0x1) {
	    my $I  = getINFO( $vcf->[INFO] );
	    if (! defined($I->{'P0GT'}) || ! exists($I->{'P0PHASED'})) { 
		next;
	    }
	    @P0GTs = split(',',$I->{'P0GT'});

	} else {
	    @P0GTs = @{$vcf}[ @FIELD{@$P0IDs} ];
	}
	if (@P0GTs!= 2) {
	    throw('Two parental genotypes required.');
	}

	for(my $i  = 0; $i < @P0GTs; ++$i) { 
	    my @g  = getSample($P0GTs[$i],\%FORMAT);
	    if ($g[$FORMAT{'GT'}] =~ /^(\d+)[\/\|](\d+)/) {
		if ($1 != $2) { $het |= ($i+1) }
		$G[$i] |= (1 << $1);
		$G[$i] |= (1 << $2);
	    } else { # parents: missing data
		next SITE;
	    }
	}

	my $sharedbits = $G[0] & $G[1];
	my $familybits = $G[0] | $G[1];
	my $uniquebits = $G[0] ^ $G[1];
	my $numalleles = bitcount($familybits);

	debug(sprintf('alleles: %u; %s:%u',$familybits,@{$vcf}[0,1]));
	if ($numalleles < 2) { # not useful
	    next SITE;
	}
	if ($sharedbits == $familybits) {
	    my $bt0 = 1 << numShifts($G[0]);
	    my $bt1 = $sharedbits ^ $bt0;
	    $alleles[numShifts($bt0)] = 'h';
	    $alleles[numShifts($bt1)] = 'k';
	    $seg   = '<hkxhk>';
	    $class = '(hh,hk,kk)';

	} elsif ($sharedbits == 0) {
	    if ($numalleles == 2) {
		next SITE;
	    } elsif ($numalleles == 3) {
		if ($het == 1) {
		    my $bt0 = 1 << numShifts($G[0]);
		    my $bt1 = $G[0] ^ $bt0;
		    $alleles[numShifts($G[1])] = 'l';
		    $alleles[numShifts($bt0)] = 'l';
		    $alleles[numShifts($bt1)] = 'm';
		    $seg   = '<lmxll>';
		    $class = '(ll,lm)';
		} else {
		    my $bt0 = 1 << numShifts($G[1]);
		    my $bt1 = $G[1] ^ $bt0;
		    $alleles[numShifts($G[0])] = 'n';
		    $alleles[numShifts($bt0)] = 'n';
		    $alleles[numShifts($bt1)] = 'p';
		    $seg   = '<nnxnp>';
		    $class = '(nn,np)';
		}

	    } elsif ($numalleles == 4) {
		my $bt0 = 1 << numShifts($G[0]);
		my $bt1 = 1 << numShifts($G[1]);
		$alleles[numShifts($bt0)] = 'a';
		$alleles[numShifts($bt1)] = 'c';
		$alleles[numShifts($G[0] ^ $bt0)] = 'b';
		$alleles[numShifts($G[1] ^ $bt1)] = 'd';
		$seg   = '<abxcd>';
		$class = '(ac,ad,bc,bd)';
	    } else {
		throw AssertionError("hom-hom genotype ",
				     "configuration detected.");
	    }
	} else {
	    if ($het == 3) {
		debug('=> 3 alleles');
		# which allele is the common/shared allele?
       		$alleles[numShifts($sharedbits)] = 'e';          # 0 idx
		$alleles[numShifts($G[0] & ~$sharedbits)] = 'f'; # 1 idx
		$alleles[numShifts($G[1] & ~$sharedbits)] = 'g'; # 2 idx
		$seg      = '<efxeg>';
		$class    = '(ee,ef,eg,fg)';
	    } elsif ($het == 1) {
		debug('   => het-hom');
		$alleles[numShifts($sharedbits)] = 'l';
		$alleles[numShifts($G[0] & ~$sharedbits)] = 'm';
		$seg   = '<lmxll>';
		$class = '(ll,lm)';
	    } elsif ($het == 2) {
		debug('   => hom-het');
		$alleles[numShifts($sharedbits)] = 'n';
		$alleles[numShifts($G[1] & ~$sharedbits)] = 'p';
		$seg   = '<nnxnp>';
		$class = '(nn,np)';
	    } else {
		throw AssertionError("hom-hom genotype ",
				     "configuration detected.");
	    }
	}
	
	my @LOC = ($locus_id,$seg,'',$class);
	for(my $i = FORMAT+1; $i < @{$vcf}; ++$i) {
	    my @g = getSample($vcf->[$i],\%FORMAT);

	    if ($g[$FORMAT{'GT'}] =~ /^(\d+)[\/|](\d+)/) {
		if (!defined($alleles[$1]) || !defined($alleles[$2])) {
		    warn('Encoding non-parental alleles as missing:') 
			if ! ($warned & 0x2);
		    if (!defined($alleles[$1])) {
			warn(sprintf('Non-parental allele detected: %u (%s:%u,%s).',
				     $1,@{$vcf}[CHROM,POS],$HEADER->[$i]));
		    }
		    if (!defined($alleles[$2])) {
			warn(sprintf('Non-parental allele detected: %u (%s:%u,%s).',
				     $2,@{$vcf}[CHROM,POS],$HEADER->[$i]));
		    }	    
		    $LOC[$i-5] = '--';
		    $warned |= 0x2;
		    next;
		}
		$LOC[$i-5] = join('',sort($alleles[$1],$alleles[$2]));

	    } else {
		$LOC[$i-5] = '--';
	    }
	}
	print($fdo join("\t",@LOC),"\n");
    }
    print($fdo join("\n",'','individual names:','',
		    @{$HEADER}[(FORMAT+1)..$#{$HEADER}],''));

    progressReport();

    return 1;
}		


sub JMLOC_USAGE {
    die qq/
Usage:   $PROGRAM jmloc [options] <multi_sample.vcf>

Options: -o FILE     Write output to FILE [stdout]
         -P STR,STR  Sample IDs for the population's two parents
         -p STR      Population name (no spaces allowed) ['CP_POP']
         -r PATTERN  Remove PATTERN from locus ids [null]
         -I          Use INFO-P0GT sub-field as parents (see Note 3)
         -h          This help message

Notes: 
 
  1. Requires at least three genotype\/sample fields (two parents with
     offspring).

  2. Assumes diploid individuals from a cross-pollination (CP)-type
     population.

  3. The INFO-P0GT sub-field is added by the `$PROGRAM chif1` command and is
     used in place of the observed parental genotypes to determine locus
     segregation and genotype encoding types.


/;
}


sub jmloc {
    my $popname = 'CP_POP';
    my (%argv,$modpat,@parents);
    my ($option,$outfile) = (0,STDIO);
    my $command = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('XhIo:P:p:r:',\%argv) && !$argv{'h'} && @ARGV == 1 || JMLOC_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq 'I') { $option |= 0x1 }
	elsif ($o eq 'r') { $modpat  = is_regex($argv{$o},\&JMLOC_USAGE)  }
	elsif ($o eq 'o') { $outfile = is_string($argv{$o},\&JMLOC_USAGE) }
	elsif ($o eq 'p') { $popname = is_string($argv{$o},\&JMLOC_USAGE) }
	elsif ($o eq 'P') { @parents = is_sample(2,2,$argv{$o},\&JMLOC_USAGE) }
    }
    if (!($option & 0x1) && @parents != 2) {
	report('Must provide sample IDs for exactly 2 parents.');
	JMLOC_USAGE;
    }
        
    report('Starting ',scalar(localtime));
    report('Command-line: ',$command);

    my $fdi = open(shift(@ARGV),READ,NOSTREAM);
    my $fdo = open($outfile,WRITE);

    jmlocCore($fdo,$fdi,\@parents,$option,$popname,$modpat);
    
    close($fdo); 
    close($fdi);

    report('Finished ',scalar(localtime));

    return 1;
}


sub readjmlocPhase {
    my $file = is_file(shift,'fr');

    my %dat;
    my $loc = open($file,READ);
    while (local $_ = $loc->getline()) {
	if (/^;|^\s*$|^name|^popt|^nloc|^nind/) { next }

	my @loc = split(/\s+/); # marker_id seg phase class ...
	if (/^\S/ && @loc > 4) {
	    $loc[1] =~ s/^\<|x|\>$//g;
	    $loc[2] =~ s/^\{|\}$//g;
	    $dat{$loc[0]}{'seg'}   = $loc[1];
	    $dat{$loc[0]}{'phase'} = $loc[2];
	}
    }
    $loc->close();

    return \%dat
}

sub jmphaseCore {
    my $fdo   = is_subclass(shift,'IO::Handle');
    my $fdi   = is_subclass(shift,'IO::Handle');
    my $P0IDs = is_arrayref(shift);
    my $loc   = is_hashref(shift);
    my $opt   = is_uint(shift);
    my $RMPAT = shift;

    my ($META,$HEADER) = getMeta($fdi);

    my $nloci = 0;
    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - ($FIELD{'INFO'}+2));

    if (! ($opt & 0x1)) {
	throw(sprintf('Parental and offspring samples required by %s().',
		      __FUNCTION__)) if ($nGeno < 3);
	
	if (!defined($P0IDs->[0]) || !exists($FIELD{$P0IDs->[0]})) {
	    throw("Parent ID ($P0IDs->[0]) not found in VCF.");
	}
	if (!defined($P0IDs->[1]) || !exists($FIELD{$P0IDs->[1]})) {
	    throw("Parent ID ($P0IDs->[1]) not found in VCF.");
	}
    }
    

    print($fdo setMeta($META,$HEADER),"\n");

    # start core loop
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
  SITE:
    while (my $vcf = getEntry($fdi,$width)) {
	progressCounter();

	my $locus_id = $vcf->[$FIELD{'CHROM'}].':'.$vcf->[$FIELD{'POS'}];

	if ($RMPAT) { $locus_id =~ s/$RMPAT// }


        # filters
        if ($vcf->[ALT] eq NULL_FIELD) { 
	    print($fdo join("\t",@$vcf),"\n");
	    next;
	}
        if ($vcf->[ALT] =~ BREAKEND_PAT ) { 
	    print($fdo join("\t",@$vcf),"\n");
	    next;
	}	
	if (!defined($loc->{$locus_id})) {
	    print($fdo join("\t",@$vcf),"\n");
	    next;
	}
	
	
	# what segregation encoding should we use?
	# <abxcd> het for four observed alleles
	# <efxeg> het for three observed alleles
	# <hkxhk> het for two observed alleles
	# <lmxll> het for first parent, two alleles
	# <nnxnp> het for second parent, two alleles

	my @P0GTs;
	my %alleles;
	my @G = (0,0);
	my $het    = 0;
	my $bits   = 0;
	my $seg    = '';
	my $I  = getINFO( $vcf->[INFO] );
	my %FORMAT = %{scalar( getFORMAT($vcf->[FORMAT]) )};
	if ($opt & 0x1) {
	    if (! defined($I->{'P0GT'}) || ! exists($I->{'P0PHASED'})) { 
		print($fdo join("\t",@$vcf),"\n");
		next;
	    }
	    @P0GTs = split(',',$I->{'P0GT'});

	} else {
	    @P0GTs = @{$vcf}[ @FIELD{@$P0IDs} ];
	}
	if (@P0GTs!= 2) {
	    throw('Two parental genotypes required.');
	}

	for(my $i  = 0; $i < @P0GTs; ++$i) { 
	    my @g  = getSample($P0GTs[$i],\%FORMAT);
	    if ($g[$FORMAT{'GT'}] =~ /^(\d+)[\/\|](\d+)/) {
		if ($1 != $2) { $het |= ($i+1) }
		$G[$i] |= (1 << $1);
		$G[$i] |= (1 << $2);
	    } else { # parents: missing data
		print($fdo join("\t",@$vcf),"\n");
		next SITE;
	    }
	}

	$bits |= $G[0];
	$bits |= $G[1];

	my $nalleles = bitcount($bits);

	debug(sprintf('alleles: %u; %s:%u',$bits,@{$vcf}[0,1]));
	if (! $het) { # not useful
	    print($fdo join("\t",@$vcf),"\n");
	    next SITE;
	} elsif ($nalleles > 1) {
	    my $sharedbit = (1 << numShifts($G[0] & $G[1]));
	    if ($nalleles == 4) {
		debug('=> 4 alleles');
		my $s0 = numShifts($G[0]);
		my $s1 = numShifts($G[1]);
		$alleles{'a'} = $s0;
		$alleles{'b'} = log(2,$G[0] & ~(1 << $s0));
		$alleles{'c'} = $s1;
		$alleles{'d'} = log(2,$G[1] & ~(1 << $s1));
		$seg      = 'abcd';
	    } elsif ($nalleles == 3) {
		debug('=> 3 alleles');
		# which allele is the common/shared allele?
       		$alleles{'e'} = log(2,$sharedbit);
		$alleles{'f'} = log(2,$G[0] & ~$sharedbit); # 1 idx
		$alleles{'g'} = log(2,$G[1] & ~$sharedbit); # 2 idx
		$seg      = 'efeg';
	    } elsif ($nalleles == 2) {
		debug('=> 2 alleles');
		if (($het & 3) == 3) {
		    debug('   => het-het');
		    $alleles{'h'} = log(2,$sharedbit);
		    $alleles{'k'} = log(2,$G[0] & ~$sharedbit);
		    $seg   = 'hkhk';
		} elsif ($het & 1) {
		    debug('   => het-hom');
		    $alleles{'l'} = log(2,$sharedbit);
		    $alleles{'m'} = log(2,$G[0] & ~$sharedbit);
		    $seg   = 'lmll';
		} elsif ($het & 2) {
		    debug('   => hom-het');
		    $alleles{'n'} = log(2,$sharedbit);
		    $alleles{'p'} = log(2,$G[1] & ~$sharedbit);
		    $seg   = 'nnnp';
		} else {
		    throw AssertionError("hom-hom genotype ",
			  "configuration detected.");
		}
	    } else {
		throw AssertionError("hom-hom genotype ",
		      "configuration detected.");
	    }
	} else {
	    throw AssertionError("Invariant site detected.");
	}
	
	if ($seg ne $loc->{$locus_id}->{'seg'}) {
	    throw(sprintf('Incompatible seg type: %s != %s (%s:%u).',
		  $loc->{$locus_id}->{'seg'},$seg,@{$vcf}[CHROM,POS]));
	}

	# phase the @P0GT array here
	for(my $i = 0; $i < @P0GTs; ++$i) {
	    my @g = getSample($P0GTs[$i],{GT=>0});
	    my $p = substr($loc->{$locus_id}->{'phase'},$i,1);
	    if ($g[$FORMAT{'GT'}] =~ /^(\d+)[\/|](\d+)/) {
		if ($p eq '-') {
		    $g[$FORMAT{'GT'}] = join('|',$1,$2);
		} else {
		    my $a0 = $alleles{substr($seg,2*$i+ $p,1)};
		    my $a1 = $alleles{substr($seg,2*$i+!$p,1)};
		    if (!(defined($a0) && defined($a1))) {
			throw(sprintf('Allele out of bounds: %s, %s (%s:%u).',
				      2*$i+ $p, 2*$i+!$p, @{$vcf}[CHROM,POS]));
		    }
		    $g[$FORMAT{'GT'}] = join('|',$a0,$a1);
		} 
		$P0GTs[$i] = $g[$FORMAT{'GT'}];
	    }
	}
	$I->{'P0GT'} = join(',',@P0GTs);
	$vcf->[INFO] = setINFO($I);
	
	print($fdo join("\t",@$vcf),"\n");
    }
    progressReport();


    return 1;
}		


sub JMPHASE_USAGE {
    die qq/
Usage:   $PROGRAM jmphase [options] <multi_sample.vcf> <phased.loc>

Options: -o FILE     Write output to FILE [stdout]
         -P STR,STR  Sample IDs for the population's two parents
         -p STR      Population name (no spaces allowed) ['CP_POP']
         -r PATTERN  Remove PATTERN from locus ids [null]
         -I          Use INFO-P0GT sub-field as parents (see Note 3)
         -h          This help message

Notes: 
 
  1. Assumes diploid individuals from a cross-pollination (CP)-type
     population.

  2. The INFO-P0GT sub-field is added by the `$PROGRAM chif1` command and is
     used in place of the observed parental genotypes to determine locus
     segregation and genotype encoding types.

  3. phased.loc input file is a .loc file exported from JoinMap4.x after
     having computed the linkage matrix.

/;
}


sub jmphase {
    my (%argv,$modpat,@parents);
    my ($option,$outfile) = (0,STDIO);
    my $command = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('XhIo:P:r:',\%argv) && !$argv{'h'} && @ARGV == 2 || JMPHASE_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq 'I') { $option |= 0x1 }
	elsif ($o eq 'r') { $modpat  = is_regex($argv{$o},\&JMPHASE_USAGE)  }
	elsif ($o eq 'o') { $outfile = is_string($argv{$o},\&JMPHASE_USAGE) }
	elsif ($o eq 'P') { @parents = is_sample(2,2,$argv{$o},\&JMPHASE_USAGE) }
    }
    if (!($option & 0x1) && @parents != 2) {
	report('Must provide sample IDs for exactly 2 parents.');
	JMPHASE_USAGE;
    }
        
    report('Starting ',scalar(localtime));
    report('Command-line: ',$command);

    my $fdi = open(shift(@ARGV),READ,NOSTREAM);
    my $loc = readjmlocPhase(shift(@ARGV));
    my $fdo = open($outfile,WRITE);

    jmphaseCore($fdo,$fdi,\@parents,$loc,$option,$modpat);
    
    close($fdo); 
    close($fdi);

    report('Finished ',scalar(localtime));

    return 1;
}


sub is_indel {
    my $ref = is_string(shift);
    my $alt = is_string(shift);

    my $ind = 0;
    if (length($ref) != 1 || length($alt) != length($ref)) {
	my $maxlen = 0;
	my @allele = ($ref,split(',',$alt));
	for(my $i  = 0; $i < @allele; ++$i) {
	    if (length($allele[$i]) > $maxlen) {
		$maxlen = length($allele[$i]);
	    }
	}
	for(my $i  = 0; $i < @allele; ++$i) {
	    if (length($allele[$i]) < $maxlen) { 
		$ind |= (1 << $i);
	    }
	}
    }
    return $ind;
}


sub oneCore {
    my $fdo   = is_subclass(shift,'IO::Handle');
    my $fdi   = is_subclass(shift,'IO::Handle');
    my $P0IDs = is_arrayref(shift);
    my $opt   = is_uint(shift);
    my $RMPAT = shift;

    my ($META,$HEADER) = getMeta($fdi);

    my $nloci = 0;
    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - ($FIELD{'INFO'}+2));

    if (! ($opt & 0x1)) {
	throw(sprintf('Parental and offspring samples required by %s().',
		      __FUNCTION__)) if ($nGeno < 3);
	
	if (!defined($P0IDs->[0]) || !exists($FIELD{$P0IDs->[0]})) {
	    throw("Parent ID ($P0IDs->[0]) not found in VCF.");
	}
	if (!defined($P0IDs->[1]) || !exists($FIELD{$P0IDs->[1]})) {
	    throw("Parent ID ($P0IDs->[1]) not found in VCF.");
	}
    }

    my $num_totl = 0;
    my $num_site = 0;
    report('Counting loci...');
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});

  LOC: # we need to know the number of loci:
    while (my $vcf = getEntry($fdi,$width)) {
	$num_totl++;
	
        if ($vcf->[ALT] eq NULL_FIELD) { next }
        if ($vcf->[ALT] =~ BREAKEND_PAT ) { 
            warn(sprintf(BREAKEND_MSG,@{$vcf}[CHROM,POS]));
	    next;  
        }

	my @P0GTs;
	my %FORMAT = %{scalar( getFORMAT($vcf->[FORMAT]) )};
	if ($opt & 0x1) {
	    my $I  = getINFO( $vcf->[INFO] );
	    if (! defined($I->{'P0GT'}) || ! exists($I->{'P0PHASED'})) { 
		warn(sprintf('Site missing INFO-P0GT/-P0PHASED sub-field(s),'.
		     ' skipping (%s:%u).',@{$vcf}[CHROM,POS]));
		next;
	    }
	    @P0GTs = split(',',$I->{'P0GT'});

	} else {
	    @P0GTs = @{$vcf}[ @FIELD{@$P0IDs} ];
	}
	if (@P0GTs!= 2) {
	    throw('Two parental genotypes required.');
	}

	my $het    = 0;
	for(my $i  = 0; $i < @P0GTs; ++$i) { 
	    my @g  = getSample($P0GTs[$i],\%FORMAT);
	    if ($g[$FORMAT{'GT'}] =~ /^(\d+)[\/|](\d+)/ && $1 != $2) {
		$het |= ($i+1);
	    }
	}
    	if ($het & 3) { $nloci++ }
    }
    seek($fdi,0,0) || throw('Failed to seek().');

    report('Input samples: ',$nGeno||0);
    report('Total sites: ',$nloci||0);
    report(sprintf('Encoding sites: %u / %u (%.4f)',
		   $nloci,$num_totl,($nloci||0)/($num_totl||1)));

    # print meta info
    print($fdo "$nGeno $nloci\n");

    my $warned = 0;
    my %CLASS  = (
	'A.1'  => {map {$_ => 1} qw(ac ad bc bd)},
	'A.2'  => {map {$_ => 1} qw(a ac ba bc)},
	'B3.7' => {map {$_ => 1} qw(a ab b)},
	'D1.9' => {map {$_ => 1} qw(ac bc)},
	'D2.14'=> {map {$_ => 1} qw(ac bc)}
	);


    # start core loop
  SITE:
    while (my $vcf = getEntry($fdi,$width)) {
	progressCounter();
        # filters
        if ($vcf->[ALT] eq NULL_FIELD) { next }
        if ($vcf->[ALT] =~ BREAKEND_PAT ) { next }

	my $locus_id = $vcf->[CHROM].':'.$vcf->[POS];

	if ($RMPAT) { $locus_id =~ s/$RMPAT// }
	if (!($warned & 0x1) && length($locus_id) > 20) {
	    warn("Locus ID '$locus_id' greater than 20 chars ",
		 "in length, consider re-running with '-r' option.");
	    $warned |= 0x1;
	}
		
	# what segregation encoding should we use? see:
	# Wu et al. 2002. "Simultaneous Maximum Likelihood Estimation of
	#   Linkage and Linkage Phases in Outcrossing Species". Theoretical
	#   Population Biology. 61:349-363. doi:10.1006/tpbi.2002.1577

	my @P0GTs;
	my @alleles;
	my @G = (0,0);
	my $het    = 0;
	my $bits   = 0;
	my $seg    = '';
	my $class  = '';
	my %FORMAT = %{scalar( getFORMAT($vcf->[FORMAT]) )};
	if ($opt & 0x1) {
	    my $I  = getINFO( $vcf->[INFO] );
	    if (! defined($I->{'P0GT'}) || ! exists($I->{'P0PHASED'})) { 
		next;
	    }
	    @P0GTs = split(',',$I->{'P0GT'});

	} else {
	    @P0GTs = @{$vcf}[ @FIELD{@$P0IDs} ];
	}
	if (@P0GTs!= 2) {
	    throw('Two parental genotypes required.');
	}

	my $indel  = is_indel($vcf->[REF],$vcf->[ALT]);
	for(my $i  = 0; $i < @P0GTs; ++$i) { 
	    my @g  = getSample($P0GTs[$i],\%FORMAT);
	    if ($g[$FORMAT{'GT'}] =~ /^(\d+)[\/\|](\d+)/) {
		if ($1 != $2) { $het |= ($i+1) }
		$G[$i] |= (1 << $1);
		$G[$i] |= (1 << $2);
	    } else { # parents: missing data
		next SITE;
	    }
	}

	$bits |= $G[0];
	$bits |= $G[1];

	my $nalleles = bitcount($bits);

	debug(sprintf('alleles: %u; %s:%u',$bits,@{$vcf}[0,1]));
	if (! $het) { # not useful
	    next SITE;
	} elsif ($nalleles > 1) {
	    my $sharedbit = (1 << numShifts($G[0] & $G[1]));
	    if ($nalleles == 4) {
		debug('=> 4 alleles');
		my $s0 = numShifts($G[0]);
		my $s1 = numShifts($G[1]);
		$alleles[$s0] = 'a';
		$alleles[log(2,$G[0] & ~(1 << $s0))] = 'b';
		$alleles[$s1] = 'c';
		$alleles[log(2,$G[1] & ~(1 << $s1))] = 'd';
		$class = 'A.1';
	    } elsif ($nalleles == 3) {
		debug('=> 3 alleles');
		# which allele is the common/shared allele?
       		$alleles[log(2,$sharedbit)] = 'a';         # 0 idx
		$alleles[log(2,$G[0] & ~$sharedbit)] = 'b'; # 1 idx
		$alleles[log(2,$G[1] & ~$sharedbit)] = 'c'; # 2 idx
		$class = 'A.2';
	    } elsif ($nalleles == 2) {
		debug('=> 2 alleles');
		if (($het & 3) == 3) {
		    debug('   => het-het');
		    $alleles[log(2,$sharedbit)] = 'a';
		    $alleles[log(2,$G[0] & ~$sharedbit)] = 'b';
		    $class = 'B3.7';
		} elsif ($het & 1) {
		    debug('   => het-hom');
		    $alleles[log(2,$sharedbit)] = 'a';
		    $alleles[log(2,$G[0] & ~$sharedbit)] = 'b';
		    $class = 'D1.9';
		} elsif ($het & 2) {
		    debug('   => hom-het');
		    $alleles[log(2,$sharedbit)] = 'a';
		    $alleles[log(2,$G[1] & ~$sharedbit)] = 'b';
		    $class = 'D2.14';
		} else {
		    throw AssertionError("hom-hom genotype ",
			  "configuration detected.");
		}
	    } else {
		throw AssertionError("hom-hom genotype ",
		      "configuration detected.");
	    }
	} else {
	    throw AssertionError("Invariant site detected.");
	}
	
	

	my @LOC;
	for(my $i = FORMAT+1; $i < @{$vcf}; ++$i) {
	    my @g = getSample($vcf->[$i],\%FORMAT);

	    if ($g[$FORMAT{'GT'}] =~ /^(\d+)[\/|](\d+)/) {
		if (!defined($alleles[$1]) || !defined($alleles[$2])) {
		    warn('Encoding non-parental alleles as missing:') 
			if ! ($warned & 0x2);
		    if (!defined($alleles[$1])) {
			warn(sprintf('Non-parental allele detected: %u (%s:%u,%s).',
				     $1,@{$vcf}[CHROM,POS],$HEADER->[$i]));
		    }
		    if (!defined($alleles[$2])) {
			warn(sprintf('Non-parental allele detected: %u (%s:%u,%s).',
				     $2,@{$vcf}[CHROM,POS],$HEADER->[$i]));
		    }	    
		    $LOC[$i - FORMAT - 1] = '-';
		    $warned |= 0x2;
		    next;
		}
		my $a1 = $alleles[$1];
		my $a2 = $alleles[$2];
		if ($class eq 'D1.9' || $class eq 'D2.14') {
		    my $shared = log(2,$G[0] & $G[1]);
		    if ($2 eq $shared) {
			$a2 = 'c';
		    } elsif ($1 eq $shared) {
			$a1 = 'c';
		    }
		}
		if ($a1 eq $a2) { $a2 = '' }
		
 
		$LOC[$i - FORMAT - 1] 
		    = $CLASS{$class}->{join('',sort($a1,$a2))} 
		    ? join('',sort($a1,$a2)) : '-';

		if ($class eq 'A.1' && $LOC[$i - FORMAT - 1] eq 'ab') {
		    $LOC[$i - FORMAT - 1] = 'ba';
		}
	    } else {
		$LOC[$i - FORMAT - 1] = '-';
	    }
	}
	print($fdo join("\t",'*'.$locus_id,$class,join(',',@LOC)),"\n" );
    }
    progressReport();

    return 1;
}		


sub ONE_USAGE {
    die qq/
Usage:   $PROGRAM one [options] <multi_sample.vcf>

Options: -o FILE     Write output to FILE [stdout]
         -P STR,STR  Sample IDs for the population's two parents
         -p STR      Population name (no spaces allowed) ['CP_POP']
         -r PATTERN  Remove PATTERN from locus ids [null]
         -I          Use INFO-P0GT sub-field as parents (see Note 3)
         -h          This help message

Notes: 
 
  1. Requires at least three genotype\/sample fields (two parents with
     offspring).

  2. The INFO-P0GT sub-field is added by the `$PROGRAM chif1` command and is
     used in place of the observed parental genotypes to determine locus
     segregation and genotype encoding types.


/;
}


sub one {
    my (%argv,$modpat,@parents);
    my ($option,$outfile) = (0,STDIO);
    my $command = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('XhIo:P:p:r:',\%argv) && !$argv{'h'} && @ARGV == 1 || ONE_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq 'I') { $option |= 0x1 }
	elsif ($o eq 'r') { $modpat  = is_regex($argv{$o},\&ONE_USAGE)  }
	elsif ($o eq 'o') { $outfile = is_string($argv{$o},\&ONE_USAGE) }
	elsif ($o eq 'P') { @parents = is_sample(2,2,$argv{$o},\&ONE_USAGE) }
    }
    if (!($option & 0x1) && @parents != 2) {
	report('Must provide sample IDs for exactly 2 parents.');
	ONE_USAGE;
    }
    
    report('Starting ',scalar(localtime));
    report('Command-line: ',$command);

    my $fdi = open(shift(@ARGV),READ,NOSTREAM);
    my $fdo = open($outfile,WRITE);

    oneCore($fdo,$fdi,\@parents,$option,$modpat);
    
    close($fdo); 
    close($fdi);

    report('Finished ',scalar(localtime));

    return 1;
}


sub eigenCore {
    my $ofdg = is_subclass(shift,'IO::Handle');
    my $ofds = is_subclass(shift,'IO::Handle');
    my $ofdi = is_subclass(shift,'IO::Handle');
    my $fdi  = is_subclass(shift,'IO::Handle');
    my $map  = is_hashref(shift);
    
    my @I;
    my ($META,$HEADER) = getMeta($fdi);
    
    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - ($FIELD{'INFO'}+2));

    throw(sprintf('At least one sample required by %s()',
		  __FUNCTION__)) if ($nGeno < 1);

    for(my $i = $FIELD{'FORMAT'}+1; $i < $width; ++$i) {
        # printf($ofdi "%20s%2s%11s\n",$HEADER->[$i],'U','Group');
	print($ofdi join("\t",$HEADER->[$i],'U','Group'),"\n");
    }

    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) {
	progressCounter();

	if ($vcf->[FILTER] !~ PASS_FILTER_PAT) { next }
	if ($vcf->[ALT] eq NULL_FIELD) { next }
	if ($vcf->[ALT] =~ BREAKEND_PAT ) {
	    warn(sprintf(BREAKEND_MSG,@{$vcf}[CHROM,POS]));
	    next;
	}
	
	my $sca = $vcf->[CHROM];
	my $chr = $map->{$vcf->[CHROM]} // $vcf->[CHROM];
	if (! Valkyr::Data::Type::Test::is_uint($chr)) {
	    throw("Chromosome ID is required to be numeric ($chr). Please use the ",
		  "chromosome map (-C) option to translate to numeric chromosome IDs.");
	}
	
        # printf($ofds "%20s%8s%16s%16s\n",
	print($ofds join("\t",$sca.':'.$vcf->[POS],$chr,sprintf('%0.4f',0),$vcf->[POS]),"\n");
	
	my @geno;
	my %FORMAT= %{scalar( getFORMAT($vcf->[FORMAT]) )};
	for(my $i = FORMAT+1; $i < @{$vcf}; ++$i) {
	    my @g = getSample($vcf->[$i],\%FORMAT);
	    if ($g[$FORMAT{'GT'}] =~ /^(\d+)[\/|](\d+)/) {
		my $nRef = 0;
		$nRef += ($1 == 0);
		$nRef += ($2 == 0);
		push(@geno, $nRef);
	    } else {
		push(@geno, 9);
	    }
	}
	if (@geno) {
	    if (@geno != $nGeno) {
		throw(sprintf('Missing genotype(s) detected (%s:%u).',
			      @{$vcf}[CHROM,POS]));
	    }
	    print($ofdg join('',@geno),"\n");
	} else {
	    throw(sprintf('Assertion Failure: No genotypes detected (%s:%u).',
			  @{$vcf}[CHROM,POS]));
	}
    }
    progressReport();

    return 1;
}


sub EIGEN_USAGE {
    die "Usage: $PROGRAM eigen [-C <chrmap.list>] <multisample.vcf> [<out.prefix>]\n"
}


sub eigen {
    my $chrmap;
    my (%argv,$outfile);
    my $commandline = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('Xh:C:',\%argv) && !$argv{'h'} && @ARGV || EIGEN_USAGE;
    for my $o (keys %argv) {
	if ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	if ($o eq 'C') { $chrmap = is_string($argv{$o},\&EIGEN_USAGE) }
    }

    report('Starting ',scalar(localtime));
    report('Command-line: ',$commandline);

    my $infile = shift(@ARGV);
    my $prefix = shift(@ARGV) // $infile;

    my $fdi = open($infile,READ);
    my $map = defined($chrmap) ? readList($chrmap,1) : {};
    my $gen = open("$prefix.geno",WRITE);
    my $snp = open("$prefix.snp", WRITE);
    my $ind = open("$prefix.ind", WRITE);

    eigenCore($gen,$snp,$ind,$fdi,$map);
    
    close($gen) && close($snp) && close($ind) ||
	throw("Could not close all $prefix.* output files.");
    close($fdi);

    report('Finished ',scalar(localtime));

    return 1;
}


sub numShifts {
    my $array = is_uint(shift);
    my $count = 0;
    while ($array && !($array & 0x1)) {
	$array >>= 1;
	$count++;
    }
    return $count;
}


sub argtop {
    my $num = max(0,is_uint(shift) - 1);
    my $in  = is_arrayref(shift);

    my $idx = 0;
    my %idx = map  {$idx++ => is_uint($_)} @$in;
    my @top = sort {$idx{$b} <=> $idx{$a}} keys(%idx);

    return sort {$a <=> $b} @top[0..$num];
}


sub argbot {
    my $num = max(0,is_uint(shift) - 1);
    my $in  = is_arrayref(shift);

    my $idx = 0;
    my %idx = map  {$idx++ => is_uint($_)} @$in;
    my @bot = sort {$idx{$a} <=> $idx{$b}} keys(%idx);

    return sort {$a <=> $b} @bot[0..$num];
}


sub fit_model {
    my $sort     = is_bool(shift||0);
    my $error    = is_ufloat(shift);
    my $observed = is_arrayref(shift);
    my @expected = map {is_arrayref($_)} @_;
    
    my @X_max = (-1,-1,-1,-1);
    my $o_num = scalar(@$observed);
    my $N_tot = sum(map{$_ || 0} @$observed);
    
    $error = POSIX::ceil($N_tot * $error);
    
    for(my $i = 0; $i < @expected; ++$i) {
	my @o = @{$observed};
	my @e = @{$expected[$i]};
	
	my $n = max($o_num,scalar(@e));
	for (my $j = 0; $j < $n; ++$j) {
	    $e[$j] = defined($e[$j]) ? int(($e[$j] * $N_tot) + 0.5) : $error;
	    $o[$j] ||= 0;
	}
	
	if ($sort) { @o = sort {$b <=> $a} @o }
	if ($sort) { @e = sort {$b <=> $a} @e }

	my ($pvalue,$x2,$df) = Valkyr::Math::Statistics::Tests::chiSquaredTest(\@o,\@e);
	
	debug(sprintf('    P = %.3e (X^2 = %.2f; df = %u)',$pvalue,$x2,$df));
	
	if ($pvalue > $X_max[1]) {
	    @X_max  = ($i,$pvalue,$x2,$df);
	}
    }
    return @X_max;
}


sub model_f1_genotypes {
    my $alleles = is_arrayref(shift);
    my @genotypes;

    my (@P1,@P2);
    (@P1[0,1],@P2[0,1]) = map {is_uint($_)} @$alleles;
    for (my $i = 0; $i < @P1; ++$i) {
	for (my $j = 0; $j < @P2; ++$j) {
	    my ($p1,$p2) = sort {$a <=> $b} ($P1[$i],$P2[$j]);
	    $genotypes[ (($p2 * ($p2+1)) / 2) + $p1 ] += 0.25;
	}
    }
    return \@genotypes;
}


sub model_diallelic_genotypes {
    my $alleles  = is_arrayref(shift);
    my $bestfit  = is_uint(shift);
    my $shifts   = is_uint(shift||0);
    my @var      = argtop(2,$alleles);
    my @P0GTs    = qw(. . . .);

    if ($bestfit == 0) { # either 3:1 or 1:3
	if ($alleles->[$var[0]] < $alleles->[$var[1]]) { 
	    @P0GTs = (@var[0,1],@var[1,1]);
	} else {
	    @P0GTs = (@var[0,0],@var[0,1]);
	}
    } else { # 2:2
	@P0GTs = (@var[0,1],@var[0,1]);
    }

    my $model  = model_f1_genotypes(\@P0GTs);    
    if ($shifts) { @P0GTs = map {$_ + $shifts} @P0GTs }

    debug(sprintf("  Best-fit P0 (unphased) genotypes = %u/%u,%u/%u",@P0GTs));

    return($model,\@P0GTs);
}


sub model_triallelic_genotypes {
    my $alleles  = is_arrayref(shift);
    my $shifts   = is_uint(shift||0);
    my @alleles  = argtop(3,$alleles);

    my ($major)  = argmax($alleles) + $shifts;
    my ($a1,$a2) = grep {$_ != $major} @alleles;
    my $model    = model_f1_genotypes([$major,$a1,$major,$a2]);

    ($major,$a1,$major,$a2) = map {$_ + $shifts} ($major,$a1,$major,$a2);

    debug("    Major = $major; Minor = $a1,$a2");
    debug(sprintf("  Best-fit P0 (unphased) genotypes = %u/%u,%u/%u",
		  $major,$a1,$major,$a2));

    return($model,[$major,$a1,$major,$a2]);
}


sub model_tetrallelic_genotypes {
    my $shifts = is_uint(shift||0); # there should be none, but j.i.c...
    my @P0GTs  = ([0,1,2,3],[0,2,1,3],[0,3,1,2]);

    my @model;
    for (my $i = 0; $i < @P0GTs; ++$i) {
	$model[$i] = model_f1_genotypes($P0GTs[$i]);
	$P0GTs[$i] = [ map {$_ + $shifts} @{$P0GTs[$i]} ];
    }
    return(\@model,\@P0GTs);
}


sub phase_p0_genotypes {
    my $GT = is_arrayref(shift); # inferred GTs
    my $P0 = is_arrayref(shift); # observed GTs
    
    if ($#{$P0} < 1 || $#{$GT} < 1) { 
	throw('At least one parental genotype required.');
    }
    my @P0 = (0,0);
    my @GT = (0,0);
    for (my $i = 0; $i < @$GT; ++$i) {
	if (defined($GT->[$i]) && $GT->[$i] ne NULL_FIELD) {
	    $GT[int($i/2)] |= ((4**$GT->[$i]) << ($i & 1));
	}
	if (defined($P0->[$i]) && $P0->[$i] ne NULL_FIELD) {
	    $P0[int($i/2)] |= ((4**$P0->[$i]) << ($i & 1));
	}
    }

    my @phased = qw(. . . .);
    for (my $i = 0; $i < @GT; ++$i) {
	for (my $j = 0; $j < @P0; ++$j) {
	    if ($GT[$i] != $GT[!$i] && $P0[$j] == $P0[!$j]) {
		#$P0[$j] == $P0[!$j] && $GT[$i] == $P0[ $j]) {
		return($GT,0);
	    }
	    if ($GT[$i] && $P0[ $j] && $GT[$i] == $P0[ $j]) {
		$phased[2*( $j)]   = $GT->[2*( $i)];
		$phased[2*( $j)+1] = $GT->[2*( $i)+1];
		$phased[2*(!$j)]   = $GT->[2*(!$i)];
		$phased[2*(!$j)+1] = $GT->[2*(!$i)+1];
		return(\@phased,1);
	    }
	}
    }
    return($GT,0);
}


sub printVCF {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $vcf = is_arrayref(shift);
    my $opt = is_uint(shift||0);

    print($fdo join("\t",@$vcf),"\n") unless $opt & 3;
}


# sub genotype_diploid {
#     my $ad = is_arrayref(shift);
#     my $f  = is_hashref(shift||{});
#     my $dp = sum($ad);

#     # f(k;n,p) = P(X==k) choose(n,k) * p**k * (1-p)**(n-k)
#     #
#     # log10( f(k;n,p) ) = log10( choose(n,k) * p**k * (1-p)**(n-k) )
#     #   = log10( choose(n,k) ) + k * log10(p) + (n-k) * log10(1-p)
#     #
#     # and since choose(n,k) = n! / k! * (n-k)! :
#     #  log10(choose(n,k))   = sum log10(i) for i=1->n
#     #                       - sum log10(i) for i=1->k
#     #                       - sum log10(i) for i=1->n-k

#     # cache the factorials, they are expensive to recalculate
#     $f->{$dp}    //= sum(map{log(10,$_)} 1..($dp||1));
#     $f->{$ad[0]} //= sum(map{log(10,$_)} 1..($ad[0]||1));
#     $f->{$ad[1]} //= sum(map{log(10,$_)} 1..($ad[1]||1));

#     my $nCk = $f->{$dp} - ($f->{$ad[0]} + $f->{$ad[1]});
    
#     # log10(0.01) = -2.00000000000000000
#     # log10(0.50) = -1.30102999566398000
#     # log10(0.99) = -0.00436480540245009
#     return($nCk + $ad[0] * -2.00000000000000000 + $ad[1] * -0.00436480540245009,
# 	   $nCk + $ad[0] * -1.30102999566398000 + $ad[1] * -1.30102999566398000,
# 	   $nCk + $ad[0] * -0.00436480540245009 + $ad[1] * -2.00000000000000000);
# }


sub chif1Core {
    my $fdo   = is_subclass(shift,'IO::Handle');
    my $fdi   = is_subclass(shift,'IO::Handle');
    my $P0IDs = is_arrayref(shift||[]);
    my $opt   = is_uint(shift);
    my $error = is_ufloat(shift);
    my $maxAF = is_ufloat(shift);
    my $maxGF = is_ufloat(shift);
    my $cmd   = is_string(shift || '');

    my ($META,$HEADER) = getMeta($fdi);    
    
    my @P0idx;
    my @F1IDs;
    my $REQD  = ['AD','FT'];
    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - ($FIELD{'INFO'}+2));

    my @META  = (
	['FORMAT','FT','String',1,'"Genotype-level filter"'],
	['INFO','F1AFP','Float',1,'"Phred-scaled P-value for best-fit AF model"'],
	['INFO','F1GTP','Float',1,'"Phred-scaled P-value for departure from '.
	 'Mendelian frequencies, given the best-fit AF model"'                  ],
	['INFO','F1X2', 'Float',1,'"Chi-squared value for departure from '.
	 'Mendelian frequencies, given the best-fit AF model"'                  ],
	['INFO','P0GT','String',2,'"The most-likely parental genotypes, given '.
	 'the best-fit AF model (both haplotypes and source-parents unphased)"' ],
	);

    throw(sprintf('At least five samples required by %s().',
		  __FUNCTION__)) if ($nGeno < 5);
    warn(sprintf('At least ten diploid samples required '.
		  'by %s() for accurate calculations.',
		  __FUNCTION__)) if ($nGeno < 10);

    if (@$P0IDs) {
	report('INFO-P0GT phasing: enabled');

	for (my $i = 0; $i < @$P0IDs; ++$i) {
	    if (! defined( $FIELD{ is_string($P0IDs->[$i]) })) {
		throw ValueError("Sample ID not found in VCF: $P0IDs->[$i]");
	    } elsif ($FIELD{ $P0IDs->[$i] } <= $FIELD{'FORMAT'}) {
		throw ValueError("Invalid Sample ID: $P0IDs->[$i]");
	    }
	    $P0idx[$i] = $FIELD{$P0IDs->[$i]}; #  - FORMAT - 1;
	}
	for(my $i = FORMAT + 1; $i < @$HEADER; ++$i) {
	    if (($HEADER->[$i] ne $P0IDs->[0]) && ($HEADER->[$i] ne $P0IDs->[1])) {
		push(@F1IDs,$HEADER->[$i]);
	    }
	}
	if ($FIELD{$P0IDs->[0]} > $FIELD{$P0IDs->[1]}) {
	    @P0idx  = reverse(@P0idx);
	    @$P0IDs = reverse(@$P0IDs);
	}
	push(@META,['INFO','P0PHASED','Flag',0,'"Imputed P0 genotypes '.
		    'are ordered (phased) with respect to the parent IDs"']);
    } else {
	report('INFO-P0GT phasing: disabled');

	@F1IDs = @{$HEADER}[(FORMAT+1)..$#{$HEADER}];
    }

    if ($opt & 0x2) {
	report('Output format: table');
	print($fdo join("\t",
	    qw(CHROM POS REF ALT F1AFP F1GTP F1X2 df P0GT P0PHASED)),"\n"
        );
    } else {
	report('Output format: VCF');
	addMeta($META,'VCFtk',
		{
		    '_ORDER_' => [qw(ID Version Date CommandlineOptions)],
		    'ID'      => 'chif1',
		    'Version' => $VERSION,
		    'Date'    => '"'.scalar(localtime).'"',
		    'CommandlineOptions' => '"'.$cmd.'"'
		});
	addMeta($META,'FILTER',
		{
		    '_ORDER_' => [qw(ID Description)],
		    'ID'      => 'FAILX2',
		    'Description' => '"Locus exceeds F1 allele (F1AFP) or genotype (F1GTP) phred-scaled chi-squared P-value threshold"'
		});
	while (my $meta = shift(@META)) {
	    addMeta($META,$meta->[0],
	        {
		    '_ORDER_' => [qw(ID Number Type Description)],
		    'ID'      => $meta->[1],
		    'Type'    => $meta->[2],
		    'Number'  => $meta->[3],
		    'Description' => $meta->[4],
		});
	}
	print($fdo setMeta($META,$HEADER),"\n");
    }
    
    
    my $fact = {};
    my $multi = 10;
    my $level = 1e3;
    my $num_totl = 0;
    my $num_site = 0;
    my $num_filt = 0;
    my $num_geno = 0;
    my $num_phzd = 0;
    my @AFMODELS = (
	[0.75,0.25],[0.50,0.50], # 2 alleles
	[0.50,0.25,0.25],        # 3 alleles 
	[0.25,0.25,0.25,0.25]    # 4 alleles
    );
    
    # Now, loop over every input VCF line and do some necessary 
    # checks to make sure the data are complete:
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) {
	progressCounter();
	$num_totl++;

	if ($vcf->[FILTER] !~ PASS_FILTER_PAT) {
	    printVCF($fdo,$vcf,$opt); next;
	}
	if ($vcf->[ALT] eq NULL_FIELD) { 
	    printVCF($fdo,$vcf,$opt); next;
	}
	if ($vcf->[ALT] =~ BREAKEND_PAT) {
	    warn(sprintf(BREAKEND_MSG,@{$vcf}[CHROM,POS]));
	    printVCF($fdo,$vcf,$opt); next;
	}
	
	
	# Because of soft filtering (the presence of non-PASS/non-'.' tags),
	# we cannot trust the INFO-AF tag, so iterate over samples and 
	# collect this information. We (often) encounter sites with two non-
	# reference alleles segregating amongst the offspring (and because
	# tallying allele counts depends on allele index (0,1,2,...) we must
	# "shift down" the allele count array so that the zeroth element is
	# always occupied:
	
	my $shifts = 0;
	my $num_alleles = 0;
	my $num_genotyped = 0;
	my ($alleles,@alleles,@genotypes,@SAMPLES);
	my %FILTER = %{scalar( getFILTER($vcf->[FILTER]) )};
	my %FORMAT = %{scalar( getFORMAT($vcf->[FORMAT],$REQD) )};
	if ($opt & 0x10) {
	    for my $I (@F1IDs) { # (my $i  = FORMAT+1; $i < @{$vcf}; ++$i) {
		my @s  = getSample($vcf->[$FIELD{$I}],\%FORMAT);
		push(@SAMPLES,\@s);

		if ($s[$FORMAT{'GT'}] =~ /\./) { next}
		if ($s[$FORMAT{'AD'}] eq NULL_FIELD) { next }
		if ($s[$FORMAT{'FT'}] !~ PASS_FILTER_PAT ) { next }
		
		my $a = 0;
		map {$alleles[$a++]+=is_uint($_)} split(ALLELE_FIELD_SEP,$s[$FORMAT{'AD'}]);
	    }
	    while (@alleles && (($alleles[0]||0) < 1)) {
		shift(@alleles);
		$shifts++;
	    }
	    $num_alleles = @alleles;

	    if ($num_alleles > 1) { 
		@alleles  = map {$_ || 0} @alleles;
		for(my $i = 0; $i < @SAMPLES; ++$i) {
		    my @s = @{$SAMPLES[$i]};
		    if ($s[$FORMAT{'GT'}] =~ /\./) { next }
		    if ($s[$FORMAT{'AD'}] eq NULL_FIELD) { next }
		    if ($s[$FORMAT{'FT'}] !~ PASS_FILTER_PAT) { next }
		    
		    my @gt = split(GT_FIELD_SEP,$s[$FORMAT{'GT'}]);
		    my @ad = split(ALLELE_FIELD_SEP,$s[$FORMAT{'AD'}]);
		    
		    $genotypes[ ((($gt[1]-$shifts) * ($gt[1]-$shifts+1)) / 2) + ($gt[0]-$shifts) ] += (
			$gt[0] == $gt[1] ? $ad[$gt[0]] : $ad[$gt[0]]+$ad[$gt[1]]
			);
		    $num_genotyped++;
		}
	    }
	} else {
	    for my $I (@F1IDs) { # (my $i  = FORMAT+1; $i < @{$vcf}; ++$i) {
		my @s  = getSample($vcf->[$FIELD{$I}],\%FORMAT);

		push(@SAMPLES,\@s);
		
		if ($s[$FORMAT{'GT'}] =~ /\./) { next }
		if ($s[$FORMAT{'FT'}] !~ PASS_FILTER_PAT ) { next }
		
		my @geno = split(GT_FIELD_SEP,$s[$FORMAT{'GT'}]);
		$alleles[$geno[0]]++;
		$alleles[$geno[1]]++;
	    }
	    while (@alleles && (($alleles[0]||0) < 1)) {
		shift(@alleles);
		$shifts++;
	    }
	    $num_alleles = @alleles;

	    if ($num_alleles > 1) { 
		@alleles  = map {$_ || 0} @alleles;
		for(my $i = 0; $i < @SAMPLES; ++$i) {
		    my @s = @{$SAMPLES[$i]};
		    if ($s[$FORMAT{'GT'}] =~ /\./) { next }
		    if ($s[$FORMAT{'FT'}] !~ PASS_FILTER_PAT) { next }
		    
		    my @geno  = sort split(GT_FIELD_SEP,$s[$FORMAT{'GT'}]);
		    $geno[0] -= $shifts;
		    $geno[1] -= $shifts;
		    
		    $genotypes[ (($geno[1] * ($geno[1]+1)) / 2) + $geno[0] ]++;
		    $num_genotyped++;
		}
	    }
	}
	debug(join(':',@{$vcf}[CHROM,POS]));
	
	# We want to determine:
	# The probability of obtaining genotype frequencies deviating more or 
	# as extreme as the observed genotype frequencies, given the observed 
	# allele frequencies. More formally stated:
	
	# Ho: The site genotype frequencies do not deviate from those dictated
	#     by Mendel's Law of Segregation, given the observed allele freq.
	# Ha: The site genotype frequencies deviate from Mendelian frequencies,
	#     given the observed allele frequencies.
	
	# To determine which model of F1 genotype frequencies to perform our 
	# Chi-Squared test for depature from Menelian frequencies against, the
	# best-fit model of F1 allele frequencies must first be determined. I
	# read a bit about the Bonferroni-Holm Correction and determined it was
	# irrelevant--even though it is meant to correct for multiple testing--
	# because: 1) the method below is just to sort for the largest (least
	# significant) P-value and test only it against the value of alpha;
	# 2) the Bonferroni-Holm corrected alpha = raw alpha for the least
	# significant P-value; and 3) a non- Bonferroni-corrected P-value for 
	# departure from Ho is actually more conservative.
	
	# Do the goodness-of-fit test for allele frequencies:
	my ($bestfitAF,$pvalueAF,$phased) = (-1,-1,0);
	if (($num_alleles < 5) && ($num_genotyped > 4)) {
	    ($bestfitAF,$pvalueAF) = fit_model(1,$error,\@alleles,@AFMODELS);
	
	} else {
	    if (($num_alleles < 2) || ($num_genotyped < 5)) {
		warn(sprintf('Insufficient number of genotyped alleles (%u), skipping (%s:%u).',
			     $num_alleles,@{$vcf}[CHROM,POS])) unless $opt & 0x4;
	    } else {
		warn(sprintf('Excessive number of genotyped alleles (%u), skipping (%s:%u).',
			     $num_alleles,@{$vcf}[CHROM,POS])) unless $opt & 0x4;
	    }
	    $FILTER{'FAILX2'} = 1;
	    $vcf->[FILTER] = setFILTER(\%FILTER);
	    printVCF($fdo,$vcf,$opt); next;
	}
	
	debug(sprintf('  Best-fit AF model: P = %3e; Model = [%s];'.
		      ' Observed = [%s]',$pvalueAF, $bestfitAF < 0 ? ('','') :
		      (join(',',@{$AFMODELS[$bestfitAF]}),join(',',@alleles))))
	    if $Valkyr::Error::DEBUG;
	
	
	# If our P-value for F1 allele frequencies is too low, we cannot
	# confidently estimate the Mendelian genotype frequencies. Set P-value
	# for genotype frequencies to 0.0 to reflect this. The specific 
	# threshold set below will depend on the number of samples observed, 
	# but this value should be lenient enough to exclude sites at a low 
	# false-negative rate:
	
	# Do the goodness-of-fit test for genotype frequencies:	
	my ($bestfitGT,$pvalueGT,$x2,$df,$model,$P0GTs) 
	    = (-1,-1,0,0,[],['.','.','.','.']);
	if (float2phred($pvalueAF,'1e-999.9') > $maxAF) {
	    ($bestfitGT,$pvalueGT,$x2,$df) = (-1,0,0,0);
	    $FILTER{'FAILX2'} = 1;
	    $num_filt++;

	} elsif (($bestfitAF > 0) && ($opt & 0x8)) {
	    # User has requested to fail any sites inconsistent
	    # with being an assumed pseudo-testcross. Fail:
	    ($bestfitGT,$pvalueGT,$x2,$df) = (-1,0,0,0);
	    $FILTER{'FAILX2'} = 1;
	    $num_filt++;

	} elsif ($bestfitAF <= 1) {
	    ($model,$P0GTs) = model_diallelic_genotypes(\@alleles,$bestfitAF,$shifts);
	    ($bestfitGT,$pvalueGT,$x2,$df) = fit_model(0,$error,\@genotypes,$model);
	    
	} elsif ($bestfitAF == 2) {
	    ($model,$P0GTs) = model_triallelic_genotypes(\@alleles,$shifts);
	    ($bestfitGT,$pvalueGT,$x2,$df) = fit_model(0,$error,\@genotypes,$model);

	} elsif ($bestfitAF == 3) {
	    my $p0gts = [];
	    ($model,$p0gts) = model_tetrallelic_genotypes($shifts);
	    ($bestfitGT,$pvalueGT,$x2,$df) = fit_model(0,$error,\@genotypes,@$model);
	    if ($bestfitGT >= 0) {
		$P0GTs = $p0gts->[$bestfitGT];
		$model = $model->[$bestfitGT];
	    } else {
		$model = [0,0,0,0];
		$FILTER{'FAILX2'} = 1;
	    }
	} else {
	    throw(sprintf('Assertion failure: site AF out of range (%u > 3) (%s:%u).',
			  $bestfitAF,@{$vcf}[CHROM,POS]));
	}
	debug(sprintf('  Tested GT model: %s; P = %3e; Model = [%s]; Observed = [%s]',
		      $bestfitGT,$pvalueGT,join(',',map {$_ || 0} @$model),
		      join(',',map {$_ || 0} @genotypes))) if $Valkyr::Error::DEBUG;
	
	# Since the parental genotypes have been inferred above, we now just
	# need to phase the genotypes w.r.t source-parent:
	if ($bestfitGT >= 0 && @P0idx) {
	    ($P0GTs,$phased) = phase_p0_genotypes($P0GTs, 
	        [
		    map {split(GT_FIELD_SEP,(getSample($vcf->[$_],\%FORMAT))[$FORMAT{'GT'}])} @P0idx
		]
	    );
	}

	# Convert our INFO to something more VCF-ish:
	$P0GTs    = sprintf('%s/%s,%s/%s',@$P0GTs);
	$pvalueAF = sprintf('%.3f',float2phred($pvalueAF, '1e-999.9'));
	$pvalueGT = sprintf('%.3f',float2phred($pvalueGT, '1e-999.9'));
	$x2       = sprintf('%.3f',$x2);
	$df       = int($df);
	
	# Now, as promised, add F1AFP, F1GTP, F1X2, and F1df tags to the 
	# VCF, or output a list of scores a-la `vcftools --get-INFO <tag>`
	if ($pvalueGT > $maxGF) {
	    $FILTER{'FAILX2'} = 1;
	    $phased = 0;
	} else {
	    $num_phzd += $phased;
	    $num_geno++;
	}
	if ($opt & 0x2) { 
	    @{$vcf}  = (@{$vcf}[CHROM,POS,REF,ALT],
			$pvalueAF,$pvalueGT,$x2,$df,$P0GTs,$phased);
	    
	} else {
	    my $INFO = getINFO($vcf->[INFO]);
	    
	    $INFO->{'F1AFP'}    = $pvalueAF;
	    $INFO->{'F1GTP'}    = $pvalueGT;
	    $INFO->{'F1X2'}     = $x2;
	    $INFO->{'P0GT'}     = $P0GTs;
	    $INFO->{'P0PHASED'} = undef if $phased;
	    
	    $vcf->[FILTER] = setFILTER(\%FILTER);
	    $vcf->[INFO]   = setINFO($INFO);
	}
	
	print($fdo join("\t",@{$vcf}),"\n");

	$num_site++;
    }
    progressReport();
    report('Input samples: ',$nGeno||0);
    report(sprintf('AF-filtered sites: %u / %u (%.4f)',
		   $num_filt,$num_site,($num_filt||0)/($num_site||1)));
    report(sprintf('INFO-P0GT imputed: %u / %u (%.4f)',
		   $num_geno,$num_site,($num_geno||0)/($num_site||1)));
    report(sprintf('INFO-P0GT phased: %u / %u (%.4f)',
		   $num_phzd,$num_geno,($num_phzd||0)/($num_geno||1)));

    return 1;
}


sub CHIF1_USAGE {
    die qq/
Usage:   $PROGRAM chif1 [options] <in.vcf>

Options: -o FILE      Write output to FILE [stdout]
         -A FLOAT     Max. phred allele freq. best-fit P-val [50.0]
         -G FLOAT     Max. phred genotype freq. best-fit P-val [9999.0]
         -P STR[,STR] Parental IDs for the input population (recommended)
         -e INT       Phred-scaled genotype-call error (see Notes 3) [30]
         -a           Perform chi-sqr on allele depths (AD) field [GT]
         -F           Exclude sites for which scores cannot be applied
         -p           Force pseudo-testcross markers only
         -S           Silence\/disable verbose reporting
         -t           Write output as a table (see Note 4) [VCF]
         -h           This help message

Notes:

  1. This script applies a chi-squared goodness-of-fit test for Mendelian 
     genotype frequencies, for a determined allele frequency class
     (3:1, 1:1, etc). The allele frequency class is selected within a 
     goodness-of-fit tolerance threshold set by the '-A' option. The chi-
     squared values calculated here have been compared to those calculated
     by JoinMap4 and there is (almost) complete agreement. 

  2. Parental genotypes are inferred and stored by the 'P0GT' key in the 
     INFO field. If the IDs of the parents are passed via the '-P' option,
     and the samples are included in the input VCF, an attempt to orient
     the inferred genotype calls with respect to parent (determined by the 
     relative order of the parental samples listed in the VCf header) is 
     made. If successful, a 'P0PHASED' key is applied to the INFO field.

  3. Requires at least five diploid F1 samples (ten or more recommended). 

  4. Yates' correction for continuity is applied to sites with less than
     ten observed chromosomes, and no calculations are performed on sites
     with less than five observed chromosomes.

  5. Value passed to the '-e' argument must be a positive integer. It is
     recommended to set '-e' to the minimum GQ value use for filtering.

  6. When enabling the '-a' flag, and the input data are at low-coverage, 
     it is necessary to include the parental IDs via '-P' to calculate the
     P-value statistics accurately.

  7. The default output format is VCF, the '-t' option outputs a tab-
     delimited table (list of sub-field values) of relevant statistics
     a-la `vcftools --get-INFO`.


/;
}


sub chif1 {
    load('POSIX');
    load('Valkyr::Math::Statistics::Tests');
    
    my (%argv,@parents);
    my ($option,$gterror,$afthrsh,$gfthrsh,$outfile) = (0,30,50,9999,STDIO);
    my $command = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('XFSahptA:G:e:o:P:',\%argv) && !$argv{'h'} && @ARGV == 1 || CHIF1_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'F') { $option |= 0x1 }
	elsif ($o eq 't') { $option |= 0x2 }
	elsif ($o eq 'S') { $option |= 0x4 }
	elsif ($o eq 'p') { $option |= 0x8 }
	elsif ($o eq 'a') { $option |= 0x10 }
	elsif ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq 'A') { $afthrsh = is_uint($argv{$o},\&CHIF1_USAGE) }
	elsif ($o eq 'G') { $gfthrsh = is_uint($argv{$o},\&CHIF1_USAGE) }
	elsif ($o eq 'e') { $gterror = is_uint($argv{$o},\&CHIF1_USAGE) }
	elsif ($o eq 'o') { $outfile = is_string($argv{$o},\&CHIF1_USAGE) }
	elsif ($o eq 'P') { @parents = is_sample(1,2,$argv{$o},\&CHIF1_USAGE) }
    }
    if (defined($gterror)) {
	if ($gterror == 0) { 
	    throw("GQ value '$gterror' is not of expected type: ",
		  'positive unsigned int.');
	} elsif ($gterror > 99) {
	    throw("GQ value upper-bound (99) exceeded: $gterror.");
	}
	$gterror = 10 ** ( -$gterror / 10 );
    }
    
    report('Starting ',scalar(localtime));
    report('Command-line: ',$command);

    my $fdi = open(shift(@ARGV),READ);
    my $fdo = open($outfile,WRITE);

    chif1Core($fdo,$fdi,\@parents,$option,$gterror,$afthrsh,$gfthrsh,$command);

    close($fdo);
    close($fdi);

    report('Finished ',scalar(localtime));
    
    return 1;
}


sub initPseudoPL {
    my $mx = is_uint(shift);
    my $a1 = is_uint(shift);
    my $a2 = is_uint(shift);

    if ($a1 eq NULL_FIELD || $a2 eq NULL_FIELD) { return NULL_FIELD }

    ($a2,$a1) = sort {$b<=>$a} ($a1,$a2);
    
    my @pl = map {9999} (0..(($mx*($mx+1)/2)+$mx));
    
    $pl[ ($a2*($a2+1)/2)+$a1 ] = 0;

    return join(',',@pl) || '.';
}


sub initbCore {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $fdi = is_subclass(shift,'IO::Handle');
    my $ped = is_subclass(shift,'IO::Handle');
    my $fam = is_string(shift);
    my $fat = is_string(shift);
    my $mot = is_string(shift);
    my $gtp = is_ufloat(shift);
    my $cmd = is_string(shift || '');
    
    my ($META,$HEADER) = getMeta($fdi);
    
    my @P0ID;
    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - ($FIELD{'INFO'}+2));
    
    if ($nGeno < 3) {
	throw("At least 3 samples required.");
    }
    if (!defined($FIELD{$fat}) || !defined($FIELD{$mot})) {
	throw("Parental ID not found in VCF Meta header.");
    } elsif ($FIELD{$fat} < $FIELD{$mot}) {
	@P0ID = map {'P0.'.$_} ($fat,$mot);
    } else {
	@P0ID = map {'P0.'.$_} ($mot,$fat);
    }

    splice(@$HEADER,FORMAT+1,0,@P0ID);


    addMeta($META,'FORMAT',
        {
	    '_ORDER_' => [qw(ID Number Type Description)],
	    'ID'      => 'PL',
	    'Type'    => 'Integer',
	    'Number'  => 'G',
	    'Description' => '"Normalized, Phred-scaled likelihoods for '.
		'genotypes as defined in the VCF specification"'
	});
    addMeta($META,'VCFtk',
        {
	    '_ORDER_' => [qw(ID Version Date CommandlineOptions)],
	    'ID'      => 'initb',
	    'Version' => $VERSION,
	    'Date'    => '"'.scalar(localtime).'"',
	    'CommandlineOptions' => '"'.$cmd.'"'
	});

    print($fdo setMeta($META,$HEADER),"\n");

    my $num_sites = 0;
    my $notphased = 0;
    my $gtpfiltered = 0;
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) {
	progressCounter();
	$num_sites++;

	my $INFO   = getINFO($vcf->[INFO]);
        my $FORMAT = getFORMAT($vcf->[$FIELD{'FORMAT'}],['GQ','PL']);
        if (exists($INFO->{'P0PHASED'})) {
            if (! defined($INFO->{'P0GT'})) {
                throw('INFO-P0PHASED field detected without INFO-P0GT.');
            }
            if (! defined($INFO->{'F1GTP'})) {
                throw('INFO-P0PHASED field detected without INFO-F1GTP.');
            } elsif ($INFO->{'F1GTP'} > $gtp) {
		$gtpfiltered++;
		next;
	    }
	} else {
	    $notphased++;
	}
	
	my $nA = scalar(grep {!/\./} split(',',$vcf->[$FIELD{'ALT'}]));
	my @GT = split(',',$INFO->{'P0GT'});
	for(my $i = 0; $i < @GT; ++$i) {
	    my @g = getSample($GT[$i],$FORMAT);
	    $g[$FORMAT->{'PL'}] = initPseudoPL($nA,split(GT_FIELD_SEP,$g[$FORMAT->{'GT'}]));
	    $g[$FORMAT->{'GQ'}] = MAX_GQ;
	    $GT[$i] = join(':',@g);
	}
	
	splice(@$vcf,$FIELD{'FORMAT'}+1,0,@GT);

	print($fdo join("\t",@$vcf),"\n");
	
    }

    for(my $i = $FIELD{'FORMAT'}+1; $i < @$HEADER; ++$i) {
	if ($HEADER->[$i] eq $P0ID[0] || $HEADER->[$i] eq $P0ID[1]) {
	    print($ped join("\t",$fam,$HEADER->[$i],0,0),"\n");
	} elsif ($HEADER->[$i] eq $fat) { 
	    print($ped join("\t",$fam,$HEADER->[$i],"P0.$fat","P0.$fat"),"\n");
	} elsif ($HEADER->[$i] eq $mot) { 
	    print($ped join("\t",$fam,$HEADER->[$i],"P0.$mot","P0.$mot"),"\n");
	} else {
	    print($ped join("\t",$fam,$HEADER->[$i],"P0.$fat","P0.$mot"),"\n");
	}
    }
    progressReport();

    report(sprintf('Total sites: %u',$num_sites||0));
    report(sprintf('Unphased sites: %.4f (%u / %u)',
		   ($notphased||0)/($num_sites||1),$notphased,$num_sites));
    report(sprintf('F1GTP-filtered sites: %.4f (%u / %u)',
		   ($gtpfiltered||0)/($num_sites||1),$gtpfiltered,$num_sites));

    return 1;
}


sub INITB_USAGE {
    die qq/
Usage:    $PROGRAM initb [options] <in.vcf> <out.prefix>

Required: -1 STR   Sample ID for male parent [null]
          -2 STR   Sample ID for female parent [null]
          -F STR   Family ID [null]

Options:  -G INT   Max. Phred-scaled F1GTP best-fit P-value [20]
          -h       This help message

Notes:

  1. Adds the imputed genotypes (INFO-P0GT sub-field) among the Samples as 
     P0.<parent.id> with Sample-PL fields set to '9999' in preparation for
     running BEAGLEv4 to phase genotypes.


/;
}


sub initb {
    warn DeprecationWarning("This function will be removed in the future");
    my %argv;
    my $gtpval = 20;
    my ($family,$mother,$father);
    my $commandline = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('Xh1:2:F:G:',\%argv) && !$argv{'h'} && @ARGV == 2 || &INITB_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq '1') {($father) = is_sample(1,1,$argv{$o},\&INITB_USAGE) }
	elsif ($o eq '2') {($mother) = is_sample(1,1,$argv{$o},\&INITB_USAGE) }
	elsif ($o eq 'F') { $family  = is_string($argv{$o},\&INITB_USAGE) }
	elsif ($o eq 'G') { $gtpval  = is_ufloat($argv{$o},\&INITB_USAGE) }
    }
    if (! Valkyr::Data::Type::Test::is_string($father) || 
    	! Valkyr::Data::Type::Test::is_string($mother) || ($father eq $mother)) {
        throw("Must provide sample IDs for exactly 2 parents.");
    } elsif (! Valkyr::Data::Type::Test::is_string($family)) {
        throw("Must provide a family ID.");
    }

    report('Starting ',scalar(localtime));
    report('Command-line: ',$commandline);
    
    my $infile = is_string(shift(@ARGV));
    my $prefix = is_string(shift(@ARGV)); 
    my $fdi = open($infile,READ);
    my $ped = open("$prefix.ped",WRITE);
    my $fdo = open("$prefix.vcf",WRITE);

    initbCore($fdo,$fdi,$ped,$family,$father,$mother,$gtpval,$commandline);

    $fdo->close();
    $ped->close();
    $fdi->close();

    report('Finished ',scalar(localtime));
    
    return 1;
}


sub mergebCore {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $fdi = is_subclass(shift,'IO::Handle');
    my $bgl = is_subclass(shift,'IO::Handle');
    my $cmd = is_string(shift || '');

    my ( $META, $HEADER) = getMeta($fdi);
    my ($BMETA,$BHEADER) = getMeta($bgl);

    my $width  = @$HEADER;
    my $bwidth = @$BHEADER;
    my %FIELD  = %{scalar( indexArray(@$HEADER) )};
    my %BFIELD = %{scalar( indexArray(@$BHEADER) )};
    my $nGeno  = max(0, $width - ($FIELD{'INFO'}+2));
    my $bnGeno = max(0,$bwidth - ($BFIELD{'INFO'}+2));

    my @P0ID   = grep {/^P0\./} @$BHEADER;

    throw(sprintf('Unexpected number of parental Sample IDs (%u != 2; %s).',
		  scalar(@P0ID),join(',',@P0ID))) if (@P0ID != 2);
    throw(sprintf('At least two sample required by %s().',
		  __FUNCTION__)) if ($nGeno < 2);


    for my $field (keys(%$BMETA)) {
	if (Valkyr::Data::Type::Test::is_string($BMETA->{$field})) {
	    addMeta($META,$field,$BMETA->{$field});
	} else {
	    for my $subfield (keys(%{$BMETA->{$field}})) {
		if ($subfield eq '_ORDER_') { next }
		addMeta($META,$field,$BMETA->{$field}->{$subfield});
	    }
	}
    }
    
    addMeta($META,'VCFtk',
        {
	    '_ORDER_' => [qw(ID Version Date CommandlineOptions)],
	    'ID'      => 'mergeb',
	    'Version' => $VERSION,
	    'Date'    => '"'.scalar(localtime).'"',
	    'CommandlineOptions' => '"'.$cmd.'"'
	});


    print($fdo setMeta($META,$HEADER),"\n");

    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) {
	my $bvcf = getEntry($bgl,$bwidth);
	progressCounter();

	if ($vcf->[$FIELD{'POS'}] != $bvcf->[$BFIELD{'POS'}] ||
	    $vcf->[$FIELD{'CHROM'}] ne $bvcf->[$BFIELD{'CHROM'}]) {
	    throw(sprintf('Mis-mapped line in beagle-output (%s:%u != $s:%u).',
		@{$bvcf}[@BFIELD{'CHROM','POS'}],@{$vcf}[@FIELD{'CHROM','POS'}]));
	}
	
	my $INFO  = getINFO($vcf->[$FIELD{'INFO'}]);
	my $BINFO = getINFO($bvcf->[$BFIELD{'INFO'}]);
	for my $subfield (keys(%$BINFO)) {
	    $INFO->{$subfield} = $BINFO->{$subfield};
	}
	
	my %BFORMAT = %{scalar( getFORMAT($bvcf->[$BFIELD{'FORMAT'}]) )};
	my %FORMAT  = %{scalar( getFORMAT($vcf->[$FIELD{'FORMAT'}],[keys(%BFORMAT)]) )};
	
	# transfer phased P0.* parent genotypes to INFO-P0GT field
	my @P0GT;
	for(my $i = 0; $i < @P0ID; ++$i) {
	    my @b = getSample($bvcf->[$BFIELD{$P0ID[$i]}],\%BFORMAT);
	    $P0GT[$i] = $b[$BFORMAT{'GT'}];
	}
	$INFO->{'P0GT'} = join(',',@P0GT);

	my @fmt = keys(%BFORMAT);
	for(my $i = $FIELD{'FORMAT'}+1; $i < @$HEADER; ++$i) {
	    my $I = $HEADER->[$i];
	    my @b = getSample($bvcf->[$BFIELD{$I}],\%BFORMAT);
	    my @g = getSample($vcf->[$FIELD{$I}],\%FORMAT);
	    for my $f (@fmt) {
		$g[$FORMAT{$f}] = $b[$BFORMAT{$f}];
	    }
	    $vcf->[$i] = join(':',@g);
	}

	$vcf->[$FIELD{'FORMAT'}] = setFORMAT(\%FORMAT);
	$vcf->[$FIELD{'INFO'}] = setINFO($INFO);

	print($fdo join("\t",@$vcf),"\n");
    }
    progressReport();
    
    return 1;
}


sub MERGEB_USAGE {
    die "Usage: $PROGRAM mergeb [-o FILE] <unphased.vcf> <phased.vcf>\n";
}


sub mergeb {
    warn DeprecationWarning("This function will be removed in the future");
    my %argv;
    my $outfile = STDIO;
    my $commandline = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('Xho:',\%argv) && !$argv{'h'} && @ARGV == 2 || &MERGEB_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq 'o') { $outfile = is_string($argv{$o},\&MERGEB_USAGE) }
    }
    
    report('Starting ',scalar(localtime));
    report('Command-line: ',$commandline);


    my $fdi = open(shift(@ARGV),READ);
    my $bgl = open(shift(@ARGV),READ);
    my $fdo = open($outfile,WRITE);

    mergebCore($fdo,$fdi,$bgl);
    
    $fdo->close();
    $bgl->close();
    $fdi->close();

    report('Finished ',scalar(localtime));
    
    return 1;
}


sub runRscript {
    my $Rinterp = is_file(shift,'x');
    my $Rscript = is_file(shift,'r');
    my $cleanup = is_uint(shift||0);

    report("Rscript command: $Rinterp $Rscript");

    my $status = `$Rinterp $Rscript 2>&1`;

    if ($Valkyr::Error::DEBUG) {
	map {debug($_)} split("\n",$status);
    }
    if ($cleanup) {
	unlink($Rscript);
    }

    return 1;
}


sub ibsRscriptWrite {
    my $fdo     = is_subclass(shift,'IO::Handle');
    my $Rscript = is_file(shift,'x');
    my $ibsdata = is_file(shift,'rf');
    my @P0ID    = @_;

    $P0ID[0] //= 'P01';
    $P0ID[1] //= 'P02';

    report('Writing Rscript');

    print($fdo "#!$Rscript\n");
    printf($fdo "# Auto-generated by %s ibs %s\n\n",$PROGRAM,scalar(localtime));
    print($fdo "d  = read.table('$ibsdata', header=T, row.names=NULL);\n");
    print($fdo "pp = d[which((d\$INDV1 == '$P0ID[0]' & d\$INDV2 == '$P0ID[1]') | ",
	  "(d\$INDV1 == '$P0ID[1]' & d\$INDV2 == '$P0ID[0]')),];\n");
    print($fdo "po = d[which(!(d\$INDV1 == pp\$INDV1 & d\$INDV2 == pp\$INDV2) & ",
	  "(d\$INDV1 == pp\$INDV1 | d\$INDV2 == pp\$INDV2)),];\n");
    print($fdo "oo = d[which(!(d\$INDV1 == pp\$INDV1 & d\$INDV2 == pp\$INDV2)),];\n");
    print($fdo "\n");
    print($fdo "pdf('$ibsdata.2hat.pdf');\n");
    print($fdo "plot(oo\$IBS2hatRatio, oo\$IBS2Ratio, pch='+', cex=0.5, ",
	  "xlim=range(d\$IBS2hatRatio), ylim=range(d\$IBS2Ratio), ",
	  "xlab='(IBS1 + IBS2*) / (IBS0 + IBS1 + IBS2*)', ",
	  "ylab='IBS2 / (IBS0 + IBS1 + IBS2)', ",
	  "main='IBS2hat-ratio vs IBS2-ratio', ",
	  "col='grey10', sub='Blue = parent-parent, Red = parent-offspring, ",
	  "Black = offspring-offspring');\n");
    print($fdo "points(po\$IBS2hatRatio, po\$IBS2Ratio, pch='+', cex=0.5, col='red');\n");
    print($fdo "points(pp\$IBS2hatRatio, pp\$IBS2Ratio, cex=0.7, col='blue');\n");
    print($fdo "invisible(dev.off());\n");
    print($fdo "\n");
    print($fdo "pdf('$ibsdata.2star.pdf');\n");
    print($fdo "plot(oo\$IBS2starRatio, oo\$pctInformative, pch='+', cex=0.5, ",
	  "xlim=range(d\$IBS2starRatio), ylim=range(d\$pctInformative), ",
	  "xlab='IBS2*-ratio', ylab='Percent Informative SNPs', ",
	  "main='IBS2*-ratio vs Percent Informative SNPs', ",
	  "col='grey10', sub='Blue = parent-parent, Red = parent-offspring, ",
	  "Black = offspring-offspring');\n");
    print($fdo "points(po\$IBS2starRatio, po\$pctInformative, pch='+', cex=0.5, col='red');\n");
    print($fdo "points(pp\$IBS2starRatio, pp\$pctInformative, cex=0.7, col='blue');\n");
    print($fdo "invisible(dev.off());\n");

    return 1;
}


sub ibs2hatRatio {
    my $ibs = is_arrayref(shift);
    
    return( ($ibs->[1] + $ibs->[3]) / 
	    ($ibs->[0] + $ibs->[1] + $ibs->[3]) );
}


sub ibs2Ratio {
    my $ibs = is_arrayref(shift);
    
    return(  $ibs->[2] / 
	    ($ibs->[0] + $ibs->[1] + $ibs->[2]) );
}


sub ibs2starRatio {
    my $ibs = is_arrayref(shift);

    return( $ibs->[3] / ($ibs->[3] + $ibs->[0]) );
}


sub pctInformative {
    my $ibs = is_arrayref(shift);
    
    return( ($ibs->[0] + $ibs->[3]) / 
	    ($ibs->[0] + $ibs->[1] + $ibs->[2]) );
}


sub ibsCore {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $fdi = is_subclass(shift,'IO::Handle');

    my ($META,$HEADER) = getMeta($fdi);

    my @IBS;
    my @P0ID;
    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - ($FIELD{'INFO'}+2));
    
    throw(sprintf('At least two sample required by %s().',
		  __FUNCTION__)) if ($nGeno < 2);
    

    # TODO: update this using IBS measures in wgs-analysis/srt/IBD.pl
    
    for (my $i = 0; $i < @$HEADER - (FORMAT+2); ++$i) {
	for (my $j = $i + 1; $j < @$HEADER - (FORMAT + 1); ++$j) {
	    $IBS[$i][$j] ||= [0,0,0,0];
	}
    }

    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) {
	progressCounter();

	if ($vcf->[FILTER] !~ PASS_FILTER_PAT) { next }
	if ($vcf->[ALT] eq NULL_FIELD) { next }
	if ($vcf->[ALT] =~ BREAKEND_PAT ) { next }

	my @GT;
	my %FORMAT = %{scalar( getFORMAT($vcf->[FORMAT]) )};
	for(my $i  = 0; $i < @$vcf - (FORMAT+1); ++$i) {
	    my @s  = getSample($vcf->[$i+FORMAT+1],\%FORMAT);
	    my @g  = sort(split(GT_FIELD_SEP,$s[$FORMAT{'GT'}]));

	    $GT[$i] = 0;
	    for(my $a = 0; $a < 2; ++$a) {
		if ($g[$a] eq NULL_FIELD) { next }
		$GT[$i] |= ((4**$g[$a]) << $a);
	    }
	}

	for(my $i = 0; $i < @GT - 1; ++$i) {
	    for(my $j = $i + 1; $j < @GT; ++$j) {
		if (!$GT[$i] || !$GT[$j]) { next }
		
		my $ibs = bitcount($GT[$i] & $GT[$j]);
		if (!($GT[$i] & ($GT[$i] >> 1)) && 
		    !($GT[$j] & ($GT[$j] >> 1)) && $ibs == 2) { 
		    $ibs++ # 3 = IBS2* 
		}
		$IBS[$i][$j][$ibs]++;
	    }
	}
    }
    progressReport();

    report('Writing IBS data');

    print($fdo join("\t",qw(INDV1 INDV2 IBS0 IBS1 IBS2 IBS2star IBS2Ratio
                            IBS2hatRatio IBS2starRatio pctInformative)),"\n");
    for(my $i = 0; $i < @IBS; ++$i) {
	for(my $j = $i + 1; $j < @{$IBS[$i]}; ++$j) {
	    print($fdo join("\t",$HEADER->[$i+FORMAT+1],
			         $HEADER->[$j+FORMAT+1],
			         @{$IBS[$i][$j]},
		                 ibs2Ratio($IBS[$i][$j]),
		                 ibs2hatRatio($IBS[$i][$j]),
		                 ibs2starRatio($IBS[$i][$j]),
		                 pctInformative($IBS[$i][$j])),"\n"
		);
	}
    }

    return 1;
}


sub IBS_USAGE {
    die qq/
Usage:   $PROGRAM ibs [options] <in.vcf> <out.prefix>

Options: -P STR,STR  Sample IDs for the population's parents
         -R          Print R script for out.prefix and exit
         -D          Do not delete intermediate Rscript
         -h          This help message

Notes: Requires at least two diploid Samples


/;
}


sub ibs {
    my %argv;
    my @parents;
    my ($option,$outfile) = (0x2,STDIO);
    my $commandline = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('XhP:RD',\%argv) && !$argv{'h'} && @ARGV == 2 || &IBS_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq 'R') { $option |=  0x1 }
	elsif ($o eq 'D') { $option &= ~0x2 }
	elsif ($o eq 'P') { @parents = is_sample(2,2,$argv{$o},\&IBS_USAGE) }
    }
    if (@parents != 2) {
	report('Must provide sample IDs for exactly 2 parents.');
	IBS_USAGE;
    }    

    my $Rscript = which('Rscript');
    my $infile  = is_string(shift(@ARGV));
    my $prefix  = is_string(shift(@ARGV));
    if ($option & 0x1) {
	my $Rfh = open("$prefix.Rscript",WRITE);

	ibsRscriptWrite($Rfh,$Rscript,"$prefix.dat",@parents);

	$Rfh->close();

    } else {
	report('Starting ',scalar(localtime));
	report('Command-line: ',$commandline);

	my $fdi = open($infile,READ);
	my $fdo = open("$prefix.dat",WRITE);
	my $Rfh = open("$prefix.Rscript",WRITE);
	
	ibsCore($fdo,$fdi);
	ibsRscriptWrite($Rfh,$Rscript,"$prefix.dat",@parents);
	
	close($fdo);
	close($fdi);
	close($Rfh);
	
	runRscript($Rscript,"$prefix.Rscript", $option & 0x2);
	
	report('Finished ',scalar(localtime));
    }
    return 1;
}


sub is_phased {
    my $gt = is_arrayref(shift);
    return scalar(is_string($gt->[0]) =~ /\|/) || 0;
}


sub hapbinSelect {
    my $fdo   = is_subclass(shift,'IO::Handle');
    my $field = is_hashref(shift);
    my $loci  = is_arrayref(shift);

    my @scores;
    my @markrs;
    for(my $i = 0; $i < @{$loci}; ++$i) {
	my $l = is_arrayref($loci->[$i]);
	
	my $INFO = getINFO($l->[$field->{'INFO'}]);
	if (!exists($INFO->{'P0PHASED'}) || 
	    !defined($INFO->{'F1GTP'}) || !defined($INFO->{'P0GT'})) {
	    throw('INFO-F1GTP, INFO-P0GT, or INFO-P0PHASED ',
		  'sub-fields missing or malformed.');
	}
	my $class = 0;
	my @geno  = split(',',$INFO->{'P0GT'});
	for(my $j = 0; $j < 2; ++$j) {
	    my @g = split(GT_FIELD_SEP,(getSample($geno[$j],{'GT'=>0}))[0]);
	    if ($g[0] ne $g[1]) {
		$class |= ($j+1);
	    }
	}
	if (!defined($scores[$class-1]) || 
	    $scores[$class-1] > $INFO->{'F1GTP'}) {
	    $scores[$class-1] = $INFO->{'F1GTP'};
	    $markrs[$class-1] = $l;
	}
    }

    my $pos = $field->{'POS'};
    @markrs = sort {$a->[$pos] <=> $b->[$pos]} 
        grep {Valkyr::Data::Type::Test::is_arrayref($_)} @markrs;

    for(my $i = 0; $i < @markrs; ++$i) {
	print($fdo join("\t",@{$markrs[$i]}),"\n");
    }
    return 1;
}


sub hapbinCore {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $fdi = is_subclass(shift,'IO::Handle');
    my $bed = is_arrayref(shift);

    my ($META,$HEADER) = getMeta($fdi);    
    
    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    
    print($fdo setMeta($META,$HEADER),"\n");

    my @haplo;
    my $prevchr;
    my $prevpos;
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) {
	progressCounter();
	
	if ($vcf->[$FIELD{'FILTER'}] !~  PASS_FILTER_PAT ) { next }
	if ($vcf->[$FIELD{'ALT'}] eq NULL_FIELD) { next }
	if ($vcf->[$FIELD{'ALT'}] =~ BREAKEND_PAT ) {
	    warn(sprintf(BREAKEND_MSG,@{$vcf}[@FIELD{'CHROM','POS'}])); next;
	}

	
	my ($chr,$pos) = @{$vcf}[@FIELD{'CHROM','POS'}];

	if (((defined($prevchr) && $prevchr ne $chr) ||
	     (defined($prevpos) && $prevpos < $pos)) && @haplo) {
	    hapbinSelect($fdo,\%FIELD,\@haplo);
	    @haplo = ();
	}
	if (intersectsInterval($bed,$chr,$pos)) {
	    $prevchr = $bed->[0]->{'CHROM'};
	    $prevpos = $bed->[0]->{'END'};
	    push(@haplo,$vcf);
	}
    }
    if (@haplo) {
	hapbinSelect($fdo,\%FIELD,\@haplo);
    }
    progressReport();

    return 1;
}


sub HAPBIN_USAGE {
    die "Usage: $PROGRAM hapbin [-o FILE] <in.vcf> <hap.bed>\n";
}


sub hapbin {
    warn DeprecationWarning("This function will be removed in the future");
    my %argv;
    my $outfile = STDIO;
    my $command = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('Xho:',\%argv) && !$argv{'h'} && @ARGV == 2 || &HAPBIN_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq 'o') { $outfile = is_string($argv{$o},\&HAPBIN_USAGE) }
    }
    
    report('Starting ',scalar(localtime));
    report('Command-line: ',$command);

    my $fdi = open(shift(@ARGV),READ);
    my $bed = readRNG(shift(@ARGV));
    my $fdo = open($outfile,WRITE);

    hapbinCore($fdo,$fdi,$bed);

    $fdo->close();
    $fdi->close();

    report('Finished ',scalar(localtime));
    
    return 1;
}
   
 
sub assembleHaplotypes {
    my $indv  = is_uint(shift);
    my $loci  = is_arrayref(shift);
    my $isF1  = is_bool(shift||0);

    my @hap = ('','');
    for(my $l = 0; $l < @{$loci}; ++$l) {
	my $I = is_arrayref(is_arrayref($loci->[$l])->[$indv]);
	my $h = is_arrayref($I->[0]);
	for(my $i = 0; $i < 2; ++$i) {
	    if (!$h->[2] || ($isF1 && $h->[0] != $h->[1])) {
		$hap[$i] .= NULL_FIELD;
	    } else {
		$hap[$i] .= $h->[$i];
	    }
	}
    }
    return @hap;
}


sub P0haplotype {
    # PURPOSE: Identify which parental haplotype the F1 by finding the first
    #   difference between the P0 haplotypes
    # ____________________________________________________________________________
    my $P0hap = is_arrayref(shift);
    my $F1hap = is_string(shift);

    if (length($P0hap->[0]) != length($F1hap)) {
	throw(sprint('P0 and F1 haplotype lengths differ (%u != %u).',
		     length($P0hap->[0]),length($F1hap)));
    }
    if (length($P0hap->[1]) != length($F1hap)) {
	throw(sprint('P0 and F1 haplotype lengths differ (%u != %u).',
		     length($P0hap->[1]),length($F1hap)));
    }

    if (is_string($P0hap->[0]) eq is_string($P0hap->[1])) { 
	return 0;
    }
    for(my $o = 0; $o < length($F1hap); ++$o) {
	my $n = substr($F1hap,$o,1);
	my $p = substr(is_string($P0hap->[0]),$o,1);
	my $q = substr(is_string($P0hap->[1]),$o,1);

	if ($n eq $p && $n ne $q) {
	    return 0;
	} elsif ($n eq $q && $n ne $p) {
	    return 1;
	}
    }
    #throw('Assertion failure: Unrelated haplotypes.');
}


sub recomDetect {
    my $P0hap = is_arrayref(shift);
    my $F1hap = is_string(shift);
    my $F1idx = is_uint(shift);
    my $coord = is_arrayref(shift);
    my $pthr  = is_ufloat(shift);

    my @events;
    my $events= 0;
    my $hplen = 0;
    my $index = $F1idx;
    my $blkco = is_uint($coord->[0]); # init block end coord
    for(my $i = 0; $i < length($F1hap); ++$i) {
	my $n = substr($F1hap,$i,1);
	if ($n ne substr($P0hap->[ $index],$i,1) &&
	    $n eq substr($P0hap->[!$index],$i,1)) {
	    if (@events && ((0.5**$hplen) > $pthr)) { 
		pop(@events);
	    }
	    push(@events,is_uint($blkco));
	    $index = !$index;
	    $hplen = 0;
	    $events++;
	}
	$blkco = is_uint($coord->[$i]);
	$hplen++;
    }
    if (($events > 1) && ((0.5**$hplen) > $pthr)) {
	pop(@events);
    }

    return(\@events,scalar(@events));
}


sub scanHaplotypes {
    my $fdo   = is_subclass(shift,'IO::Handle');
    my $P0hap = is_arrayref(shift);
    my $F1hap = is_arrayref(shift);
    my $coord = is_arrayref(shift);
    my $pthr  = is_ufloat(shift);
    my $opt   = is_bool(shift||0);
    my $id    = shift;

    my @juncs;
    my $F1flt = filterHetProband($P0hap,$F1hap);
    for(my $i = 0; $i < 2; ++$i) {
	my $h = P0haplotype($P0hap->[$i],$F1hap->[$i]);
	my @r = recomDetect($P0hap->[$i],$F1flt->[$i],$h,$coord,$pthr);
	
	if ($Valkyr::Error::DEBUG) {
	    map {debug($_)} (
		$id // '',
		"  P0-$i: ".$P0hap->[$i]->[!$h],
		"  P0-$i: ".$P0hap->[$i]->[ $h],
		"  F1-$i: ".$F1hap->[$i]
	    );
	}
	
	if (scalar(@{$r[0]}) && $r[1]) {
	    push(@juncs,@{$r[0]});
	}
    }
    return(@juncs);
}


sub filterHetProband {
    my $P0hap = is_arrayref(shift);
    my $F1hap = is_arrayref(shift);

    my (@F1,@P1,@P2);
    $F1[0] = is_string($F1hap->[0]);
    $F1[1] = is_string($F1hap->[1]);
    $P1[0] = is_string(is_arrayref($P0hap->[0])->[0]);
    $P1[1] = is_string(is_arrayref($P0hap->[0])->[1]);
    $P2[0] = is_string(is_arrayref($P0hap->[1])->[0]);
    $P2[1] = is_string(is_arrayref($P0hap->[1])->[1]);
    for (my $i = 0; $i < length($F1hap->[0]); ++$i) {
	if (substr($F1[0],$i,1) ne substr($F1[1],$i,1) && 
	    substr($P1[0],$i,1) ne substr($P1[1],$i,1) &&
	    substr($P2[0],$i,1) ne substr($P2[1],$i,1)) {
	    substr($F1[0],$i,1,NULL_FIELD);
	    substr($F1[1],$i,1,NULL_FIELD);
	}
    }
    return \@F1;
}


sub hapmarcRecombinations {
    # PURPOSE: Identify recombination events among offspring, using the 
    #   parentally phased genotypes as a reference (this is the most direct).
    # METHODS: Examine two loci at a time and identify whether the offspring
    #   haplotype matches that of the parents
    # _________________________________________________________________________
    my $fdo   = is_subclass(shift,'IO::Handle');
    my $ref   = is_hashref(shift);
    my $P0ID  = is_arrayref(shift);
    my $HDR   = is_arrayref(shift);
    my $FLD   = is_hashref(shift);
    my $loci  = is_arrayref(shift);
    my $exc   = is_hashref(shift);
    my $dist  = is_uint(shift);
    my $pthr  = is_ufloat(shift);

    my @coord;
    my %recom;
    my @P0hap;
    my $iter  = 0;
    my $start = 0;
    my $nflds = scalar(@{is_arrayref($loci->[0])});
    my $scaff = is_string($loci->[0]->[$FLD->{'CHROM'}]);

    my $d = $loci->[$#{$loci}]->[$FLD->{'POS'}]
	  - $loci->[0]->[$FLD->{'POS'}];
    if (@{$loci} > 2 || (@{$loci} == 2 && $d >= $dist)) { 
	for(my $l = 0; $l < @$loci; ++$l) {
	    push(@coord,is_uint(is_arrayref($loci->[$l])->[$FLD->{'POS'}]));
	}
	
	$P0hap[0] = [ assembleHaplotypes($FLD->{$P0ID->[0]},$loci) ];
	$P0hap[1] = [ assembleHaplotypes($FLD->{$P0ID->[1]},$loci) ];
	for(my $i = $FLD->{'FORMAT'} + 1; $i < $nflds; ++$i) {
	    if ($exc->{$HDR->[$i]}) { next }
	    my @h = assembleHaplotypes($i,$loci);
	    my @r = scanHaplotypes($fdo,\@P0hap,\@h,\@coord,$pthr,0,$HDR->[$i]);

	    if(@r) { map {$recom{$_}++} @r }
	}
	
	for my $site (sort {$a<=>$b} keys(%recom)) {
	    print($fdo join("\t",$scaff,$start,$site,
		  sprintf('%s.%04s',$scaff,$iter++),$recom{$site}),"\n");
	    $start = $site;
	}
    }
    print($fdo join("\t",$scaff,$start,$ref->{$scaff}->{'length'},
	  sprintf('%s.%04s',$scaff,$iter++),0),"\n");
}


sub hapmarcCore {
    my $fdo   = is_subclass(shift,'IO::Handle');
    my $fdi   = is_subclass(shift,'IO::Handle');
    my $dist  = is_uint(shift);
    my $pthr  = is_ufloat(shift);
    my $ref   = shift;

    my ($META,$HEADER) = getMeta($fdi);

    my %excld;
    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - ($FIELD{'INFO'}+2));
    my @P0ID  = reverse(grep {/^P0\./} @$HEADER);


    if (@P0ID != 2) {
	throw('No parental samples detected: perhaps no P0.* samples in VCF?');
    } else {
	map { $excld{$_}++ } @P0ID;
	map { $excld{$1}++ if m/^P0\.(.*)/ } @P0ID;
    }
    if (!defined($META->{'contig'}) && !defined($ref)) {
	throw('No reference sequence (.faidx) dictionary/index detected.');
    } elsif (defined($META->{'contig'})) {
	$ref  = $META->{'contig'};
    }


    my @scaff;
    my $previous;
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) {
	progressCounter();

	if (!exists($ref->{$vcf->[$FIELD{'CHROM'}]} )) {
	    throw('No sequence index entry for: ',$vcf->[$FIELD{'CHROM'}]);
	}
	$previous //= $vcf->[$FIELD{'CHROM'}];

	my $INFO   = getINFO($vcf->[$FIELD{'INFO'}]);
	my %FORMAT = %{scalar( getFORMAT($vcf->[$FIELD{'FORMAT'}]) )};
	for(my $i  = $FIELD{'FORMAT'}+1; $i < @{$vcf}; ++$i) {
	    my @g  = getSample($vcf->[$i],\%FORMAT);
	    
	    $g[$FORMAT{'GT'}] = [ 
		CORE::split(GT_FIELD_SEP,$g[$FORMAT{'GT'}]),
			is_phased([ $g[$FORMAT{'GT'}] ])
	    ];
	    $vcf->[$i] = \@g;
	}

	if ($vcf->[$FIELD{'CHROM'}] ne $previous) {
	    hapmarcRecombinations($fdo,$ref,\@P0ID,$HEADER,\%FIELD,
				  \@scaff,\%excld,$dist,$pthr);
	    @scaff = ();
	}

	push(@scaff,$vcf);

	$previous = $vcf->[$FIELD{'CHROM'}];
    }

    if (@scaff) {
	hapmarcRecombinations($fdo,$ref,\@P0ID,$HEADER,\%FIELD,
			      \@scaff,\%excld,$dist,$pthr);
    }
    progressReport();

    return 1;
}


sub HAPMARC_USAGE {
    die qq/
Usage:   $PROGRAM hapmarc [options] <phased.vcf>

Options: -o FILE   Write output to FILE [stdout]
         -D FILE   Reference genome dictionary\/index
         -d INT    Min. marker distance (see Note 3) [500000]
         -p FLOAT  Prob. of random haplotype match by chance [0.01]
         -s        Assembly is not chromosome-scale
         -h        This help message

Notes:

  1. Input phased.vcf must be a vcftk-initb VCF file phased by BEAGLEv4.

  2. The output is a BED file demarcating the boundaries between putative
     haplotype blocks.

  3. The minimum physical distance between the loci on scaffolds represented
     by only two loci is controlled by the '-d' option. Observing crossovers 
     between any two adjacent markers will be rare and the option is implem-
     ented to modulate "how distant is distant enough" to believe a putative
     recombination between the two markers.


/;
}


sub hapmarc {
    warn DeprecationWarning("This function will be removed in the future");
    my (%argv,@parents,$refdict);
    my ($option,$pthrsh,$snpdist,$outfile) = (0,0.01,500000,STDIO);
    my $commandline = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('Xho:D:d:p:s',\%argv) && !$argv{'h'} && @ARGV || &HAPMARC_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq 't') { $option |= 0x1 }
	elsif ($o eq 'd') { $snpdist = is_uint(expand($argv{$o}),\&HAPMARC_USAGE) }
	elsif ($o eq 'D') { $refdict = is_file($argv{$o},'f',\&HAPMARC_USAGE) }
	elsif ($o eq 'p') { $pthrsh  = is_ufloat($argv{$o},\&HAPMARC_USAGE) }
	elsif ($o eq 'o') { $outfile = is_string($argv{$o},\&HAPMARC_USAGE) }
    }

    report('Starting ',scalar(localtime));
    report('Command-line: ',$commandline);

    my $fai = readFaidx($refdict) if defined($refdict);
    my $fdi = open(shift(@ARGV),READ);
    my $fdo = open($outfile,WRITE);

    hapmarcCore($fdo,$fdi,$snpdist,$pthrsh,$fai);

    $fdo->close();
    $fdi->close();

    report('Finished ',scalar(localtime));
    
    return 1;
}

sub mtvrRscriptWrite {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $R   = is_string(shift);
    my $dat = is_string(shift);
    my $P0s = is_arrayref(shift);

    my $color = @$P0s ? "'grey60'" : 'i';

    report('Writing Rscript');

    printf($fdo "#!%s\n# Auto-generated by %s mtvr %s\n\n",$R,$PROGRAM,scalar(localtime));
    printf($fdo "d = read.table('%s');\n\n",$dat);
    printf($fdo "pdf('%s.pdf');\n",$dat);
    print($fdo "plot(as.numeric(rownames(d)), d[,1], xlim=range(as.numeric(rownames(d))), ylim=range(d), type='l', main='Genotype Quality vs. Rate of Mendelian Violation', xlab='Minimum GQ', ylab='Rate', xaxp=c(range(as.numeric(rownames(d))),10));\n");
    print($fdo "for (i in seq(1,length(d))) {\n");
    print($fdo "    lines(as.numeric(rownames(d)), d[,i], col=$color);\n");
    print($fdo "}\n");
    for my $p (@$P0s) {
	print($fdo "lines(as.numeric(rownames(d)), d[,\"$p\"], col='black');\n");
    }
    print($fdo "invisible(dev.off());\n\n");
    
    return 1;
}


sub mtvrTableWrite {
    my $fdo     = is_subclass(shift,'IO::Handle');
    my $samples = is_arrayref(shift);
    my $misgt   = is_arrayref(shift);
    my $count   = is_arrayref(shift);
    my $total   = is_arrayref(shift);
    my $numbins = is_uint(shift);

    print($fdo join("\t",'',@$samples),"\n");
    for(my $j = 0; $j <= 1+(MAX_GQ/$numbins); ++$j) {
	print($fdo int($j*$numbins));
	for(my $i = 0; $i < @$samples; ++$i) {
	    $count->[$i] ||= 0;
	    $count->[$i] -= ($misgt->[$i]->[$j] || 0);
	    printf($fdo "\t%0.5f", $count->[$i] / ($total->[$i]||1));
	}
	print($fdo "\n");
    }
    
    return 1;
}

sub mtvrCore {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $fdi = is_subclass(shift,'IO::Handle');
    my $P0s = is_arrayref(shift);

    my ($META,$HEADER) = getMeta($fdi);

    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - (INFO+2));
    
    throw(sprintf('At least one sample required by %s().',
		  __FUNCTION__)) if ($nGeno < 1);
    
    if (@$P0s) {
	# check that parent genotypes exist in VCF
	for (my $i = 0; $i < @$P0s; ++$i) {
	    if (! defined( $FIELD{ is_string($P0s->[$i]) })) {
		throw("Sample ID ($P0s->[$i]) not found in VCF.");
	    }
	}
    }

    my @MISGT;
    my @COUNT;
    my @TOTAL;
    my $MULT = 5;
    my $REQUIRE = ['GT','GQ'];
    my @SAMPLES = @{$HEADER}[(FORMAT+1)..$#{$HEADER}];
    map{ $MISGT[$_] = [0]; $COUNT[$_] = $TOTAL[$_] = 0 } 0..$#SAMPLES;
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) {	
	progressCounter();
	
	if ($vcf->[ALT] eq NULL_FIELD) { next }
	if ($vcf->[ALT] =~ BREAKEND_PAT) { next }
	if ($vcf->[FILTER] !~ PASS_FILTER_PAT) { next }	

	my ($P0BT,$P1BT) = (0,0);
	my $INFO = getINFO($vcf->[INFO]);
	if (defined($INFO->{'P0GT'}) && exists($INFO->{'P0PHASED'})) {
	    my @P = split(',',$INFO->{'P0GT'});
	    map{ $P0BT |= as_sbitA($_)} split(GT_FIELD_SEP,(getSample($P[0],{GT=>0}))[0]);
	    map{ $P1BT |= as_sbitA($_)} split(GT_FIELD_SEP,(getSample($P[1],{GT=>0}))[0]);
	    
	} else {
	    throw(sprintf('No INFO-P0GT or INFO-P0PHASED field(s) detected (%s:%u).',
			  @{$vcf}[CHROM,POS]));
	}
	
	my $sharedbit   = $P0BT & $P1BT;
	my $parentalbit = $P0BT | $P1BT;
	my $unsharedbit = $parentalbit & ~$sharedbit;
	my %FORMAT = %{scalar( getFORMAT($vcf->[FORMAT],$REQUIRE) )};
	my ($GT,$GQ) = @FORMAT{'GT','GQ'};
	for(my $i  = FORMAT+1; $i < @$vcf; ++$i) {
	    my @s  = getSample($vcf->[$i],\%FORMAT);
	    if ($s[$GT] !~ /\./ && $s[$GT] =~ GT_GROUP_PAT) {
		my $F1BT = ( as_sbitA($1) | as_sbitA($2) );

		# A Mendelian violation will be an allele not observed in either parent, or
                # a genotype that is homozygous for an allele not shared by both parents 

		if ($s[$GQ] eq NULL_FIELD) { $s[$GQ] = 0 }
		if ($F1BT & ~$parentalbit) {
		    $MISGT[$i-(FORMAT+1)][1+$s[$GQ]/$MULT]++;
		    $COUNT[$i-(FORMAT+1)]++;

		} elsif ($unsharedbit == $parentalbit) {
		    if ($F1BT != $parentalbit) {
			$MISGT[$i-(FORMAT+1)][1+$s[$GQ]/$MULT]++;
			$COUNT[$i-(FORMAT+1)]++;
		    }
		} elsif ($unsharedbit) {
		    if ($F1BT == ($F1BT & $unsharedbit)) {
			$MISGT[$i-(FORMAT+1)][1+$s[$GQ]/$MULT]++;
			$COUNT[$i-(FORMAT+1)]++;
		    }
		}
		    
		$TOTAL[$i-(FORMAT+1)]++;
	    }
	}
    }
    progressReport();

    return mtvrTableWrite($fdo,\@SAMPLES,\@MISGT,\@COUNT,\@TOTAL,$MULT);
}


sub MTVR_USAGE {
    die "Usage: $PROGRAM mtvr [-D] <in.vcf> <out.prefix>\n";
}


sub mtvr {
    my %argv;
    my @parents;
    my $option = 0x1;
    my $commandline = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('XhP:D',\%argv) && !$argv{'h'} && @ARGV == 2 || &MTVR_USAGE;
    for my $o (keys %argv) {
	if ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	if ($o eq 'D') { $option &= ~0x1 }
	if ($o eq 'P') { @parents = is_sample(1,2,$argv{$o},\&MTVR_USAGE) }
    }

    report('Starting ',scalar(localtime));
    report('Command-line: ',$commandline);

    my $Rscript = which('Rscript');
    my $vcffile = shift(@ARGV);
    my $prefix  = shift(@ARGV);

    my $fdi = open($vcffile,READ);
    my $dat = open("$prefix.dat",WRITE);
    my $Rfh = open("$prefix.Rscript",WRITE);


    mtvrCore($dat,$fdi,\@parents);

    mtvrRscriptWrite($Rfh,$Rscript,"$prefix.dat",\@parents);

    $dat->close();
    $Rfh->close();
    $fdi->close();

    runRscript($Rscript,"$prefix.Rscript", $option & 0x1);

    report('Finished ',scalar(localtime));
    
    return 1;
}


sub trioCore {
    my $fdo  = is_subclass(shift,'IO::Handle');
    my $fdi  = is_subclass(shift,'IO::Handle');
    my @INDV = @{is_arrayref(shift)};
    my $opt  = is_uint(shift);

    my ($META,$HEADER) = getMeta($fdi);

    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - (INFO+2));
    
    throw(sprintf('At least two samples required by %s().',
		  __FUNCTION__)) if ($nGeno < 2);
    
    if (@INDV) {
	# check that parent genotypes exist in VCF
	$nGeno = @INDV;
	for (my $i = 0; $i < $nGeno; ++$i) {
	    if (! defined( $FIELD{ is_string($INDV[$i]) })) {
		throw("Sample ID ($INDV[$i]) not found in VCF.");
	    }
	}
    } else {
	@INDV = @{$HEADER}[(FORMAT+1)..$#{$HEADER}];
    }
    if ((@INDV < 2) && ($opt & 0x1)) {
	throw("Must specify two or more individuals for a selfing trio");
    } elsif ((@INDV < 3) && (~$opt & 0x1)) {
	throw("Must specify three or more individuals for an out-crossing trio");
    }

    my @TRIOS;
    my $o = $opt & 0x1 ? 0 : 1;
    for(my $i = 0; $i < $nGeno-2; ++$i) {
	for(my $j = $i+$o; $j < $nGeno-1; ++$j) {
	    for(my $k = $j+$o; $k < $nGeno; ++$k) {
		push(@TRIOS,[$INDV[$i],$INDV[$j],$INDV[$k],0,0,0,0]);
	    }
	}
    }

    my @GENO;
    my %GIDX = %{scalar( indexArray(@INDV) )};
    my $REQUIRE = ['FT'];
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) {	
	progressCounter();
	
	if ($vcf->[ALT] =~ /,/) { next }
	if ($vcf->[ALT] eq NULL_FIELD) { next }
	if ($vcf->[ALT] =~ BREAKEND_PAT) { next }
	if ($vcf->[FILTER] !~ PASS_FILTER_PAT) { next }	

	my %FORMAT = %{scalar( getFORMAT($vcf->[FORMAT],$REQUIRE) )};
	for(my $i  = 0; $i < @INDV; ++$i) {
	    my @s  = getSample($vcf->[$FIELD{$INDV[$i]}],\%FORMAT);
	    if ($s[$FORMAT{'FT'}] =~ PASS_FILTER_PAT && 
		$s[$FORMAT{'GT'}] =~ GT_GROUP_PAT) {
		$GENO[$i] = ($1 eq NULL_ALLELE || $2 eq NULL_ALLELE) ? 0 : (as_sbitA($1) | as_sbitA($2));
	    } else {
		$GENO[$i] = 0;
	    }
	}
	
	for(my $j = 0; $j < @TRIOS; ++$j) {
	    my $T = $TRIOS[$j];
	    if (! $GENO[$GIDX{$T->[0]}]) { next }
	    if (! $GENO[$GIDX{$T->[1]}]) { next }
	    if (! $GENO[$GIDX{$T->[2]}]) { next }
	    if (bitcount($GENO[$GIDX{$T->[0]}] | $GENO[$GIDX{$T->[1]}] | $GENO[$GIDX{$T->[2]}]) < 2) { next }
	    for(my $Ip = 0; $Ip < 3; ++$Ip) {
		my $I1 = 1 & ~$Ip;
		my $I2 = 2 & ~$Ip;
		
		my $P1 = $GENO[$GIDX{$T->[$I1]}];
		my $P2 = $GENO[$GIDX{$T->[$I2]}];
		my $p  = $GENO[$GIDX{$T->[$Ip]}];

		my $shared = $P1 & $P2;
		my $filial = $P1 | $P2;
		my $unique = $filial & ~$shared;
		
		# A Mendelian violation will be an allele not observed in either parent, or
		# a genotype that is homozygous for an allele not shared by both parents

		if (($p & ~$filial)) { # contains non-mendelian allele
		    $T->[$Ip+3]++;
		} elsif ($unique == $filial) { # both parents hom for diff alleles
		    if ($p != $filial) { $T->[$Ip+3]++ }
		} elsif ($unique) {
		    if ($p == ($p & $unique)) { $T->[$Ip+3]++ }
		} #else can be any genotype
	    }
	    $T->[6]++;
	}
    }
    progressReport();

    for(my $j = 0; $j < @TRIOS; ++$j) {
	for(my $Ip = 0; $Ip < 3; ++$Ip) {
	    my $I1 = 1 & ~$Ip;
	    my $I2 = 2 & ~$Ip;
	    
	    printf($fdo "Trio%05d.%u\t%s\t%s\t%s\t0\t%0.5f\n",$j+1,$Ip+1,
		   $TRIOS[$j][$Ip],$TRIOS[$j][$I1],$TRIOS[$j][$I2],
		   ($TRIOS[$j][$Ip+3]||0)/($TRIOS[$j][6]||1));
	}
    }
    return 1;
}


sub TRIO_USAGE {
    die qq/
Usage:   $PROGRAM trio [options] <in.vcf>

Options: -o FILE        Write output to FILE [stdout]
         -i FILE        Include samples listed in FILE only [null]
         -T ID1,ID2,ID3 Specify a trio family on the command-line 
         -S             Allow trio pedigrees from selfing [false]
         -h             This help message

Notes:   

  1. Writes a file of trio combinations to a PED file, where the
     phenotype column contains the fraction of loci genotyped in
     all three individuals that violate Mendelian transmission.

  2. Requires at least three genotype\/sample fields.


/;
}


sub trio {
    my %argv;
    my $include;
    my $option = 0;
    my $triofam = [];
    my $outfile = STDIO;
    my $commandline = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('XhSo:i:T:',\%argv) && !$argv{'h'} && @ARGV == 1 || &TRIO_USAGE;
    for my $o (keys %argv) {
	if ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	if ($o eq 'S') { $option |= 0x1 }
	if ($o eq 'i') { $include = is_string($argv{$o},\&TRIO_USAGE) }
	if ($o eq 'o') { $outfile = is_string($argv{$o},\&TRIO_USAGE) }
	if ($o eq 'T') { $triofam =[is_sample(3,3,$argv{$o},\&TRIO_USAGE)] }
    }

    report('Starting ',scalar(localtime));
    report('Command-line: ',$commandline);

    my $vcffile = shift(@ARGV);
    my $samples = defined($include) ? readOrderedList($include) : $triofam;

    my $fdi = open($vcffile,READ);
    my $fdo = open($outfile,WRITE);


    trioCore($fdo,$fdi,$samples,$option);

    $fdo->close();
    $fdi->close();

    report('Finished ',scalar(localtime));
    
    return 1;
}


sub addCounts {
    my $c = is_arrayref(shift);
    my $v = is_sfloat(shift);
    my $l = is_uint(shift);
    
    for(my $i = 0; $i < $l; ++$i) {
	$c->[$i] += $v;
    }
}


sub printSnvRateTable {
    my $fdo   = is_subclass(shift,'IO::Handle');
    my $chrom = is_string(shift);
    my $start = is_uint(shift);
    my $end   = is_uint(shift);
    my $calls = is_uint(shift);
    my $hets  = is_uint(shift);
    my $homs  = is_uint(shift);

    
    progressCounter();
    
    print($fdo join("\t",$chrom,$start,$end,'.',0,'+',$calls,($calls ? $hets/$calls : -1),($calls ? $homs/$calls : -1)),"\n");

    # for(my $b = 0; $b < @$call; ++$b) {
    # 	my $c = is_arrayref($call->[$b]||[]);
    # 	my $v = is_arrayref($var->[$b]||[]);

    # 	printf($fdo "%s\t%u\t%u",$cntg,$win*$b+1,
    # 	       min($win*($b+1),$fai->{$cntg}->{'length'}));
    # 	for(my $i = 0; $i < $N; ++$i) {
    # 	    if (!$c->[$i]) {
    # 		printf($fdo "\t-1");
    # 	    } else {
    # 		printf($fdo "\t%.04f", ($v->[$i]||0)/$c->[$i]);
    # 	    }
    # 	}
    # 	print($fdo "\n");
    # }
}


sub snvrateCore {
    my $fdo    = is_subclass(shift,'IO::Handle');
    my $fdi    = is_subclass(shift,'IO::Seekable');
    my $contig = is_hashref(shift);
    my $indv   = is_string(shift);
    my $Wsize  = is_uint(shift);
    my $Ssize  = is_uint(shift);
    my $opts   = is_uint(shift);
    
    my ($META,$HEADER) = getMeta($fdi);	
    my $width  = @$HEADER;
    my %FIELD  = %{scalar( indexArray(@$HEADER) )};
    my $nGeno  = max(0,$width - (INFO+2));
    my $indx   = $FIELD{$indv};
    my $REQD   = ['FT'];

    
    if ($nGeno < 1) {
	throw('One or more samples required at ',__FUNCTION__,'.');

    } elsif (! $indx && $nGeno == 1) {
	$indv = $HEADER->[FORMAT+1];
	$indx = FORMAT+1;
	
    } elsif (! $indx || $indx < 9 || $width <= $indx) {
	throw("Invalid sample ID: '$indv'");

    } 	
    
    $fdo->autoflush($Valkyr::Error::DEBUG);
    
    configCounter({'prefix' => 'Windows processed: ', 'granularity' => 10});
    
    my $het = 0;
    my $hom = 0;
    my $callable = 0;
    my $prevbin  = 0;
    my $currbin  = 0;
    my $nextbin  = 0;
    my $prevchr  = '';
    my $prevpos  = 0;
    my $firstpos = undef;
    my $nextseek = undef;
    print($fdo join("\t",qw(CHROM START END NAME SCORE STRAND CALLABLE HET HOM)),"\n");
    while (my $vcf = getEntry($fdi,$width)) {
	
	$currbin = int((is_uint($vcf->[POS])-1) / $Ssize);
	$nextbin = int((is_uint($vcf->[POS])-0) / $Ssize);
	
	if ($vcf->[CHROM] ne $prevchr) {
	    printSnvRateTable($fdo,$prevchr,$firstpos,$prevpos,$callable,$het,$hom) if defined($firstpos);
	    $firstpos = $currbin * $Ssize;
	    $nextseek = undef;
	    $callable = 0;
	    $het = 0;
	    $hom = 0;
	    
	} elsif ($currbin != $nextbin) {
	    $nextseek ||= is_uint($fdi->tell());
	}

	$firstpos //= $currbin * $Ssize;
	if ($vcf->[FILTER] =~ PASS_FILTER_PAT) {
	    # parse genotype and determine if callable here:
	    my $F = getFORMAT($vcf->[FORMAT],$REQD);
	    my @v = getSample($vcf->[$indx],$F);
	    
	    if ($v[$F->{'GT'}] !~ /\./ && 
		$v[$F->{'FT'}] =~ PASS_FILTER_PAT) {
		my @g = split(GT_FIELD_SEP,$v[$F->{'GT'}]);
		$hom += ($g[0] && $g[1] && $g[0] == $g[1]);
		$het += ($g[0] != $g[1]);

		$callable++;
	    }
	}
	if ($callable == $Wsize) {
	    printSnvRateTable($fdo,$prevchr,$firstpos,$vcf->[POS],$callable,$het,$hom);
	    $fdi->seek(is_uint($nextseek),0);
	    $firstpos = undef;
	    $nextseek = undef;
	    $callable = 0;
	    $het = 0;
	    $hom = 0;
	}

	$prevchr = $vcf->[CHROM];
	$prevpos = $vcf->[POS];
	$prevbin = $currbin;
    }
    printSnvRateTable($fdo,$prevchr,$firstpos,$prevpos,$callable,$het,$hom) if defined($firstpos);

    progressReport();
    
    return 1;
}


sub SNVRATE_USAGE {
    die qq/
Usage:   $PROGRAM snvrate [options] <in.vcf>

Options: -o FILE  Write output to FILE [stdout]
         -R FILE  faidx indexed reference sequence [null]
         -i STR   Individual ID for which to calc rate [null]
         -w INT   Width of the window [1k]
         -s INT   Step size of the window [1k]
         -h       This help message


/;
         
}


sub snvrate {
    my (%argv,$ref);
    my ($opts,$Wsize,$Ssize,$ofile,$indv) = (0,1000,1000,STDIO,'undefined');
    my $commandline = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('Xho:w:s:R:i:',\%argv) && !$argv{'h'} && @ARGV == 1 || &SNVRATE_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq 'o') { $ofile = is_string($argv{$o},\&SNVRATE_USAGE) }
	elsif ($o eq 'R') { $ref   = is_string($argv{$o},\&SNVRATE_USAGE) }
	elsif ($o eq 'i') { $indv  = is_string($argv{$o},\&SNVRATE_USAGE) }
	elsif ($o eq 'w') { $Wsize = is_uint(expand($argv{$o}),\&SNVRATE_USAGE) }
	elsif ($o eq 's') { $Ssize = is_uint(expand($argv{$o}),\&SNVRATE_USAGE) }
    }
    if ($Ssize > $Wsize) {
	throw('Step size must be less than or equal to window size.');
    }

    report('Starting ',scalar(localtime));
    report('Command-line: ',$commandline);


    my $fdi = open(shift(@ARGV),READ,NOSTREAM);
    my $fai = defined($ref) ? readFaidx($ref) : {};
    my $fdo = open($ofile,WRITE);

    snvrateCore($fdo,$fdi,$fai,$indv,$Wsize,$Ssize,$opts);

    $fdo->close();
    $fdi->close();

    report('Finished ',scalar(localtime));
    
    return 1;
}


sub Valkyr::Data::Type::Assert::is_validkey {
    my ($val,$func) = @_;
    if ( ! Valkyr::Data::Type::Test::is_string($val) || $val !~ /^[a-zA-Z]\w*/) {
	Valkyr::Data::Type::Test::REFUTED( $val,'info key',$func )
    }
    return $val;
}


sub annotateCore {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $fdi = is_subclass(shift,'IO::Handle');
    my $bed = is_arrayref(shift);
    my $ord = is_arrayref(shift);
    my $fai = is_hashref(shift);
    my $key = is_string(shift);
    my $pop = is_string(shift);
    my $opt = is_uint(shift);
    my $cmd = shift;

    my ($META,$HEADER) = getMeta($fdi);	
    my $width  = @$HEADER;
    my %FIELD  = %{scalar( indexArray(@$HEADER) )};
    my $nGeno  = max(0,$width - (INFO+2));

    addMeta($META,'VCFtk',
        {
	    '_ORDER_' => [qw(ID Version Date CommandlineOptions)],
	    'ID'      => 'annotate',
	    'Version' => $VERSION,
	    'Date'    => '"'.scalar(localtime).'"',
	    'CommandlineOptions' => '"'.$cmd.'"'
	});


    my @sample;
    my %sample;
    my %filter;
    if ($opt & 0x1) {
	if (lc($pop) eq 'all') {
	    for(my $i = FORMAT+1; $i < $width; ++$i) {
		$sample{$HEADER->[$i]} = $i;
	    }
	} else {
	    for my $id (split(/,/,$pop)) {
		if (! $FIELD{$id}) {
		    throw("Sample not defined in VCF header: $id");
		} elsif ($FIELD{$id} <= FORMAT) {
		    throw("Invalid sample name: $id");
		} else {
		    $sample{$id} = $FIELD{$id};
		}
	    }
	}
	@sample = sort keys(%sample);

	throw(sprintf('At least two samples required by %s()',
		      __FUNCTION__)) if (@sample < 1);
	

	for my $interval (@$bed) {
	    if (!defined( is_hashref($interval)->{'NAME'} )) {
		throw('Input range requires a NAME field entry: %s:%u-%u',
		      is_string($interval->{'CHROM'}),is_uint($interval->{'START'}),is_uint($interval->{'END'}));
	    }
	    if ($opt & 0x2) {
		$interval->{'NAME'} = $key;
		$filter{$key} = 1;
	    } else {
		for my $filter (split(/,/,$interval->{'NAME'})) {
		    $filter{Valkyr::Data::Type::Assert::is_validkey($filter)} = 1;
		}
	    }
	}
	addMeta($META,'FORMAT',
	    {
		'_ORDER_' => [qw(ID Number Type Description)],
		'ID'     => 'FT',
		'Type'   => 'String',
		'Number' => 1,
		'Description' => '"Genotype-level filter"'
	    });

	for my $filter (keys(%filter)) {
	    addMeta($META,'FILTER',
		{
		    '_ORDER_' => ['ID','Description'],
		    'ID' => $filter,
		    'Description' => '"Overlaps a user-input mask"'
		});
	}

    } else {
	addMeta($META,'INFO',
	    {
		'_ORDER_' => [qw(ID Number Type Description)],
		'ID'      => $key,
		'Type'    => 'String',
		'Number'  => '.',
		'Description' => '"User-defined annotation"'
	    });		    
    }
	
    if (!@$ord) {
	$fai = $META->{'contig'} || {};
	$ord = indexArray(@{$fai->{'_ORDER_'}||[]});
    } else {
	$ord = indexArray(@$ord);
    }

    my $REQ = ['FT'];
    print($fdo setMeta($META,$HEADER),"\n");
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) {
	progressCounter();

	if (!$fai->{$vcf->[CHROM]} || !$fai->{$vcf->[CHROM]}->{'length'}) {
	    throw AssertionError('Missing Meta ##contig entry in VCF file: ',$vcf->[CHROM],'.');
	}

	my @cache;
	if (intersectsInterval($bed,$ord,@{$vcf}[CHROM,POS],\@cache)) {
	    if ($opt & 0x1) {
		my $FORMAT = getFORMAT($vcf->[FORMAT],$REQ);
		for my $i (@sample) {
		    my @v  = getSample($vcf->[$sample{$i}],$FORMAT);

		    $v[$FORMAT->{'FT'}] = addFT($v[$FORMAT->{'FT'}], map{split(/[;,]/,$_->{'NAME'})} @cache);
		    $vcf->[$sample{$i}] = join(':',@v);
		}
		$vcf->[FORMAT] = setFORMAT($FORMAT);
		
	    } else {
		my $INFO = getINFO($vcf->[INFO]);
		$INFO->{$key} = join(',', map{$_->{'NAME'}} @cache);
		$vcf->[INFO]  = setINFO($INFO);
	    }
	}
	
	print($fdo join("\t",@$vcf),"\n");
    }
    progressReport();

    return 1;
}


sub ANNOTATE_USAGE {
    die qq/
Usage:   $PROGRAM annotate [options] <in.vcf> <in.bed>

Options: -o FILE  Write output to FILE [stdout]
         -R FILE  faidx indexed reference sequence [null]
         -k STR   Use STR as annotation key in INFO field ['resource']
         -s STR   Mask sample genotypes listed by STR ['all']
         -M       Mask sample genotype with NAME field of in.bed (requires -s)


/;
}

sub annotate {
    my %argv;
    my ($outfile,$option,$reffile,$infokey,$samples) = (STDIO,0,undef,'resource','all');
    my $commandline = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('o:R:k:s:M',\%argv) && !$argv{'h'} && @ARGV == 2 || &ANNOTATE_USAGE;
    for my $o (keys(%argv)) {
	if    ($o eq 'M') { $option |= 0x1 }
	elsif ($o eq 's') { $samples = is_string($argv{$o},\&ANNOTATE_USAGE) }
	elsif ($o eq 'o') { $outfile = is_string($argv{$o},\&ANNOTATE_USAGE) }
	elsif ($o eq 'R') { $reffile = is_string($argv{$o},\&ANNOTATE_USAGE) }
	elsif ($o eq 'k') { $infokey = Valkyr::Data::Type::Assert::is_validkey($argv{$o},\&ANNOTATE_USAGE); $option |= 0x2 }
    }
    
    report('Starting ',scalar(localtime));
    report('Command-line: ',$commandline);

    my $fdi = open(shift(@ARGV),READ);
    my $bed = readRNG(shift(@ARGV));
    my $fai = defined($reffile) ? readFaidx($reffile) : {};
    my $ord = defined($reffile) ? readOrderedList(faidxName($reffile)) : [];
    my $fdo = open($outfile,WRITE);

		
    annotateCore($fdo,$fdi,$bed,$ord,$fai,$infokey,$samples,$option,$commandline);

    close($fdo);
    close($fdi);

    report('Finished ',scalar(localtime));

    return 1;
}


sub closeContig {
    my $file   = is_hashref(shift);
    my $open   = is_arrayref(shift);
    my $contig = shift(@$open);

    $file->{$contig}->{'open'} = 0;
    $file->{$contig}->{'fh'}->close() if $file->{$contig}->{'fh'}->opened();
}


sub openContig {
    my $contig = is_string(shift);
    my $prfx   = is_string(shift);
    my $suff   = is_string(shift);
    my $file   = is_hashref(shift);
    my $open   = is_arrayref(shift);
    
    $file->{$contig}->{'idx'}++;
    $file->{$contig}->{'fh'}   = open(sprintf('%s.%u.%s.%04d.%s',$prfx,$$,escChar($contig),$file->{$contig}->{'idx'},$suff),WRITE);
    $file->{$contig}->{'open'} = 1;
    push(@$open,$contig);
}


sub appendReorderedContig {
    my $contig = is_string(shift);
    my $prfx   = is_string(shift);
    my $suff   = is_string(shift);
    my $file   = is_hashref(shift);
    my $sort   = is_file(shift,'x');

    my @args;
    for(my $i = 1; $i <= $file->{$contig}->{'idx'}; ++$i) {
	push(@args,is_file(sprintf('%s.%u.%s.%04d.%s',$prfx,$$,escChar($contig),$i,$suff),'fr'));
    }
    
    my $command = "$sort -k2,2n @args >>$prfx.$suff";
    debug("Command: $command");
    if (system("$command")) {
	throw("Command failure: $command");
    }
    delete($file->{$contig});
    unlink(@args);
}


sub sortlociCore {
    my $fdo   = is_subclass(shift,'IO::Handle');
    my $fdi   = is_subclass(shift,'IO::Handle');
    my $ref   = is_arrayref(shift);
    my $prfx  = is_string(shift);
    my $suffx = is_string(shift);
    my $ulim  = is_file(shift,'x'); # usually protected 
    my $sort  = is_file(shift,'x');
    my $maxfh = is_uint(shift);
    my $cmd   = is_string(shift || '');
    my $FDLIM = `$ulim -n` || 256; 
    
    chomp($FDLIM);

    $FDLIM  = min($FDLIM,$maxfh);
    $FDLIM -= 2; # for the input + ouput files

    report("Max open files: ",$FDLIM+2);

    my $parse;
    my $width;
    if ($suffx eq 'bed') {
	$width = 3;
	$parse = sub {
	    my $fh = is_subclass(shift,'IO::Handle');
	    my $N  = is_uint(shift // 3);

	    while (defined( my $line = $fh->getline() )) {
		$line =~ /^#|^\s*$/ && next;
		chomp($line);

		my @tab = split("\t",$line);

		if (@tab < $N) {
		    throw("Malformed file: Missing required-field(s).");
		}
		return \@tab;
	    }
	};

    } else {   
	my ($META,$HEADER) = getMeta($fdi);
	$parse = \&Valkyr::FileUtil::VCF::getEntry;
	$width = @$HEADER;

	addMeta($META,'VCFtk',
	    {
		'_ORDER_' => [qw(ID Version Date CommandlineOptions)],
		'ID'      => 'sort-loci',
		'Version' => $VERSION,
		'Date'    => '"'.scalar(localtime).'"',
		'CommandlineOptions' => '"'.$cmd.'"'
	    });
	
	if (@$ref) { 
	    # replace ##contig lines (someday)
	} elsif (defined($META->{'contig'})) {
	    $ref = $META->{'contig'}->{'_ORDER_'} || [];
	} else {
	    throw('No reference sequence dictionary/index detected.');
	}
	
	print($fdo setMeta($META,$HEADER),"\n");

	$fdo->close();
    }
    # open LIMIT - 2 fileHandles for sorting into individual chroms;
    # if we reach the limit, close the filehandles, store the name by chrom
    # name; if we ever encounter another by the same name and the handle is
    # closed, open new handle and write.
    # At the very end, sort chrom files (one chrom at a time) by coord.

    report('Scattering...');

    my %file;
    my @opened;
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while(my $vcf  = $parse->($fdi,$width)) {
	progressCounter();

	my $contig = $vcf->[CHROM];
	if (!$file{$contig}{'open'}) {
	    if (@opened < $FDLIM) {
		openContig($contig,$prfx,$suffx,\%file,\@opened);
	    } else {
		closeContig(\%file,\@opened);
		openContig($contig,$prfx,$suffx,\%file,\@opened);
	    }
	}
	$file{$contig}{'fh'}->print(join("\t",@$vcf),"\n");
    }
    progressReport();
    resetCounter();

    while (@opened) {
	closeContig(\%file,\@opened);
    }


    report('Gathering... ');
    configCounter({'prefix' => 'Contigs processed: '});
    for my $contig (@$ref) {
	if (!$file{$contig}) { next }
	progressCounter();
	appendReorderedContig($contig,$prfx,$suffx,\%file,$sort);
    }

    for my $contig (sort(keys(%file))) {
	progressCounter();
	appendReorderedContig($contig,$prfx,$suffx,\%file,$sort);
    }
    progressReport();


    return 1;
}


sub SORTLOCI_USAGE {
    die qq/
Usage:   $PROGRAM sort-loci [options] <in.file> <out.prefix>

Options: -R FILE  faidx indexed target reference sequence [null]
         -M UINT  Maximum open filehandles [ulimit]
         -B       in.file is a BED file [VCF]
         -h       This help message


/;
}


sub sortloci {
    my (%argv,$ref);
    my ($suffx,$maxfh) = ('vcf',2**31 - 1);
    my $commandline = join(' ',$PROGRAM,'sort-loci',@ARGV);
    getopts('XhR:M:B',\%argv) && !$argv{'h'} && @ARGV == 2 || &SORTLOCI_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq 'B') { $suffx = 'bed' }
	elsif ($o eq 'M') { $maxfh = is_uint($argv{$o},\&SORTLOCI_USAGE) }
	elsif ($o eq 'R') { $ref   = is_file($argv{$o},'f',\&SORTLOCI_USAGE) }
    }

    report('Starting ',scalar(localtime));
    report('Command-line: ',$commandline);

    my $in  = shift(@ARGV);
    my $out = shift(@ARGV);
    my $fdi = open( $in,READ );
    my $fai = defined($ref) ? readOrderedList(faidxName($ref)) : [];
    my $fdo = open("$out.$suffx",WRITE,NOSTREAM);

    sortlociCore($fdo,$fdi,$fai,$out,$suffx,which('ulimit'),which('sort'),$maxfh,$commandline);

    close($fdi);
    close($fdo);

    report('Finished ',scalar(localtime));
    
    return 1;
}

sub sortindvCore {
    my $fdi = is_subclass(shift,'IO::Handle');
    my $fdo = is_subclass(shift,'IO::Handle');

    my ($META,$HEADER) = getMeta($fdi);

    my $width = 0;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my @FIELD = map{[$_, $width++]} @$HEADER;

    # sort sample fields, allowing for duplicate sample IDs:
    if ($FIELD{'FORMAT'}) {
	@FIELD = ( 
	    @FIELD[0..$FIELD{'FORMAT'}], 
	    sort_naturally2(@FIELD[($FIELD{'FORMAT'}+1)..$#FIELD])
	);
    }

    print($fdo setMeta($META,[map{$_->[0]} @FIELD]),"\n");
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $old_vcf = getEntry($fdi,$width)) {
	progressCounter();

	my @new_vcf = ();
	my $new_field = 0;
	for my $old_field (@FIELD) {
	    $new_vcf[$new_field] = $old_vcf->[$old_field->[1]];
	    $new_field++;
	}
	print($fdo join("\t",@new_vcf),"\n");
    }
    return 1;
}

sub SORTINDV_USAGE {
    die "Usage: $PROGRAM sort-indv <in.vcf> <out.vcf>\n";
}

sub sortindv {
    if (@ARGV != 2) {
	&SORTINDV_USAGE;
    }
    my $commandline = join(' ',$PROGRAM,'sort-indv',@ARGV);
    
    report('Starting ',scalar(localtime));
    report('Command-line: ',$commandline);

    my $in  = shift(@ARGV);
    my $out = shift(@ARGV);
    my $fdi = open($in, READ);
    my $fdo = open($out, WRITE);

    sortindvCore($fdi, $fdo);

    close($fdo);
    close($fdi);

    report('Finished ',scalar(localtime));
}

sub haploidCore {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $fdi = is_subclass(shift,'IO::Handle');
    my $opt = is_uint(shift);

    my ($META,$HEADER) = getMeta($fdi);

    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - ($FIELD{'INFO'}+2));

    my @lab = (".min", ".max");
    
    throw(sprintf('At least one sample required by %s()',
		  __FUNCTION__)) if ($nGeno < 1);

    if ($opt & 0x2) {
	my @header = ();
	for (my $i = FORMAT+1; $i < $width; ++$i) {
	    for (my $j = 0; $j < 2; ++$j) {
		$header[2*($i-FORMAT-1)+$j] = $HEADER->[$i] . $lab[$j];
	    }
	}
	@header = (@{$HEADER}[CHROM..FORMAT],@header);

	print($fdo setMeta($META,\@header),"\n");
    } else {
	print($fdo setMeta($META,$HEADER),"\n");
    }
    
    my $REQD = ['AD','PL'];
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) {
	progressCounter();

	if ($vcf->[ALT] =~ BREAKEND_PAT) {
	    warn(sprintf(BREAKEND_MSG,@{$vcf}[CHROM,POS]));
	    next;
	}

	my @alleles = ($vcf->[REF]);
	if ($vcf->[ALT] ne NULL_FIELD) {
	    push(@alleles,split(/,/,$vcf->[ALT]));
	}
	
	my $is_var = 0;
	my $is_het = 0;
	my %FORMAT = %{scalar( getFORMAT($vcf->[FORMAT],$REQD) )};
	my @samples = @{$vcf}[(FORMAT+1)..$#{$vcf}];
	my @output = ();
	
	for(my $i  = 0; $i < @samples; ++$i) {
	    my $I  = ($opt & 0x2) ? 2*$i+1 : $i;
	    my @g  = getSample($samples[$i],\%FORMAT);
	    
	    if ($g[$FORMAT{'GT'}] =~ /\./) {
		@g = map { NULL_ALLELE } @g;
		$is_het = 0;
		
	    } else {
		if (($g[$FORMAT{'GT'}] =~ GT_GROUP_PAT) && ($1 != $2)) {
		    $is_het = 1;
		} else {
		    $is_het = 0;
		}
		if ($g[$FORMAT{'PL'}] ne NULL_FIELD) {
		    my $Q = MAX_GQ;
		    my @p = map { 9999 } @alleles;
		    my @P = split(/,/,$g[$FORMAT{'PL'}]);
		    for(my $a = 0; $a < @alleles; ++$a) {
			$p[$a] = $P[(($a+1)*$a)/2+$a];
		    }
		    $g[$FORMAT{'GT'}] = argmin(\@p);
		    
		    for(my $j = 0; $j < @p; ++$j) {
			$p[$j] = int(is_ufloat($p[$j]-$p[$g[$FORMAT{'GT'}]]));
			if ($p[$j] && $p[$j] < $Q) { $Q = $p[$j] }
		    }
		    $g[$FORMAT{'PL'}] = join(',',@p);
		    $g[$FORMAT{'GQ'}] = $Q if $opt & 0x1;
		    
		    $is_var += $g[$FORMAT{'GT'}];
		    
		} elsif ($g[$FORMAT{'AD'}] ne NULL_FIELD) {
		    $g[$FORMAT{'GT'}] = argmax([ map {$_ eq NULL_FIELD ? 0 : $_} split(/,/,$vcf->[$FORMAT{'AD'}])]);	     
		    
		    $is_var += $g[$FORMAT{'GT'}];

		} elsif (($g[$FORMAT{'GT'}] =~ GT_GROUP_PAT) && ($1 == $2)) {
		    # GATK may not output AD or PL for non-variant sites:
		    $g[$FORMAT{'GT'}] = $1;
		    
		    $is_var += $g[$FORMAT{'GT'}];
		    
		} else {
		    throw(sprintf('Sample- AD or PL field(s) required for variant sites: %s:%u.',@{$vcf}[CHROM,POS]));
		}
	    }
	    if ($opt & 0x2) {
		my @_g = @g;
		if ($is_het) {
		    $_g[$FORMAT{'GT'}] = int(!$g[$FORMAT{'GT'}]);
		}
		$output[$I-1] = join(':',@_g);
	    }
	    $output[$I] = join(':',@g);
	}
	
	$vcf->[FORMAT] = setFORMAT(\%FORMAT);
	
	if (! $is_var) {
	    if ($opt & 0x2) {
		next;
	    }
	    $vcf->[ALT] = $vcf->[FILTER] = NULL_FIELD;
	}
	$vcf = [@{$vcf}[CHROM..FORMAT],@output];
	
	print($fdo join("\t",@$vcf),"\n");
    }
    progressReport();

    return 1;
}


sub HAPLOID_USAGE {
    die "Usage: $PROGRAM haploid [-hmQ] [-o <out.vcf>] <in.vcf>\n";
}


sub haploid {
    my %argv;
    my $opt = 0;
    my $out = STDIO;
    my $commandline = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('Xhmo:Q',\%argv) && !$argv{'h'} && @ARGV == 1 || &HAPLOID_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq 'o') { $out  = is_string($argv{$o},\&HAPLOID_USAGE) }
	elsif ($o eq 'Q') { $opt |= 0x1 }
	elsif ($o eq 'm') { $opt |= 0x2 }
    }

    report('Starting ',scalar(localtime));
    report('Command-line: ',$commandline);

    my $vcf = shift(@ARGV);
    my $fdi = open($vcf,READ);
    my $fdo = open($out,WRITE);

    haploidCore($fdo,$fdi,$opt);

    close($fdo);
    close($fdi);

    report('Finished ',scalar(localtime));
    
    return 1;
}
    

sub phylipCore {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $fdi = is_subclass(shift,'IO::Handle');
    my $opt = is_uint(shift);

    my ($META,$HEADER) = getMeta($fdi);

    my $REQD  = ['FT'];
    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - ($FIELD{'INFO'}+2));

    throw(sprintf('At least two samples required by %s()',
		  __FUNCTION__)) if ($nGeno < 2);

    my @alignment;
    my $is_diploid = 0;
    my $num_sites  = 0;
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) {
	progressCounter();

	if ($vcf->[FILTER] !~ PASS_FILTER_PAT) { next }
	if (is_indel($vcf->[REF],$vcf->[ALT]) || $vcf->[ALT] =~ BREAKEND_PAT) { 
	    warn('Breakend and indels not supported. skipping.');
	    next;
	}
	
	my @alleles = ($vcf->[REF]);
	if ($vcf->[ALT] ne NULL_FIELD) {
	    push(@alleles, (map {is_char($_)} split(/,/,$vcf->[ALT])));
	}

	my %FORMAT = %{scalar( getFORMAT($vcf->[FORMAT],$REQD) )};
	for(my $i  = 0; $i < $nGeno; ++$i) {
	    my @g  = split(/:/,$vcf->[$i+FORMAT+1]); 
	    # cant use getSample() here because it will throw 
	    # an exception when a haploid genotype is encountered
	    if ($is_diploid || $g[$FORMAT{'GT'}] =~ GT_FIELD_SEP) { 
		$is_diploid = 1;
		if ($g[$FORMAT{'GT'}] =~ /\./) { 
		    $g[$FORMAT{'GT'}] = NULL_ALLELE.GT_PHASED.NULL_ALLELE;
		}
		if (defined($g[$FORMAT{'FT'}]) && $g[$FORMAT{'FT'}] !~ PASS_FILTER_PAT) {
		    $alignment[2*$i  ] .= '-';
		    $alignment[2*$i+1] .= '-';
		} elsif (is_phased(\@g)) {
		    @g = split(GT_FIELD_SEP,$g[$FORMAT{'GT'}]);
		    $alignment[2*$i  ] .= $g[0] eq NULL_ALLELE ? '-' : is_char($alleles[$g[0]]);
		    $alignment[2*$i+1] .= $g[1] eq NULL_ALLELE ? '-' : is_char($alleles[$g[1]]);
		} else {
		    throw('Cannot format unphased genotypes into PHYLIP format.');
		}
	    } elsif (length($g[$FORMAT{'GT'}]) == 1) { # is haploid
		if (defined($g[$FORMAT{'FT'}]) && $g[$FORMAT{'FT'}] !~ PASS_FILTER_PAT) {
		    $alignment[$i] .= '-';
		} else {
		    $alignment[$i] .= $g[$FORMAT{'GT'}] eq NULL_ALLELE ? '-' : is_char($alleles[$g[$FORMAT{'GT'}]]);
		}
	    } else {
		throw(sprintf('Invalid GT format: %s (%s:%u,%s).',$g[$FORMAT{'GT'}],
			      @{$vcf}[CHROM,POS],$HEADER->[$i+FORMAT+1]));
	    }
	}
	if ($opt & 0x2){
	    $alignment[$nGeno] .= $vcf->[REF];
	}
	$num_sites++;
    }
    progressReport();
    

    my $hap1 = '.1';
    my $hap2 = '.2';
    my $posindex = 0;
    my $blockfmt = 0;
    my $blocklen = $opt & 0x1 ? 60 : $num_sites;
    for(my $i = FORMAT+1; $i < $width; ++$i) {
	if (length($HEADER->[$i]) > $blockfmt) {
	    $blockfmt = length($HEADER->[$i]); 
	}
    }
    $blockfmt += 2;
    $blockfmt = '%-'.$blockfmt."s  %s\n";
    
    printf($fdo " %u %u\n", scalar(@alignment), $num_sites);
    for(my $j = 0; $j < $num_sites; $j += $blocklen) {
	if ($opt & 0x2) {
	    printf($fdo $blockfmt,$HEADER->[REF],substr($alignment[$nGeno],$j,$blocklen));
	}
	for(my $i = 0; $i < $nGeno; ++$i) {
	    if ($is_diploid) {
		printf($fdo $blockfmt,$HEADER->[$i+FORMAT+1].$hap1,substr($alignment[2*$i  ],$j,$blocklen));
		printf($fdo $blockfmt,$HEADER->[$i+FORMAT+1].$hap2,substr($alignment[2*$i+1],$j,$blocklen));
	    } else {
		printf($fdo $blockfmt,$HEADER->[$i+FORMAT+1],substr($alignment[$i],$j,$blocklen));
	    }
	    $HEADER->[$i+FORMAT+1] = '';
	}
	print($fdo "\n\n");
	$hap1 = '';
	$hap2 = '';
    }
}

sub PHYLIP_USAGE {
    die "Usage: $PROGRAM phylip [-hIR] [-o <out.phylip>] <in.vcf>\n";
}


sub phylip {
    my %argv;
    my $opt = 0; # interleaved format = 0x1
    my $out = STDIO;
    my $commandline = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('XhIRo:',\%argv) && !$argv{'h'} && @ARGV == 1 || &PHYLIP_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq 'I') { $opt |= 0x1 }
	elsif ($o eq 'R') { $opt |= 0x2 }
	elsif ($o eq 'o') { $out = is_string($argv{$o},\&PHYLIP_USAGE) }
    }

    report('Starting ',scalar(localtime));
    report('Command-line: ',$commandline);

    my $vcf = shift(@ARGV);
    my $fdi = open($vcf,READ);
    my $fdo = open($out,WRITE);

    phylipCore($fdo,$fdi,$opt);

    close($fdo);
    close($fdi);

    report('Finished ',scalar(localtime));
    
    return 1;
}   


sub calcAB {
    my $gt  = is_arrayref(shift);
    my $ad  = is_arrayref(shift);
    my $opt = is_uint(shift//0);

    if ($gt->[0] ne NULL_ALLELE && $gt->[1] ne NULL_ALLELE) {
	for(my $i = 0; $i < @$ad; ++$i) {
	    if (! Valkyr::Data::Type::Test::is_uint($ad->[$i])) {
		$ad->[$i] = 0;
	    }
	}	
	if (!$opt && ($gt->[0] ne $gt->[1])) {
	    my $a = argmin($gt);
	    my $d = $ad->[$gt->[$a]];
	    return sprintf('%0.3f',$ad->[$gt->[$a]]/(sum(grep{!/\./}@$ad)||1));

	} elsif ($opt && ($gt->[0] ne $gt->[1])) {
	    return sprintf('%0.3f',$ad->[0]/(sum(grep{!/\./}@$ad)||1));
	}
    }
    return NULL_FIELD;
}


sub filterGTCore {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $fdi = is_subclass(shift,'IO::Handle');
    my $bed = is_arrayref(shift);
    my $FLT = is_hashref(shift);
    my $opt = is_uint(shift);
    my $cmd = is_string(shift);
    my $idx;

    my %FILTER;
    for my $sample (keys(%{$FLT})) {
	for my $filter (keys(%{$FLT->{$sample}})) {
	    $FILTER{$filter} = 1;
	}
    }

    my ($META,$HEADER) = getMeta($fdi);
    
    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - ($FIELD{'INFO'}+2));

    if (defined($META->{'contig'})) {
	$idx = indexArray(@{$META->{'contig'}->{'_ORDER_'}||[]});
    } else {
	throw('No \'##contig\' Meta entries detected.');
    }

    throw(sprintf('At least one sample required by %s().',
		  __FUNCTION__)) if ($nGeno < 1);
    
    
    for my $sample (sort keys(%$FLT)) {
	if (! $FIELD{$sample}) {
	    throw("Sample not defined in VCF header: $sample");
	} elsif ($FIELD{$sample} <= FORMAT) {
	    throw("Invalid sample name: $sample");
	}
    }
    
    my @format = sort(keys(%FILTER));
    my $REQD = [ @format ];
    if (!$FILTER{'FT'}) { unshift(@$REQD,'FT') }
    if (($opt & 0x8) && !$FILTER{'AD'}) { unshift(@$REQD,'AD') }
    if (($opt & 0x8) && !$FILTER{'AB'}) { unshift(@$REQD,'AB') }
    addMeta($META,'VCFtk',
	    {
		'_ORDER_' => [qw(ID Version Date CommandlineOptions)],
		'ID'      => basefunc(__FUNCTION__),
		'Version' => $VERSION,
		'Date'    => '"'.scalar(localtime).'"',
		'CommandlineOptions' => '"'.$cmd.'"'
	    });
    addMeta($META,'FORMAT',
	    {
		'_ORDER_' => [qw(ID Number Type Description)],
		'ID'      => 'FT',
		'Number'  => 1,
		'Type'    => 'String',
		'Description' => '"Genotype-level filter"'
	    });
    addMeta($META,'FORMAT',
	    {
		'_ORDER_' => [qw(ID Number Type Description)],
		'ID'      => 'AB',
		'Number'  => 1,
		'Type'    => 'Float',
		'Description' => '"Reference allele balance"'
	    });

    for my $filter (@format) {
	addMeta($META,'FILTER',
		{
		    '_ORDER_' => [qw(ID Description)],
		    'ID'      => 'X'.$filter,
		    'Description' => "\"Fails user-input genotype-level $filter filter\""
		});
	for my $sample (@$HEADER[(FORMAT+1)..($width-1)]) {
	    if(!$FLT->{$sample}->{$filter}) {
		$FLT->{$sample}->{$filter} = {
		    'MIN' => -1,
		    'MAX' => inf,
		    'NEG' => 0
		};
	    }
	}
    }    

    print($fdo setMeta($META,$HEADER),"\n");

    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) {
	progressCounter();
	
	if ($vcf->[FILTER] !~ PASS_FILTER_PAT) {
	    print($fdo join("\t",@$vcf),"\n");
	    next;
	}
	if ($vcf->[ALT] =~ BREAKEND_PAT) {
	    warn(sprintf(BREAKEND_MSG,@{$vcf}[CHROM,POS]));
	    print($fdo join("\t",@$vcf),"\n");
	    next;
	}
	
	my @cache;
	my $intersects = 1;
	if ($opt & 0x2) {
	    if (intersectsInterval($bed,$idx,@{$vcf}[CHROM,POS])) {
		$intersects = 1;
	    } else {
		$intersects = 0;
	    }
	    if ($opt & 0x4) {
		$intersects = !$intersects;
	    }
	}

	if ($intersects) {
	    my %FORMAT = %{scalar( getFORMAT($vcf->[FORMAT],$REQD) )};
	    for(my $i = FORMAT+1; $i < $width; ++$i) {
		if ($vcf->[$i] =~ /^\./) { next }
		if (! $FLT->{$HEADER->[$i]}) {
		    if ($opt & 0x10) {
			throw("Sample not defined in filter spec: $HEADER->[$i]");			
		    }
		}
		
		my @v = getSample($vcf->[$i],\%FORMAT);
		my $s = $FLT->{$HEADER->[$i]};	    
		my @g = split(GT_FIELD_SEP,$v[$FORMAT{'GT'}]);

		if ($opt & 0x8) {
		    $v[$FORMAT{'AB'}] = calcAB(\@g, [ split(/,/,$v[$FORMAT{'AD'}]) ], $s->{'AB'}->{'NEG'});
		}
		
		my @fltr;
		my $fail = 0;
		for my $f (@format) {
		    if($v[$FORMAT{$f}] eq NULL_FIELD) { next }
		    my $fail = 0;
		    my $F = is_hashref($s->{$f});
		    my $x = is_ufloat($v[$FORMAT{$f}]);
		    
		    if (($F->{'MIN'} > $x) || 
			($F->{'MAX'} < $x)) {
			$fail = 1;
		    }
		    if ($F->{'NEG'}) { 
			$fail = !$fail;
		    }
		    if ($fail) {
			push(@fltr,"X$f");
		    }
		}
		$v[$FORMAT{'GT'}] = NULL_GT if (($opt & 0x1) && @fltr);
		$v[$FORMAT{'FT'}] = addFT($v[$FORMAT{'FT'}],@fltr);

		$vcf->[$i] = join(':',@v);
	    }

	    $vcf->[FORMAT] = setFORMAT(\%FORMAT);
	}

	print($fdo join("\t",@$vcf),"\n");
    }
    progressReport();
    

    return 1;
}

sub readSampleConfig {
    my $file = is_file(shift,'f');
    
    # future supported keywords/actions:
    # FXN, MIN, MAX, NEGate
    # FXN would take a user-defined function and eval() it.

    my %seen;
    my $data = {};
    my $fd = open($file,READ,NOSTREAM);
    while (my $line = $fd->getline()) {
	if ($line =~ /^#|^\s*$/) { next }
	
	my @line = split(/\t/,$line);
	
	if ($seen{is_string($line[0]).':'.is_string($line[1])}) {
	    warn(sprintf('Sample seen multiply: %s:%s',$line[0],$line[1]));
	}

	$data->{$line[0]}{is_string(length($line[1]) ? $line[1] : undef)} = {
	    'MIN' => is_ufloat($line[2]),
	    'MAX' => is_ufloat($line[3]),
	    'NEG' => is_bool($line[4])
	};
    }
    close($fd);

    return $data;
}
	    
sub FILTERGT_USAGE {
    die qq/
Usage:   $PROGRAM filterGT [options] [-o <out.vcf>] <in.vcf> <in.spec>

Options: -b FILE  Input BED file of target regions to filter
         -o FILE  Write output to FILE [stdout]
         -A       Calculate ref-allele balance from allele depth
         -C       Require samples in in.spec to be set-complete
         -H       Hard filter GT fields failing filters [soft]
         -x       Exclude {-b} target regions from filtering
         -h       This help message

Notes:

  in.spec has the following tabular format:
    <SampleID>\\t<Field>\\t<MinValue>\\t<MaxValue>\\t<Exclude>\\n

  SampleID   VCF header sample ID to apply the filter to (string)
  Field      The FORMAT sub-field to apply the filter to (string)
  MinValue   The (inclusive) minimum threshold value (unsigned float)
  MaxValue   The (inclusive) maximum threshold value (unsigned float)
  Exclude    Filter genotypes between (rather than more extreme than)
             the MinValue and MaxValue thresholds (bool: 0|1)

  
/;
}

sub filterGT {
    my %argv;
    my $options  = 0;
    my $outfile  = STDIO;
    my $interval = undef;
    my $command  = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('AXho:b:CHx',\%argv) && !$argv{'h'} && @ARGV == 2 || &FILTERGT_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq 'H') { $options |= 0x1 }
	elsif ($o eq 'x') { $options |= 0x4 }
	elsif ($o eq 'A') { $options |= 0x8 }
	elsif ($o eq 'C') { $options |= 0x10 }
	elsif ($o eq 'b') { $options |= 0x2; 
			    $interval = is_string($argv{$o},\&FILTERGT_USAGE) }
	elsif ($o eq 'o') { $outfile  = is_string($argv{$o},\&FILTERGT_USAGE) }
    }
    
    report('Starting ',scalar(localtime));
    report('Command-line: ',$command);
    
    my $cdp  = readSampleConfig($ARGV[1]);
    my $fdi  = open($ARGV[0],READ);
    my $fdo  = open($outfile,WRITE);
    my $bed  = $options & 0x2 ? readRNG($interval) : [];

    filterGTCore($fdo,$fdi,$bed,$cdp,$options,$command);

    report('Finished ',scalar(localtime));

    close($fdo);
    close($fdi);

    return 1;
}


sub popvarCore {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $fdi = is_subclass(shift,'IO::Handle');
    my $pop = is_arrayref(shift);
    my $frq = is_arrayref(shift);
    my $mis = is_arrayref(shift);
    my $opt = is_uint(shift);

    my %FILTER;
    my ($META,$HEADER) = getMeta($fdi);
        
    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - ($FIELD{'INFO'}+2));
    my $REQD  = ['FT'];
    
    throw(sprintf('At least two samples required by %s().',
		  __FUNCTION__)) if ($nGeno < 2);

    my @col;
    my @frq;
    my @dat;
    my @num;
    my %seen;
    if (@{$pop->[1]} < 1) {
	%seen = map{$_=>1} @{$pop->[0]};
	for my $sample (@{$HEADER}[(FORMAT+1)..($width-1)]) {
	    if (! $seen{$sample}) {
		push(@{$pop->[1]},$sample);
	    }
	}
	%seen = ();
    }
    for(my $j = 0; $j < @$pop; ++$j) {
	for my $sample (sort @{$pop->[$j]}) {
	    if (! $FIELD{$sample}) {
		throw("Sample not defined in VCF header: $sample");
	    } elsif ($FIELD{$sample} <= FORMAT) {
		throw("Invalid sample name: $sample");
	    } elsif ($seen{$sample}) {
		throw("Sample specified in both populations: $sample");
	    }
	}
	$pop->[$j] = [ sort @{$pop->[$j]} ];
	$col[$j] = [ @FIELD{ @{$pop->[$j]} } ];
	$num[$j] = @{$pop->[$j]};

	report('Pop. member(s): [',join(', ',@{$pop->[$j]}),']');
    }
    if (($num[0] < 1) || ($num[1] < 1)) {
	throw('Too few samples to contrast');
    }
    $num[0] = max(1, $num[0]);
    $num[1] = max(1, $num[1]);
    
    my $missA = ($mis->[0] >= 1) ? $mis->[0] / $num[0] : $mis->[0];
    my $missB = ($mis->[1] >= 1) ? $mis->[1] / $num[1] : $mis->[1];
    my $freqA = 0;
    my $freqB = 0;
    
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
  SITE:
    while (my $vcf = getEntry($fdi,$width)) {
	progressCounter();
	
	if ($vcf->[FILTER] !~ PASS_FILTER_PAT) {
	    next;
	}
	if ($vcf->[ALT] =~ BREAKEND_PAT) {
	    warn(sprintf(BREAKEND_MSG,@{$vcf}[CHROM,POS]));
	    next;
	}

	my @alleles = ($vcf->[REF], split(ALLELE_FIELD_SEP, $vcf->[ALT]));

	if (@alleles > 2) {
	    next
	}
	
	$dat[0] = $dat[1] = 0;
	$frq[0] = [ map{0}@alleles ];
	$frq[1] = [ map{0}@alleles ];
	my %FORMAT = %{scalar( getFORMAT($vcf->[FORMAT],$REQD) )};
	for(my $j = 0; $j < @$pop; ++$j) {
	    for my $i (@{$col[$j]}) {
		my @v = getSample($vcf->[$i],\%FORMAT);
		
		if (($v[$FORMAT{'GT'}] !~ /\./) &&
		    ($v[$FORMAT{'FT'}] =~ PASS_FILTER_PAT)) {
		    my @g = split(GT_FIELD_SEP, $v[$FORMAT{'GT'}]);
		    $frq[$j][$g[0]]++;
		    $frq[$j][$g[1]]++;
		    $dat[$j]++;
		}
	    }
	    for(my $i = 0; $i < @alleles; ++$i) {
		$frq[$j][$i] /= max(1, 2*$dat[$j]);
	    }
	}
	$freqA = ($frq->[0] >= 1) ? $frq->[0] / max(1, 2*$dat[0]) : ($frq->[0] >= 0) ? $frq->[0] : 1.0;
	$freqB = ($frq->[1] >= 1) ? $frq->[1] / max(1, 2*$dat[1]) : ($frq->[1] >= 0) ? $frq->[1] : 1.0;

	if (((1 - ($dat[0] / $num[0])) > $missA) ||
	    ((1 - ($dat[1] / $num[1])) > $missB)) {
	    next;
	}
	for(my $a = $#alleles; $a >= 0; --$a) {
	    if (($frq[0][$a] >= $freqA) && ($frq[1][$a] == 0)) {
		print($fdo join("\t","$vcf->[CHROM]:$vcf->[POS]","$vcf->[REF],$vcf->[ALT]",$a),"\n");
		last;
	    } elsif ($opt & 0x2) {
		next;
	    } elsif (($frq[1][$a] >= $freqB) && ($frq[0][$a] == 0)) {	
		print($fdo join("\t","$vcf->[CHROM]:$vcf->[POS]","$vcf->[REF],$vcf->[ALT]",$a),"\n");
		last;
	    }
	}
    }
    progressReport();
	
    return 1;
}
    

sub POPVAR_USAGE {
    die qq/
Usage:   $PROGRAM popvar [options] <in.vcf>

Options: -A FILE   File listing samples in pop A (required)
         -B FILE   File listing samples in pop B
         -a FLOAT  Max missing data fraction of pop A [0.0]
         -b FLOAT  Max missing data fraction of pop B [0.0]
         -o FILE   Write output to FILE [stdout]
         -p FLOAT  Min allele frequency in pop A [-1]
         -q FLOAT  Min allele frequency in pop B [-1]
         -h        Write this help message and exit

Notes:

  1. Specifying only -A scans the samples for A-specific alleles
     contrasted against all other samples in the VCF, whereas 
     specifying -A and -B scans for alleles present\/absent with
     respect to the two populations, ignoring the presence\/absence
     of those alleles in any samples not listed in -A or -B.

  2. Setting -a and\/or -b to a value < 1 specifies the fraction
     of no-call genotypes permitted within a population; values 
     >= 1, the permitted counts of no-call genotypes.

  3. Setting -p and\/or -q to a negative value triggers requiring
     a fixed allele frequency within that population. Setting -p 
     and\/or -q to a value >= 1 will specify the allele count 
     required to be observed.

/;
}

sub popvar {
    my %argv;
    my $options = 0;
    my $popA = [];
    my $popB = [];
    my $freqA = -1;
    my $freqB = -1;
    my $missA = 0;
    my $missB = 0;
    my $outfile = STDIO;
    my $command = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('XFho:A:B:a:b:p:q:',\%argv) && !$argv{'h'} && @ARGV == 1 || &POPVAR_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq 'F') { $options |= 0x1 }
	elsif ($o eq 'a') { $missA    = is_ufloat($argv{$o},\&POPVAR_USAGE) }
	elsif ($o eq 'b') { $missB    = is_ufloat($argv{$o},\&POPVAR_USAGE) }
	elsif ($o eq 'p') { $freqA    = is_sfloat($argv{$o},\&POPVAR_USAGE) }
	elsif ($o eq 'q') { $freqB    = is_sfloat($argv{$o},\&POPVAR_USAGE) }
	elsif ($o eq 'o') { $outfile  = is_string($argv{$o},\&POPVAR_USAGE) }
    }
    if (defined($argv{'A'})) {
	$popA = readOrderedList($argv{'A'});
    } else {
	throw('Must define A population to contrast.');
    }
    if (defined($argv{'B'})) {
	$popB = readOrderedList($argv{'B'});
    } else {
	$options |= 0x2;
    }
    if (($missA < 0) || ($missB < 0)) {
	throw("Missingness must be in range [0..1]");
    }

    report('Starting ',scalar(localtime));
    report('Command-line: ',$command);
    
    my $fdi  = open($ARGV[0], READ);
    my $fdo  = open($outfile, WRITE);

    popvarCore($fdo,$fdi,[$popA,$popB],[$freqA,$freqB],[$missA,$missB],$options);

    report('Finished ',scalar(localtime));

    close($fdo);
    close($fdi);

    return 1;
}


sub reduceCore {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $fdi = is_subclass(shift,'IO::Handle');
    my $rmv = is_hashref(shift);

    my ($META,$HEADER) = getMeta($fdi);

    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - ($FIELD{'INFO'}+2));
    my $refsq = $META->{'contig'};

    if ($refsq && %$refsq) {
	# remove contigs listed in $rmv
	my @order;
	for my $contig (@{$refsq->{'_ORDER_'}}) {
	    if ($rmv->{$contig}) {
		delete($refsq->{$contig});
	    } else {
		push(@order,$contig);
	    }
	}
	$refsq->{'_ORDER_'} = \@order;
    }

    print($fdo setMeta($META,$HEADER),"\n");
    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) {
	progressCounter();
	
	if ($rmv->{$vcf->[CHROM]}) { next }

	print($fdo join("\t",@$vcf),"\n");
    }
    return 1;
}


sub REDUCE_USAGE {
    die sprintf("Usage: %s reduce [-h] [-o <out.vcf>] <in.vcf> <rm.list>\n",basename($0));
}


sub reduce {
    my %argv;
    my $outfile = STDIO;
    my $command = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('ho:',\%argv) && !$argv{'h'} && @ARGV == 2 || &REDUCE_USAGE;
    for my $o (keys(%argv)) {
	if ($o eq 'o') { $outfile = is_string($argv{$o},\&REDUCE_USAGE) }
    }

    report('Starting ',scalar(localtime));
    report('Command-line: ',$command);

    my $list = readList($ARGV[1]);
    my $fdi  = open($ARGV[0],READ);
    my $fdo  = open($outfile,WRITE);

    reduceCore($fdo,$fdi,$list);

    report('Finished ',scalar(localtime));

    close($fdo);
    close($fdi);

    return 1;
}


sub stripGTCore {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $fdi = is_subclass(shift,'IO::Handle');

    my ($META,$HEADER) = getMeta($fdi);
    
    my $REQD  = [];
    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - ($FIELD{'INFO'}+2));
    
    throw(sprintf('At least one sample required by %s().',
		  __FUNCTION__)) if ($nGeno < 1);
    
    
    print($fdo setMeta($META,$HEADER),"\n");

    configCounter({'prefix' => 'Sites processed: ', 'multiplier' => 1.5});
    while (my $vcf = getEntry($fdi,$width)) {
	progressCounter();
		
	my %FORMAT = %{scalar( getFORMAT($vcf->[FORMAT],$REQD) )};
	for(my $i = FORMAT+1; $i < $width; ++$i) {
	    my @v = getSample($vcf->[$i],\%FORMAT);
	    	    
	    $vcf->[$i] = $v[$FORMAT{'GT'}];
	}
	
	$vcf->[FORMAT] = setFORMAT({'GT' => 0});

	print($fdo join("\t",@$vcf),"\n");
    }
    progressReport();
    

    return 1;
}


sub STRIPGT_USAGE {
    die sprintf("Usage: %s stripGT [-h] [-o <out.vcf>] <in.vcf>\n",basename($0));
}


sub stripGT {
    my %argv;
    my $outfile = STDIO;
    my $command = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('ho:',\%argv) && !$argv{'h'} && @ARGV == 1 || &STRIPGT_USAGE;
    for my $o (keys(%argv)) {
	if ($o eq 'o') { $outfile = is_string($argv{$o},\&STRIPGT_USAGE) }
    }

    report('Starting ',scalar(localtime));
    report('Command-line: ',$command);

    #my $list = readList($ARGV[1]);
    my $fdi  = open($ARGV[0],READ);
    my $fdo  = open($outfile,WRITE);

    stripGTCore($fdo,$fdi);

    report('Finished ',scalar(localtime));

    close($fdo);
    close($fdi);

    return 1;
}


sub listCore {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $fdi = is_subclass(shift,'IO::Handle');
    my $opt = is_uint(shift);

    my ($META,$HEADER) = getMeta($fdi);

    my $sample = ($opt & 0x1) ? 9 : 0;
    my $offset = ($opt & 0x1) && ($opt & 0x2) ? 9 : 0;
    my $system = ($opt & 0x4) ? 1 : 0;
    for(my $i  = $sample; $i < @$HEADER; ++$i) {
	print($fdo join("\t",$i+$system-$offset,$HEADER->[$i]),"\n");
    }

    return 1;
}


sub LIST_USAGE {
    die qq/
Usage:   $PROGRAM list [options] <in.vcf>

Options: -i   Initiate count from first output field
         -f   List field number [index]
         -S   List only samples [all]
         

/;
}


sub list {
    my %argv;
    my $outfile = STDIO;
    my $options = 0;
    # my $command = join(' ',$PROGRAM,basefunc(__FUNCTION__),@ARGV);
    getopts('hiSf',\%argv) && !$argv{'h'} && @ARGV == 1 || &LIST_USAGE;
    for my $o (keys(%argv)) {
	if    ($o eq 'S') { $options |= 0x1 }
    	elsif ($o eq 'i') { $options |= 0x2 }
    	elsif ($o eq 'f') { $options |= 0x4 }
    }

    # report('Starting ',scalar(localtime));
    # report('Command-line: ',$command);

    #my $list = readList($ARGV[1]);
    my $fdi  = open($ARGV[0],READ);
    my $fdo  = open($outfile,WRITE);

    listCore($fdo,$fdi,$options);

    # report('Finished ',scalar(localtime));

    close($fdo);
    close($fdi);

    return 1;
}



sub VERSION_MESSAGE {
    report("v$VERSION");
    exit(1);
}


sub HELP_MESSAGE {
    die qq/
Program: $PROGRAM ($PURPOSE)
Version: $PACKAGE $VERSION
Contact: $CONTACT

Usage:   $PROGRAM <command> [options]

Command: eigen      VCF->EIGENSOFT (EIGENSTRAT) format
         frappe     VCF->FRAPPE PED format
         gvf        VCF->GVF\/GFF3 format
         jmloc      VCF->JoinMap .loc format
         phylip     VCF->PHYLIP format
         raxbin     VCF->raxml binary char alignment format
         structure  VCF->STRUCTURE .txt format

         asd        Calc pair-wise allele-sharing distances
         chif1      Apply Mendelian departure P-value for F1s
         Dst        Calc pair-wise divergence
         ibs        Calc and plot pair-wise IBS estimates
         hwexact    Calc HW departure by Fisher's Exact test
         mtvr       Calc F1 Mendelian Transmission Violation Rate
         popvar     Identify fixed differences between two pops
         snvrate    Calc SNV rate for a single individual
         trio       Test a population for trios

         annotate   Add annotations to INFO or genotype field
         binvar     Select highest scoring variant per bin
         filterGT   Apply genotype-level filters
         stripGT    Remove genotype-level annotations
         haploid    Force diploid genotypes to haploid
         jmphase    Transfer JM4.1 CP phase info to P0s in VCF
         reduce     Exclude reference contigs from VCF
         sort-loci  Sort VCF or BED by reference order
         sort-indv  Sort individuals in a VCF
         list       List header and exit

Notes:   

  1. All commands assume diploid individuals.

  2. '*' indicates command is deprecated.


/;
}
  

main: {
    my %FUNCTION = (
	'raxbin'    => \&raxbin,   'structure' => \&structure,
	'hwexact'   => \&hwexact,  'snvrate'   => \&snvrate,   
	'binvar'    => \&binvar,   'Dst'       => \&Dst,
	'jmloc'     => \&jmloc,    'gvf'       => \&gvf,
	'eigen'     => \&eigen,    'chif1'     => \&chif1,
	'initb'     => \&initb,    'mergeb'    => \&mergeb,
	'ibs'       => \&ibs,      'hapbin'    => \&hapbin,
	'hapmarc'   => \&hapmarc,  'reorder'   => \&sortloci,
	'mtvr'      => \&mtvr,     'annotate'  => \&annotate,
	'jmphase'   => \&jmphase,  'reduce'    => \&reduce,
	'phylip'    => \&phylip,   'haploid'   => \&haploid,
	'frappe'    => \&frappe,   'filterGT'  => \&filterGT,
	'popvar'    => \&popvar,   'stripGT'   => \&stripGT,
	'list'      => \&list,     'trio'      => \&trio,
	'sort-loci' => \&sortloci, 'asd'       => \&asd,
	'sort-indv' => \&sortindv,
	'--version' => \&VERSION_MESSAGE,
	'-version'  => \&VERSION_MESSAGE,
	'-v'        => \&VERSION_MESSAGE,
	'--help'    => \&HELP_MESSAGE,
	'-help'     => \&HELP_MESSAGE,
	'-h'        => \&HELP_MESSAGE,
	);
    
    my $CMD = shift( @ARGV ) || &HELP_MESSAGE;
    if (! exists($FUNCTION{ $CMD })) { 
	report("Unrecognized command: $CMD");
	exit 1;
    }
    &{$FUNCTION{ $CMD }} || throw("$CMD unexpectedly aborted");
    
    exit 0;
}

__END__


