
my $PACKAGE = '__PACKAGE_NAME__';
my $VERSION = '__PACKAGE_VERSION__';
my $CONTACT = '__PACKAGE_CONTACT__';
my $PURPOSE = 'Clip linker from mate-pair reads';

use strict;
use warnings;

use Getopt::Long;
use File::Basename;
use IO::Compress::Gzip;
use IO::Uncompress::Gunzip;

use vars qw($PROGRAM $HEADER);

BEGIN {
    $PROGRAM = basename($0);
    $HEADER = qr/^@([^\s\/]+)/ 
}

sub revS {
    my $rev = reverse(shift);
    $rev =~ tr/atcgATCG/tagcTAGC/;
    return $rev;
}

sub revQ {
    return scalar(reverse(shift));
}

sub readLinkerBED {
    my $bed     = shift;
    my $end     = shift;
    my $linker  = shift;
    my $exclude = shift;
    my $CHRNAME = qr/^([^\/\s]+)/;

    open(LINKER,$bed) || die "Cannot open end $end linker BED $bed for reading: $!\n";
    while (<LINKER>) {
	m/^#|^\s*$/ && next;
	chomp();
	
	my @bed = split(/\t/);
	$bed[0] =~ $CHRNAME || die "Malformed BED entry chr: $bed[0]\n";
	$bed[0] = $1;
	$bed[4] = $bed[4] + 0; # coerce to numeric
	my $lnk = $bed[1]; # "$bed[1]:$bed[2]"

	if ($exclude->{$bed[0]}) { next }
	if (defined($linker->{$end}->{$bed[0]})) {
	    if ($lnk == $linker->{$end}->{$bed[0]}) { next }
	    print(STDERR "Possible chimeric sequence, discarding: $bed[0]\n");
	    my $chim = $linker->{$end};
	    delete($chim->{$bed[0]});
	    $exclude->{$bed[0]}++;
	    
	} elsif (~$bed[4] & 0x1) { 
	    $exclude->{$bed[0]}++;
	} else {
	    $linker->{$end}->{$bed[0]} = $lnk;
	}
    }
    close(LINKER);

    return 1;
}

sub readLinkerData {
    my $bed  = shift;
    my $end  = shift;
    my $lnk  = shift;
    my $chm  = shift;

    if ($end & 0x1) {
	if (length($bed->[0]||'')) {
	    readLinkerBED($bed->[0],$end & 0x1,$lnk,$chm);
	} else {
	    die "No forward bed file passed.\n";
	}
    }
    if ($end & 0x2) {
	if (length($bed->[1]||'')) {
	    readLinkerBED($bed->[1],$end & 0x2,$lnk,$chm);
	} else {
	    die "No reverse bed file passed.\n";
	}
    }
    $end || die "Please specify a forward or reverse direction\n";
    
    return 1;
}

sub read_fq {
    my $fh = shift;
    my @entry;
    $entry[0] = $fh->getline() // return; # allow catching invalid fastq formats
    $entry[0] && $entry[0] =~ /^\@/ || 
        die("Fastq header expected, line ",$fh->input_line_number(),"\n");
    $entry[1] = $fh->getline();
    $entry[2] = $fh->getline();
    $entry[3] = $fh->getline() // 
        die("Unexpected EOF, line ",$fh->input_line_number(),"\n");
    $entry[2] && $entry[2] =~ /^\+/ || 
        die("Must use Illumina 4-line fastq format\n");

    return(\@entry);
}

sub linkerClipper {
    my $fq  = shift;
    my $end = shift;
    my $linker  = shift;
    my $exclude = shift;

    my $status = 0;
    if ($fq->[0] =~ $HEADER) {
	if ($exclude->{$1}) { return $status }

	$fq->[0] = "\@$1/$end\n";
	$fq->[2] = "+\n";
	if (defined($linker->{$end}->{$1})) { 
	    #requires clipping
	    $fq->[1] = substr($fq->[1],0,$linker->{$end}->{$1})."\n";
	    $fq->[3] = substr($fq->[3],0,$linker->{$end}->{$1})."\n";
	}
	$status = 1

    } else {
	die "Malformed read header: $fq->[0]\n";
    }
    return $status;
}

sub linkerClipperCore {
    my $out1    = shift;
    my $out2    = shift;
    my $in1     = shift;
    my $in2     = shift;
    my $linker  = shift;
    my $exclude = shift;
    my $fq1;
    my $fq2;

    while (($fq1 = read_fq($in1)) && ($fq2 = read_fq($in2))) {
	my $pass1 = linkerClipper($fq1,1,$linker,$exclude);
	my $pass2 = linkerClipper($fq2,2,$linker,$exclude);

	if ($pass1 && $pass2) {
	    print($out1 @$fq1);
	    print($out2 @$fq2);
	}
    }

    return 1;
}

sub HELP_MESSAGE {
    die qq/
Program: $PROGRAM ($PURPOSE)
Version: $PACKAGE $VERSION
Contact: $CONTACT

Usage: $PROGRAM \\
           --fq-1 <in1.fastq> [--fq-2 <in2.fastq>] \\
           --bed-1 <linker1.bed> --bed-2 <linker2.bed> <out.prefix>

/;
}

main: {
    my $fq1;
    my $fq2;
    my $bed1;
    my $bed2;
    Getopt::Long::Configure("bundling");
    GetOptions(
	'bed-1=s',\$bed1,
	'bed-2=s',\$bed2,
	'fq-1=s',\$fq1,
	'fq-2:s',\$fq2,
	) && 
	@ARGV == 1 || &HELP_MESSAGE;
    
    my %linker;
    my %chimera;
    my $prefx = shift(@ARGV);

    readLinkerBED($bed1,1,\%linker,\%chimera);
    readLinkerBED($bed2,2,\%linker,\%chimera);
    
    my $in1  = IO::Uncompress::Gunzip->new($fq1) || die "Cannot open $fq1 for reading: $!\n";
    my $in2  = !defined($fq2) ? $in1 : IO::Uncompress::Gunzip->new($fq2) || die "Cannot open $fq2 for reading: $!\n";
    my $out1 = IO::Compress::Gzip->new(sprintf("%s_1.fastq.gz",$prefx))  || die "Cannot open $prefx end-1 outfile: $!\n";
    my $out2 = IO::Compress::Gzip->new(sprintf("%s_2.fastq.gz",$prefx))  || die "Cannot open $prefx end-2 outfile: $!\n";

    linkerClipperCore($out1,$out2,$in1,$in2,\%linker,\%chimera);

    close($out1);
    close($out2);
    close($in1);
    close($in2);

    exit(0);
}
