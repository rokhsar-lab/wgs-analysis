
PROGRAM=$(basename $0);
PACKAGE='__PACKAGE_NAME__';
VERSION='__PACKAGE_VERSION__';
CONTACT='__PACKAGE_CONTACT__';
PURPOSE='Convenience wrapper for WGS HTS processing';


function log_info () { 
    printf "%s %s INFO  [$PROGRAM] $1" `date '+%F %H:%M:%S,%3N'` >>/dev/fd/2;
}

function log_warn () { 
    printf "%s %s WARN  [$PROGRAM] $1" `date '+%F %H:%M:%S,%3N'` >>/dev/fd/2;
}

function log_error () {
    printf "%s %s ERROR [$PROGRAM] $2.\n\n" `date '+%F %H:%M:%S,%3N'` >>/dev/fd/2;
    exit $1;
}

function error () {
    printf "Error: $2.\n\n" >>/dev/fd/2;
    exit $1;
}

function log_depend () { 
    if [[ ! -e "${WORKDIR}/$1.complete" ]]; then
        log_error 1 "Missing dependency ($1), stage did not complete";
    fi
}

function abspath () {
    if [[ -z "$1" ]]; then
        log_error 255 "abspath(): one argument expected";
    fi

    var=\$"$1";
    val=`eval "expr \"$var\" "`;
    if [[ -z "$val" ]]; then
        log_error 255 "abspath($var): path is empty"

    elif [[ `echo "$val" | grep '^\/'` ]]; then
	# already absolute
        eval "$1=\"$val\"";
    
    else
        if [[ "$val" == "." || "$val" == "./" ]]; then
            eval "$1=\"$PWD\"";

        elif [[ `echo "$val" | grep '^\.\/'` ]]; then
            val=`echo "$val" | sed 's/^\.\/*//'`;
            eval "$1=\"$PWD/$val\"";

        else
            eval "$1=\"$PWD/$val\"";
        
        fi
    fi
}

function validateFOFN () {
    if [[ -z "$1" ]]; then
	log_error 1 "No FOFN given";
	
    elif [[ ${READS_PAIRED} && ${READS_COLLATED} ]]; then
	${GREP} -v '^#' $1 | while read f; do 
	    if   [[ -z "$f" || ! -f "$f" ]]; then
		log_error 1 "$1 collated PE file not defined or does not exist: $f";
	    fi
	done
    
    elif [[ ${READS_PAIRED} ]]; then
	${GREP} -v '^#' $1 | while read f r; do 
	    if   [[ -z "$f" || ! -f "$f" ]]; then
		log_error 1 "$1 uncollated PE 1 file not defined or does not exist: $f";
	    elif [[ -z "$r" || ! -f "$r" ]]; then
		log_error 1 "$1 uncollated PE 2 file not defined or does not exist: $r";
	    fi
	done
    else
	${GREP} -v '^#' $1 | while read f; do 
	    if   [[ -z "$f" || ! -f "$f" ]]; then
		log_error 1 "$1 SR file not defined or does not exist: $f";
	    fi
	done	
    fi
}

function usage () {
    printf "\n" >>/dev/fd/2;
    printf "Program: $PROGRAM ($PURPOSE)\n" >>/dev/fd/2;
    printf "Version: $PACKAGE $VERSION\n" >>/dev/fd/2;
    printf "Contact: $CONTACT\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "Usage:   $PROGRAM --config <config> [--stop-after <stage>]\n" >>/dev/fd/2;
    printf "                [--redo-from <stage>] [--cleanup]\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
}

function usage_short () {
    usage;
    printf "Notes:\n\n" >>/dev/fd/2;
    printf "  - Use \`$PROGRAM --help\` for more information about the above\n" >>/dev/fd/2;
    printf "    options and software dependencies.\n\n\n" >>/dev/fd/2;
}

function usage_long () {
    # TODO: fill out long-option information
    # stats index filter markdup merge sort fixmate align adapter_trim input refidx
    usage;
    printf "Command-line arguments:\n\n" >>/dev/fd/2;
    printf "    --config <config>\n" >>/dev/fd/2;
    printf "       File; required. The configuration file parameterizing a wgs run.\n" >>/dev/fd/2;
    printf "       The config specifies, at a minimum, the REFERENCE_FASTA, READS_FOFN,\n" >>/dev/fd/2;
    printf "       and WORKDIR variables. See \`examples/wgs.config\' for details.\n" >>/dev/fd/2;
    printf "       Default: Null\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "    --redo-from <stage>\n" >>/dev/fd/2;
    printf "       String; optional. Force redo all stages starting from (and including)\n" >>/dev/fd/2;
    printf "       the specified stage. Valid stages are: refidx, input, adapter_trim,\n" >>/dev/fd/2;
    printf "       align, fixmate, sort, merge, markdup, filter, index, stats\n" >>/dev/fd/2;
    printf "       Default: Null\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "    --stop-after <stage>\n" >>/dev/fd/2;
    printf "       String; optional. Stop the pipeline after the specified stage is\n" >>/dev/fd/2;
    printf "       completed. See \`--redo-from\' option for a list of valid stages.\n" >>/dev/fd/2;
    printf "       Default: Null\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "    --clean\n" >>/dev/fd/2;
    printf "       Flag; optional. Remove intermediate files in completed project stage\n" >>/dev/fd/2; 
    printf "       subdirectories.\n" >>/dev/fd/2;
    printf "       Default: false\n"  >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "    --clean-all\n" >>/dev/fd/2;
    printf "       Flag; optional. Perform \'--clean\' actions but also remove all job\n" >>/dev/fd/2;
    printf "       submission directories and log files.\n" >>/dev/fd/2;
    printf "       Default: false\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "    --archive\n" >>/dev/fd/2;
    printf "       Flag; optional. After a completed run, perform \'--clean-all\' action\n" >>/dev/fd/2;
    printf "       then archive the run directory with \'tar -czf\'\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "Notes:\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "   1. The following bioninformatic sofware must be accessible via \$PATH:\n" >>/dev/fd/2;
    printf "      bwa, seqtk, fastq-mcf & determine-phred (ea-utils), samtools\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
}


# ==================== SYSTEM =====================
_SH=`which bash`;  # force bash over the cluster 
_OS=`uname`;

if [[ ${_OS} == 'Darwin' ]]; then
    _HOST=`hostname`;
else
    _HOST=`hostname --fqdn`;
fi


# ==================== PACKAGE ====================
PKG_PATH=$(dirname $(dirname "$0"));
PKG_BASE=$(basename "$0");

# ==================== RUNTIME ====================
WORKDIR=;
REFERENCE_FASTA=;
READS_FOFN=;
READS_COLLATED=;
READS_PAIRED=1;

ARCHIVE=;
CURRSTAGE=;
PREVSTAGE=;
REDOSTAGE='';
STOPSTAGE='';
CLEANUP=;
CLEANUP_STAGES="index markdup merge sort fixmate align adapter_trim input refidx";
HELP_MESSAGE=;
COMMANDLINE="${PKG_BASE} $*";
while [[ -n $@ ]]; do
    case "$1" in
	'--config') shift; CONFIG="$1";;
	'--stop-after') shift; STOPSTAGE="$1";;
	'--redo-from') shift; REDOSTAGE="$1";;
	'--clean') CLEANUP=1;;
	'--cleanup') CLEANUP=1;;
	'--clean-all') CLEANUP=2;;
	'--archive') CLEANUP=2; ARCHIVE=1;;
	'--help') HELP_MESSAGE='long';;
	'-h') HELP_MESSAGE='short';;
	-*) usage_short; error 2 "Invalid option: $1";;
	*) break;;
    esac;
    shift;
done

if [[ -n "${HELP_MESSAGE}" ]]; then
    if [[ ${HELP_MESSAGE} == 'long' ]]; then
	usage_long;
    else
	usage_short;
    fi
    exit 1;
fi

if [[ -z ${CONFIG} || ! -f ${CONFIG} ]]; then
    usage_short;
    error 1 "Argument to '--config' not defined or file does not exist";
fi

for stage in ${REDOSTAGE} ${STOPSTAGE}; do
    case "${stage}" in
	'refidx');;
	'input');;
	'adapter_trim');;
	'align');;
	'fixmate');;
	'sort');;
	'merge');;
	'markdup');;
	'filter');;
	'index');;
	'stats');;
	*) usage_short; error 1 "Invalid stage name: ${stage}";;
    esac
done

DATE=`date +%FT%H:%M:%S+%4N`;
printf "%s %s INFO  [%s] Command-line: ${COMMANDLINE}\n" \
    `date '+%F %H:%M:%S,%3N'` ${PKG_BASE} >>/dev/fd/2;
printf "%s %s INFO  [%s] Executing %s (v%s) as %s@%s on [%s] %s %s (%s)\n" \
    `date '+%F %H:%M:%S,%3N'` ${PKG_BASE} ${PKG_BASE} ${VERSION} ${USER} ${_HOST} \
    `ps -o comm -p ${PPID} | grep -v 'COMM'` `uname -srm` >>/dev/fd/2;


PATH="${PKG_PATH}/bin:${PATH}";
PERL5LIB="${PKG_PATH}/lib:${PERL5LIB}";
LIBRARY_PATH="${PKG_PATH}/lib:${LIBRARY_PATH}";
LD_LIBRARY_PATH="${PKG_PATH}/lib:${LD_LIBRARY_PATH}";

# Validate the accessibility of tools and the config:
TR=`which tr`;
AWK=`which awk`;
BWA=`which bwa`;
CAT=`which cat`;
CUT=`which cut`;
GREP=`which grep`;
GZIP=`which gzip`;
MKDIR=`which mkdir`;
SEQTK=`which seqtk`;
TOUCH=`which touch`;
QBATCH=`which qbatch`;
GNUPLOT=`which gnuplot`;
SAMTOOLS=`which samtools`;
FASTQMCF=`which fastq-mcf`;
DETPHRED=`which determine-phred`;
PLOTBAMSTATS=`which plot-bamstats`;
if [[ -z "${TR}" || ! -x "${TR}" ]]; then
    log_error 127 "tr not in PATH env variable or is not executable";

elif [[ -z "${AWK}" || ! -x "${AWK}" ]]; then
    log_error 127 "awk not in PATH env variable or is not executable";

elif [[ -z "${BWA}" || ! -x "${BWA}" ]]; then
    log_error 127 "bwa not in PATH env variable or is not executable";

elif [[ -z "${CAT}" || ! -x "${CAT}" ]]; then
    log_error 127 "cat not in PATH env variable or is not executable";

elif [[ -z "${CUT}" || ! -x "${CUT}" ]]; then
    log_error 127 "cut not in PATH env variable or is not executable";

elif [[ -z "${GREP}" || ! -x "${GREP}" ]]; then
    log_error 127 "grep not in PATH env variable or is not executable";

elif [[ -z "${GZIP}" || ! -x "${GZIP}" ]]; then
    log_error 127 "gzip not in PATH env variable or is not executable";

elif [[ -z "${SEQTK}" || ! -x "${SEQTK}" ]]; then
    log_error 127 "seqtk not in PATH env variable or is not executable";

elif [[ -z "${TOUCH}" || ! -x "${TOUCH}" ]]; then
    log_error 127 "touch not in PATH env variable or is not executable";

elif [[ -z "${QBATCH}" || ! -x "${QBATCH}" ]]; then
    log_error 127 "qbatch not in PATH env variable or is not executable";

elif [[ -z "${GNUPLOT}" || ! -x "${GNUPLOT}" ]]; then
    log_error 127 "gnuplot not in PATH env variable or is not executable";

elif [[ -z "${SAMTOOLS}" || ! -x "${SAMTOOLS}" ]]; then
    log_error 127 "samtools not in PATH env variable or is not executable";

elif [[ -z "${FASTQMCF}" || ! -x "${FASTQMCF}" ]]; then
    log_error 127 "fastq-mcf not in PATH env variable or is not executable";

elif [[ -z "${DETPHRED}" || ! -x "${DETPHRED}" ]]; then
    log_error 127 "determine-phred not in PATH env variable or is not executable";

elif [[ -z "${PLOTBAMSTATS}" || ! -x "${PLOTBAMSTATS}" ]]; then
    log_error 127 "plot-bamstats not in PATH env variable or is not executable";

fi



BWA_VERSION=`${BWA} 2>&1 | ${GREP} ^Version | ${CUT} -f2 -d' ' | ${CUT} -f1,2 -d'.'`;
if [[ ${BWA_VERSION} < 0.7 ]]; then
    log_error 1 "bwa v0.7.0+ required, detected v${BWA_VERSION}";
fi

SAMTOOLS_VERSION=`${SAMTOOLS} 2>&1 | ${GREP} ^Version | ${CUT} -f2 -d' ' | ${CUT} -f1,2 -d'.'`;
if [[ -z ${SAMTOOLS_VERSION} || ${SAMTOOLS_VERSION} < 1.0 ]]; then
    log_error 1 "samtools v1.0+ required, detected v${SAMTOOLS_VERSION}";
fi


# TODO: Invest some time in shielding this from malicious or accidental failure:
. ${CONFIG};

if [[ ${READS_COLLATED} ]]; then
    READS_PAIRED=1;
fi

if [[ ${READS_PAIRED} && ${PROPER_PAIRS_ONLY} ]]; then
    FILTER_EXE_OPTIONS="${FILTER_EXE_OPTIONS} -f3 -F3852";
fi


for stage in ${REDOSTAGE} ${STOPSTAGE}; do
    case "${stage}" in
	'refidx');;
	'input');;
	'adapter_trim');;
	'align');;
	'fixamate');;
	'sort');;
	'merge');;
	'markdup');;
	'filter');;
	'index');;
	'stats');;
	*) usage_short; error 1 "Invalid stage name: ${stage}";;
    esac
done

if   [[ -z "${REFERENCE_FASTA}" || ! -f "${REFERENCE_FASTA}" ]]; then
    log_error 1 "Config parameter REFERENCE_FASTA not defined or the file does not exist";

elif [[ -z "${ADAPTER_FASTA}" || ! -f "${ADAPTER_FASTA}" ]]; then
    log_error 1 "Config parameter ADAPTER_FASTA not defined or the file does not exist";

elif [[ -z "${READS_FOFN}" || ! -f "${READS_FOFN}" ]]; then
    log_error 1 "Config parameter READS_FOFN not defined or the file does not exist";

elif [[ -z "${WORKDIR}" ]]; then
    log_error 1 "Config parameter WORKDIR not defined";

elif [[ -z "${SAMPLE_ID}" ]]; then
    log_error 1 "Config parameter SAMPLE_ID not defined";

elif [[ -z "${LIBRARY_ID}" ]]; then
    log_error 1 "Config parameter LIBRARY_ID not defined";

elif [[ -z "${LIBRARY_INDEX}" ]]; then
    log_error 1 "Config parameter LIBRARY_INDEX not defined";

elif [[ -z "${SEQUENCING_CENTER}" ]]; then
    log_error 1 "Config parameter SEQUENCING_CENTER not defined";

elif [[ -z "${SEQUENCING_PLATFORM}" ]]; then
    log_error 1 "Config parameter SEQUENCING_PLATFORM not defined";

elif [[ -z "${FLOWCELL_ID}" ]]; then
    log_error 1 "Config parameter FLOWCELL_ID not defined";

elif [[ -z "${FLOWCELL_LANE}" ]]; then
    log_error 1 "Config parameter FLOWCELL_LANE not defined";

fi
mkdir -p ${WORKDIR};

abspath REFERENCE_FASTA;
abspath ADAPTER_FASTA;
abspath READS_FOFN;
abspath WORKDIR;


if [[ -n "${REDOSTAGE}" ]]; then
    stages="";
    case "${REDOSTAGE}" in
	'refidx') stages="stats index filter markdup merge sort fixmate align adapter_trim input refidx";;
	'input') stages="stats index filter markdup merge sort fixmate align adapter_trim input";;
	'adapter_trim') stages="stats index filter markdup merge sort fixmate align adapter_trim";;
	'align') stages="stats index filter markdup merge sort fixmate align";;
	'fixmate') stages="stats index filter markdup merge sort fixmate";;
	'sort') stages="stats index filter markdup merge sort";;
	'merge') stages="stats index filter markdup merge";;
	'markdup') stages="stats index filter markdup";;
	'filter') stages="stats index filter";;
	'index') stages="stats index";;
	'stats') stages="stats";;
    esac
    if [[ -n "${stages}" && -e "${WORKDIR}.complete" ]]; then
	rm "${WORKDIR}.complete";
    fi
    for stage in ${stages}; do
	if [[ -e "${WORKDIR}/${stage}.complete" ]]; then
	    rm "${WORKDIR}/${stage}.complete";
	fi
    done
fi

if [[ -e "${WORKDIR}.complete" ]]; then
    log_info "Finished\n";
    exit 0;
fi


REFERENCE_BASE=`basename ${REFERENCE_FASTA} | sed 's/\.f\(ast\|n\|\)a$//'`;
if [[ ! -e "${WORKDIR}/${REFERENCE_BASE}.fasta" ]]; then
    ln -sf "${REFERENCE_FASTA}" "${WORKDIR}/${REFERENCE_BASE}.fasta";
fi
REFERENCE_FASTA="${WORKDIR}/${REFERENCE_BASE}.fasta";

PREFIX="${LIBRARY_ID}_${LIBRARY_INDEX}_${FLOWCELL_ID}_${FLOWCELL_LANE}";


echo "export PATH=\"${PATH}:\${PATH}\";" >${WORKDIR}/.wgs_profile;
echo "export INCLUDE_PATH=\"${INCLUDE_PATH}:\${INCLUDE_PATH}\";" >>${WORKDIR}/.wgs_profile;
echo "export LIBRARY_PATH=\"${LIBRARY_PATH}:\${LIBRARY_PATH}\";" >>${WORKDIR}/.wgs_profile;
echo "export LD_LIBRARY_PATH=\"${LD_LIBRARY_PATH}:\${LD_LIBRARY_PATH}\";" >>${WORKDIR}/.wgs_profile;
echo "export PERL5LIB=\"${PERL5LIB}:\${PERL5LIB}\";" >>${WORKDIR}/.wgs_profile;
echo "export WORKDIR=\"${WORKDIR}\";" >>${WORKDIR}/.wgs_profile;
echo "export REFERENCE_FASTA=\"${REFERENCE_FASTA}\";" >>${WORKDIR}/.wgs_profile;
echo "export READS_FOFN=\"${READS_FOFN}\";" >>${WORKDIR}/.wgs_profile;
echo "export REFIDX_EXE_OPTIONS=\"${REFIDX_EXE_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;
echo "export REFIDX_SUBMIT_OPTIONS=\"${REFIDX_SUBMIT_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;
echo "export INPUT_EXE_OPTIONS=\"${INPUT_EXE_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;
echo "export INPUT_SUBMIT_OPTIONS=\"${INPUT_SUBMIT_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;
echo "export ADAPTER_TRIM_EXE_OPTIONS=\"${ADAPTER_TRIM_EXE_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;
echo "export ADAPTER_TRIM_SUBMIT_OPTIONS=\"${ADAPTER_TRIM_SUBMIT_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;
echo "export ALIGN_EXE_OPTIONS=\"${ALIGN_EXE_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;
echo "export ALIGN_SUBMIT_OPTIONS=\"${ALIGN_SUBMIT_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;
echo "export FIXMATE_EXE_OPTIONS=\"${FIXMATE_EXE_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;
echo "export FIXMATE_SUBMIT_OPTIONS=\"${FIXMATE_SUBMIT_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;
echo "export SORT_EXE_OPTIONS=\"${SORT_EXE_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;
echo "export SORT_SUBMIT_OPTIONS=\"${SORT_SUBMIT_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;
echo "export MERGE_EXE_OPTIONS=\"${MERGE_EXE_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;
echo "export MERGE_SUBMIT_OPTIONS=\"${MERGE_SUBMIT_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;
echo "export MARKDUP_EXE_OPTIONS=\"${MARKDUP_EXE_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;
echo "export MARKDUP_SUBMIT_OPTIONS=\"${MARKDUP_SUBMIT_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;
echo "export FILTER_EXE_OPTIONS=\"${FILTER_EXE_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;
echo "export FILTER_SUBMIT_OPTIONS=\"${FILTER_SUBMIT_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;
echo "export INDEX_EXE_OPTIONS=\"${INDEX_EXE_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;
echo "export INDEX_SUBMIT_OPTIONS=\"${INDEX_SUBMIT_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;
echo "export STATS_EXE_OPTIONS=\"${STATS_EXE_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;
echo "export STATS_SUBMIT_OPTIONS=\"${STATS_SUBMIT_OPTIONS}\";" >>${WORKDIR}/.wgs_profile;



################################################################################
# create Burrows-Wheeler index (if necessary)
################################################################################
PREVSTAGE=;
CURRSTAGE='refidx';
log_info "${CURRSTAGE}: ";
if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then
    echo "executing" >>/dev/fd/2;
    printf ". \"${WORKDIR}/.wgs_profile\"; " >${WORKDIR}/${CURRSTAGE}.batch;
    printf "${SAMTOOLS} faidx ${REFERENCE_FASTA} && " >>${WORKDIR}/${CURRSTAGE}.batch;
    printf "${BWA} index \${REFIDX_EXE_OPTIONS} ${REFERENCE_FASTA} && " >>${WORKDIR}/${CURRSTAGE}.batch;
    printf "${SAMTOOLS} dict ${REFERENCE_FASTA} >${WORKDIR}/${REFERENCE_BASE}.dict\n" >>${WORKDIR}/${CURRSTAGE}.batch;

    ${QBATCH} submit ${REFIDX_SUBMIT_OPTIONS} -n ${CURRSTAGE} -Wr -e ${_SH} ${WORKDIR}/${CURRSTAGE}.batch && \
	${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;
else
    echo "current" >>/dev/fd/2;
fi
if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
    log_info "Stop requested after ${CURRSTAGE}\n";
    exit 0;
fi



################################################################################
# "Import" reads into the project directory with phred+33 offset qualities
################################################################################
PREVSTAGE=${CURRSTAGE};
CURRSTAGE='input';
log_info "${CURRSTAGE}: ";
if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then
    echo "executing" >>/dev/fd/2;
    log_depend ${PREVSTAGE};

    mkdir -p ${WORKDIR}/${CURRSTAGE};
    printf '' >${WORKDIR}/.${CURRSTAGE}.fofn;

    if [[ ${READS_COLLATED} ]]; then
	n=1;
	${GREP} -v '^#' ${READS_FOFN} | ${CUT} -f1 | while read file; do
	    N=`printf %03d $n`;
	    abspath file;

	    QUAL_OFFSET=`${DETPHRED} ${file}`;

	    printf ". \"${WORKDIR}/.wgs_profile\"; ";
	    printf "${SEQTK} seq -1 -V -Q ${QUAL_OFFSET} $file | ${GZIP} >${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}_1.fastq.gz\n";
	    printf ". \"${WORKDIR}/.wgs_profile\"; ";
	    printf "${SEQTK} seq -2 -V -Q ${QUAL_OFFSET} $file | ${GZIP} >${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}_2.fastq.gz\n";
	    printf "${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}_1.fastq.gz\t" >>${WORKDIR}/.${CURRSTAGE}.fofn;
	    printf "${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}_2.fastq.gz\n" >>${WORKDIR}/.${CURRSTAGE}.fofn;
	    n=$(( $n + 1 ));

	done >${WORKDIR}/${CURRSTAGE}.batch;
	
	${QBATCH} submit ${INPUT_SUBMIT_OPTIONS} -n ${CURRSTAGE} -Wr -e ${_SH} ${WORKDIR}/${CURRSTAGE}.batch && \
	    ${TOUCH} ${WORKDIR}/.${CURRSTAGE}.complete;

    else
	n=1;
	${GREP} -v '^#' ${READS_FOFN} | ${CUT} -f1,2 | while read file1 file2; do 
	    N=`printf %03d $n`;
	    abspath file1;
	    abspath file2;

	    QUAL_OFFSET1=`${DETPHRED} ${file1}`;
	    QUAL_OFFSET2=`${DETPHRED} ${file2}`;

	    if [[ ${QUAL_OFFSET1} -eq 33 && ${QUAL_OFFSET2} -eq 33 ]]; then
		base1=`basename $file1`;
		base2=`basename $file2`;
		echo "${WORKDIR}/${CURRSTAGE}/$base1 ${WORKDIR}/${CURRSTAGE}/$base2" >>${WORKDIR}/.${CURRSTAGE}.fofn;
		printf ". \"${WORKDIR}/.wgs_profile\"; ";		
		printf "ln -sf $file1 ${WORKDIR}/${CURRSTAGE}/$base1; ln -sf $file2 ${WORKDIR}/${CURRSTAGE}/$base2\n";
	    else
		printf ". \"${WORKDIR}/.wgs_profile\"; ";
		printf "${SEQTK} seq -V -Q ${QUAL_OFFSET} $file1 | ${GZIP} >${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}_1.fastq.gz\n";
		printf ". \"${WORKDIR}/.wgs_profile\"; ";
		printf "${SEQTK} seq -V -Q ${QUAL_OFFSET} $file2 | ${GZIP} >${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}_2.fastq.gz\n";
		printf "${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}_1.fastq.gz\t" >>${WORKDIR}/.${CURRSTAGE}.fofn;
		printf "${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}_2.fastq.gz\n" >>${WORKDIR}/.${CURRSTAGE}.fofn;
	    fi
	    n=$(( $n + 1 ));

	done >${WORKDIR}/${CURRSTAGE}.batch;

	${QBATCH} submit ${INPUT_SUBMIT_OPTIONS} -n ${CURRSTAGE} -Wr -e ${_SH} ${WORKDIR}/${CURRSTAGE}.batch && \
	    ${TOUCH} ${WORKDIR}/.${CURRSTAGE}.complete;

    fi

    if [[ -e "${WORKDIR}/.${CURRSTAGE}.complete" ]]; then
	READS_COLLATED=;
	validateFOFN "${WORKDIR}/.${CURRSTAGE}.fofn" && \
	    mv "${WORKDIR}/.${CURRSTAGE}.fofn" "${WORKDIR}/${CURRSTAGE}.fofn" && \
	    mv "${WORKDIR}/.${CURRSTAGE}.complete" "${WORKDIR}/${CURRSTAGE}.complete";
    fi
else
    echo "current" >>/dev/fd/2;
fi

if [[ ! -s "${WORKDIR}/${CURRSTAGE}.fofn" ]]; then
    log_error 1 "Encountered an error localizing read files";
fi

if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
    log_info "Stop requested after ${CURRSTAGE}\n";
    exit 0;
fi


################################################################################
# Update the FOFN file with abs paths
################################################################################
READS_FOFN="${WORKDIR}/${CURRSTAGE}.fofn";
READS_COLLATED=;
validateFOFN "${READS_FOFN}";


echo "export READS_FOFN=\"${READS_FOFN}\";" >>${WORKDIR}/.wgs_profile;



################################################################################
# Remove adapters
################################################################################
PREVSTAGE=${CURRSTAGE};
CURRSTAGE='adapter_trim';
log_info "${CURRSTAGE}: ";
if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then
    echo "executing" >>/dev/fd/2;
    log_depend ${PREVSTAGE};

    [[ -d "${WORKDIR}/${CURRSTAGE}" ]] || mkdir -p ${WORKDIR}/${CURRSTAGE};

    n=1;
    ${GREP} -v '^#' ${READS_FOFN} | ${CUT} -f1,2 | while read f r; do
        N=`printf %03d $n`;
        fo="${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}_1.fastq.gz";
        ro="${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}_2.fastq.gz";
	printf ". \"${WORKDIR}/.wgs_profile\"; ";
        printf "${FASTQMCF} \${ADAPTER_TRIM_EXE_OPTIONS} -o $fo -o $ro ${ADAPTER_FASTA} $f $r\n";

        n=$(( $n + 1 ));
    done >${WORKDIR}/${CURRSTAGE}.batch;
    
    ${QBATCH} submit ${ADAPTER_TRIM_SUBMIT_OPTIONS} -n ${CURRSTAGE} -Wr -e ${_SH} ${WORKDIR}/${CURRSTAGE}.batch && \
        ${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;

else
    echo "current" >>/dev/fd/2;
fi

if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
    log_info "Stop requested after ${CURRSTAGE}\n";
    exit 0;
fi



################################################################################
# Align the reads using BWA
################################################################################
PREVSTAGE=${CURRSTAGE};
CURRSTAGE='align';
log_info "${CURRSTAGE}: ";
if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then
    echo "executing" >>/dev/fd/2;
    log_depend ${PREVSTAGE};
    
    [[ -d "${WORKDIR}/${CURRSTAGE}" ]] || mkdir -p ${WORKDIR}/${CURRSTAGE};
    
    RG="@RG"
    RG="${RG}\\\\tID:${PREFIX}";
    RG="${RG}\\\\tSM:${SAMPLE_ID}";
    RG="${RG}\\\\tLB:${LIBRARY_ID}";
    RG="${RG}\\\\tCN:${SEQUENCING_CENTER}";
    RG="${RG}\\\\tPL:${SEQUENCING_PLATFORM}";
    RG="${RG}\\\\tPU:${FLOWCELL_ID}-${LIBRARY_INDEX}.${FLOWCELL_LANE}";
    RG="${RG}\\\\tSB:${FLOWCELL_ID}";
    RG="${RG}\\\\tSL:${FLOWCELL_LANE}";
    RG="${RG}\\\\tDT:${DATE}";

    if [[ ${USE_LEGACY_BWA} ]]; then
	# Run bwa-aln and bwa-sampe:
	if [[ ! -e "${WORKDIR}/${CURRSTAGE}/bwa-aln.complete" ]]; then
	    log_depend ${PREVSTAGE};
	    
	    n=1;
	    ${GREP} -v '^#' ${READS_FOFN} | ${CUT} -f1,2 | while read file1 file2; do
		N=`printf %03d $n`;
		f="${WORKDIR}/${PREVSTAGE}/${PREFIX}_${N}_1.fastq.gz";
		r="${WORKDIR}/${PREVSTAGE}/${PREFIX}_${N}_2.fastq.gz";
		fo="${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}_1.fastq.sai";
		ro="${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}_2.fastq.sai";
		
		printf ". \"${WORKDIR}/.wgs_profile\"; ";
		printf "${BWA} aln \${ALIGN_EXE_OPTIONS} -f $fo ${REFERENCE_FASTA} $f\n";
		printf ". \"${WORKDIR}/.wgs_profile\"; ";
		printf "${BWA} aln \${ALIGN_EXE_OPTIONS} -f $ro ${REFERENCE_FASTA} $r\n";
		n=$(( $n + 1 ));

	    done >${WORKDIR}/${CURRSTAGE}/bwa-aln.batch;
	    
	    ${QBATCH} submit ${ALIGN_SUBMIT_OPTIONS} -n ${CURRSTAGE} -Wr -e ${_SH} ${WORKDIR}/${CURRSTAGE}/bwa-aln.batch && \
		${TOUCH} ${WORKDIR}/${CURRSTAGE}/bwa-aln.complete;


	fi
	if [[ ! -e "${WORKDIR}/${CURRSTAGE}/bwa-sampe.complete" ]]; then
	    log_depend "${CURRSTAGE}/bwa-aln";
	    n=1;
	    ${GREP} -v '^#' ${READS_FOFN} | ${CUT} -f1,2 | while read file1 file2; do
		N=`printf %03d $n`;
		f="${WORKDIR}/${PREVSTAGE}/${PREFIX}_${N}_1.fastq.gz";
		r="${WORKDIR}/${PREVSTAGE}/${PREFIX}_${N}_2.fastq.gz";
		fo="${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}_1.fastq.sai";
		ro="${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}_2.fastq.sai";
		o="${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}.bam";		

		printf ". \"${WORKDIR}/.wgs_profile\"; ";
		printf "${BWA} sampe -r '$RG' ${REFERENCE_FASTA} $fo $ro $f $r | ";
		printf "${SAMTOOLS} view -bS - >$o\n";
		n=$(( $n + 1 ));

	    done >${WORKDIR}/${CURRSTAGE}/bwa-sampe.batch;

	    ${QBATCH} submit ${ALIGN_SUBMIT_OPTIONS} -n ${CURRSTAGE} -Wr -e ${_SH} ${WORKDIR}/${CURRSTAGE}/bwa-sampe.batch && \
		${TOUCH} ${WORKDIR}/${CURRSTAGE}/bwa-sampe.complete;
	    

	    if [[ -e "${WORKDIR}/${CURRSTAGE}/bwa-aln.complete" && -e "${WORKDIR}/${CURRSTAGE}/bwa-sampe.complete" ]]; then
		${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;
	    fi
	fi
    else
	n=1;
	${GREP} -v '^#' ${READS_FOFN} | ${CUT} -f1,2 | while read file1 file2; do
	    N=`printf %03d $n`;
            f="${WORKDIR}/${PREVSTAGE}/${PREFIX}_${N}_1.fastq.gz";
            r="${WORKDIR}/${PREVSTAGE}/${PREFIX}_${N}_2.fastq.gz";
	    o="${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}.bam";
	    
	    printf ". \"${WORKDIR}/.wgs_profile\"; ";
	    printf "${BWA} mem \${ALIGN_EXE_OPTIONS} -R '${RG}' ${REFERENCE_FASTA} $f $r | ";
	    printf "${SAMTOOLS} view -bS - >$o\n";
	    
	    n=$(( $n + 1 ));
	done >${WORKDIR}/${CURRSTAGE}.batch;
	
	${QBATCH} submit ${ALIGN_SUBMIT_OPTIONS} -n ${CURRSTAGE} -Wr -e ${_SH} ${WORKDIR}/${CURRSTAGE}.batch && \
	    ${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;

    fi
else
    echo "current" >>/dev/fd/2;
fi

if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
    log_info "Stop requested after ${CURRSTAGE}\n";
    exit 0;
fi



################################################################################
# Fix mate information
################################################################################
PREVSTAGE=${CURRSTAGE};
CURRSTAGE='fixmate';
log_info "${CURRSTAGE}: ";
if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then
    echo "executing" >>/dev/fd/2;
    log_depend ${PREVSTAGE};

    [[ -d "${WORKDIR}/${CURRSTAGE}" ]] || mkdir -p ${WORKDIR}/${CURRSTAGE};
    
    n=1;
    ${GREP} -v '^#' ${READS_FOFN} | while read file; do
	N=`printf %03d $n`;
	printf ". \"${WORKDIR}/.wgs_profile\"; ";
	printf "${SAMTOOLS} fixmate -m \${FIXMATE_EXE_OPTIONS}";
	printf " ${WORKDIR}/${PREVSTAGE}/${PREFIX}_${N}.bam ";
	printf " ${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}.bam\n";
	n=$(( $n + 1 ));

    done >${WORKDIR}/${CURRSTAGE}.batch;

    ${QBATCH} submit ${FIXMATE_SUBMIT_OPTIONS} -n ${CURRSTAGE} -Wr -e ${_SH} ${WORKDIR}/${CURRSTAGE}.batch && \
	${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;

else
    echo "current" >>/dev/fd/2;
fi

if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
    log_info "Stop requested after ${CURRSTAGE}\n";
    exit 0;
fi



################################################################################
# Sort mate-fixed BAM files by reference coordinate
################################################################################
PREVSTAGE=${CURRSTAGE};
CURRSTAGE='sort';
log_info "${CURRSTAGE}: ";
if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then
    echo "executing" >>/dev/fd/2;
    log_depend ${PREVSTAGE};
    
     [[ -d "${WORKDIR}/${CURRSTAGE}" ]] || mkdir -p ${WORKDIR}/${CURRSTAGE};

    n=1;
    ${GREP} -v '^#' ${READS_FOFN} | while read file; do 
	N=`printf %03d $n`;
	# must have whitespace before `-' chars or printf will interpret 
	# them as flags (yes, even if they are quoted)
	printf ". \"${WORKDIR}/.wgs_profile\"; ";
	printf "${SAMTOOLS} sort \${SORT_EXE_OPTIONS} -o";
	printf " ${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}.bam";
	printf " ${WORKDIR}/${PREVSTAGE}/${PREFIX}_${N}.bam\n";
	n=$(( $n + 1 ));

    done >${WORKDIR}/${CURRSTAGE}.batch;
    
    ${QBATCH} submit ${SORT_SUBMIT_OPTIONS} -n ${CURRSTAGE} -Wr -e ${_SH} ${WORKDIR}/${CURRSTAGE}.batch && \
	${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;

else
    echo "current" >>/dev/fd/2;
fi

if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
    log_info "Stop requested after ${CURRSTAGE}\n";
    exit 0;
fi



################################################################################
# Merge sorted BAM files
################################################################################
PREVSTAGE=${CURRSTAGE};
CURRSTAGE='merge';
log_info "${CURRSTAGE}: ";
if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then
    echo "executing" >>/dev/fd/2;
    log_depend ${PREVSTAGE};

    printf ". \"${WORKDIR}/.wgs_profile\"; " >${WORKDIR}/${CURRSTAGE}.batch;
    printf "${SAMTOOLS} merge \${MERGE_EXE_OPTIONS} " >>${WORKDIR}/${CURRSTAGE}.batch;
    printf "${WORKDIR}/${PREFIX}.bam ${WORKDIR}/${PREVSTAGE}/${PREFIX}_[0-9][0-9][0-9].bam\n" >>${WORKDIR}/${CURRSTAGE}.batch;

    ${QBATCH} submit ${MERGE_SUBMIT_OPTIONS} -n ${CURRSTAGE} -Wr -e ${_SH} ${WORKDIR}/${CURRSTAGE}.batch && \
	${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;
else
    echo "current" >>/dev/fd/2;
fi
if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
    log_info "Stop requested after ${CURRSTAGE}\n";
    exit 0;
fi



[[ -d "${WORKDIR}/stats" ]] || mkdir ${WORKDIR}/stats;



PREVSTAGE=${CURRSTAGE};
CURRSTAGE='markdup';
log_info "${CURRSTAGE}: ";
if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" || \
      ! -e "${WORKDIR}/stats/${PREFIX}.mdup.metrics" ]]; then
    echo "executing" >>/dev/fd/2;
    log_depend ${PREVSTAGE};

    [[ -d "${WORKDIR}/${CURRSTAGE}" ]] || mkdir -p ${WORKDIR}/${CURRSTAGE};

    printf ". \"${WORKDIR}/.wgs_profile\"; " >${WORKDIR}/${CURRSTAGE}.batch;
    printf "${SAMTOOLS} markdup \${MARKDUP_EXE_OPTIONS} -s" >>${WORKDIR}/${CURRSTAGE}.batch;
    printf " ${WORKDIR}/${PREFIX}.bam ${WORKDIR}/${PREFIX}.mdup.bam" >>${WORKDIR}/${CURRSTAGE}.batch;
    printf " &>${WORKDIR}/stats/${PREFIX}.mdup.metrics.tmp &&" >>${WORKDIR}/${CURRSTAGE}.batch;
    printf " ${SAMTOOLS} index ${WORKDIR}/${PREFIX}.mdup.bam" >>${WORKDIR}/${CURRSTAGE}.batch;
    
    ${QBATCH} submit ${MARKDUP_SUBMIT_OPTIONS} -n ${CURRSTAGE} -Wr -e ${_SH} ${WORKDIR}/${CURRSTAGE}.batch && \
	sed 's/DULPICATE/DUPLICATE/' ${WORKDIR}/stats/${PREFIX}.mdup.metrics.tmp >${WORKDIR}/stats/${PREFIX}.mdup.metrics && \
	rm  ${WORKDIR}/stats/${PREFIX}.mdup.metrics.tmp && \
	${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;

    PREFIX="${PREFIX}.mdup";
else
    echo "current" >>/dev/fd/2;
fi

if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
    log_info "Stop requested after ${CURRSTAGE}\n";
    exit 0;
fi


################################################################################
# Calculate BAM statistics
################################################################################
PREVSTAGE=${CURRSTAGE};
CURRSTAGE='stats';
log_info "${CURRSTAGE}: ";
if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then
    echo "executing" >>/dev/fd/2;
    
    printf ". \"${WORKDIR}/.wgs_profile\";" >${WORKDIR}/${CURRSTAGE}.batch;
    printf "  ${SAMTOOLS} flagstat ${WORKDIR}/${PREFIX}.bam" >>${WORKDIR}/${CURRSTAGE}.batch;
    printf "  >${WORKDIR}/stats/${PREFIX}.flagstat\n" >>${WORKDIR}/${CURRSTAGE}.batch;

    printf ". \"${WORKDIR}/.wgs_profile\";" >>${WORKDIR}/${CURRSTAGE}.batch;
    printf "  ${SAMTOOLS} stats --ref-seq ${REFERENCE_FASTA} ${WORKDIR}/${PREFIX}.bam" >>${WORKDIR}/${CURRSTAGE}.batch;
    printf "  >${WORKDIR}/stats/${PREFIX}.bam.stats &&" >>${WORKDIR}/${CURRSTAGE}.batch;
    printf "  ${PLOTBAMSTATS} -s ${REFERENCE_FASTA} >${WORKDIR}/stats/${REFERENCE_BASE}.gc &&" >>${WORKDIR}/${CURRSTAGE}.batch;
    printf "  ${PLOTBAMSTATS} -r ${WORKDIR}/stats/${REFERENCE_BASE}.gc" >>${WORKDIR}/${CURRSTAGE}.batch;
    printf "  -p ${WORKDIR}/stats/${PREFIX} ${WORKDIR}/stats/${PREFIX}.bam.stats\n" >>${WORKDIR}/${CURRSTAGE}.batch;

    ${QBATCH} submit ${STATS_SUBMIT_OPTIONS} -n ${CURRSTAGE} -Wr -e ${_SH} ${WORKDIR}/${CURRSTAGE}.batch && \
	${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;

else
    echo "current" >>/dev/fd/2;
fi
if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
    log_info "Stop requested after ${CURRSTAGE}\n";
    exit 0;
fi



################################################################################
# Filter the BAM file for properly-paired reads
################################################################################
log_info "filter";
if [[ -n ${FILTER_EXE_OPTIONS} ]]; then
    PREVSTAGE=${CURRSTAGE};
    CURRSTAGE='filter';
    if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then
	echo "executing" >>/dev/fd/2;
	log_depend ${PREVSTAGE};

	printf ". \"${WORKDIR}/.wgs_profile\"; " >${WORKDIR}/${CURRSTAGE}.batch;
	printf "${SAMTOOLS} view \${FILTER_EXE_OPTIONS} -b -o" >>${WORKDIR}/${CURRSTAGE}.batch;
	printf " ${WORKDIR}/${PREFIX}.filter.bam" >>${WORKDIR}/${CURRSTAGE}.batch;
	printf " ${WORKDIR}/${PREFIX}.bam\n" >>${WORKDIR}/${CURRSTAGE}.batch;
	
	${QBATCH} submit ${FILTER_SUBMIT_OPTIONS} -n ${CURRSTAGE} -Wr -e ${_SH} ${WORKDIR}/${CURRSTAGE}.batch && \
	    ${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;

    else
	echo "current" >>/dev/fd/2;
    fi
    
    PREFIX="${PREFIX}.filter";
else
    echo "skipping" >>/dev/fd/2;
fi

if [[ "filter" == "${STOPSTAGE}" ]]; then
    log_info "Stop requested after filter\n";
    exit 0;
fi



################################################################################
# Index the BAM file
################################################################################
PREVSTAGE=${CURRSTAGE};
CURRSTAGE='index';
log_info "${CURRSTAGE}: ";
if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" || \
    "${WORKDIR}/${PREFIX}.bam.bai" -ot "${WORKDIR}/${PREFIX}.bam" ]]; then
    echo "executing" >>/dev/fd/2;
    log_depend ${PREVSTAGE};
    
    mkdir -p ${WORKDIR}/${CURRSTAGE};

    if [[ -e "${WORKDIR}/${PREFIX}.bam.bai" ]]; then
	rm "${WORKDIR}/${PREFIX}.bam.bai";
    fi

    printf ". \"${WORKDIR}/.wgs_profile\"; " >${WORKDIR}/${CURRSTAGE}.batch;
    printf "${SAMTOOLS} index ${WORKDIR}/${PREFIX}.bam" >>${WORKDIR}/${CURRSTAGE}.batch;
    
    ${QBATCH} submit ${INDEX_SUBMIT_OPTIONS} -n ${CURRSTAGE} -Wr -e ${_SH} ${WORKDIR}/${CURRSTAGE}.batch && \
	${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;    
else
    echo "current" >>/dev/fd/2;
fi
if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
    log_info "Stop requested after ${CURRSTAGE}\n";
    exit 0;
fi



################################################################################
# Remove intermediate directories
################################################################################
if [[ -e "${WORKDIR}/${PREVSTAGE}.complete" ]]; then 
    ${TOUCH} ${WORKDIR}.complete;
    if [[ ${CLEANUP} ]]; then
	log_info "Removing intermediate files\n";
	for stage in ${CLEANUP_STAGES}; do
	    if [[ -e "${WORKDIR}/${stage}.complete" ]]; then
		rm ${WORKDIR}/${stage}.complete;
	    fi
	    if [[ -d "${WORKDIR}/${stage}" ]]; then
		rm -r ${WORKDIR}/${stage};
	    fi
	    if [[ ${CLEANUP} -eq 2 ]]; then
		rm -r ${WORKDIR}/${stage}.batch.log;
	    fi
	done
    fi
    if [[ ${ARCHIVE} ]]; then
	log_info "Archiving run\n";
	tar -czf ${WORKDIR}.tar.gz ${WORKDIR} && rm -r ${WORKDIR};
    fi
    log_info "Finished\n";
fi


