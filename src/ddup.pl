
use strict;
use warnings;
no  warnings 'uninitialized';

use Getopt::Std;
use File::Basename;

use Valkyr::FileUtil::IO;
use Valkyr::FileUtil::Counter
    qw(progressReport progressCounter configCounter);

use Valkyr::Error qw(throw report);
use Valkyr::Data::Type::Assert 
    qw(is_subclass is_coderef is_arrayref is_hashref is_uint is_file);

use vars qw/$PROGRAM $PACKAGE $PURPOSE $VERSION $CONTACT $CACHE $DEFAULT_k $DEFAULT_o/;

BEGIN { 
    $PROGRAM = basename($0);
    $PACKAGE = '__PACKAGE_NAME__';
    $VERSION = '__PACKAGE_VERSION__';
    $CONTACT = '__PACKAGE_CONTACT__';
    $PURPOSE = 'k-mer based reference-free duplicate removal';
    $DEFAULT_k = 34; 
    $DEFAULT_o = 0;
}

sub rev {
    my $rev = reverse(shift);
    $rev =~ tr/atcgATCG/tagcTAGC/;
    return $rev;
}

sub read_fq {
    my $fh = shift;
    my @entry;
    $entry[0] = $fh->getline() // return; # allow catching invalid fastq formats
    $entry[0] && $entry[0] =~ /^\@/ || 
        throw("FASTQ header expected, line ",$fh->input_line_number());
    $entry[1] = $fh->getline();
    $entry[2] = $fh->getline();
    $entry[3] = $fh->getline() // 
        throw("Unexpected EOF, line ",$fh->input_line_number());
    $entry[2] && $entry[2] =~ /^\+/ || 
        throw("Must use Illumina 4-line FASTQ format");

    return(\@entry);
}

sub read_fa {
    my $fh = shift;
    my @entry;
    
    $entry[1] = '';
    $entry[0] = $CACHE || $fh->getline() || return;
    if ($entry[0] !~ /^>/) {
	throw("Invalid FASTA header: ",$entry[0]);
    }
    while (my $entry = $fh->getline()) {
	if ($entry =~ /^>/) { 
	    $CACHE = $entry; 
	    $entry[1] =~ s/\s+//g;
	    return(\@entry);
	}
	$entry[1] .= $entry;
    }
    $entry[1] =~ s/\s+//g;


    if (length($entry[1]) < 1) {
	throw("Empty FASTA entry detected: $entry[0]");
    }

    return(\@entry);
}
	

sub canonize_kmer_fr {
    my $r1 = is_arrayref(shift);
    my $r2 = is_arrayref(shift);
    my $o  = is_uint(shift);
    my $k  = is_uint(shift);

    # The common/standard paired-end or mate-pair case
    my $s = substr($r1->[1],$o,$k).rev(substr($r2->[1],$o,$k));
    my $r = rev($s);

    return $s lt $r ? $s : $r;
}


sub canonize_kmer_ss {
    my $r1 = is_arrayref(shift);
    my $r2 = is_arrayref(shift);
    my $o  = is_uint(shift);
    my $k  = is_uint(shift);

    # For PE-sequenced Nextera mate-pair libraries, the
    # fragment's 5'-end (end-1) is dowstream of the linker,
    # but is recorded in the fastq file as end-2. Because
    # Illumina PE libraries are rotationally symmetric, we 
    # must produce a k-mer that is rotationally symmetric.
    # as well.
    my $s = substr($r2->[1],$o,$k).rev(substr($r1->[1],$o,$k));
    my $r = rev($s);

    return $s lt $r ? $s : $r;
}


sub canonize_kmer_sr {
    my $r1 = is_arrayref(shift);
    my $r2 = is_arrayref(shift);
    my $o  = is_uint(shift);
    my $k  = is_uint(shift);

    # Choose k-mers from the sequence adajcent to where the
    # internal linker was removed. The adjacent sequence is
    # less likely to be affected by poor quality (as it must
    # be high enough to identify the linker sequence and 
    # split into mates) and contain end adaptor sequences. 

    # subtract 1 from end for '\n' char, as it is not chomp()'d.
    my $s = substr($r2->[1],length($r2->[1])-$k-$o-1,$k).rev(substr($r1->[1],length($r1->[1])-$k-$o-1,$k));
    my $r = rev($s);

    return $s lt $r ? $s : $r;
}


# sub kmerBuild {
#     my $in1  = is_subclass(shift,'IO::Handle');
#     my $in2  = is_subclass(shift,'IO::Handle');
#     my $out  = is_subclass(shift,'IO::Handle');
#     my $can  = is_coderef(shift);
#     my $get  = is_coderef(shift);
#     my $off  = is_uint(shift);
#     my $klen = is_uint(shift);
#     my %kmer;
#     my $r1;
#     my $r2;

#     $klen /= 2;

#     report('Counting k-mers');
#     configCounter({'prefix' => 'Pairs processed: ', 'granularity' => 1e6});
#     while (($r1 = $get->($in1)) && ($r2 = $get->($in2))) {
# 	progressCounter();

# 	if ((length($r1->[1])+$off) <= $klen) { next }
# 	if ((length($r2->[1])+$off) <= $klen) { next }
	
# 	my $canonical = $can->($r1,$r2,$off,$klen);

# 	$kmer{$canonical}++;
#     }
#     progressReport();

#     report('Writing k-mers');
#     while (my ($mer,$count) = each(%kmer)) {
# 	print($out join("\t",$mer,$count),"\n") if $count > 1;
#     }

#     return 1;
# }    


sub read_list {
    my $file  = is_file(shift,'f');
    my $field = is_uint(shift // 0);

    report('Loading duplicate k-mers');
    
    my $data = {};
    
    my $list = open($file,READ);
    while (defined( local $_ = $list->getline() )) {
        m/^#|^\s*$/ && next;
        chomp();
        my @F = split(/\t/);
        $data->{$F[0]} = $F[$field];
    }
    $list->close();

    return $data;
}

sub ddupCore {
    my $in1  = is_subclass(shift,'IO::Handle');
    my $in2  = is_subclass(shift,'IO::Handle');
    my $out1 = is_subclass(shift,'IO::Handle');
    my $out2 = is_subclass(shift,'IO::Handle');
    my $out3 = is_subclass(shift,'IO::Handle');
    my $can  = is_coderef(shift);
    my $get  = is_coderef(shift);
    my $off  = is_uint(shift);
    my $klen = is_uint(shift);
    my $total= 0;
    my $dups = 0;
    my $kmer = {};
    my $r1;
    my $r2;

    $klen /= 2;

    configCounter({'prefix' => 'Pairs processed: ', 'granularity' => 1e5});
    while (($r1 = $get->($in1)) && ($r2 = $get->($in2))) {
	progressCounter();
	
	if ((length($r1->[1])+$off) <= $klen) { next }
	if ((length($r2->[1])+$off) <= $klen) { next }
	
	my $canonical = $can->($r1,$r2,$off,$klen);

	if (exists($kmer->{$canonical})) {
	    print($out3 @$r1);
	    print($out3 @$r2);
	    $dups++;
	} else {
	    print($out1 @$r1);
	    print($out2 @$r2);
	    $kmer->{$canonical} = 1;
	} 
	$total++;
    }
    progressReport();

    report(sprintf('Detected duplicate pairs: %u / %u (%.06f)',$dups,$total,$dups/($total||1)));

    return 1;
}


sub get_type {
    my $fh = shift;

    my $line = $fh->getline() // return 1;

    if ($line =~ /^([@>])/) {
	return $1 eq '>' ? 2 : 1;
    } else {
	throw('Cannot determine input file type.');
    }
}


sub HELP_MESSAGE {
    die qq/
Program: $PROGRAM ($PURPOSE)
Version: $PACKAGE $VERSION
Contact: $CONTACT

Usage:   $PROGRAM [options] -1 <in_1.fq> <prefix>

Options: -2 FILE  Read end-2 paired-end fastq from FILE [collated] 
         -k UINT  k-mer length (must be even int.) [$DEFAULT_k]
         -o UINT  0-based offset from read start [$DEFAULT_o]
         -S       Input in strand-specific RF mate-pair orientation
         -s       Input is single-read sequenced mate-pairs

Notes:

   1. Paired-end or mate-pair data only. Mates\/Pairs with length less
      than 0.5 * {-k} + {-o} are discarded.

   2. For standard Illumina paired-end and non- strand-specific RF 
      mate-pair (e.g. Nextera) libraries, the default k-mer database
      building procedure is recommended ('-s' and '-S' options not 
      recommended). If a strand-specific primer is used for paired-end
      sequencing, set the '-S' option to maintain strandedness.

   3. Input pyroseq data is assumed to have been single-read seqenced
      and the internal linker removed in such a way that the sequence
      downstream of the linker is the (rev. comp'd) end-1 fragment and
      the sequence upstream is end-2, producing the expected RF pairs.
      It is recommended to enable the '-s' option.

   4. Merged\/assembled (e.g. via PEAR, or FLASH, etc) Illumina mate-
      pair or paired-end data is best treated as pyroseq data.

   5. Recommended _prior_ to quality trimming.

   6. Stores k-mer hash in memory, may require 100s of GBs of RAM.

   7. Recommended k-mer size ~ 2 * ceil(log(genome_size)\/log(4))


/;
}


main: {
    my $fq1;
    my $fq2;
    my %argv;
    my $build = 0;
    my $handler;
    my $function;
    my $commandline = join(' ',$PROGRAM,@ARGV);
    my ($opts,$off,$klen) = (0,$DEFAULT_o,$DEFAULT_k);
    getopts('Ssh1:2:k:o:',\%argv) && !$argv{'h'} && @ARGV == 1 || &HELP_MESSAGE;
    for my $o (keys(%argv)) {
	if    ($o eq 'o') { $off   = is_uint($argv{$o},\&HELP_MESSAGE) }
	elsif ($o eq 'k') { $klen  = is_uint($argv{$o},\&HELP_MESSAGE) }
	elsif ($o eq 'S') { $opts |= 0x1 }
	elsif ($o eq 's') { $opts |= 0x2 }
    }
    if ($klen % 2) {
	throw('K must be an even integer.');
    }
    
    if ($opts == 3) { 
	throw("Invalid option combination: '-s' and '-S'.");
    } elsif ($opts & 0x2) {
	$function = \&canonize_kmer_sr;
    } elsif ($opts & 0x1) { 
	$function = \&canonize_kmer_ss;
    } else {
	$function = \&canonize_kmer_fr;
    }
    
    my $ft1 = 0;
    my $ft2 = 0;
    # is the input file fa or fq?
    $fq1 = $fq2 = open($argv{'1'},READ,NOSTREAM);
    $ft1 = $ft2 = get_type($fq1);
    $fq1 = $fq2 = open($argv{'1'},READ,NOSTREAM);
    if (exists($argv{'2'})) { 
	$fq2 = open($argv{'2'},READ,NOSTREAM);
	$ft2 = get_type($fq2);
	$fq2 = open($argv{'2'},READ,NOSTREAM);
    }
    if ($ft1 != $ft2) {
	throw('Mixed file type disallowed');
    } else {
	$handler = $ft1 & 0x1 ? \&read_fq : \&read_fa;
    }

    report('Starting ',scalar(localtime));
    report('Command-line: ',$commandline);
    
    my $out1 = open("$ARGV[0].k$klen.ddup_1.fastq.gz",WRITE);
    my $out2 = open("$ARGV[0].k$klen.ddup_2.fastq.gz",WRITE);
    my $out3 = open("$ARGV[0].k$klen.dups.fastq.gz",WRITE);
    
    ddupCore($fq1,$fq2,$out1,$out2,$out3,$function,$handler,$off,$klen);
    
    close($out1);
    close($out2);
    close($out3);
    close($fq1);
    close($fq2);

    report('Finished ',scalar(localtime));

    exit(0);
}
