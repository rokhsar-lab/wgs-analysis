#!/usr/bin/env perl

my $PACKAGE = '__PACKAGE_NAME__';
my $VERSION = '__PACKAGE_VERSION__';
my $CONTACT = '__PACKAGE_CONTACT__';
my $PURPOSE = 'Write pair-wise combos of input elements';

use strict;
use warnings;

use Getopt::Std;
use File::Basename;

use vars qw($PROGRAM);

BEGIN {
    $PROGRAM = basename($0);
}


sub readList {
    my $filename = shift;
    my $file;
    my @list;
    open($file,$filename) || die "Cannot open $filename\n";
    while (<$file>) {
	m/^\s*#|^\s*$/ && next;
	chomp();
	
	my @F = split(/\t/);

	push(@list,$_);
    }
    return \@list;
}


sub HELP_MESSAGE {
    die qq/
Program: $PROGRAM ($PURPOSE)
Version: $PACKAGE $VERSION
Contact: $CONTACT

Usage: $PROGRAM [-dl] <in1.list> [in2.list]

Options: -d  Include the diagonal (i == j) cells
         -l  Include lower half of matrix


/;
}

main: {
    my %argv;
    getopts('ld',\%argv) && @ARGV && !$argv{'h'} || &HELP_MESSAGE;

    $argv{'l'} = $argv{'l'} ? 1 : 0;
    $argv{'d'} = $argv{'d'} ? 1 : 0;
    
    my $list1 = readList($ARGV[0]);
    my $list2 = $list1;
    if (@ARGV > 1) {
	$list2 = readList($ARGV[1]);
	$argv{'d'} = 1;
	$argv{'l'} = 1;
    }
    my $M = @$list1 - 1;
    my $N = @$list2 - 1;
    my $d = 1;
    if ($argv{'d'}) {
	$d--;
    }
    if ($argv{'l'}) {
	for(my $j = 0; $j <= $M; ++$j) {
	    for(my $i = 0; $i <= $N; ++$i) {
		if ($d && $j == $i) { next }
		print(STDOUT join("\t", $list1->[$j], $list2->[$i]),"\n");
	    }
	}
    } else {
	for(my $j = 0; $j <= $M; ++$j) {
	    for(my $i = $j+$d; $i <= $N; ++$i) {
		print(STDOUT join("\t", $list1->[$j], $list2->[$i]),"\n");
	    }
	}
    }
    
    exit(0);
}
