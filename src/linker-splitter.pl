
my $PACKAGE = '__PACKAGE_NAME__';
my $VERSION = '__PACKAGE_VERSION__';
my $CONTACT = '__PACKAGE_CONTACT__';
my $PURPOSE = 'Split reads with internal linker';

use strict;
use warnings;

use Getopt::Long;
use File::Basename;
use IO::Compress::Gzip;
use IO::Uncompress::Gunzip;

use vars qw/$PROGRAM/;

BEGIN {
    $PROGRAM = basename($0);
}

sub revS {
    my $rev = reverse(shift);
    $rev =~ tr/atcgATCG/tagcTAGC/;
    return $rev;
}

sub revQ {
    return scalar(reverse(shift));
}

sub readLinkerBED {
    my $bed     = shift;
    my $linker  = shift;
    my $CHRNAME = qr/^([^\s\/]+)/;

    my %chimera;
    open(LINKER,$bed) || die "Cannot open linker BED $bed for reading: $!\n";
    while (<LINKER>) {
	m/^#|^\s*$/ && next;
	chomp();
	
	my @bed = split(/\t/);
	$bed[0] =~ $CHRNAME || die "Malformed BED entry chr: $bed[0]\n";
	$bed[0] = $1;
	$bed[4] = $bed[4] + 0; # coerce to numeric
	my $lnk = "$bed[1]:$bed[2]";

	if ($chimera{$bed[0]}) { next }
	if (defined($linker->{$bed[0]})) {
	    if ($lnk eq $linker->{$bed[0]}) { next }
	    print(STDERR "Possible chimeric sequence, discarding: $bed[0]\n");
	    delete($linker->{$bed[0]});
	    $chimera{$bed[0]}++;
	    
	} elsif ($bed[4] != 3) {
	    $chimera{$bed[0]}++
	} else {
	    $linker->{$bed[0]} = $lnk;
	}
    }
    close(LINKER);

    return 1;
}

sub read_fq {
    my $fh = shift;
    my @entry;
    $entry[0] = $fh->getline() // return; # allow catching invalid fastq formats
    $entry[0] && $entry[0] =~ /^\@/ || 
        die("Fastq header expected, line ",$fh->input_line_number(),"\n");
    $entry[1] = $fh->getline();
    $entry[2] = $fh->getline();
    $entry[3] = $fh->getline() // 
        die("Unexpected EOF, line ",$fh->input_line_number(),"\n");
    $entry[2] && $entry[2] =~ /^\+/ || 
        die("Must use Illumina 4-line fastq format\n");

    return(\@entry);
}

sub linkerSplitterCore {
    my $fdo1    = shift;
    my $fdo2    = shift;
    my $fdi     = shift;
    my $linker  = shift;
    my $fr      = shift;
    my $pyroseq = shift;
    my $HEADER  = qr/^\@([^\/\s]+)/;

    while (my $fq = read_fq($fdi)) {
	chomp($fq->[0]);
	chomp($fq->[1]);
	chomp($fq->[2]);
	chomp($fq->[3]);
	

	if ($fq->[0] =~ $HEADER) { 
	    if ($linker->{$1}) {
		my ($e,$s) = split(/:/,$linker->{$1});
		if ($fr) {
		    # if we merged PE illumina reads using FLASH or PEAR or some other software
		    # (which presumably merges the reads so that end-1 is always 5'->3'), then we
		    # should maintain the endedness of the input data in the output. Pyroseq data
		    # creates an inconsistency because the data are directionally-primed and the
		    # sequence downstream of the linker is end-1 and the upstream sequence end-2.
		    print($fdo1 join("\n","\@$1/1",revS(substr($fq->[1],0,$e)),'+',revQ(substr($fq->[3],0,$e)),''));
		    print($fdo2 join("\n","\@$1/2",substr($fq->[1],$s),'+',substr($fq->[3],$s),''));
		    
		} elsif ($pyroseq) {
		    print($fdo1 join("\n","\@$1/1",revS(substr($fq->[1],$s)),'+',revQ(substr($fq->[3],$s)),''));
		    print($fdo2 join("\n","\@$1/2",substr($fq->[1],0,$e),'+',substr($fq->[3],0,$e),''));
		    
		} else {
		    print($fdo1 join("\n","\@$1/1",substr($fq->[1],0,$e),'+',substr($fq->[3],0,$e),''));
		    print($fdo2 join("\n","\@$1/2",revS(substr($fq->[1],$s)),'+',revQ(substr($fq->[3],$s)),''));
		}
	    }
	} else {
	    die "Invalid fastq header format: $fq->[0]\n";
	}
    }
    
    return 1;
}


sub HELP_MESSAGE {
die qq/
Program: $PROGRAM ($PURPOSE)
Version: $PACKAGE $VERSION
Contact: $CONTACT

Usage: $PROGRAM [--FR] [--pyroseq] --bed <linkers.bed> <reads.fastq> <out.prefix>


/;
}

main: {
    my $bed;
    my $fr = 0;
    my $pyroseq = 0;
    GetOptions(
	'pyroseq',\$pyroseq,
	'bed=s',\$bed,
	'FR',\$fr,
    ) && @ARGV == 2 || &HELP_MESSAGE;
    
    if ($fr && $pyroseq) { 
	die "Incompatible options: data cannot be pyroseq and output in FR orientation\n";
    }

    my %linker;
    my $fastq = shift(@ARGV);
    my $prefx = shift(@ARGV);
    my $fdi   = IO::Uncompress::Gunzip->new($fastq) || die "Cannot open $fastq for reading: $!\n";
    my $fdo1  = IO::Compress::Gzip->new(sprintf("%s_1.fastq.gz",$prefx)) || die "Cannot open $prefx end-1 outfile: $!\n";
    my $fdo2  = IO::Compress::Gzip->new(sprintf("%s_2.fastq.gz",$prefx)) || die "Cannot open $prefx end-2 outfile: $!\n";
    
    readLinkerBED($bed,\%linker);
    
    linkerSplitterCore($fdo1,$fdo2,$fdi,\%linker,$fr,$pyroseq);

    close($fdo1);
    close($fdo2);
    close($fdi);

    exit(0);
}
