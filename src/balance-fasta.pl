
my $PACKAGE = '__PACKAGE_NAME__';
my $VERSION = '__PACKAGE_VERSION__';
my $CONTACT = '__PACKAGE_CONTACT__';
my $PURPOSE = 'Split a fasta file into many balanced files';


use 5.010;
use strict;
use warnings;
no warnings 'recursion';

use Getopt::Std;
use FileHandle;
use POSIX qw(ceil);
use File::Basename qw(basename);

use vars qw($PROGRAM);
use constant {TLEN => 0,FILE => 1};

BEGIN {$PROGRAM = basename($0)}

sub throw {
    printf(STDERR "[%s %s] %s\n",$PROGRAM,
	   (reverse(split '::',(caller(1))[3]))[0],join('',@_)); 
    exit(1);
}

sub warn {
    printf(STDERR "[%s %s] %s\n",$PROGRAM,
	   (reverse(split '::',(caller(1))[3]))[0],join('',@_)); 
}

sub is_file {
    my $val = shift // return 0;
    -f $val || (&warn("$val is not a regular file") && return 0);
    -r $val || (&warn("$val is not readable") && return 0);
    -s $val ||  &warn("$val is empty");
    return 1;
}

sub read_faidx {
    my $file = shift;
    is_file($file) || 
        do {$file //= 'File';&throw("$file does not exist or is empty!")
    };
    my $data = {};
    open(FAI,$file) || &throw("Cannot open $file for reading!");
    local ($_,$.) = ('',0);
    while (<FAI>) {
        (m/^#/ || m/^\s*$/) && next;
        m/([!-)+-<>-~][!-~]*)\t(\d+)\t(\d+)/ || 
            &throw("Invalid identifier or insufficient data (line $.)!");
        exists $data->{$1} && 
            &warn("Duplicate entry detected in $file (line $.)!");
        $data->{$1}->{'length'} = $2;
        $data->{$1}->{'pos'} = $3;
    }
    close(FAI);
    return $data;
}

sub distribute {
    my ($dist,$srt,$len,$numparts,$binsz) = @_;
    
    my $i = 0; 
    while (@$srt && ($i < $$numparts)) {
	if (($dist->[$i]->[TLEN] + $len->{$srt->[0]}) > $$binsz) {
	    @$dist = sort {$a->[TLEN] <=> $b->[TLEN]} @$dist;
	    $i = 0;
	}
	my $fh = $dist->[$i]->[1];
	print($fh join("\t",$srt->[0],0,$len->{$srt->[0]}),"\n");
	$dist->[$i]->[0] += $len->{$srt->[0]};
	shift @$srt;
	$i++;
    }
    # don't worry too much about performance here, the list
    # will be partially sorted, which is efficient ~ O(N*log(N))
    @$dist = sort {$a->[TLEN] <=> $b->[TLEN]} @$dist;
    
    if (@$srt) {
	return distribute($dist,$srt,$len,$numparts,$binsz);
    } else { 
	return 1;
    }
}

sub core {
    my $infile      = shift;
    my $prefix      = shift;
    my $numparts    = shift;
    my $mkbreaks    = shift;
    my $len_overlap = shift;
    my $initial     = shift;
    
    -s $infile.'.fai' || &throw("$infile not (faidx) indexed.");
    my $fai = read_faidx($infile.'.fai');
    
    my ($binsz,$nSeqs,%len) = (0,0);
    my @len_sorted = map {substr($_,15)} CORE::sort 
	                  map {$len{$_} = $fai->{$_}->{'length'};
			       $binsz  += $len{$_}; $nSeqs++;
			       sprintf('%015s%s',$len{$_},$_)} keys %$fai;
    @len_sorted = reverse(@len_sorted);
    $binsz = ceil($binsz/$numparts);
    $fai = undef;

    my $p = 0; 
    my %intervals;
    my @distributed;
    while (@len_sorted && ($p < $numparts)) {
	my $scaff = shift @len_sorted;
	my @intervals;
	if ($mkbreaks && ($len{$scaff} > $binsz)) {
	    my $n_intervals = ceil($len{$scaff}/$binsz);
	    for(my $i = 0; $i < $n_intervals; ++$i) {
		my $s = $binsz * $i;
		my $t = $binsz * ($i + 1) + $len_overlap;
		if ($t > $len{$scaff}) { $t = $len{$scaff} }
		push @intervals,[$s,$t];
	    }
	} else { 
	    @intervals = ([0,$len{$scaff}]);
	}

	while (@intervals && ($p < $numparts)) {
	    my $fh = FileHandle->new(sprintf('>%s.%05s.bed',$prefix,$initial+$p)) || 
		&throw(sprintf("Could not open %s.%05s.bed for writing\n",$prefix,$initial+$p));
	    my $i = shift @intervals;
	    print($fh join("\t",$scaff,@$i),"\n");
	    $distributed[$p]->[TLEN] += ($i->[1] - $i->[0]);
	    $distributed[$p]->[FILE]  = $fh;
	    $p++;
	}
	if (@intervals) { &throw("BUG: Assert fail: intervals remain.") }
    }
    
    distribute(\@distributed,\@len_sorted,\%len,\$numparts,\$binsz) ||
	&warn("failed to complete at sub distribute");
        
    for (my $i = 0; $i < scalar(@distributed); ++$i) {
	$distributed[$i]->[FILE] && $distributed[$i]->[FILE]->close;
    }
    
    return 1;
}

sub HELP_MESSAGE {
    die qq/
Program: $PROGRAM ($PURPOSE)
Version: $PACKAGE $VERSION
Contact: $CONTACT

Usage:   $PROGRAM [options] <in.fasta> <out.prefix>

Options: -p INT   Split in.fasta to INT parts [10]
         -i INT   Initialize the output file counter with INT [1]
         -b       Allow breaking large scaffolds into sub-parts
         -o INT   When breaking scaffolds, create overlap of INT bp [0]
         -h       This help message

Notes: Best balance is achieved with 100 parts or less.

/;
}

sub main {
    my (%argv,$outfile);
    my ($numparts,$mkbreaks,$len_overlap,$initial) = (10,0,0,1);
    getopts('bho:p:i:',\%argv) && @ARGV == 2 || &HELP_MESSAGE;
    for my $o (keys %argv) {
	if    ($o eq 'h') { &HELP_MESSAGE }
	elsif ($o eq 'b') { $mkbreaks = 1 }
	elsif ($o eq 'i') { $initial  = abs(int($argv{$o})) || 1 }
	elsif ($o eq 'p') { $numparts = abs(int($argv{$o})) || 10 }
	elsif ($o eq 'o') { $len_overlap = abs(int($argv{$o})) || 0 }
    }

    core(@ARGV[0,1],$numparts,$mkbreaks,$len_overlap,$initial);
    
    return 0;
}


main();

exit(0);

