
$PURPOSE = 'Calculate SNV rate from a VCF file';
$PACKAGE = '__PACKAGE_NAME__';
$VERSION = '__PACKAGE_VERSION__';
$CONTACT = '__PACKAGE_CONTACT__';

use strict;
use warnings;

use Getopt::Std;

use Tabix;
use File::Which;
use File::Basename;
use Valkyr::Error qw(throw report);
use Valkyr::Error::Exception {
    'RuntimeError' => ['MalformedBed']
};

use Valkyr::Math::Utils qw(max min);
use Valkyr::Math::Units::HumanReadable 'expand';
use Valkyr::FileUtil::IO;
use Valkyr::FileUtil::VCF ':all';
use Valkyr::FileUtil::Fasta 'readFaidx';
use Valkyr::FileUtil::Counter 
    qw(progressReport progressCounter configCounter);
use Valkyr::Data::Type::Test;
use Valkyr::Data::Type::Assert 
    qw(is_uint is_string is_hashref is_arrayref is_subclass is_file);
use Valkyr::Util::Token qw(__PROGRAM__ __FUNCTION__);

use vars qw($PROGRAM $PACKAGE $PURPOSE $VERSION $CONTACT $DEFAULT_w $DEFAULT_s);

BEGIN {
    $PROGRAM = basename($0);
    $DEFAULT_w = '10k';
    $DEFAULT_s = '1k';
}

sub chrom  () { 0 }
sub start  () { 1 }
sub end    () { 2 }
sub name   () { 3 }
sub score  () { 4 }
sub strand () { 5 }

sub parse_from_vcf_file  () { 0x1 }
sub window_width_dynamic () { 0x2 }
sub window_width_fixed   () { 0x4 }
sub count_singletons     () { 0x8 }


sub readSingletons {
    my $file = is_file(shift,'fr');

    report('Reading singletons');

    my %singletons;
    my $singletons = open($file,READ);
    while(local $_ = $singletons->getline()) {
	m/^CHROM|^#|^\s*$/ && next
	chomp();

	my @single = split(/\t/);
	

	if (! Valkyr::Data::Type::Test::is_string($single[CHROM])) {
	    throw("CHROM field unexpected value: $single[CHROM]");
	}
	if (! Valkyr::Data::Type::Test::is_uint($single[POS])) {
	    throw("POS field unexpected value: $single[POS]");
	}
	if (! (Valkyr::Data::Type::Test::is_string($single[2]) && $single[2] =~ /^[DS]$/)) {
	    throw("SINGLETON/DOUBLETON field unexpected value: $single[2]");
	}
	if (! (Valkyr::Data::Type::Test::is_string($single[3]) && $single[3] =~ /^[ATCG]+$/)) {
	    throw("ALLELE field unexpected value: $single[3]");
	}

	$singletons{$single[CHROM]}{$single[CHROM].':'.$single[POS]} = $single[3];
    }
    close($file);

    return \%singletons;
}   


sub calcVariableEndPos {
    my $bed = is_arrayref(shift);
    my $off = is_uint(shift);
    my $len = is_uint(shift);

    my $sum = 0;
    my $end = $bed->[0]->[start]+1;
    for(my $i = 0; $i < @$bed; ++$i) {
	my $call  = $bed->[$i];
	my $start = max($off,$call->[start]);
	my $width = $call->[end] - $start;
	
	if ($sum+$width < $len) {
	    # |-----------|
	    # ==============//=======
	    $end  = $call->[end];
	    $sum += $width;
	} else {
	    # |---------||-----------------)
	    # ==============//=======
	    $end  = $call->[end] - ($sum + $width - $len); 
	    return $end;
	}
    }
    
    return $end;
}


sub slideWindow {
    my $bed = is_arrayref(shift);
    my $fai = is_hashref(shift);
    my $Wsz = is_uint(shift);
    my $Ssz = is_uint(shift);
    my $opt = is_uint(shift);

    my $chr = is_string($fai->{'contig'});
    my $max = is_uint($fai->{'length'});

    my $dynamic_window = $opt & (window_width_dynamic | window_width_fixed);

    my @positions;
    my @intervals;
    my $off = 0;
    my $dat = @$bed;
    my $end = max(1, $max-$Wsz);
    if ($opt & window_width_fixed) {
	for(my $pos = 0; $pos < @$bed; $pos += $Ssz) {
	    push(@positions, $bed->[$pos/$Ssz]->[start]);
	}
    } else {
	for(my $pos = 0; $pos <= $end; $pos += $Ssz) {
	    push(@positions, $pos);
	}
    }
    my $n = @$bed;
    for my $pos (@positions) {
	while ($n && $bed->[0]->[end] < $pos) {
	    shift(@$bed);
	    $n--;
	    # $dat = @$bed;
	}
	if ($dynamic_window && ($n > 0)) {
	    push(@intervals,[$chr,$pos,max(min($pos+$Wsz,$max),calcVariableEndPos($bed,$pos,$Wsz))]);
	} else {
	    push(@intervals,[$chr,$pos,min($pos+$Wsz,$max)]);
	}
    }

    #use Data::Dumper; print STDERR Dumper(\@intervals); exit 1;

    return \@intervals;
}


sub calcVcfIntervals {
    report('Calculating intervals from VCF file');    
    my $vcf = is_file(shift, 'fr');
    my $fai = is_hashref(shift);
    my $trg = is_hashref(shift);
    my $Wsz = is_uint(shift);
    my $Ssz = is_uint(shift);
    my $opt = is_uint(shift);

    my $fdi = open($vcf,READ);

    my ($META,$HEADER) = getMeta($fdi);
    
    my $width = @$HEADER;
    my %FIELD = %{scalar( indexArray(@$HEADER) )};
    my $nGeno = max(0,$width - ($FIELD{'INFO'}+2));

    throw(sprintf('At least five samples required by %s().',
		  __FUNCTION__)) if ($nGeno < 1);

    my @callable;
    my @interval;
    
    my $currchr;
    my $currbeg;
    my $currend;
    
    my $prevchr;
    my $prevbeg;
    my $prevend;
    my $intervals = 0;
    while (my $vcf = getEntry($fdi,$width)) {
	if ($vcf->[FILTER] !~ PASS_FILTER_PAT) {
	    next;
	}
	if ($vcf->[ALT] =~ BREAKEND_PAT) {
	    next;
	}
	my $FORMAT = getFORMAT($vcf->[FORMAT]);
	my @sample = getSample($vcf->[FORMAT+1],$FORMAT);
	if ($sample[$FORMAT->{'GT'}] =~ /\./) {
	    next;
	}
	$currchr = is_string($vcf->[CHROM]);
	$currbeg = is_uint($vcf->[POS]) - 1;
	$currend = is_uint($vcf->[POS]) - 1 + length($vcf->[REF]);
	
	if (defined($prevend)) {
	    if ($currchr ne $prevchr) {
		push(@callable, [$prevchr, $prevbeg, $prevend]);
		push(@interval, slideWindow(\@callable,$fai->{$prevchr},$Wsz,$Ssz,$opt));
		$intervals += scalar(@{$interval[$#interval]});
		$prevchr = $currchr;
		$prevbeg = $currbeg;
		$prevend = $currend;
		@callable = ();
	    
	    } elsif (($currbeg - $prevend) > 0) {
		push(@callable, [$prevchr, $prevbeg, $prevend]);
		$prevbeg = $currbeg;
	    }
	} else {
	    $prevchr = $currchr;
	    $prevbeg = $currbeg;
	    $prevend = $currend;
	}
	$prevend = $currend;
    }
    if (defined($prevend)) {
	push(@callable, [$prevchr, $prevbeg, $prevend]);
	push(@interval, slideWindow(\@callable,$fai->{$prevchr},$Wsz,$Ssz,$opt));
	$intervals += scalar(@{$interval[$#interval]});
	@callable = ();
    }

    report("Interval count: $intervals");
    
    return \@interval;
}


sub calcBedIntervals {
    report('Calculating intervals from BED file');    
    my $bed = is_file(shift,'fr');
    my $fai = is_hashref(shift);
    my $trg = is_hashref(shift);
    my $Wsz = is_uint(shift);
    my $Ssz = is_uint(shift);
    my $opt = is_uint(shift);    

    my $fdi = open($bed,READ);

    my @callable; # windowed intervals 
    my @interval; # contig-level
    my $prevchr;
    my $prevpos = 0;
    my $intervals = 0;
    while (my $line = $fdi->getline()) {
	if($line =~ /^#|^\s*$/) { next }
	chomp($line);
	
	my @bed = split(/\t/,$line);

	if (! $fai->{is_string($bed[chrom])}) {
	    throw('BED file contig not found in reference file: ',$bed[chrom]);
	}
	if (! $trg->{is_string($bed[chrom])}) {
	    next
	}
	
	$prevchr //= $bed[chrom];
	$prevpos //= $bed[start];
	if (($bed[chrom] ne $prevchr) && @callable) {
	    push(@interval,slideWindow(\@callable,$fai->{$prevchr},$Wsz,$Ssz,$opt));
	    $intervals += scalar(@{$interval[$#interval]});
	    @callable = ();

	} elsif (is_uint($bed[end]) <= is_uint($bed[start])) {
	    throw MalformedBed(sprintf('Invalid BED interval (start > end): %s:%u-%u',
				       @bed[chrom,start,end]));

	} elsif ($bed[start] < $prevpos) {
	    throw MalformedBed(sprintf('Unsorted BED entry (previous > current): %s > %s',$prevpos,$bed[start]));
	}
	push(@callable,[ @bed[chrom,start,end] ]);

	$prevchr = $bed[chrom];
	$prevpos = $bed[start];
    }
    if (@callable) {
	push(@interval,slideWindow(\@callable,$fai->{$prevchr},$Wsz,$Ssz,$opt));
	$intervals += scalar(@{$interval[$#interval]});
    }
    @callable = ();


    close($fdi);

    report("Interval count: $intervals");

    return \@interval;
}


sub calcIntervals {
    my $fni = is_file(shift, 'fr');
    my $fai = is_hashref(shift);
    my $trg = is_hashref(shift);
    my $Wsz = is_uint(shift);
    my $Ssz = is_uint(shift);
    my $opt = is_uint(shift);

    if ($opt & parse_from_vcf_file) {
	return calcVcfIntervals($fni,$fai,$trg,$Wsz,$Ssz,$opt);
    } else {
	return calcBedIntervals($fni,$fai,$trg,$Wsz,$Ssz,$opt);
    }
}


sub numMismatches {
    # fast tallying of mismatches to be summed across sites:
    # O(2N) fast; Divide result by (min? avg?) length of sequences?
    # PI = (sum(mismatches) / M) / L; 
    # where M = number of variant markers(loci), L = length of sequences
    # TODO: *add the proof here*
    my $n = is_arrayref(shift);
    my $D = 0;
    my $N = sum(@$n);
    for (my $i = 0; $i < $#{$n}; ++$i) { 
	$N -= $n->[$i]; 
	$D += $n->[$i]*$N 
    } 
    return $D;
}


sub snvrateCore {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $bed = is_subclass(shift,'Tabix');
    my $vcf = is_subclass(shift,'Tabix');
    my $cal = is_arrayref(shift);
    my $fai = is_hashref(shift);
    my $trg = is_hashref(shift);
    my $sng = is_hashref(shift);
    my $opt = is_uint(shift);
    my $req = ['FT'];

    configCounter({'prefix' => 'Intervals processed: ', 'granularity' => 10});

    report('Calculating rates over ',scalar(@$cal),' contig(s)');

    print($fdo join("\t",qw(CHROM START END NAME SCORE STRAND CALLABLE HET HOM)),"\n");
    while (my $chr = shift(@$cal)) {
	if (! @{is_arrayref($chr)}) { next }
	for(my $i = 0; $i < @$chr; ++$i) {
	    progressCounter();
	    my $I = is_arrayref($chr->[$i]);

	    # print(STDERR "@$I\n");
	    
	    if (!defined($I->[chrom]) || 
		!Valkyr::Data::Type::Test::is_uint($I->[start]) ||
		!Valkyr::Data::Type::Test::is_uint($I->[end])) {
		throw("Invalid interval: '@$I'");
	    }

	    my $B = $bed->query($I->[chrom],$I->[start],$I->[end]);
	    my $S = $sng->{$I->[chrom]} || {};

	    my $F = 0;
	    my $H = 0;
	    my $L = 0;
	    if (Valkyr::Data::Type::Test::is_subclass($B,'TabixIterator') &&
		Valkyr::Data::Type::Test::is_instance($B->get(),'ti_iter_t')) {
		while (local $_ = $bed->read($B)) {
		    if (/^#|^\s*$/) { next }
		    chomp();
		    
		    my @callable = split(/\t/);

		    if ($opt & parse_from_vcf_file) {
			if ($callable[FILTER] !~ PASS_FILTER_PAT) {
			    next;
			}
			if ($callable[ALT] =~ BREAKEND_PAT) {
			    next;
			}
			my $FORMAT = getFORMAT($callable[FORMAT]);
			my @sample = getSample($callable[FORMAT+1],$FORMAT);
			if ($sample[$FORMAT->{'GT'}] =~ /\./) {
			    next;
			}
			@callable = (
			    $callable[CHROM],
			    $callable[POS] - 1,
			    $callable[POS] - 1 + length($callable[REF])
			);
		    }
		    $callable[start] = max($I->[start],$callable[start]);
		    $callable[end]   = min($I->[end],  $callable[end]);

		    $L += $callable[end]-$callable[start];

		    if (!defined($callable[chrom]) || 
			!Valkyr::Data::Type::Test::is_uint($callable[start]) ||
			!Valkyr::Data::Type::Test::is_uint($callable[end])) {
			throw("Invalid interval: '@callable'");
		    }
		    
		    my $V = $vcf->query(@callable[chrom,start,end]); # requires half-open interval coords
		    if (Valkyr::Data::Type::Test::is_subclass($V,'TabixIterator') && 
			Valkyr::Data::Type::Test::is_instance($V->get(),'ti_iter_t')) {
			while (local $_ = $vcf->read($V)) {
			    if (/^#|^\s*$/) { next }
			    chomp();
			    
			    my @variant = split(/\t/);

			    if ($variant[FILTER] =~ PASS_FILTER_PAT) {
				my %F = %{scalar( getFORMAT($variant[FORMAT],$req) )};
				my @v = getSample($variant[FORMAT+1],\%F);
			
				if ($v[$F{'GT'}] eq NULL_GT || 
				    $v[$F{'FT'}] !~ PASS_FILTER_PAT) {
				    $L--;
				    next;
				}
				my @g = split(GT_FIELD_SEP,$v[$F{'GT'}]);

				# could swap in numMismatch() here for multi-sample 
				# (dont forget to divide by (2N(2N-1)/2) !):
				if ($opt & count_singletons) {
				    if ($S->{"$variant[CHROM]:$variant[POS]"}) {
					$H += ($g[0] != $g[1]);
					$F += ($g[0] && $g[1] && $g[0] == $g[1]);
				    }
				} else {
				    $H += ($g[0] != $g[1]);
				    $F += ($g[0] && $g[1] && $g[0] == $g[1]);
				}
			    } else {
				$L--;
			    }
			}
		    }
		}
	    }
	    print($fdo join("\t",@{$I}[chrom,start],
			    min($I->[end],$fai->{$I->[chrom]}->{'length'}),
			    '.', 0, '+', $L, ($L ? sprintf("%.2e",$H/$L) : -1), ($L ? sprintf("%.2e",$F/$L) : -1)),"\n");
	}
    }
    progressReport();

    return 1;
}


sub HELP_MESSAGE {
    die qq/
Program: $PROGRAM ($PURPOSE)
Version: $PACKAGE $VERSION
Contact: $CONTACT

Usage:   $PROGRAM [options] <ref.fasta> <in.vcf.gz> [in.bed.gz]

Options: -o FILE  Write output to FILE [stdout]
         -S FILE  Calc. rate of singletons listed in FILE (see Note 4)
         -w INT   Sliding window width of loci (0 to disable) [$DEFAULT_w]
         -s INT   Sliding window step-size [$DEFAULT_s]
         -L STR   Examine a single contig [null]
         -f       Fixed window size of {-w} variants
         -v       Variable window size [fixed-width]

Notes: 

  1. in.vcf.gz and in.bed.gz are bgzip'ed and tabix indexed VCF and
     BED files (respectively) for a single individual of interest.

  2. in.bed.gz is recommened to be derived from calling the GATK
     HaplotypeCaller SNP calling procedure with -allSites genotyped.
     After hard filtering the VCF file genotypes, use the following
     command to extract the remaining callable loci:
      vcftools --indv sample.id --max-missing 1.0 \
       --recode --stdout --gzvcf allSites.vcf.gz | \
       bedtools merge -sorted -i - | bgzip >in.bed.gz

  3. Window width and step sizes may contain human-readable numeric 
     suffixes [kMGT]

  4. Argument to '-S' is a .singletons file with the entries for the 
     target individual extracted. This file is generated by the 
     `vcftools --singletons` command.


/;
}


main: {
    my %argv;
    my $opts = 0;
    my $step = expand($DEFAULT_s);
    my $width = expand($DEFAULT_w);
    my @targets = ();
    my $single  = undef;
    my $outfile = STDIO;
    my $command = join(' ',basename($0),@ARGV);
    getopts('Xho:w:s:S:L:vf',\%argv) && ((@ARGV == 2) || (@ARGV == 3)) || &HELP_MESSAGE;
    for my $o (keys(%argv)) {
	if ($o eq 'v') {
	    $opts |= window_width_dynamic;
	} elsif ($o eq 'f') {
	    $opts |= window_width_fixed;
	} elsif ($o eq 'S') {
	    $single = is_file($argv{$o},'fr',\&HELP_MESSAGE);
	    $opts |= count_singletons;
	} elsif ($o eq 'w') {
	    $width = is_uint(expand($argv{$o}),\&HELP_MESSAGE);
	} elsif ($o eq 's') {
	    $step  = is_uint(expand($argv{$o}),\&HELP_MESSAGE); 
	} elsif ($o eq 'L') {
	    push(@targets, is_string($argv{$o},\&HELP_MESSAGE));
	} elsif ($o eq 'o') {
	    $outfile = is_string($argv{$o},\&HELP_MESSAGE);
	} elsif ($o eq 'X') {
	    $Valkyr::Error::DEBUG = 1;
	}
    }
    if (($opts & window_width_dynamic) && ($opts & window_width_fixed)) {
	throw('Window cannot be both variable- and fixed-width');
    }
    if ($width < 0) {
	throw('Window width must be 0 or greater (0 to disable)');
    }
    if ($width == 0) {
	$width = 2**32 - 1;
    }
    if (!(0 <= $step && $step <= $width)) {
	throw('Step size must be greater than 0 and less than or equal to the window size');
    }
    if ($step == 0) {
	$step = 2**32 - 1;
    }
    my $dynamic_window = $opts & (window_width_dynamic | window_width_fixed);
    
    report('Starting ',scalar(localtime));
    report('Command-line: ',$command);
    
    if ($ARGV[1] !~ /\.b?gz$/ || 
	!Valkyr::Data::Type::Test::is_file($ARGV[1],'fr') || 
	!Valkyr::Data::Type::Test::is_file($ARGV[1].'.tbi','fr')) {
	throw("VCF must exist, be bgzip'd, and tabix indexed: $ARGV[1]");
    }
    if ((@ARGV == 3) && 
	(($ARGV[2] !~ /\.b?gz$/) ||
	 !Valkyr::Data::Type::Test::is_file($ARGV[2],'fr') || 
	 !Valkyr::Data::Type::Test::is_file($ARGV[2].'.tbi','fr'))) {
	throw("BED must exist, be bgzip'd, and tabix indexed: $ARGV[2]");
    } elsif (@ARGV == 2) {
	$opts |= parse_from_vcf_file;
	push(@ARGV,$ARGV[1]);
    }
    
    my $fai = readFaidx($ARGV[0]);
    my $sng = defined($single) ? readSingletons($single) : {};
    my $trg = {};
    if (@targets) {
	for my $target (@targets) {
	    if (! exists($fai->{$target})) {
		throw('User-passed contig not found in reference file: ',$target);
	    }
	    $trg->{$target} = $fai->{$target};
	}
    } else {
	$trg = $fai;
    }
    
    my $fdout = open($outfile,WRITE);
    my $fdvcf = Tabix->new(-data => $ARGV[1]);    
    my $fdbed = Tabix->new(-data => $ARGV[2]);
    my $itr = calcIntervals($ARGV[2],$fai,$trg,$width,$step,$opts);
    
    snvrateCore($fdout,$fdbed,$fdvcf,$itr,$fai,$trg,$sng,$opts);

    $fdbed->close();
    $fdvcf->close();
    $fdout->close();

    report('Finished ',scalar(localtime));

    exit(0);
}
