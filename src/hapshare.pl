
$PACKAGE = '__PACKAGE_NAME__';
$VERSION = '__PACKAGE_VERSION__';
$CONTACT = '__PACKAGE_CONTACT__';
$PURPOSE = 'Calculate rate of shared SNVs';


use strict;
use warnings;

use Getopt::Std;

use Tabix;
use File::Which;
use File::Basename;
use Valkyr::Error qw(throw report);
use Valkyr::Error::Exception {
    'RuntimeError' => ['MalformedVcf']
};
use Valkyr::Math::Utils qw(max argmax min sum);
use Valkyr::Math::Units::HumanReadable 'expand';
use Valkyr::FileUtil::IO;
use Valkyr::FileUtil::VCF ':all';
use Valkyr::FileUtil::Fasta 'readFaidx';
use Valkyr::Data::Type::Test;
use Valkyr::Data::Type::Assert 
    qw(is_uint is_ufloat is_string is_hashref is_arrayref is_subclass is_file);

use vars qw($PROGRAM $PACKAGE $PURPOSE $VERSION $CONTACT $DEFAULT_w $DEFAULT_s @COLORS $NULL_LINE_PAT);

BEGIN {
    $PROGRAM = basename($0);
    $DEFAULT_w = '100';
    $DEFAULT_s = '10';
    $NULL_LINE_PAT = qr/^#|^\s*$/;
    ###          #ORANGE       #GREEN        #BLUE         #GREY
    @COLORS = ('"#FC9407FF"','"#81EE7EFF"','"#76C3E6FF"','"#B0B0B0FF"');
}

#### CONSTANTS ####
# intervals file
sub LOCUS  () { 0 }
sub ALLELE () { 1 }
sub AIDX   () { 2 }
# BED file
sub chrom  () { 0 }
sub pos    () { 1 }
sub start  () { 1 }
sub end    () { 2 }
sub name   () { 3 }
sub score  () { 4 }
sub strand () { 5 }
# MAP file
sub CHR    () { 0 }
sub MRK    () { 1 }
sub GPOS   () { 2 }
sub PPOS   () { 3 }
# __internal
sub dose   () { 3 }

# sub numMismatches {
#     # fast tallying of mismatches to be summed across sites:
#     # O(2N) fast; Divide result by (min? avg?) length of sequences?
#     # PI = (sum(mismatches) / M) / L; 
#     # where M = number of variant markers(loci), L = length of sequences
#     # TODO: *add the proof here*
#     my $n = is_arrayref(shift);
#     my $D = 0;
#     my $N = sum(@$n);
#     for (my $i = 0; $i < $#{$n}; ++$i) { 
# 	$N -= $n->[$i]; 
# 	$D += $n->[$i]*$N 
#     } 
#     return $D;
# }


sub as_sbitA {
    my $allele = is_string(shift);

    return $allele eq NULL_ALLELE ? 0 : (1 << is_uint($allele));
}


sub getNextEntry {
    my $fh = is_subclass(shift,'IO::Handle');
    my $N  = is_uint(shift//1);

    while ( my $line = $fh->getline() ) {
        $line =~ $NULL_LINE_PAT && next;
        chomp($line);
        
        my @fields = split(/\t/,$line);

        # do some basic entry checking for required fields
        if (@fields < $N) {
            throw("MAP format required field (=$N) missing.");
        }

        return(\@fields);
    }
}


sub nextLocus {
    my $fh = is_subclass(shift,'IO::Handle');
    my $w  = is_uint(shift);
    
    my $locus = getNextEntry($fh,$w) || return;

    if (!Valkyr::Data::Type::Test::is_string($locus->[CHR])) {
        throw("Invalid Chr value: $locus->[CHR]");
    }
    if (!Valkyr::Data::Type::Test::is_string($locus->[ID])) {
        throw("Invalid Marker value: $locus->[ID]");
    }
    if (!Valkyr::Data::Type::Test::is_ufloat($locus->[GPOS])) {
        throw("Invalid genetic position value: $locus->[GPOS]");
    }
    if (defined($locus->[PPOS]) && 
        !Valkyr::Data::Type::Test::is_uint($locus->[PPOS])) {
        throw("Invalid physical position value: $locus->[PPOS]");
    }

    return $locus;
}


sub readMap {
    my $file = is_file(shift,'fr');

    my %map;
    my $fh = open($file,READ);
    while (my $locus = nextLocus($fh,4)) {
        $map{$locus->[CHR]}->{$locus->[PPOS]} = $locus->[GPOS];
    }

    return \%map;
}


sub readOrderedList {
    my $file  = is_file(shift,'f');
    my $field = is_uint(shift // 0);
    
    my %seen;
    my @data;
    
    my $list = open($file,READ);
    while (defined( local $_ = $list->getline() )) {
        m/^#|^\s*$/ && next;
        chomp();
        my @F = split(/\t/);
        
        if ($seen{$F[$field]}) {
            warn(sprintf('Duplicate entry detected (%s, line %s).',
                         $file,$list->input_line_number()));
        }

        push(@data,$F[$field]);
        $seen{$F[$field]}++;
    }
    $list->close();

    return \@data;
}


sub readAlleles {
    my $alleles = is_string(shift);
    
    my %allele;
    my $fh = open($alleles,READ);
    while ($_ = $fh->getline()) {
        if (/^#/) { next }
        chomp(); 
        #Decorated .interval format:
        #chr:pos Ref,Alt1[,Alt2[,..]] alleleIndex
        # Chromosome01:150 C,T 1
        # Chromosome01:155 T,C 1

        my @variant = split(/\t/);
	
	if (@variant < 3) {
	    throw("Incomplete .intervals entry (line ",$fh->input_line_number(),")");
	}
        my ($locus) = split(/\-/,$variant[LOCUS]);
        my @alleles = split(/,/,$variant[ALLELE]);
        

        if ($variant[AIDX] >= @alleles) {
            throw("Allele index out of bounds: $locus");
        }

        $allele{$locus} = $alleles[$variant[AIDX]];
    }

    return \%allele;
}


sub runRscript {
    my $Rinterp = is_file(shift,'x');
    my $Rscript = is_file(shift,'r');
    my $cleanup = is_uint(shift||0);

    report("Rscript command: $Rinterp $Rscript");

    my $status = `$Rinterp $Rscript 2>&1`;

    if ($Valkyr::Error::DEBUG) {
        map {debug($_)} split("\n",$status);
    }
    if ($cleanup) {
        unlink($Rscript);
    }

    return 1;
}


sub hapSharePlot {
    my $mic = is_file(shift,'frs');
    my $mac = is_file(shift,'frs');
    my $pfx = is_string(shift);
    my $mTh = is_ufloat(shift);


    my $R = open("$pfx.Rscript",WRITE);

    print($R qq/#!\/usr\/bin\/env Rscript

MICRO = read.table("$mic",header=T, na.strings='NA');
MACRO = read.table("$mac",header=T, na.strings='NA');
CHROM = MACRO[which(MACRO\$FLD == 'CHR'),];
MACRO = MACRO[which(MACRO\$FLD == 'SEG'),];

MACRO\$COLOR = as.character(MACRO\$COLOR);

num.chrs = length(unique(MACRO\$CHROM));
num.cols = 1;
if (num.chrs > 1) {
    num.cols = min(3,num.chrs);
}
num.rows = ceiling(num.chrs \/ num.cols);

pdf("$mac.pdf", height=4*num.rows, width=7*num.cols);
par(mfrow=c(num.rows,num.cols));


for (chr in unique(CHROM\$CHROM)) { 
    chrom = CHROM[which(CHROM\$CHROM == chr),];
    macro = MACRO[which(MACRO\$CHROM == chr),];
    micro = MICRO[which(MICRO\$CHROM == chr),];

    plot(micro\$START\/1e6,micro\$AA, type='l', lwd=2, col=$COLORS[0], bty='n', yaxp=c(0,1,5), las=1, xlim=c(0,max(micro\$END)\/1e6), ylim=c(0,1.2), main=chr, xlab='Genomic position (Mbp)', ylab='Allele fraction');
    lines(micro\$START\/1e6,micro\$AB, lwd=2, col=$COLORS[1]);
    lines(micro\$START\/1e6,micro\$BB, lwd=2, col=$COLORS[2]);
    abline(h=$mTh, lty=2);

    segments(chrom[1,c('START')]\/1e6,1.12,chrom[1,c('END')]\/1e6,1.12, col=$COLORS[3], lwd=28, lend=1);
    for (block in 1:length(macro\$START)) {
        segments(macro[block,c('START')]\/1e6,1.12,macro[block,c('END')]\/1e6,1.12, col=macro[block,c('COLOR')], lwd=28, lend=1);
    }
}
invisible(dev.off());

/);

    close($R);
}


sub calcVariableEndPos {
    my $bed = is_arrayref(shift);
    my $off = is_uint(shift);
    my $len = is_uint(shift);

    my $sum = 0;
    my $end = $bed->[0]->[start]+1;
    for(my $i = 0; $i < @$bed; ++$i) {
	my $call  = $bed->[$i];
	my $start = max($off,$call->[start]);

	my $seglen = $call->[end] - $start;
	if ($sum+$seglen < $len) {
	    # |-----------|
	    # ==============//=======
	    $end  = $call->[end];
	    $sum += $seglen;
	} else {
	    # |---------||-----------------)
	    # ==============//=======
	    $end  = $call->[end] - ($sum + $seglen - $len); 
	    return $end;
	}
    }
    
    return $end;
}


sub slideWindow {
    my $bed = is_arrayref(shift);
    my $fai = is_hashref(shift);
    my $Wsz = is_uint(shift);
    my $Ssz = is_uint(shift);

    my $chr = is_string($fai->{'contig'});
    my $max = @{$bed};

    my @intervals;
    my $end = max(1,$max-$Wsz);
    for(my $p = 0; $p <= $end; $p += $Ssz) {
	push(@intervals,[$chr,$bed->[$p]->[start], $bed->[min($max-1,$p+$Wsz)]->[start] ]);
    }

    #use Data::Dumper; print STDERR Dumper(\@intervals); exit 1;

    return \@intervals;
}


sub calcIntervals {
    my $vcf = is_subclass(shift,'Tabix');
    my $ord = is_arrayref(shift);
    my $fai = is_hashref(shift);
    my $all = is_hashref(shift);
    my $Wsz = is_uint(shift);
    my $Ssz = is_uint(shift);

    report('Calculating intervals');

    my $REQD = ['FT'];

    my @callable;
    my @interval;
    for my $Contig (@$ord) {
	my $contig = $Contig->{'contig'};
	
	if (!$fai->{$contig}) { next }
	if (!Valkyr::Data::Type::Test::is_uint($fai->{$contig}->{'length'})) {
            throw("Invalid contig length: '$contig'")
	}
	my $interval = $vcf->query($contig,0,$fai->{$contig}->{'length'});
	
	if (Valkyr::Data::Type::Test::is_subclass($interval,'TabixIterator') &&
	    Valkyr::Data::Type::Test::is_instance($interval->get(),'ti_iter_t')) {
	    
	    
	    my $prevpos;
	    while (my $line = $vcf->read($interval)) {
		if($line =~ /^#|^\s*$/) { next }
		chomp($line);
		
		my @variant = split(/\t/,$line);
				
		$prevpos //= $variant[POS];
		if ($variant[POS] < $prevpos) {
		    throw MalformedVariant(sprintf('Unsorted VCF (%s !< %s)',$prevpos,$variant[POS]));
		}
		if ($variant[FILTER] !~ PASS_FILTER_PAT || ! $all->{$variant[CHROM].':'.$variant[POS]}) {
		    # pass
		} else {
		    if ($variant[FORMAT] && $variant[FORMAT+1]) {
			my %FORMAT = %{scalar( getFORMAT($variant[FORMAT],$REQD) )};
			my @v = getSample($variant[FORMAT+1],\%FORMAT);
			
			if ($v[$FORMAT{'GT'}] ne NULL_GT &&
			    $v[$FORMAT{'FT'}] =~ PASS_FILTER_PAT) {
			    push(@callable,[ $variant[CHROM],$variant[POS]-1,$variant[POS] ]);	
			}
		    } else {
			throw(sprintf("No sample at locus: %s:%u",@variant[CHROM,POS]));
		    }
		}
		
		
		$prevpos = $variant[POS];
	    }
	    if (@callable) {
		push(@interval,slideWindow(\@callable,$fai->{$contig},$Wsz,$Ssz));
	    }
	}
	@callable = ();
    }

    return \@interval;
}


sub hapShareCore {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $vcf = is_subclass(shift,'Tabix');
    my $out = is_arrayref(shift);
    my $faI = is_arrayref(shift);
    my $fai = is_hashref(shift);
    my $all = is_hashref(shift);
    my $mTh = is_ufloat(shift);
    my $req = ['FT'];


    report('Calculating rates over ',scalar(@$faI),' contig(s)');

    my %aidx;
    print($fdo join("\t",qw(CHROM START END NAME SCORE STRAND LOCI AA AB BB)),"\n");
    while (my $chr = shift(@$faI)) {
	if (! @{is_arrayref($chr)}) { next }
	for(my $i = 0; $i < @$chr; ++$i) {
	    my $I = is_arrayref($chr->[$i]);
	    if (!defined($I->[chrom]) || 
                !Valkyr::Data::Type::Test::is_uint($I->[start]) ||
                !Valkyr::Data::Type::Test::is_uint($I->[end])) {
                throw("Invalid interval: '@$I'");
            }
	    my $V = $vcf->query($I->[chrom],$I->[start],$I->[end]); # requires half-open interval coords

	    my @hapdose = (0,0,0);
	    if (Valkyr::Data::Type::Test::is_subclass($V,'TabixIterator') && 
		Valkyr::Data::Type::Test::is_instance($V->get(),'ti_iter_t')) {
		while (local $_ = $vcf->read($V)) {
		    if (/^#|^\s*$/) { next }
		    chomp();
		    
		    my @variant = split(/\t/);
		    my $locus   = join(':',@variant[CHROM,POS]);
		    
		    if ($all->{$locus}) {
			if (!exists($aidx{$locus})) {
			    $aidx{$locus} = 0;

			    my @A = ($variant[REF],split(/,/,$variant[ALT]));
			    for(my $a = 0; $a < @A; ++$a) {
				if ($A[$a] eq $all->{$locus}) {
				    $aidx{$locus} = as_sbitA($a);
				}
			    }
			}
		    } else {
			$aidx{$locus} = 0;
		    }
		    my  $allele = $aidx{$locus};
		    
		    if ($allele && $variant[FILTER] =~ PASS_FILTER_PAT) {
			my %F = %{scalar( getFORMAT($variant[FORMAT],$req) )};
			my @v = getSample($variant[FORMAT+1],\%F);
			
			if ($v[$F{'GT'}] eq NULL_GT || 
			    $v[$F{'FT'}] !~ PASS_FILTER_PAT) {
			    next;
			}
			my @g = split(GT_FIELD_SEP,$v[$F{'GT'}]);
			my @a = (as_sbitA($g[0]),as_sbitA($g[1]));
			my $g = $a[0] | $a[1];
			
			if ($allele & $g) {
			    if ($a[0] & $a[1]) { 
				$hapdose[2]++; # hom-var
			    } else { 
				$hapdose[1]++;
			    }
			} else {
			    $hapdose[0]++;
			}
		    }
		}
	    }

	    my $total = sum(@hapdose);
	    for(my $i = 0; $i < 3; ++$i) { 
		$hapdose[$i] = $total ? $hapdose[$i]/$total : 0;
	    }
	    my $hidx  = argmax(@hapdose);

	    # if (($hapdose[$hidx] / (max(@hapdose[(($hidx ^ 3) & 1),(($hidx ^ 3) & 2)])||$pseudo)) > $mTh) {
	    if ($hapdose[$hidx] < $mTh) {
		$hidx = 3; # undetermined
	    }

	    print($fdo join("\t",@{$I}[chrom,start,end],'.',0,'+',$total,@hapdose),"\n");
	    push(@$out, [@{$I}[chrom,start,end],$hidx]);

	    @hapdose = (0,0,0)
	}
    }

    return 1;
}


sub hapShareSumm {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $fds = is_subclass(shift,'IO::Handle');
    my $dat = is_arrayref(shift);
    my $ord = is_arrayref(shift);
    my $fai = is_hashref(shift);
    my $map = is_hashref(shift);

    report('Reducing haplotype information');

    my @HAPp = (0,0,0,0);
    my @HAPg = (0,0,0,0);

    my $CLASS;
    my $class;
    my $prevchr;
    my $prevend;
    my $prevstart;
    my $add_maplen = %$map ? 1 : 0;
    print($fdo join("\t",qw(FLD CHROM START END COLOR)),"\n");
    for(my $i = 0; $i < @$ord; ++$i) {
	if ($fai->{$ord->[$i]->{'contig'}}) {
	    print($fdo join("\t",'CHR',$ord->[$i]->{'contig'},0,$ord->[$i]->{'length'},'NA'),"\n");
	}
    }
    while (my $interval = shift(@$dat)) {

        $prevchr   //= is_string($interval->[chrom]);
        $prevstart //= is_uint($interval->[start]);
        $prevend   //= is_uint($interval->[end]);
        if ($prevchr ne is_string($interval->[chrom])) {
            print($fdo join("\t",'SEG',$prevchr,$prevstart,$prevend,$COLORS[$CLASS]),"\n");
            if ($add_maplen) {
                if (!$map->{$prevchr}
                    || !defined($map->{$prevchr}->{$prevend}) 
                    || !defined($map->{$prevchr}->{$prevstart})) {
                    throw(sprintf('No genetic information at locus: %s:%u-%u',$prevchr,$prevstart,$prevend));
                } else {
                    $HAPg[$CLASS] += max(0,$map->{$prevchr}->{$prevend} - $map->{$prevchr}->{$prevstart});
                }
            }
            $HAPp[$CLASS] += ($prevend-$prevstart);
            $prevstart = $interval->[start];
            $prevchr   = $interval->[chrom];
        }
	$CLASS //= is_uint($interval->[dose]);;

        if (is_uint($interval->[dose]) != $CLASS) {
            print($fdo join("\t",'SEG',$prevchr,$prevstart,$interval->[1],$COLORS[$CLASS]),"\n");
            $HAPp[$CLASS] += ($interval->[start]-$prevstart);
            $prevstart = $interval->[start];
            $CLASS     = $interval->[dose];
        }
        $prevchr = $interval->[chrom];
        $prevend = $interval->[end];
    }
    if (defined($CLASS)) {
        print($fdo join("\t",'SEG',$prevchr,$prevstart,$prevend,$COLORS[$CLASS]),"\n");
        if ($add_maplen) {
            if (!$map->{$prevchr}
                || !defined($map->{$prevchr}->{$prevend}) 
                || !defined($map->{$prevchr}->{$prevstart})) {
                throw(sprintf('No genetic information at locus: %s:%u-%u',$prevchr,$prevstart,$prevend));
            } else {
                $HAPg[$CLASS] += max(0,$map->{$prevchr}->{$prevend} - $map->{$prevchr}->{$prevstart});
            }
        }
        $HAPp[$CLASS] += ($prevend-$prevstart);
    }

    printf($fds "AA_p\tAB_p\tBB_p\tND_p\tAA_g\tAB_g\tBB_g\tND_g\n");
    if ($add_maplen) {
        printf($fds "%.05f\t%.05f\t%.05f\t%.05f\t%.05f\t%.05f\t%.05f\t%.05f\n",
               $HAPp[0]/(sum(@HAPp)||1),$HAPp[1]/(sum(@HAPp)||1),$HAPp[2]/(sum(@HAPp)||1),$HAPp[3]/(sum(@HAPp)||1),
               $HAPg[0]/(sum(@HAPg)||1),$HAPg[1]/(sum(@HAPg)||1),$HAPg[2]/(sum(@HAPg)||1),$HAPg[3]/(sum(@HAPg)||1));
    } else {
        printf($fds "%.05f\t%.05f\t%.05f\t%.05f\tNA\tNA\tNA\tNA\n",
               $HAPp[0]/(sum(@HAPp)||1),$HAPp[1]/(sum(@HAPp)||1),$HAPp[2]/(sum(@HAPp)||1),$HAPp[3]/(sum(@HAPp)||1));
    }

    return 1;
}


sub HELP_MESSAGE {
    die qq/
Program: $PROGRAM ($PURPOSE)
Version: $PACKAGE $VERSION
Contact: $CONTACT

Usage:   $PROGRAM [options] <in.vcf.gz> <in.intervals> <out.prefix>

Options: -L STR    Restrict analysis to contig STR (see Note 3)
         -R FILE   faidx-indexed reference sequence FILE (see Note 4)
         -t FLOAT  Min ratio of IBD_best:IBD_second [2]
         -w INT    Sliding window width [$DEFAULT_w]
         -s INT    Sliding window step-size [$DEFAULT_s]
         

Notes: 

  1. in.vcf.gz is a bgzip'ed and tabix-indexed VCF file for a single 
     individual of interest.

  2. in.intervals is a GATK interval file decorated with the (tab-separated)
     ref and alt alleles, and the allele index of the haplotype's allele. 

  3. The argument to the '-L' option may be a contig ID or .list file of
     contig IDs (one per line).

  4. Enabling the '-R' option is required when in.vcf.gz lacks the '##contig'
     metadata entries.

  5. Window width and step size values may contain human-readable numeric 
     suffixes [kMGT]


/;
}


main: {
    my %argv;
    my $step = expand($DEFAULT_s);
    my $width = expand($DEFAULT_w);
    my $minThr = 2;
    my $target;
    my $refmap;
    my $command = join(' ',basename($0),@ARGV);
    getopts('Xhw:s:L:t:R:M:',\%argv) && @ARGV == 3 || &HELP_MESSAGE;
    for my $o (keys(%argv)) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq 'w') { $width  = is_uint(expand($argv{$o}),\&HELP_MESSAGE) }
	elsif ($o eq 's') { $step   = is_uint(expand($argv{$o}),\&HELP_MESSAGE) }
	elsif ($o eq 't') { $minThr = is_ufloat($argv{$o},\&HELP_MESSAGE) }
	elsif ($o eq 'L') { $target = is_string($argv{$o},\&HELP_MESSAGE) }
	elsif ($o eq 'M') { $refmap = is_string($argv{$o},\&HELP_MESSAGE) }
    }
    if ($width < $step) {
	throw('Step size must be less than or equal to the window size');
    }
    if (is_file($ARGV[0],'f') !~ /\.gz$/ || ! -e $ARGV[0].'.tbi') {
	throw("VCF not bgzip'd and tabix indexed: $ARGV[0]");
    }
    $minThr = $minThr/(1+$minThr);
    

    report('Starting ',scalar(localtime));
    report('Command-line: ',$command);

    my $Rex = which('Rscript') || 
	throw('Rscript not found in PATH env variable.');

    my $fdi = open($ARGV[0],READ,NOSTREAM);

    my ($META,$HEADER) = getMeta($fdi);

    close($fdi);


    my $fai;
    my $ord;
    my $map;
    if (defined($refmap)) {
	$map = readMap($refmap);
    } else {
	$map = {};
    }
    if ($argv{'R'}) {
	$fai = readFaidx($argv{'R'});
	$ord = readFaidx($argv{'R'},1);

    } elsif ($META->{'contig'}) {
	$fai = $META->{'contig'};
	$ord = [];
        for my $contig (@{$fai->{'_ORDER_'}}) {
            if (!$fai->{$contig}->{'length'}) {
                throw("##contig '$contig' lacks length, must specify -R")
            }
            push(@$ord, $fai->{$contig});
        }	
    } else {
	throw("No contig Meta in VCF");
    }

    if (defined($target)) {
	if (Valkyr::Data::Type::Test::is_file($target,'fr')) {
	    $target = readOrderedList($target);
	    my %fai;
	    for(my $i = 0; $i < @$target; ++$i) {
		if (!exists($fai->{$target->[$i]})) {
		    throw('List-input contig does not exist: ',$target->[$i]);
		}
		$fai{$target->[$i]} = $fai->{$target->[$i]};
	    }
	    $fai = \%fai;

	} elsif (exists($fai->{$target})) {
	    $fai = { $target => $fai->{$target} };

	} else {
	    throw('User-input contig does not exist: ',$target);
	}
    }

    my $all = readAlleles($ARGV[1]);
    my $fdv = Tabix->new(-data => is_file($ARGV[0],'fr'));
    my $faI = calcIntervals($fdv,$ord,$fai,$all,$width,$step);
    my $fd1 = open("$ARGV[2].micro.hap",WRITE);
    my $fd2 = open("$ARGV[2].macro.hap",WRITE);
    my $fd3 = open("$ARGV[2].macro.hap.summary",WRITE);
    my @hap;

    hapShareCore($fd1,$fdv,\@hap,$faI,$fai,$all,$minThr);

    hapShareSumm($fd2,$fd3,\@hap,$ord,$fai,$map);

    $fdv->close();
    $fd1->close();
    $fd2->close();
    $fd3->close();

    hapSharePlot("$ARGV[2].micro.hap","$ARGV[2].macro.hap",$ARGV[2],$minThr);

    runRscript($Rex,"$ARGV[2].Rscript");

    report('Finished ',scalar(localtime));

    exit(0);
}
