
my $PURPOSE = 'Tools for working with SAM/BAM files';
my $PACKAGE = '__PACKAGE_NAME__';
my $VERSION = '__PACKAGE_VERSION__';
my $CONTACT = '__PACKAGE_CONTACT__';

use strict;
use warnings;

use File::Spec;
use File::Temp;
use File::Basename;
use EnvironmentModules;

use vars qw($PROGRAM $JARNAME %JAR);
BEGIN {
    $PROGRAM = basename($0);
    $JARNAME = '';
}

sub WRAPPER_VERSION {
    die "$PROGRAM $VERSION\n";
}

sub WRAPPER_USAGE {
    print STDERR qq/
Wrapper: $PROGRAM (Setup command-line for Picard)
Version: $VERSION
Contact: $CONTACT

Usage:   $PROGRAM [options] <$PROGRAM-command> [$PROGRAM-options]

Options: --module-version VSTR  Load VSTR version of Picard
         --memory INT           Set the max java heapsize [512M]
         --wrapper-help         See this help page
         --wrapper-version      See wrapper version
         --help                 See Picard help page
         --version              See Picard version

/;
    if (%JAR) {
	print STDERR 'Command: ';
	print STDERR join("\n         ",sort(keys(%JAR)));
	print STDERR "\n\n\n";
    }
    exit(1);
}


sub which {
    my $executable = shift //
        die("Must pass an executable name to search PATH\n");
    
    $executable .= $^O =~ /mswin/i ? '.exe' : '';
    
    my $exepath;
    for my $path (File::Spec->path()) {
        my $path = File::Spec->catfile($path,$executable);
        if (-x $path) { $exepath = $path; last }
    }   
    $exepath // die("Must add $executable to your PATH\n");
    
    return $exepath;
}


sub extractOptionValues {
    my $options = shift;
    my %values;
    for my $flag (keys %$options) {
	for (my $i = 0; $i < @ARGV; ++$i) {
	    if ($ARGV[$i] =~ /$flag/) {
		if ($options->{$flag}) {
		    $values{$flag} = $ARGV[$i+1];
		    splice(@ARGV,$i,2);
		    last;
		} else {
		    $values{$flag} = 1;
		    splice(@ARGV,$i,1);
		}
	    }
	}
    }
    
    return \%values;
}


sub buildCommand {
    my $java    = shift;
    my $jdir    = shift;
    my $tmpdir  = File::Temp->newdir(File::Spec->catfile(File::Spec->tmpdir(),"Picard-$$-XXXXXXXXXXXXXXXXX"), 'CLEANUP' => 1);
    my $memory  = '512M';
    my $command = "$java -Djava.io.tmpdir=$tmpdir";

    # we must extract memory option, as it is passed into the wrapper,
    # but may be mixed in among GATK's args:
    my $opt = extractOptionValues({
	'memory'          => 1,
	'wrapper-help'    => 0,
	'wrapper-version' => 0,
    });
    if ($opt->{'wrapper-help'}) {
	WRAPPER_USAGE;
    } elsif ($opt->{'wrapper-version'}) {
	WRAPPER_VERSION;
    } elsif (defined($opt->{'memory'})) {
	$memory = $opt->{'memory'};
    }
    # allow samtools-like command-line interface, tool (passed via -T)
    # must be passed as first (non-wrapper) argument:

    opendir(JARDIR,$jdir) || die "[$PROGRAM] Cannot open $jdir for reading: $!\n.";
    while (local $_ = readdir(JARDIR)) {
	s/\.jar$// || next;
	if (/^picard-([1-9]+(\.[0-9]+)*)/i) {
	    $VERSION = $1;
	    next;
	}
	if (!/^\w+$/) { next }
	$JAR{$_} = 1;
    }
    closedir(JARDIR);

    if (!defined($ARGV[0]) || !$JAR{$ARGV[0]}) {
	WRAPPER_USAGE;
    }
    $JARNAME  = File::Spec->catfile($jdir,shift(@ARGV));
    $command .= ' -Xmx'.$memory if $memory;
    $command .= join(' ','','-jar',"$JARNAME.jar",@ARGV,'TMP_DIR='.$tmpdir);
    # TMP_DIR put at the end j.i.c. user wants to over-ride TMP_DIR

    return $command;
}


sub main {
    my $opt = extractOptionValues({'module-version'=>1});

    module('load oracle-jdk');
    module('load picard'.(defined($opt->{'module-version'}) ? '/'.$opt->{'module-version'} : ''));

    # if 'which' can find it, it's executable
    my $java = which('java');
    my $pdir = dirname(which('picard'));
    if (! defined($java)) {
	die "[$PROGRAM] Cannot load Java from env.\n";
    }
    
    if (! defined($pdir)) {
	die "[$PROGRAM] Cannot load Picard from env.\n";
    }

    

    my $jdir = File::Spec->catfile($pdir,'tools','');
    if (! -d $jdir) { 
	die "[$PROGRAM] jar-dir does not exist: $jdir\n";
    }


    system( buildCommand($java,$jdir) );
    
    return($?);
}

exit main();
