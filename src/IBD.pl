
$PACKAGE = '__PACKAGE_NAME__';
$VERSION = '__PACKAGE_VERSION__';
$CONTACT = '__PACKAGE_CONTACT__';
$PURPOSE = 'Estimate segmental identity by descent';

use strict;
use warnings;

use Getopt::Std;

use Tabix;
use IO::Handle;
use File::Which;
use File::Basename;
use Valkyr::Error qw(throw report);
use Valkyr::Error::Exception {
    'RuntimeError' => ['MalformedVCF']
};
use Valkyr::Math::Utils qw(max min sum);
use Valkyr::Math::Units::HumanReadable 'expand';
use Valkyr::FileUtil::IO;
use Valkyr::FileUtil::VCF ':all';
use Valkyr::FileUtil::Fasta 'readFaidx';
use Valkyr::FileUtil::Counter 
    qw(progressReport progressCounter configCounter);
use Valkyr::Data::Type::Test;
use Valkyr::Data::Type::Assert 
    qw(is_uint is_ufloat is_sfloat is_string is_hashref is_arrayref is_subclass is_file);

use vars qw($PROGRAM $PACKAGE $PURPOSE $VERSION $CONTACT $DEFAULT_w $DEFAULT_s $IBD1THRESH $IBD2THRESH $NULL_LINE_PAT);

BEGIN {
    $PROGRAM = basename($0);
    $DEFAULT_w = '10k';
    $DEFAULT_s = '1k';
    $IBD1THRESH = 0.99;
    $IBD2THRESH = -1;
    $NULL_LINE_PAT = qr/^#|^\s*$/;
}

#### CONSTANTS ####
# BED file
sub chrom  () { 0 }
sub start  () { 1 }
sub end    () { 2 }
sub name   () { 3 }
sub score  () { 4 }
sub strand () { 5 }
# MAP file
sub CHR    () { 0 }
sub MRK    () { 1 }
sub GPOS   () { 2 }
sub PPOS   () { 3 }


sub bitcount {
    my $array = is_uint(shift); 
    my $count = 0; 
    while ($array) { 
	$array &= ($array - 1);
	$count++;  
    }
    return $count; 
}


sub as_dbitA {
    my $allele = is_string(shift);
    my $chridx = is_uint(shift);
    
    return $allele eq NULL_ALLELE ? 0 : (4 ** is_uint($allele) << $chridx);
}


sub as_sbitA {
    my $allele = is_string(shift);

    return $allele eq NULL_ALLELE ? 0 : (1 << is_uint($allele));
}


sub log_factorial {
    my $n = is_uint(shift);
    my $f = 0;
    if ($n <= 0) {
	return 0;  # log(1)
    }
    map{ $f += log($n) } 1..$n;

    return $f;
}


sub log_choose {
    my $n = is_uint(shift);
    my $k = is_uint(shift);

    return log_factorial($n) - log_factorial($k) - log_factorial($n - $k);
}


sub getNextEntry {
    my $fh = is_subclass(shift,'IO::Handle');
    my $N  = is_uint(shift//1);

    while ( my $line = $fh->getline() ) {
        $line =~ $NULL_LINE_PAT && next;
        chomp($line);
        
        my @fields = split(/\t/,$line);

        # do some basic entry checking for required fields
        if (@fields < $N) {
            throw("MAP format required field (=$N) missing.");
        }

        return(\@fields);
    }
}


sub nextLocus {
    my $fh = is_subclass(shift,'IO::Handle');
    my $w  = is_uint(shift);
    
    my $locus = getNextEntry($fh,$w) || return;

    if (!Valkyr::Data::Type::Test::is_string($locus->[CHR])) {
        throw("Invalid Chr value: $locus->[CHR]");
    }
    if (!Valkyr::Data::Type::Test::is_string($locus->[ID])) {
        throw("Invalid Marker value: $locus->[ID]");
    }
    if (!Valkyr::Data::Type::Test::is_ufloat($locus->[GPOS])) {
        throw("Invalid genetic position value: $locus->[GPOS]");
    }
    if (defined($locus->[PPOS]) && 
        !Valkyr::Data::Type::Test::is_uint($locus->[PPOS])) {
        throw("Invalid physical position value: $locus->[PPOS]");
    }

    return $locus;
}


sub readMap {
    my $file = is_file(shift,'fr');

    my %map;
    my $fh = open($file,READ);
    while (my $locus = nextLocus($fh,4)) {
	$map{$locus->[CHR]}->{$locus->[PPOS]} = $locus->[GPOS];
    }

    return \%map;
}


sub readBED {
    my $file  = is_file(shift,'f');

    my $prev_chr = '';
    my $prev_pos = -1;

    my @data;
    my $list = open($file,READ);
    while (local $_ = $list->getline()) {
        m/^#|^\s*$/ && next;
        chomp();

        my @bed = split(/\t/);

        if ($bed[0] eq $prev_chr && $bed[1] < $prev_pos) {
            throw("Input BED must be reference-order sorted: $file.");
        }
        push(@data,
             {
                 'CHROM'  => is_string($bed[0]),
                 'START'  => is_uint($bed[1]),
                 'END'    => is_uint($bed[2]),
                 'NAME'   => is_string($bed[3]//$bed[0]),
                 'SCORE'  => is_sfloat($bed[4]||0),
                 'STRAND' => is_string($bed[5]||'+')
             });
        $prev_chr = $bed[0];
        $prev_pos = $bed[1];
    }
    close($list);
    
    return \@data;
}


sub readVCF {
    my $file = is_file(shift,'f');

    local $_ = '';
    my $prev_chr = '';
    my $prev_pos = -1;

    my @data;
    my $list = open($file,READ);
    while ($_ = $list->getline()) {
        if(/^\S+/) { last }
    }
    if (!/^##fileformat=VCF/) {
        throw("Missing VCF header or not a VCF file: $file");
    }
    while ($_ = $list->getline()) {
        m/^#|^\s*$/ && next;
        chomp();

        my @vcf = split(/\t/);

        if ($vcf[0] eq $prev_chr && $vcf[1] < $prev_pos) {
            throw("Input VCF must be reference-order sorted: $file.");
        }

        push(@data,
             {
                 'CHROM'  => is_string($vcf[0]), 
                 'START'  => is_uint($vcf[1])-1, # POS
                 'END'    => is_uint($vcf[1]),   # POS
                 'NAME'   =>(is_string($vcf[2]) eq NULL_FIELD ? "$vcf[0]:$vcf[1]" : $vcf[2]),
                 'SCORE'  => is_uint($vcf[5]),   # QUAL
                 'STRAND' => '+'                 # always
             });

        $prev_chr = $vcf[0];
        $prev_pos = $vcf[1];
    }
    close($list);
    
    return \@data;
}
	

sub readIntervals {
    my $file = is_file(shift,'f');

    my $prev_chr = '';
    my $prev_pos = -1;

    my @data;
    my $list = open($file,READ);
    while (local $_ = $list->getline()) {
        m/^#|^\s*$/ && next;
        chomp();
        
        if (/^([^:\-]+):(\d+)(?:\-(\d+))?/) { # CHROM:START[-END]
            if ($1 eq $prev_chr && $2 < $prev_pos) {
                throw("Input intervals must be reference-order sorted: $file.");
            }
            push(@data,
                 {
                     'CHROM'  => $1,
                     'START'  => $2-1,
                     'END'    =>($3||$2),
                     'NAME'   => $_,
                     'SCORE'  => 0,
                     'STRAND' => '+'
		 });

            $prev_chr = $1;
            $prev_pos = $2;

        } else {
            throw("Invalid intervals format: [$_]");
        }
    }
    close($list);
    
    return \@data;
}   


sub readRNG {
    my $file = is_file(shift,'f');
    
    if ($file =~ /vcf(\.gz)?$/) {
        report('Reading VCF file');
        return readVCF($file);

    } elsif ($file =~ /bed(\.gz)?$/) {
        report('Reading BED file');
        return readBED($file);

    } elsif ($file =~ /intervals$/) { 
        report('Reading Intervals file');
        return readIntervals($file);

    } else {
        throw("Unsupported interval format: $file");
    }
}


sub calcVariableEndPos {
    my $bed = is_arrayref(shift);
    my $off = is_uint(shift);
    my $len = is_uint(shift);
    
    my $sum = 0;
    my $end = $bed->[0]->[start]+1;
    for(my $i = 0; $i < @$bed; ++$i) {
	my $call  = $bed->[$i];
	my $start = max($off,$call->[start]);
	
	my $seglen = $call->[end] - $start;
	if ($sum+$seglen < $len) {
	    # |-----------|
	    # ==============//=======
	    $end  = $call->[end];
	    $sum += $seglen;
	} else {
	    # |---------||-----------------)
	    # ==============//=======
	    $end  = $call->[end] - ($sum + $seglen - $len); 
	    return $end;
	}
    }
    
    return $end;
}


sub calcFeatureCountEndPos {
    my $bed = is_arrayref(shift);
    my $pos = is_uint(shift);
    my $Wsz = is_uint(shift);
    
    if ($bed->[$pos+$Wsz-1]) {
	return $bed->[$pos+$Wsz-1]->[end];
    } else {
	return $bed->[$#{$bed}]->[end];
    }
}


sub slideWindow {
    my $bed = is_arrayref(shift);
    my $fai = is_hashref(shift);
    my $Wsz = is_uint(shift);
    my $Ssz = is_uint(shift);
    my $opt = is_uint(shift);
    
    my $chr = is_string($fai->{'contig'});
    my $max = is_uint($fai->{'length'});
    
    my @intervals;
    if ($opt & 0x1) {
	my $num = 0;
	my $dat = @$bed;
	my $end = max(0,$dat-$Wsz);
	for(my $pos = 0; $pos <= $end; ++$pos) {
	    if (!($num % $Ssz)) {
		push(@intervals,[$chr, $bed->[$pos]->[start], calcFeatureCountEndPos($bed,$pos,$Wsz)]);
	    }
	    $num++;
	}
	
    } else {
	my $dat = @$bed;
	my $end = max(1,$max-$Wsz);
	for(my $pos = 0; $pos <= $end; $pos += $Ssz) {
	    push(@intervals,[$chr,$pos,min($pos+$Wsz,$max)]);
	}
    }
    
    return \@intervals;
}


sub calcIntervals {
    my $vcf = is_subclass(shift,'Tabix');
    my $ord = is_arrayref(shift);
    my $fai = is_hashref(shift);
    my $Wsz = is_uint(shift);
    my $Ssz = is_uint(shift);
    my $opt = is_uint(shift);
    
    report('Calculating intervals');
    
    my @callable;
    my @interval;
    for my $Contig (@$ord) {
	my $contig = $Contig->{'contig'};
	
	if (!exists($fai->{$contig})) { next }
	if (!Valkyr::Data::Type::Test::is_uint($fai->{$contig}->{'length'})) {
	    throw("Invalid contig length: '$contig'")
	}
	my $interval = $vcf->query($contig,0,$fai->{$contig}->{'length'});
	
	if (Valkyr::Data::Type::Test::is_subclass($interval,'TabixIterator') &&
	    Valkyr::Data::Type::Test::is_instance($interval->get(),'ti_iter_t')) {

	    my $prevpos = 0;	    
	    while (my $line = $vcf->read($interval)) {
		if($line =~ /^#|^\s*$/) { next }
		chomp($line);
		
		my @variant = split(/\t/,$line);	
		
		$prevpos //= $variant[POS];
		if ($variant[POS] < $prevpos) {
		    throw MalformedVCF(sprintf('Unsorted VCF (%s !< %s)',$prevpos,$variant[POS]));
		}
		push(@callable,[ $variant[CHROM], $variant[POS]-1, $variant[POS] ]);
		
		$prevpos = $variant[POS];
	    }
	    if (@callable) {
		push(@interval,slideWindow(\@callable,$fai->{$contig},$Wsz,$Ssz,$opt));
	    }
	}
	@callable = ();
    }
    
    return \@interval;
}


sub ibdCore {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $vcf = is_subclass(shift,'Tabix');
    my $cal = is_arrayref(shift);
    my $hdr = is_arrayref(shift);
    my $met = is_hashref(shift);
    my $dat = is_arrayref(shift);
    my $fai = is_hashref(shift);
    my $in1 = is_string(shift);
    my $in2 = is_string(shift);
    my $opt = is_uint(shift);
    my $req = ['FT'];
    
    my @indv = ($in1,$in2);
        
    my %FIELD = %{scalar( indexArray(@$hdr) )};
    my $width = @$hdr;
    my $nGeno = max(0,$width - (FORMAT+1));

    if (($nGeno < 2) && ($indv[0] ne $indv[1])) {
	throw("Two samples required in VCF.");
    }

    for(my $j = 0; $j < 2; ++$j) {
	if (!defined($FIELD{$indv[$j]}) || ($FIELD{$indv[$j]} <= FORMAT)) {
	    throw("Invalid sample name: $indv[$j]");
	}
    }
    
    report('Calculating IBS over ',scalar(@$cal),' contig(s)');
    configCounter({'prefix' => 'Intervals processed: ', 'granularity' => 100});

    my @het = (0,0);
    my @hom = (0,0);
    my $nIBS0 = 0;
    my $nIBS1 = 0;
    my $nIBS2 = 0;
    my $nIBS3 = 0;
    my $nLoci = 0;
    print($fdo join("\t",'CHROM','START','END','Nhet1','Nhet2','Nhom1','Nhom2',
		    'IBS0','IBS1','IBS2star','IBS2','fIBS0','fIBS1','fIBS2','PhiHat'),"\n");
    while (my $chr = shift(@$cal)) {
	if (! @{is_arrayref($chr)}) { next }
	
	for(my $i = 0; $i < @$chr; ++$i) {
	    progressCounter();
	    my $I = is_arrayref($chr->[$i]);
	    if (!defined($I->[chrom]) || 
                !Valkyr::Data::Type::Test::is_uint($I->[start]) ||
                !Valkyr::Data::Type::Test::is_uint($I->[end])) {
                throw("Invalid interval: '@$I'");
            }
	    my $V = $vcf->query(@{$I}[chrom,start,end]); # requires half-open interval coords
	    
	    if (Valkyr::Data::Type::Test::is_subclass($V,'TabixIterator') && 
		Valkyr::Data::Type::Test::is_instance($V->get(),'ti_iter_t')) {
		
		while (local $_ = $vcf->read($V)) {
		    if (/^#|^\s*$/) { next }
		    chomp();
		    
		    my @variant = split(/\t/);
		    
		    if ($variant[ALT] =~ /,/) { next } # multi-allelic
		    if ($variant[ALT] eq NULL_FIELD) { next }
		    if ($variant[FILTER] !~ PASS_FILTER_PAT) { next }
		    

		    # using the nomenclature defined in:
		    # Speed et al. 2014. Relatedness in the post-genomics era
                    # Kas(B,C) = 0.5 + 0.5 * 1/(2m) * sum((S[B]-1)(S[C]-1))
		    my @g = (0,0);
		    my @h = (0,0);
		    my $FORMAT = getFORMAT($variant[FORMAT],$req);
		    for(my $j  = 0; $j < 2; ++$j) {
			my @s = getSample($variant[$FIELD{$indv[$j]}],$FORMAT);
			if ($s[$FORMAT->{'FT'}] =~ PASS_FILTER_PAT &&
			    $s[$FORMAT->{'GT'}] =~ GT_GROUP_PAT) {
			    my $a1 = as_sbitA($1);
			    my $a2 = as_sbitA($2);
			    if ($a1 && $a2) {
				$g[$j] = $a1  | $a2;
				$h[$j] = $a1 != $a2;
			    }
			}
		    }
		    if (!($g[0] && $g[1])) { next }

		    my $u = bitcount($g[0] | $g[1]);
		    my $n = bitcount($g[0] & $g[1]);
		    $nLoci++;
		    
		    if ($u == 1) {
			$nIBS2++;
			$hom[0]++;
			$hom[1]++;
		    } else {
			if ($n ==  0) { $nIBS0++ }
			if ($n ==  1) { $nIBS1++ }
			if ($n == $u) {
			    if ($opt & 0x1) {
				# IBD2* disallowed
				$nIBS1++;
			    } else {
				$nIBS3++
			    }
			}
			$hom[0] += !$h[0];
			$hom[1] += !$h[1];
			$het[0] +=  $h[0];
			$het[1] +=  $h[1];
		    }
		}
		my $nhet = sum(@het);
		my $nhom = sum(@hom);

		# $nIBS1 is a penalty in the numerator, but an adjustment for over-counting in the
		# denominator:
		# my $fIBD0 = max(0, 2 * (    $nIBS0    ) / max(1, $nhet + $nhom - $nIBS1));
		# my $fIBD2 = max(0, 2 * ($nIBS2 + $nIBS3 - $nIBS0) / max(1, $nhet + $nhom - $nIBS1));		
		# my $fIBD1 = max(0, 1 - $fIBD0);

		# Use a decision-tree model for partitioning genome into IBD classes:
		my $fIBD0 = $nIBS0 / max(1, $nIBS0 + $nIBS2 + $nIBS3);
		my $fIBDx = 1 - $fIBD0;
		my $fIBD1 = $fIBDx;
		my $fIBD2 = 0;
		
		if ($fIBDx >= $IBD1THRESH) {
		    # IBD2 is the special case where, given diploid genotype calls 0/0, 0/1, 
		    # or 1/1 sampled from two individuals within a window of user-specified
		    # size, calls between those two individuals will be of the same genotype
		    # across all loci in the window:
		    my $fIBS2 = 2 * ($nIBS2 + $nIBS3) / max(1, ($nhet + $nhom));

		    if ($fIBS2 >= $IBD2THRESH) {
			$fIBD2 = $fIBS2;
			$fIBD1 = 1 - $fIBD2;
		    }
		}
		if (! $nLoci) {
		    $fIBD0 = -1;
		    $fIBD1 = -1;
		    $fIBD2 = -1;
		}

		# Manichaikul et al. 2010. doi:10.1093/bioinformatics/btq559, Equation 11
		# multiplied by 2 to scale range to [0, 1]:
		my $phi = 2 * max(0, ($nIBS3 - 2*$nIBS0) / max(1, 2*min($het[0],$het[1])) + 0.5 - 0.25*(($het[0] + $het[1]) / max(1, min($het[0],$het[1]))));
		
		printf($fdo "%s\t%u\t%u\t%u\t%u\t%u\t%u\t%u\t%u\t%u\t%u\t%.05f\t%.05f\t%.05f\t%.05f\n",
		    @{$I}[chrom,start,end], $het[0], $het[1], $hom[0], $hom[1], 
		       $nIBS0, $nIBS1, $nIBS3, $nIBS2, $fIBD0, $fIBD1, $fIBD2, $phi
		);

		push(@$dat, [
		    @{$I}[chrom,start,end], $het[0], $het[1], $hom[0], $hom[1], 
		       $nIBS0, $nIBS1, $nIBS3, $nIBS2, $fIBD0, $fIBD1, $fIBD2
		]);
		$het[0] = 0;
		$het[1] = 0;
		$hom[0] = 0;
		$hom[1] = 0;
		$nIBS0 = 0;
		$nIBS1 = 0;
		$nIBS2 = 0;
		$nIBS3 = 0;
		$nLoci = 0;
	    }
	}
    }
    progressReport();    

    return 1;
}


sub ibdStats {
    my $fdo = is_subclass(shift,'IO::Handle');
    my $fds = is_subclass(shift,'IO::Handle');
    my $dat = is_arrayref(shift);
    my $map = is_hashref(shift);
    my $in1 = is_string(shift);
    my $in2 = is_string(shift);
    my $ev0 = is_uint(shift // 0);

    report('Calculating IBD from IBS');

    my @IBDp = (0,0,0);
    my @IBDg = (0,0,0);

    my $CLASS;
    my $class;
    my $prevchr;
    my $prevend;
    my $prevbeg;
    my $currbeg;
    my $currend;
    my $add_maplen = %$map ? 1 : 0;
    print($fdo join("\t",qw(CHROM START END IBD)),"\n");
    while (my $interval = shift(@$dat)) {
	$prevchr //= is_string($interval->[0]);
	$prevbeg //= is_uint($interval->[1]);
	$prevend //= is_uint($interval->[2]);
	
	if (($prevchr && $prevend) &&
	    ($prevchr ne is_string($interval->[0]))) {
	    $currbeg = $prevbeg + 1;
	    $currend = $prevend;
	    print($fdo join("\t", $prevchr, $prevbeg, $prevend, $CLASS),"\n");
	    if ($add_maplen) {
		if ((! $map->{$prevchr})
		    || (! defined($map->{$prevchr}->{$currbeg}))
		    || (! defined($map->{$prevchr}->{$currend}))) { 
		    throw(sprintf('No genetic information for locus: %s:%u-%u',$prevchr,$currbeg,$currend));
		} else {
		    $IBDg[$CLASS] += max(0, $map->{$prevchr}->{$currend} - $map->{$prevchr}->{$currbeg});
		}
	    }
	    $IBDp[$CLASS] += ($prevend-$prevbeg);
	    $prevchr = $interval->[0];
	    $prevbeg = $interval->[1];
	    $prevend = $interval->[2];
	}

	if (is_sfloat($interval->[13]) >= $IBD2THRESH) {
	    $class = 2;
	} elsif (is_sfloat($interval->[12]) >= $IBD1THRESH) {
	    $class = 1;
	} elsif ($ev0) {
	    if (is_sfloat($interval->[11]) >= $IBD1THRESH) {
		$class = 0;
	    } elsif (is_sfloat($interval->[12]) < 0) {
		next;
	    } else {
		$class = -1;
	    }
	} else {
	    $class = 0;
	}
	$CLASS //= $class;

	if (defined($CLASS) && ($class != $CLASS)) {
	    $currbeg = $prevbeg + 1;
	    $currend = $interval->[1] + 1;
	    print($fdo join("\t",$prevchr,$prevbeg,$currend,$CLASS),"\n");
	    if ($add_maplen) {
		if ((! $map->{$prevchr})
		    || (! defined($map->{$prevchr}->{$currbeg}))
		    || (! defined($map->{$prevchr}->{$currend}))) {
		    throw(sprintf('No genetic information for locus: %s:%u-%u',$prevchr,$currbeg,$currend));
		} else {
		    $IBDg[$CLASS] += max(0, $map->{$prevchr}->{$currend} - $map->{$prevchr}->{$currbeg});
		}
	    }
	    $IBDp[$CLASS] += ($interval->[1]-$prevbeg);
	    $prevbeg = $interval->[1];
	    $CLASS = $class;
	}
	$prevchr = $interval->[0];
	$prevend = $interval->[2];
    }
    
    if (defined($CLASS)) {
	$currbeg = $prevbeg + 1;
	$currend = $prevend;
	print($fdo join("\t",$prevchr,$prevbeg,$prevend,$CLASS),"\n");
	if ($add_maplen) {
	    if ((! $map->{$prevchr})
		|| (! defined($map->{$prevchr}->{$currend})) 
		|| (! defined($map->{$prevchr}->{$currbeg}))) {
		throw(sprintf('No genetic information for locus: %s:%u-%u',$prevchr,$prevbeg,$prevend));
	    } else {
		$IBDg[$CLASS] += max(0, $map->{$prevchr}->{$currend} - $map->{$prevchr}->{$currbeg});
	    }
	}
	$IBDp[$CLASS] += ($prevend-$prevbeg);
    }

    if ($add_maplen) {
	printf($fds "INDV1\tINDV2\tIBD0\tIBD1\tIBD2\tIBD0g\tIBD1g\tIBD2g\tPI_HAT\n");
	printf($fds "%s\t%s\t%.05f\t%.05f\t%.05f\t%.05f\t%.05f\t%.05f\t%.05f\n",$in1,$in2,
	       $IBDp[0]/(sum(@IBDp)||1),$IBDp[1]/(sum(@IBDp)||1),$IBDp[2]/(sum(@IBDp)||1),
	       $IBDg[0]/(sum(@IBDg)||1),$IBDg[1]/(sum(@IBDg)||1),$IBDg[2]/(sum(@IBDg)||1),
	       0.5 * $IBDg[1]/(sum(@IBDg)||1) + $IBDg[2]/(sum(@IBDg)||1)
	    );
    } else {
	printf($fds "INDV1\tINDV2\tIBD0\tIBD1\tIBD2\tPI_HAT\n");
	printf($fds "%s\t%s\t%.05f\t%.05f\t%.05f\t%.05f\n",$in1,$in2,
	       $IBDp[0]/(sum(@IBDp)||1),$IBDp[1]/(sum(@IBDp)||1),$IBDp[2]/(sum(@IBDp)||1),
	       0.5 * $IBDp[1]/(sum(@IBDp)||1) + $IBDp[2]/(sum(@IBDp)||1)
	    );
    }

    return 1;
}


sub ibdWriteRscript {
    my $ibs = is_file(shift,'frs');
    my $ibd = is_file(shift,'frs');
    my $pfx = is_string(shift);


    my $R = open("$pfx.Rscript",WRITE);

    print($R qq/#!\/usr\/bin\/env Rscript

ibs1.threshold = $IBD1THRESH;
ibs2.threshold = $IBD2THRESH;
ibs0.color = '#0046E0';
ibs1.color = '#009ACD';
ibs2.color = '#FFC125';
none.color = '#CCCCD6';
phi.color = 'black';

IBS = read.table("$ibs", header=T, stringsAsFactors=F);
IBD = read.table("$ibd", header=T, stringsAsFactors=F);

IBD\$START = IBD\$START \/ 1e6;
IBD\$END   = IBD\$END   \/ 1e6;
IBS\$START = IBS\$START \/ 1e6;
IBS\$END   = IBS\$END   \/ 1e6;

num.chrs = length(unique(IBD\$CHROM));
num.cols = 1;
if (num.chrs > 1) {
    num.cols = min(3,num.chrs);
}
num.rows = ceiling(num.chrs \/ num.cols);

pdf("$ibd.pdf", height=4*num.rows, width=7*num.cols);
par(mfrow=c(num.rows,num.cols));

for (chr in unique(IBD\$CHROM)) { 
    ibd = IBD[which(IBD\$CHROM == chr),];
    ibs = IBS[which(IBS\$CHROM == chr),];
    ibs = rbind(ibs, ibs[nrow(ibs),]);

    # Exend the line to the end of the window
    ibs\$START[nrow(ibs)] = ibs\$END[nrow(ibs)]
	  
    plot(ibs\$START, ibs\$PhiHat, type='l', lwd=2, col=phi.color, bty='n', 
         yaxp=c(0,1,5), las=1, xlim=c(0,max(ibs\$END)), ylim=c(0,1.2), 
         main=chr, xlab='Genomic position (Mbp)', ylab='IBS');
    lines(ibs\$START, ibs\$fIBS0, lwd=2, col=ibs0.color);
    lines(ibs\$START, ibs\$fIBS1, lwd=2, col=ibs1.color);
    lines(ibs\$START, ibs\$fIBS2, lwd=2, col=ibs2.color);
    abline(h=ibs1.threshold, lty=2);
    abline(h=ibs2.threshold, lty=3);

    for (block in 1:nrow(ibd)) {
        color=none.color;
        if (isTRUE(ibd\$IBD[block] == 2)) {
            color=ibs2.color;
        } else if (isTRUE(ibd\$IBD[block] == 1)) {
            color=ibs1.color;
        } else if (isTRUE(ibd\$IBD[block] == 0)) {
            color=ibs0.color;
        }
        segments(ibd\$START[block], 1.12, ibd\$END[block], 1.12, col=color, lwd=28, lend=1);
    }
}
invisible(dev.off());

/);
    close($R);

    
}


sub runRscript {
    my $Rinterp = is_file(shift,'x');
    my $Rscript = is_file(shift,'r');
    my $cleanup = is_uint(shift||0);

    report("Rscript command: $Rinterp $Rscript");

    my $status = `$Rinterp $Rscript 2>&1`;

    if ($Valkyr::Error::DEBUG) {
        map {debug($_)} split("\n",$status);
    }
    if ($cleanup) {
        unlink($Rscript);
    }

    return 1;
}


sub HELP_MESSAGE {
    die qq/
Program: $PROGRAM ($PURPOSE)
Version: $PACKAGE $VERSION
Contact: $CONTACT

Usage: $PROGRAM [options] -1 <indv1> -2 <indv2> <in.vcf.gz> <out.prefix>

Options: -0        Require positive evidence of IBD0 [false]
         -1 STR    First individual in the comparison (required)
         -2 STR    Second individual in the comparison (required)
         -L STR    Examine a single chrom\/contig [null]
         -i FLOAT  Min. IBS threshold to call IBD1 segement [$IBD1THRESH]
         -I FLOAT  Min. IBS threshold to call IBD2 segement [$IBD2THRESH]
         -M FILE   Specify a linkage map for augmented IBD est.
         -R FILE   faidx-indexed reference sequence FILE
         -w INT    Sliding window width [$DEFAULT_w]
         -s INT    Sliding window step-size [$DEFAULT_s]
         -S        Calc strict IBD2 (disallow IBD2*) [false]

Notes: 

  1. in.vcf.gz is a bgzip'ed and tabix indexed multi-sample VCF file.

  2. Window width and step sizes may contain human-readable numeric 
     suffixes [kMGT]

  3. Interspecific variant loci should be removed prior to analysis. 


/;
}


main: {
    my %argv;
    my $indv1;
    my $indv2;
    my @targets;  # Actually, Getopt::Std doesn't allow multiple
    my $refmap;
    my $evid0 = 0;
    my $options = 0;
    my $step = expand($DEFAULT_s);
    my $width = expand($DEFAULT_w);
    my $command = join(' ',basename($0),@ARGV);
    getopts('01:2:Xhw:Ss:L:R:M:i:I:',\%argv) && @ARGV == 2 || &HELP_MESSAGE;
    for my $o (keys(%argv)) {
	if    ($o eq 'X') { $Valkyr::Error::DEBUG = 1 }
	elsif ($o eq '0') { $evid0  = 1 }
	elsif ($o eq 'i') { $IBD1THRESH = is_ufloat($argv{$o},\&HELP_MESSAGE) }
	elsif ($o eq 'I') { $IBD2THRESH = is_ufloat($argv{$o},\&HELP_MESSAGE) }
	elsif ($o eq '1') { $indv1  = is_string($argv{$o},\&HELP_MESSAGE) }
	elsif ($o eq '2') { $indv2  = is_string($argv{$o},\&HELP_MESSAGE) }
	elsif ($o eq 'w') { $width  = is_uint(expand($argv{$o}),\&HELP_MESSAGE) }
	elsif ($o eq 's') { $step   = is_uint(expand($argv{$o}),\&HELP_MESSAGE) }
	elsif ($o eq 'S') { $options |= 0x1 }
	elsif ($o eq 'L') { push(@targets, is_string($argv{$o},\&HELP_MESSAGE)) }
	elsif ($o eq 'M') { $refmap = is_string($argv{$o},\&HELP_MESSAGE) }
    }
    if ($width < $step) {
	throw('Step size must be less than or equal to the window size');
    }
    if (is_file($ARGV[0],'f') !~ /\.gz$/ || ! -e $ARGV[0].'.tbi') {
	throw("VCF not bgzip'd and tabix indexed: $ARGV[0]");
    }
    if (!defined($indv1) || !defined($indv2)) {
	throw("Must specify two sample names");
    }
    if ($IBD1THRESH > 1.0) {
	$IBD1THRESH /= 100.0;
    }
    if ($IBD2THRESH < 0) {
	$IBD2THRESH = $IBD1THRESH;
    }
    if ($IBD2THRESH > 1.0) {
	$IBD2THRESH /= 100.0;
    }
    
    report('Starting ',scalar(localtime));
    report('Command-line: ',$command);
    
    my $Rex = which('Rscript') || 
	throw('Rscript not found in PATH env variable.');
    
    my $fdi = open($ARGV[0],READ,NOSTREAM);

    my ($META,$HEADER) = getMeta($fdi);

    close($fdi);

    my $map = {};
    my $fai = {};
    my $ord = [];
    if (defined($refmap)) {
	$map = readMap($refmap);
    }
    if ($argv{'R'}) {
	$fai = readFaidx($argv{'R'});
	$ord = readFaidx($argv{'R'},1);

    } elsif ($META->{'contig'}) {
	$fai = $META->{'contig'};
	for my $contig (@{$fai->{'_ORDER_'}}) {
	    if (!$fai->{$contig}->{'length'}) {
		throw("##contig '$contig' lacks length, must specify -R")
	    }
	    $fai->{$contig}->{'contig'} = $fai->{$contig}->{'ID'};
	    push(@$ord, $fai->{$contig});
	}
    } else {
	throw("No contig Meta lines in VCF header");
    }

    if (@targets) {
	my $trg = {};
	my $ord = [];
	for my $contig (@targets) {
	    if (! exists($fai->{$contig})) {
		throw('User-specified contig not found in reference file: ',$contig);
	    }
	    $trg->{$contig} = $fai->{$contig};
	    push(@$ord, $fai->{$contig});
	}
	$fai = $trg;
    }
    
    my $fdv = Tabix->new(-data => is_file($ARGV[0],'rf'));
    my $itr = calcIntervals($fdv,$ord,$fai,$width,$step,1);
    my $fd1 = open("$ARGV[1].ibs",WRITE);
    my $fd2 = open("$ARGV[1].ibd",WRITE);
    my $fd3 = open("$ARGV[1].ibd.stats",WRITE);
    my @ibd;
    
    ibdCore($fd1,$fdv,$itr,$HEADER,$META,\@ibd,$fai,$indv1,$indv2,$options);

    ibdStats($fd2,$fd3,\@ibd,$map,$indv1,$indv2,$evid0);

    $fdv->close();
    $fd1->close();
    $fd2->close();
    $fd3->close();

    ibdWriteRscript("$ARGV[1].ibs","$ARGV[1].ibd",$ARGV[1]);

    runRscript($Rex,"$ARGV[1].Rscript",!$Valkyr::Error::DEBUG);

    report('Finished ',scalar(localtime));

    exit(0);
}
