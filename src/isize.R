
PACKAGE = '__PACKAGE_NAME__'
VERSION = '__PACKAGE_VERSION__'
CONTACT = '__PACKAGE_CONTACT__'
PURPOSE = 'Calculate MLE insert-size'

alpha = 0.05
args = commandArgs(TRUE);

if (length(args) < 1) {
    cat("\n", file=stderr())
    cat("Program: isize (",PURPOSE,")\n", file=stderr(), sep="")
    cat("Version:",PACKAGE,VERSION,"\n", file=stderr())
    cat("Contact:",CONTACT,"\n\n", file=stderr())
    cat('Usage: isize <insert.tsv>',"\n", file=stderr(), sep="");
    cat("\n", file=stderr())
    cat("Notes:\n", file=stderr())
    cat(" - The insert.tsv file contains integer insert sizes\n", file=stderr())
    cat("   for a read pair, one pair per line. Negative values\n", file=stderr())
    cat("   are discarded.\n", file=stderr())
    cat("\n\n", file=stderr())
    quit(status=1);
}

d = data.frame();
for (i in seq(1,length(args))) {
    N = read.table(args[i], header=FALSE, row.names=NULL, stringsAsFactors=FALSE);
    N = N[which(N[,1] > 0),1]; 

    # find the MLE for the first and second moments, but first
    # trim the data to reduce noise
    s = 0.75 * IQR(N); # ~ 3sd 
    n = N[which( abs(N-median(N)) / s <= 4)];

    if (length(n) < 35) {
       cat('ERROR: Accuracy of estimate dubious, too few eligible pairs (',length(n),').', sep='', file=stderr());
       quit(status=1);
   }

   ci.theta  = c();
   theta.hat = nlm(function(theta,x) { sum(-dnorm(x, theta[1], theta[2], log=TRUE)) }, c(median(n),s), x=n, hessian=TRUE);
   hess.inv  = solve(theta.hat$hessian);
   ci.crit   = qnorm(1 - (1 - alpha)/2/length(theta.hat));
   ci.theta[1] = round(ci.crit * sqrt(hess.inv[1,1]), 1);
   ci.theta[2] = round(ci.crit * sqrt(hess.inv[2,2]), 1);

   d = rbind(d,t(c(args[i], length(N), length(n), round(theta.hat$estimate[1],1), ci.theta[1], round(theta.hat$estimate[2],1), ci.theta[2])));


   # now make a pretty plot showing quality of fit
   pdf(paste(args[i],'pdf',sep='.'));
   n = n[which(0 < n & n <= theta.hat$estimate[1]+4*theta.hat$estimate[2])];

   hist(n, xlim=c(0,theta.hat$estimate[1]+4*theta.hat$estimate[2]), col="grey90", probability=TRUE, xlab="Insert Size (bp)", main="MLE-Fit and Observed Insert Size Distributions");
   lines(seq(0,theta.hat$estimate[1]+4*theta.hat$estimate[2]),dnorm(seq(0,theta.hat$estimate[1]+4*theta.hat$estimate[2]), theta.hat$estimate[1], theta.hat$estimate[2]), lty=2);
   abline(h=0);
   legend("topleft", legend=bquote(widehat(mu) == .(round(theta.hat$estimate[1],1)) %+-% .(ci.theta[1])), bty='n', adj=c(0,0)); 
   legend("topleft", legend=bquote(widehat(sigma) == .(round(theta.hat$estimate[2],1)) %+-% .(ci.theta[2])), bty='n', adj=c(0,1));
   dev.off()
}

colnames(d) = c('library','N','n','u','2SE(u)','s','2SE(s)');

write.table(d, quote=FALSE, row.names=FALSE, sep="\t");
