
$AUTHORS = 'Jessen V. Bredeson';
$PACKAGE = '__PACKAGE_NAME__';
$VERSION = '__PACKAGE_VERSION__';
$CONTACT = '__PACKAGE_CONTACT__';
$PURPOSE = 'Submit batch-jobs to a cluster';

require  5.010;

use strict;
use warnings;
use threads;

use Env;
use Getopt::Std;
use File::Spec;
use File::Path qw(make_path);
use File::Which 'which';
use File::Basename qw(basename);
use Valkyr::Error qw(throw warn report debug);
use Valkyr::FileUtil::IO;
use Valkyr::Math::Utils 'max';
use Valkyr::Math::Units::Time qw(HMStoS StoHMS);
use Valkyr::Util::Token '__PROGRAM__';

use Valkyr::Data::Type::Test;
use Valkyr::Data::Type::Assert 
    qw(is_uint is_ufloat is_string is_file is_hashref is_arrayref);

use vars qw(
    $AUTHORS $PURPOSE $PACKAGE $VERSION $CONTACT %SGE_PE %vQ 
    %SGE_FLAG_MAP %SLURM_FLAG_MAP %SLURM_NOTIFY $PID $DEFAULT_QOS $DEFAULT_ACC
);

use constant {
    TRUE => 1,
    FALSE => 0,
    INIT_BATCH_ONLY => 0x1,
    SUBMIT_AND_WAIT => 0x2,
    SPAWN_PARALLEL => 0x4
};

BEGIN {
    %SGE_PE = map {'pe_'.$_ => 1} qw(fill slots rrobin);
    %SGE_FLAG_MAP = (
	'E' => 'pe', 'H' => 'hold_jid', 'j' => 'j',
	'M' => 'M',  'm' => 'm',        'n' => 'N',
	'P' => 'P',  'w' => 'notify',
	);
    %SLURM_FLAG_MAP = (
	'm' => 'mail-type', 'P' => 'A',
	'M' => 'mail-user', 'n' => 'job-name',
	'j' =>  '', 'E' =>  '',
	'w' =>  '', 'H' => 'depend',
	'p' => 'c', 'R' => 'mem-per-cpu',
	't' => 't', 'q' => 'p',
	'Q' => 'qos', 'C' => 'constraint',
	);
    %SLURM_NOTIFY = (
	'NONE'    => 1,
	'BEGIN'   => 1,
	'END'     => 1,
	'FAIL'    => 1,
	'REQUEUE' => 1,
	'ALL'     => 1
	);
    $PID = $$;
    $DEFAULT_ACC = '';  # ($ENV{'NERSC_HOST'}//'') eq 'cori' ? 'plant' : '';
    $DEFAULT_QOS = '';  # ($ENV{'NERSC_HOST'}//'') eq 'cori' ? 'genepool_shared' : '';
}


sub is_datetime {
    my $string = is_string(shift);

    # HH:MM[:SS] [AM|PM]
    return TRUE if $string =~ /^\d\d:\d\d(?:\:\d\d)?\s?(?:[AP]M)?$/;
    # MMDD[YY]
    return TRUE if $string =~ /^\d\d\d\d(?:\d\d)?$/;
    # MM.DD[.YY]
    return TRUE if $string =~ /^\d\d\.\d\d(?:\.\d\d)?$/;
    # MM/DD[/YY]
    return TRUE if $string =~ /^\d\d\/\d\d(?:\/\d\d)?$/;
    # MM/DD[/YY]-HH:MM[:SS]
    return TRUE if $string =~ /^\d\d\/\d\d(?:\/\d\d)?-\d\d:\d\d(?:\:\d\d)?$/;
    # YYYY-MM-DD[THH:MM[:SS]]
    return TRUE if $string =~ /^\d\d\d\d-\d\d-\d\d(?:T\d\d:\d\d(?:\:\d\d)?)?$/;

    return FALSE;
}


sub is_hint {  # "human-readable int"
    my $string = is_string(shift);

    return TRUE if $string =~ /^\d+[KMGTkmgt]$/;

    return FALSE;
}


sub parseGridSubmitStatusSGE {
    my $status = shift;
    
    if ($status =~ /^Your job.*? (\d+)(?:\.([\d,\-]+))?/) {
	my $jid = $1;
	my @tid = ();
	if (defined($2)) {
	    for my $r (split(',', $2)) { 
		my ($s, $e) = split(/-/, $r); 
		push(@tid, ($s..($e||$s)));
	    }
	}
    }
}


sub submitGridJobSGE {
    my ($workdir, $command, $jobarray) = @_;
    ### SUBMIT ARRAY TO CLUSTER
    my $home = File::Spec->rel2abs();

    report('Submitting: ', $command, ' ', $jobarray);

    my  $status  = `$command $jobarray 2>&1`;
    if ($status !~ /has\s+been\s+submitted/) {
	chomp($status);
	warn("Failure: $jobarray");
	map {report('ERROR: ', $_)} split("\n", $status);
	exit(2);
    }
    return TRUE;
}


sub submitGridJobSLURM {
    my ($workdir, $command, $jobarray) = @_;
    ### SUBMIT ARRAY TO CLUSTER
    my $home = File::Spec->rel2abs();

    report('Submitting: ', $command, ' ', $jobarray);

    my  $status  = `$command $jobarray 2>&1`;
    if ($status !~ /Submitted\s+batch\s+job/) {
	chomp($status);
	warn("Failure: $jobarray");
	map {report('ERROR: ', $_)} split("\n", $status);
	exit(2);
    }
    return TRUE;
}    


sub mkGridJobArraySLURM {
    my $workdir  = is_file(shift, 'drwx');
    my $infile   = is_file(shift, 'fr');
    my $count    = is_uint(shift);
    my $argv     = is_hashref(shift);

    my $shell    = is_file($argv->{'e'}, 'x');
    my $index    = is_uint($argv->{'i'});
    my $step     = is_uint($argv->{'S'});
    my $opt      = is_uint($argv->{'-'});

    if ($step > $count) {
	throw("Step size must be less than task count ($step < $count).");
    }
    $infile      = basename($infile);

    my $jobarray = prefixFile($workdir, $infile).'.array';
    my $taskfile = prefixFile($workdir, $infile, $PID);
    my $array    = open($jobarray, WRITE);
    my $shellexe = basename($shell);
    $array->print("#!$shell\n");
    $array->print("#SBATCH -o ".prefixSTDOUT($workdir, $infile)."%a\n");
    $array->print("#SBATCH -e ".prefixSTDERR($workdir, $infile)."%a\n") if ! $argv->{'j'};
    
    if ($shellexe =~ /csh$/) {
	if ($step) {
	    $array->print("#SBATCH -a $index-$count:$step\n");
	    $array->print("\n");
	    $array->print("set TASK_PREFIX=\"$taskfile\";\n");
	    $array->print("set TASK_OFFSET=0;\n");
	    $array->print("while ( \${TASK_OFFSET} < $step )\n");
	    $array->print("    set TASK_NUMBER=`expr \${SLURM_ARRAY_TASK_ID} + \${TASK_OFFSET}`;\n");
	    $array->print("    set TASK_NUMBER=`printf '%05d' \${TASK_NUMBER}`;\n");
	    $array->print("    set TASK_OFFSET=`expr \${TASK_OFFSET} + 1`;\n");
	    $array->print("    if ( -e \"\${TASK_PREFIX}_\${TASK_NUMBER}\" ) then\n");
	    $array->print("        $shell \"\${TASK_PREFIX}_\${TASK_NUMBER}\"", ($opt & SPAWN_PARALLEL ? ' &' : ';'), "\n");
	    $array->print("    endif\n");
	    $array->print("end\n");
	    $array->print("wait;\n") if $opt & SPAWN_PARALLEL;
	} else {
	    $array->print("#SBATCH -a $index-$count\n");
	    $array->print("\n");
	    $array->print("set TASK_PREFIX=\"$taskfile\";\n");
	    $array->print("set TASK_NUMBER=`printf '%05d' \${SLURM_ARRAY_TASK_ID}`;\n");
	    $array->print("$shell \"\${TASK_PREFIX}_\${TASK_NUMBER}\";\n");
	}
    } else {
	if ($step) {
	    $array->print("#SBATCH -a $index-$count:$step\n");
	    $array->print("\n");
	    $array->print("TASK_PREFIX=\"$taskfile\";\n");
	    $array->print("for TASK_OFFSET in {0..", $step-1, "}; do\n");
	    $array->print("    TASK_NUMBER=`expr \${SLURM_ARRAY_TASK_ID} + \${TASK_OFFSET}`;\n");
	    $array->print("    TASK_NUMBER=`printf '%05d' \${TASK_NUMBER}`;\n");
	    $array->print("    if [ -e \"\${TASK_PREFIX}_\${TASK_NUMBER}\" ]; then\n");
	    $array->print("        $shell \"\${TASK_PREFIX}_\${TASK_NUMBER}\"", ($opt & SPAWN_PARALLEL ? ' &' : ';'), "\n");
	    $array->print("    fi\n");
	    $array->print("done\n");
	    $array->print("wait;\n") if $opt & SPAWN_PARALLEL;
	} else {
	    $array->print("#SBATCH -a $index-$count\n");
	    $array->print("\n");
	    $array->print("TASK_PREFIX=\"$taskfile\";\n");
	    $array->print("TASK_NUMBER=`printf '%05d' \${SLURM_ARRAY_TASK_ID}`;\n");
	    $array->print("$shell \"\${TASK_PREFIX}_\${TASK_NUMBER}\";\n");
	}
    }
    $array->close();
    
    return $jobarray;
}

sub mkGridJobArraySGE {
    my $workdir  = is_file(shift, 'drwx');
    my $infile   = is_file(shift, 'fr');
    my $count    = is_uint(shift);
    my $argv     = is_hashref(shift);

    my $shell    = is_file($argv->{'e'}, 'x');
    my $index    = is_uint($argv->{'i'});
    my $step     = is_uint($argv->{'S'});
    my $opt      = is_uint($argv->{'-'});
    
    if ($step > $count) {
	throw("Step size must be less than task count ($step < $count).");
    }
    $infile      = basename($infile);

    my $jobarray = prefixFile($workdir, $infile).'.array';
    my $taskfile = prefixFile($workdir, $infile, $PID);
    my $array    = open($jobarray, WRITE);
    my $shellexe = basename($shell);
    $array->print("#!$shell\n");
    $array->print("#\$ -S $shell\n");
    $array->print("#\$ -cwd\n");
    $array->print("#\$ -o ".prefixSTDOUT($workdir, $infile)."\$TASK_ID\n");
    $array->print("#\$ -e ".prefixSTDERR($workdir, $infile)."\$TASK_ID\n");

    if ($shellexe =~ /csh$/) {
	if ($step) {
	    $array->print("#\$ -t $index-$count:$step\n");
	    $array->print("\n");
	    $array->print("set TASK_PREFIX=\"$taskfile\";\n");
	    $array->print("set TASK_OFFSET=0;\n");
	    $array->print("while ( \${n} < $step )\n");
	    $array->print("    set TASK_NUMBER=`expr \${SGE_TASK_ID} + \${TASK_OFFSET}`;\n");
	    $array->print("    set TASK_NUMBER=`printf '%05d' \${TASK_NUMBER}`;\n");
	    $array->print("    set TASK_OFFSET=`expr \${TASK_OFFSET} + 1;\n");
	    $array->print("    if ( -e \"\${TASK_PREFIX}_\${TASK_NUMBER}\" ) then\n");
	    $array->print("        $shell \"\${TASK_PREFIX}_\${TASK_NUMBER}\"", ($opt & SPAWN_PARALLEL ? ' &' : ';'), "\n");
	    $array->print("    endif\n");
	    $array->print("end\n");
	    $array->print("wait;\n") if $opt & SPAWN_PARALLEL;
	} else {
	    $array->print("#\$ -t $index-$count\n\n");
	    $array->print("set TASK_PREFIX=\"$taskfile\";\n");
	    $array->print("set TASK_NUMBER=`printf '%05d' \${SGE_TASK_ID}`;\n");
	    $array->print("$shell \"\${TASK_PREFIX}_\${TASK_NUMBER}\";\n");
	}
    } else {
	if ($step) {
	    $array->print("#\$ -t $index-$count:$step\n");
	    $array->print("\n");
	    $array->print("TASK_PREFIX=\"$taskfile\";\n");
	    $array->print("for TASK_OFFSET in {0..", $step-1, "}; do\n");
	    $array->print("    TASK_NUMBER=`expr \${SGE_TASK_ID} + \${TASK_OFFSET}`;\n");
	    $array->print("    TASK_NUMBER=`printf '%05d' \${TASK_NUMBER}`;\n");
	    $array->print("    if [ -e \"\${TASK_PREFIX}_\${TASK_NUMBER}\" ]; then\n");
	    $array->print("        $shell \"\${TASK_PREFIX}_\${TASK_NUMBER}\"", ($opt & SPAWN_PARALLEL ? ' &' : ';'), "\n");
	    $array->print("    fi\n");
	    $array->print("done\n");
	    $array->print("wait;\n") if $opt & SPAWN_PARALLEL;
	} else {
	    $array->print("#\$ -t $index-$count\n");
	    $array->print("\n");
	    $array->print("TASK_PREFIX=\"$taskfile\";\n");
	    $array->print("TASK_NUMBER=`printf '%05d' \${SGE_TASK_ID}`;\n");
	    $array->print("$shell \"\${TASK_PREFIX}_\${TASK_NUMBER}\";\n");
	}
    }
    $array->close();
    
    return $jobarray;
}


sub mkGridJobTask {
    my $shell     = shift;
    my $workdir   = shift;
    my $infile    = shift;
    my $count     = shift;
    my $taskcmd   = shift;
    my $monitored = shift;

    my $taskfile  = taskFileWithPID($workdir, basename($infile), $count);
    my $statfile  = statusFile($taskfile);
    my $task      = open($taskfile, WRITE);

    $task->print("#!$shell\n");
    if ($shell =~ /csh/) {
	$task->print("setenv PERL5LIB \"$ENV{'PERL5LIB'}\";\n");
    } else {
	$task->print("export PERL5LIB=\"$ENV{'PERL5LIB'}\";\n");
    }
    $task->print(__PROGRAM__, " create '$statfile.active' && \\\n");
    $task->print("    ", __PROGRAM__, " sentinel '$taskfile' active && \\\n");
    $task->print("    $taskcmd && \\\n");
    $task->print("    ", __PROGRAM__, " create '$statfile.complete' && \\\n");
    $task->print("    ", __PROGRAM__, " delete '$statfile.active' && \\\n") if ! $monitored;
    $task->print("    ", __PROGRAM__, " sentinel '$taskfile' done\n");
    $task->close();
    
    return $taskfile;
}


sub resubmitToGridCoreSGE {
    report("Preparing previously-failed job tasks");
    my $infile  = is_file(shift, 'fr');
    my $workdir = is_file(shift, 'drwx');
    my $argv    = is_hashref(shift);

    my $shell   = is_file($argv->{'e'}, 'fx');
    my $index   = is_uint($argv->{'i'});
    my $writeit = $argv->{'c'};
    my $stoprun = $argv->{'-'} & INIT_BATCH_ONLY;
    my $monitor = $argv->{'-'} & SUBMIT_AND_WAIT;
    my $prefix  = $infile eq '-' ? 'stdin' : $infile;
    my $command = assembleCommandLineSGE($argv, $workdir, $prefix);
    
    $PID = is_uint($argv->{'r'});  # previous submission PID
    
    ### PREPARE JOB SCRIPTS
    my @newtasks;
    my @oldtasks;
    my $count = $index - 1;
    my $batch = open($infile, READ);
    while (defined( my $jobcmd = $batch->getline() )) {
	$jobcmd =~ /^#|^\s*$/ && next;
	$jobcmd =~ s/\;$//;
	chomp($jobcmd);

	$count++;
	
	my $taskfile1 = taskFileWithout($workdir, $infile, $count);
	my $taskfile2 = taskFileWithPID($workdir, $infile, $count);
	my $statfile1 = statusFile($taskfile1);
	my $statfile2 = statusFile($taskfile2);
	my $taskfile;
	
	if (Valkyr::Data::Type::Test::is_file($taskfile2, 'fs')) {
	    $taskfile = $taskfile2;
	} elsif (Valkyr::Data::Type::Test::is_file($taskfile1, 'fs')) {
	    $taskfile = $taskfile1;
	} else {
	    $taskfile = mkGridJobTask($shell, $workdir, $prefix, $count, $jobcmd, $monitor);
	}
	if ((! Valkyr::Data::Type::Test::is_file($statfile2.'.complete', 'f')) &&
	    (! Valkyr::Data::Type::Test::is_file($statfile1.'.complete', 'f'))) {
	    print(STDOUT "$jobcmd\n") if $writeit;
	    push(@oldtasks, $taskfile);
	}
    }
    close($batch);

    return FALSE if $writeit;
    return FALSE if !@oldtasks;  # all jobs complete
    
    ### PREPARE NEW WRAPPER TASKS ###
    $PID = $$;
    $count = $index - 1;
    for my $task (@oldtasks) {
	push(@newtasks, mkGridJobTask($shell, $workdir, $prefix, ++$count, "$shell $task", $monitor));
    }
    
    ### PREPARE ARRAY SCRIPT
    my $jobarray = mkGridJobArraySGE($workdir, $prefix, $count, $argv);

    return TRUE if $stoprun;
    
    submitGridJobSGE($workdir, $command, $jobarray);
    monitorGridJob(\@newtasks, HMStoS($argv->{'t'})) if $monitor;
    
    return TRUE;
}


sub submitToGridCoreSGE {
    report("Preparing new job tasks");
    my $infile  = is_file(shift, 'fr');
    my $workdir = is_file(shift, 'drwx');
    my $argv    = is_hashref(shift);

    my $shell   = is_file($argv->{'e'}, 'fx');
    my $index   = is_uint($argv->{'i'});
    my $stoprun = $argv->{'-'} & INIT_BATCH_ONLY;
    my $monitor = $argv->{'-'} & SUBMIT_AND_WAIT;
    my $prefix  = $infile eq '-' ? 'stdin' : $infile;
    my $command = assembleCommandLineSGE($argv, $workdir, $prefix);
    
    ### PREPARE JOB SCRIPTS
    my @tasks;
    my $count = $index - 1;
    my $batch = open($infile, READ);
    while (defined( my $jobcmd = $batch->getline() )) {
	$jobcmd =~ /^#|^\s*$/ && next;
	$jobcmd =~ s/\;$//;
	chomp($jobcmd);
	
	push(@tasks, mkGridJobTask($shell, $workdir, $prefix, ++$count, $jobcmd, $monitor));
    }
    close($batch);

    ### PREPARE ARRAY SCRIPT
    my $jobarray = mkGridJobArraySGE($workdir, $prefix, $count, $argv);

    return TRUE if $stoprun;
    
    submitGridJobSGE($workdir, $command, $jobarray);
    monitorGridJob(\@tasks, HMStoS($argv->{'t'})) if $monitor;

    return TRUE;
}    


sub resubmitToGridCoreSLURM {
    report("Preparing previously-failed job tasks");
    my $infile  = is_file(shift, 'fr');
    my $workdir = is_file(shift, 'drwx');
    my $argv    = is_hashref(shift);

    my $shell   = is_file($argv->{'e'}, 'fx');
    my $index   = is_uint($argv->{'i'});
    my $writeit = $argv->{'c'};
    my $stoprun = $argv->{'-'} & INIT_BATCH_ONLY;
    my $monitor = $argv->{'-'} & SUBMIT_AND_WAIT;
    my $prefix  = $infile eq '-' ? 'stdin' : $infile;
    my $command = assembleCommandLineSLURM($argv, $workdir, $prefix);

    $PID = is_uint($argv->{'r'});  # previous submission PID
    
    ### PREPARE JOB SCRIPTS
    my @newtasks;
    my @oldtasks;
    my $count = $index - 1;
    my $batch = open($infile, READ);
    while (defined( my $jobcmd = $batch->getline() )) {
	$jobcmd =~ /^#|^\s*$/ && next;
	$jobcmd =~ s/\;$//;
	chomp($jobcmd);

	$count++;
	
	my $taskfile1 = taskFileWithout($workdir, $infile, $count);
	my $taskfile2 = taskFileWithPID($workdir, $infile, $count);
	my $statfile1 = statusFile($taskfile1);
	my $statfile2 = statusFile($taskfile2);
	my $taskfile;
	
	if (Valkyr::Data::Type::Test::is_file($taskfile2, 'fs')) {
	    $taskfile = $taskfile2;
	} elsif (Valkyr::Data::Type::Test::is_file($taskfile1, 'fs')) {
	    $taskfile = $taskfile1;
	} else {
	    $taskfile = mkGridJobTask($shell, $workdir, $prefix, $count, $jobcmd, $monitor);
	}
	if ((! Valkyr::Data::Type::Test::is_file($statfile2.'.complete', 'f')) &&
	    (! Valkyr::Data::Type::Test::is_file($statfile1.'.complete', 'f'))) {
	    print(STDOUT "$jobcmd\n") if $writeit;
	    push(@oldtasks, $taskfile);
	}
    }
    close($batch);

    return FALSE if $writeit;
    return FALSE if !@oldtasks;  # all jobs complete

    ### PREPARE NEW WRAPPER TASKS ###
    $PID = $$;
    $count = $index - 1;
    for my $task (@oldtasks) {
	push(@newtasks, mkGridJobTask($shell, $workdir, $prefix, ++$count, "$shell $task", $monitor));
    }
    
    ### PREPARE ARRAY SCRIPT
    my $jobarray = mkGridJobArraySLURM($workdir, $prefix, $count, $argv);

    return TRUE if $stoprun;
    
    submitGridJobSLURM($workdir, $command, $jobarray);
    monitorGridJob(\@newtasks, HMStoS($argv->{'t'})) if $monitor;

    return TRUE;
}    


sub submitToGridCoreSLURM {
    report("Preparing new job tasks");
    my $infile  = is_file(shift, 'fr');
    my $workdir = is_file(shift, 'drwx');
    my $argv    = is_hashref(shift);

    my $shell   = is_file($argv->{'e'}, 'fx');
    my $index   = is_uint($argv->{'i'});
    my $stoprun = $argv->{'-'} & INIT_BATCH_ONLY;
    my $monitor = $argv->{'-'} & SUBMIT_AND_WAIT;
    my $prefix  = $infile eq '-' ? 'stdin' : $infile;
    my $command = assembleCommandLineSLURM($argv, $workdir, $prefix);
    
    ### PREPARE JOB SCRIPTS
    my @tasks;
    my $count = $index - 1;
    my $batch = open($infile, READ);
    while (defined( my $jobcmd = $batch->getline() )) {
	$jobcmd =~ /^#|^\s*$/ && next;
	$jobcmd =~ s/\;$//;
	chomp($jobcmd);
	
	push(@tasks, mkGridJobTask($shell, $workdir, $prefix, ++$count, $jobcmd, $monitor));
    }
    close($batch);

    ### PREPARE ARRAY SCRIPT
    my $jobarray = mkGridJobArraySLURM($workdir, $prefix, $count, $argv);

    return TRUE if $stoprun;
    
    submitGridJobSLURM($workdir, $command, $jobarray);
    monitorGridJob(\@tasks, HMStoS($argv->{'t'})) if $monitor;

    return TRUE;
}


sub submitToGridCore {
    my $inbatch = is_file(shift, 'f');
    my $workdir = is_file(shift, 'd');
    my $argv    = is_hashref(shift);
    
    if (which('sbatch')) {
	if (! validateOptsSLURM($argv)) {
	    &SUBMIT_USAGE;
	}
	if ($argv->{'r'}) {
	    return resubmitToGridCoreSLURM($inbatch, $workdir, $argv);
	} else {
	    return submitToGridCoreSLURM($inbatch, $workdir, $argv);
	}
	
    } elsif (which('qsub')) {
	if (! validateOptsSGE($argv)) {
	    &SUBMIT_USAGE;
	}
	if ($argv->{'r'}) {
	    return resubmitToGridCoreSGE($inbatch, $workdir, $argv);
	} else {
	    return submitToGridCoreSGE($inbatch, $workdir, $argv);
	}
    }
    throw('Could not detect scheduler, no submission script in PATH.');
}

sub prefixSTDOUT {
    return File::Spec->catfile(is_string(shift), basename(is_string(shift)).".o$PID.")
}

sub prefixSTDERR {
    return File::Spec->catfile(is_string(shift), basename(is_string(shift)).".e$PID.")
}
	
sub statusFile {
    my ($vol, $path, $base) = File::Spec->splitpath(is_string(shift));
    return File::Spec->catpath($vol, $path, ".$base");
}

sub prefixFile {
    my $path = is_string(shift);
    my $file = is_string(shift);
    my $pid = '';
    if (@_) {
	$pid = sprintf('_%d', is_uint(shift));
    }
    return File::Spec->catfile($path, $file.$pid);
}
sub taskFileWithout {
    return sprintf("%s\_%05s", prefixFile(shift, shift), is_uint(shift));
}

sub taskFileWithPID {
    return sprintf("%s\_%05s", prefixFile(shift, shift, $PID), is_uint(shift));
}

sub pidFile {
    return File::Spec->catfile(is_file(shift, 'drwx'), 'submit.pid');
}

sub readPidFile {
    my $procfh = open(pidFile(shift), 'r');

    my @pids;
    while (my $line = <$procfh>) {
	chomp($line);
	push(@pids, $line+0);
    }
    close($procfh);

    return \@pids;
}

sub writePidFile {
    my $procfh = open(pidFile(shift), 'w');    
    my $pids = is_arrayref(shift);

    for my $pid (@$pids) {
	print($procfh "$pid\n");
    }
    close($procfh);
}


sub monitorGridJob {
    my @queued  = map { statusFile($_) } reverse(@{is_arrayref(shift)});
    my @cached  = @queued;
    my $maxtime = is_uint(shift);

    report('Queueing tasks...');

    my @active;
    my $active   = 0;
    my $failed   = 0;
    my $tottime  = 0;
    my $runtime  = 0;
    my $interval = sqrt($maxtime - $runtime);
    my $killtime = max(2*$maxtime+sqrt($maxtime), HMStoS('1:0:0'));
    report("    Queued     Active   Time-out      Total");
    while (@queued) {
	report(join(' ', map{sprintf("%10s", $_)} scalar(@queued), scalar(@active), StoHMS($maxtime), StoHMS($tottime)));
	for(my $task = $#active; defined($#active) && 0 <= $task; --$task) {
	    if (Valkyr::Data::Type::Test::is_file("$active[$task].complete", 'f')) {
		unlink("$active[$task].active");
		splice(@active, $task, 1);
	    }
	}
	for(my $task = $#queued; defined($#queued) && 0 <= $task; --$task) {
	    if (Valkyr::Data::Type::Test::is_file("$queued[$task].active", 'f')) {
		unshift(@active, splice(@queued, $task, 1));
		$active = 1;
	    }
	}
	if (@queued) { 
	    sleep($interval);
	} 
	# if (!$active && $tottime > $killtime) {
	#     throw(scalar(@queued), ' queued tasks timed out.');
	# }

	$tottime += $interval;
    }
    
    report('All tasks active...');
    while (@active) {
	if ($runtime > $maxtime) {
	    warn(scalar(@active), ' active tasks timed out.');
	    exit(2);
	}
	report(join(' ', map{sprintf("%10s", $_)} scalar(@queued), scalar(@active), StoHMS($maxtime-$runtime), StoHMS($tottime)));
	for(my $task = $#active; 0 <= $task; --$task) {
	    if (Valkyr::Data::Type::Test::is_file("$active[$task].complete", 'f')) {
		unlink("$active[$task].active");
		splice(@active, $task, 1);
	    }
	}
	@active && sleep($interval);
	
	$runtime += $interval;
	$tottime += $interval;
	$interval = sqrt(max(1, $maxtime - $runtime));
    }
    report('All tasks complete.');
    
    for my $s (@cached) {
	if (! Valkyr::Data::Type::Test::is_file($s.'.complete', 'e')) {
	    $failed++;
	}
    }
    report("Failed tasks: $failed");

    return TRUE;
}


sub mkLocalJobTask {
    my $shell   = shift;
    my $workdir = shift;
    my $infile  = shift;
    my $count   = shift;
    my $taskcmd = shift;
    my $join    = shift;

    my $taskfile = taskFileWithPID($workdir, basename($infile), $count);
    my $statfile = statusFile($taskfile);
    my $task     = open($taskfile, WRITE);
    
    $task->print('#!'."$shell\n");
    $task->print(__PROGRAM__, " create '$statfile.active' && \\\n");
    $task->print("    ", __PROGRAM__, " sentinel '$taskfile' active && \\\n");
    $task->print("     $taskcmd && \\\n");
    $task->print("    ", __PROGRAM__, " create '$statfile.complete' && \\\n");
    $task->print("    ", __PROGRAM__, " delete '$statfile.active' && \\\n");    
    $task->print("    ", __PROGRAM__, " sentinel '$taskfile' done\n");
    $task->close();
    
    return $join 
	? "$shell $taskfile &>".prefixSTDOUT($workdir, $infile).$count 
	: "$shell $taskfile  >".prefixSTDOUT($workdir, $infile).$count
	. ' 2>'.prefixSTDERR($workdir, $infile).$count;
}


sub resubmitToLocal {
    report("Preparing previously-failed job tasks");
    my $infile  = is_file(shift, 'fr');
    my $workdir = is_file(shift, 'drwx');
    my $argv    = is_hashref(shift);
    my $shell   = $argv->{'e'};
    my $join    = $argv->{'j'};
    
    $PID = is_uint($argv->{'r'});  # previous run PID
    
    my @newtasks;
    my @oldtasks;
    my $count = 0;
    my $prefx = $infile eq '-' ? 'stdin' : $infile;
    my $batch = open($infile, READ);
    while (defined( my $jobcmd = $batch->getline() )) {
	$jobcmd =~ /^#|^\s*$/ && next;
	$jobcmd =~ s/\;$//;
	chomp($jobcmd);
	
	my $taskfile1 = taskFileWithout($workdir, $infile, $count);
	my $taskfile2 = taskFileWithPID($workdir, $infile, ++$count);
	my $statfile1 = statusFile($taskfile1);
	my $statfile2 = statusFile($taskfile2);
	my $taskfile;
	
	if (Valkyr::Data::Type::Test::is_file($taskfile2, 'fs')) {
	    $taskfile = $taskfile2;
	} elsif (Valkyr::Data::Type::Test::is_file($taskfile1, 'fs')) {
	    $taskfile = $taskfile1;
	} else {
	    $taskfile = mkLocalJobTask($shell, $workdir, $prefx, $count, $jobcmd, $join);
	}
	if ((! Valkyr::Data::Type::Test::is_file($statfile2.'.complete', 'f')) &&
	    (! Valkyr::Data::Type::Test::is_file($statfile1.'.complete', 'f'))) {
	    print(STDOUT "$jobcmd\n") if $argv->{'c'};
	    push(@oldtasks, $taskfile);
	}
    }
    close($batch);

    ### PREPARE NEW WRAPPER TASKS ###
    $PID = $$;
    $count = 0;
    for my $task (@oldtasks) {
	push(@newtasks, mkLocalJobTask($shell, $workdir, $prefx, ++$count, "$shell $task", $join))
    }    
    return @newtasks;
} 

   
sub submitToLocal {
    report("Preparing new job tasks");
    my $infile  = is_file(shift, 'fr');
    my $workdir = is_file(shift, 'drwx');
    my $argv    = is_hashref(shift);
    my $shell   = $argv->{'e'};
    my $join    = $argv->{'j'};
    
    my @tasks;
    my $count = 0;
    my $prefx = $infile eq '-' ? 'stdin' : $infile;
    my $batch = open($infile, READ);
    while (defined( my $jobcmd = $batch->getline() )) {
	$jobcmd =~ /^#|^\s*$/ && next;
	$jobcmd =~ s/\;$//;
	chomp($jobcmd);
	
	push(@tasks, mkLocalJobTask($shell, $workdir, $prefx, ++$count, $jobcmd, $join));
    }
    close($batch);
    
    return @tasks;
}


sub submitToLocalCore {
    my $infile  = is_file(shift, 'fr');
    my $workdir = is_file(shift, 'drwx');
    my $argv    = is_hashref(shift);
    my $threads = is_uint($argv->{'T'});
    my $writeit = $argv->{'c'};
    my $stoprun = $argv->{'-'} & INIT_BATCH_ONLY;
    
    my @tasks;
    if ($argv->{'r'}) {
	@tasks = resubmitToLocal($infile, $workdir, $argv)
    } else {
	@tasks = submitToLocal($infile, $workdir, $argv)
    }
    return FALSE if $writeit;
    return FALSE if !@tasks;
    return TRUE  if $stoprun;
    
    my $count = @tasks;
    my @cache = @tasks;
    my @active;
    my $active = 0;
    my $failed = 0;
    my $complete = 0;
    report("    Queued  Complete");
    while (@tasks) {
	@active = threads->list(threads::all);
	$active = scalar(@active);
	if ($active < $threads) {
	    report(map{sprintf("%10s", $_)} $count-$active-$complete, $complete);
	    for(my $t = $active; ($t < $threads) && @tasks; ++$t) {
		my $T = shift(@tasks);
		threads->create(sub { system($T) });
	    }
	}
	for my $t (@active) {
	    if ($t->error()) { 
		$t->exit(2);
	    }
	    if ($t->is_joinable()) {
		$t->join();
		$complete++;
	    }
	}
    }
    report('All tasks active...');
    @active = threads->list(threads::all);
    while (@active) {
	for my $t (@active) {
	    if ($t->error()) {
		$t->exit(2);
	    }
	    if ($t->is_joinable()) {
		$t->join();
		$complete++;
	    }
	}
	@active = threads->list(threads::all);
    }
    report('All tasks complete.');

    for my $t (@cache) {
	my $s = statusFile($t);
	if (! Valkyr::Data::Type::Test::is_file($s.'.complete', 'f')) {
	    $failed++;
	}
    }
    report("Failed tasks: $failed");
	
    return TRUE;
}
	    
	    
sub validateOptsSGE {
    my $opt = is_hashref(shift);

    if (defined($opt->{'e'})) {
	$opt->{'e'} = File::Spec->file_name_is_absolute($opt->{'e'}) ? $opt->{'e'} : which($opt->{'e'});	
	if (!defined($opt->{'e'}) || ! -x $opt->{'e'}) {
	    warn("Shell does not exist or is not executable: ", $opt->{'e'}//'undef');
	    return FALSE;
	}
	
    }
    if (defined($opt->{'a'})) { 
	if ($opt->{'a'} !~ /(?:(?:\d\d)?\d\d)?\d{8}(?:\.\d\d)?/) {
	    warn("Invalid start time format"); 
	    return FALSE;
	}
    }
    if (defined($opt->{'d'})) { 
	if ($opt->{'d'} !~ /(?:(?:\d\d)?\d\d)?\d{8}(?:\.\d\d)?/) {
	    warn("Invalid deadline time format");
	    return FALSE;
	}
    }
    if (defined($opt->{'i'})) {
	if ($opt->{'i'} !~ /^[1-9][0-9]*$/) {
            warn("Task ID must be a positive integer");
	    return FALSE;
	}
    }
    if (defined($opt->{'S'})) {
	if ($opt->{'S'} !~ /^[0-9][0-9]*$/) {
            warn("Step-size must be a positive integer");
	    return FALSE;
	}
    }
    if (defined($opt->{'M'}) || exists($opt->{'m'})) {
	$opt->{'m'} ||= 'e'; $opt->{'M'} //= $ENV{'LOGNAME'};
    }
    if (defined($opt->{'M'})) {
	if ($opt->{'M'} !~ /^[^@]+(?:@[^@]+)?(?:,[^@]+(?:@[^@]+)?)?$/) {
	    warn("Invalid mail address format");
	    return FALSE;
	}
    } elsif (defined($opt->{'w'})) {
	warn("Mail address (-M) required.");
	return FALSE;
    }
    if (defined($opt->{'m'})) {
	if ($opt->{'m'} !~ /^[ebans]+$/) {
	    warn("Invalid mail option format");
	    return FALSE;
	}
    }
    if (defined($opt->{'n'})) {
	if ($opt->{'n'} =~ /[\n\t\r\/:@\\*? ]+/) {
	    warn("Invalid character in job name");
	    return FALSE;
	}
    }
    if (defined($opt->{'P'})) {
	if ($opt->{'P'} !~ /.p$/) {
	    warn("Invalid project name, must end in '.p'");
	    return FALSE;
	}
    }
    if (defined($opt->{'q'})) {
	if (! defined($opt->{'q'}) || $opt->{'q'} !~ /\.[cq]$/) {
	    warn("Invalid queue: ", $opt->{'q'});
	    return FALSE;
	}
    }
    if (defined($opt->{'t'})) {
	if ($opt->{'t'} !~ /^\d+:\d+:\d+$/) {
	    warn("Invalid run-time limit value");
	    return FALSE;
	}
    }
    if (defined($opt->{'R'})) {
	if (is_hint($opt->{'R'})) {
	    $opt->{'R'} = uc($opt->{'R'});
	} elsif ($opt->{'R'} !~ /^(?:[0-9]+\.)?[1-9][0-9]*$/) {
	    warn("Invalid RAM limit, must be a postive float");
	    return FALSE;
	}
    }
    if (defined($opt->{'E'})) {
	if ($opt->{'E'} !~ /^pe_(?:fill|slots|rrobin)$/) {
	    warn("Invalid PE spec: $opt->{'E'}");
	    return FALSE;
	}
	if (! defined($opt->{'p'}) && $opt->{'p'} !~ /^[1-9][0-9]*$/) {
	    warn("Invalid number of slots: ", $opt->{'p'}//'undefined'); 
	    return FALSE;
	};
	$opt->{'E'} .= ' '.$opt->{'p'};

    } elsif (defined($opt->{'p'})) {
	$opt->{'E'} = 'pe_slots '.$opt->{'p'};
    }
    if (defined($opt->{'H'})) {
	if ($opt->{'H'} !~ /^[\d\w]+/) {
	    warn("Invalid job ID or name specified: $opt->{'H'}"); 
	    return FALSE;
	}
    }
    
    return TRUE;
}


sub validateOptsSLURM {
    my $opt = is_hashref(shift);

    if (defined($opt->{'e'})) {
	$opt->{'e'} = File::Spec->file_name_is_absolute($opt->{'e'}) ? $opt->{'e'} : which($opt->{'e'});
	if (!defined($opt->{'e'}) || ! -x $opt->{'e'}) {
	    warn("Shell does not exist or is not executable: ", $opt->{'e'}//'undef');
	    return FALSE;
	}
    }
    if (defined($opt->{'a'})) { 
	if (! is_datetime($opt->{'a'})) {
	    warn("Invalid start time format"); 
	    return FALSE;
	}
    }
    if (defined($opt->{'d'})) { 
	if (! is_datetime($opt->{'d'})) {
	    warn("Invalid deadline time format");
	    return FALSE;
	}
    }
    if (defined($opt->{'i'})) {
	if ($opt->{'i'} !~ /^[1-9][0-9]*$/) {
            warn("Task ID must be a positive integer");
	    return FALSE;
	}
    }
    if (defined($opt->{'S'})) {
	if ($opt->{'S'} !~ /^[0-9][0-9]*$/) {
            warn("Step-size must be a positive integer");
	    return FALSE;
	}
    }
    if (defined($opt->{'M'}) || exists($opt->{'m'})) {
	$opt->{'m'} ||= 'e'; $opt->{'M'} //= $ENV{'LOGNAME'};
    }
    if (defined($opt->{'M'})) {
	if ($opt->{'M'} !~ /^[^@]+(?:@[^@]+)?(?:,[^@]+(?:@[^@]+)?)?$/) {
	    warn("Invalid mail address format");
	    return FALSE;
	}
    }
    if (defined($opt->{'m'})) {
	for my $type (split(/,/, $opt->{'m'})) {
	    if (! $SLURM_NOTIFY{uc($type)}) {
		warn("Invalid mail option or format");
		return FALSE;
	    }
	}
    }
    if (defined($opt->{'n'})) {
	if ($opt->{'n'} =~ /[\n\t\r\/:@\\*? ]+/) {
	    warn("Invalid character in job name");
	    return FALSE;
	}
    }
    if (length($opt->{'P'})) {
	if ($opt->{'P'} !~ /^\S+$/) {
	    warn("Invalid project name (no spaces allowed): ", $opt->{'P'});
	    return FALSE;
	}
    }
    if (length($opt->{'Q'})) {
	if ($opt->{'Q'} !~ /^\S+$/) {
	    warn("Invalid qos value (no spaces allowed): ", $opt->{'Q'});
	    return FALSE;
	}
    }
    if (defined($opt->{'q'})) {
	if ($opt->{'q'} !~ /^\S+$/) {
	    warn("Invalid partition name (no spaces allowed): ", $opt->{'q'});
	    return FALSE;
	}
    }
    if (defined($opt->{'t'})) {
	if ($opt->{'t'} !~ /^\d+:\d+:\d+$/) {
	    warn("Invalid run-time limit value");
	    return FALSE;
	}
    }
    if (defined($opt->{'R'})) {
	if (is_hint($opt->{'R'})) {
	    $opt->{'R'} = uc($opt->{'R'});
	} elsif ($opt->{'R'} !~ /^(?:[0-9]+\.)?[1-9][0-9]*?$/) {
	    warn("Invalid RAM limit, must be a postive float");
	    return FALSE;
	}
    }
    if (defined($opt->{'p'}) && $opt->{'p'} !~ /^[1-9][0-9]*$/) {
	warn("Invalid number of CPUs: ", $opt->{'p'}//'undefined'); 
	return FALSE;
    }
    if (defined($opt->{'H'})) {
	if ($opt->{'H'} !~ /^[\d\w]+/) {
	    warn("Invalid job ID or name specified: $opt->{'H'}"); 
	    return FALSE;
	}
    }
    
    return TRUE;
}


sub assembleCommandLineSGE {
    my $argv  = is_hashref(shift);
    my $wd    = is_string(shift);
    my $batch = is_file(shift, 'fr');    
    my $cmd   = which('qsub');

    ### SETUP THE COMMANDLINE ARGS
    
    for my $flag (qw/a d m M n P E H/) {
	exists($argv->{$flag})  || next;
	defined($argv->{$flag}) || next;
	my $value = $argv->{$flag};
	$cmd .= ' -'.$SGE_FLAG_MAP{$flag}.' '.$value;
    }
    for my $bool (qw/j N/) {
	exists($argv->{$bool}) || next;
	my $value = $argv->{$bool} ? 'y' : 'n';
	$cmd .= ' -'.$SGE_FLAG_MAP{$bool}.' '.$value;
    }
    for my $switch (qw/w/) {
	exists($argv->{$switch}) || next;
	$cmd .= ' -'.$SGE_FLAG_MAP{$switch};
    }
    if (exists($argv->{'q'})) {
	$cmd .= ' -l '.$argv->{'q'};
    }
    if (exists($argv->{'R'})) {
	if (is_hint($argv->{'R'})) {
	    $cmd .= ' -l h_vmem='.$argv->{'R'};
	} else {
	    $cmd .= ' -l h_vmem='.is_ufloat($argv->{'R'}).'G';
	}
    }
    if (exists($argv->{'t'})) {
	$cmd .= ' -l h_rt='.HMStoS($argv->{'t'});
    }

    return $cmd;
}


sub assembleCommandLineSLURM {
    my $argv  = is_hashref(shift);
    my $wd    = is_string(shift);
    my $batch = is_file(shift, 'fr');
    my $cmd  = which('sbatch');

    ### SETUP THE COMMANDLINE ARGS
    
    # switches:
    for my $switch (qw/N/) {
	exists($argv->{$switch}) || next;
	$cmd .= ' -'.$SLURM_FLAG_MAP{$switch};
    }
    # single-dash short args:
    for my $flag (qw/N P p t/) {
	exists($argv->{$flag})  || next;
	length($argv->{$flag}) || next;
	my $value = $argv->{$flag};
	$cmd .= ' -'.$SLURM_FLAG_MAP{$flag}.' '.$value;
    }
    # double-dash long args:
    for my $flag (qw/a m M d n C H Q/) {
	exists($argv->{$flag}) || next;
	length($argv->{$flag}) || next;
	my $value = $argv->{$flag};
	$cmd .= ' --'.$SLURM_FLAG_MAP{$flag}.'='.$value;
    }
    if (exists($argv->{'R'})) {
	if (is_hint($argv->{'R'})) {
	    $cmd .= ' --'.$SLURM_FLAG_MAP{'R'}.'='.$argv->{'R'};
	} else {
	    $cmd .= ' --'.$SLURM_FLAG_MAP{'R'}.'='.int(is_ufloat(1024*$argv->{'R'})).'M';
	}
    }

    return $cmd;
}


sub SUBMIT_USAGE {
    my $PROGRAM = basename(__PROGRAM__);
    die qq/
Usage:   $PROGRAM submit [options] <batch.sh> [<batch.workdir>]

Options: -C <str>     Job submission constraint (SLURM) [null]
         -c           Write failed commands to stdout and exit (sets -r)
         -E <str>     PE Environment (see note 4; SGE only) [null]
         -e <str>     Execute in the <str> shell environment [auto]
         -H <str>     Specify job dependency hold on job ID <str>
         -h           This help message
         -I           Initialize batch files and exit (do not submit)
         -i <uint>    Initialize batch array with starting task ID [1]
         -j           Merge stdout and stderr streams
         -M <list>    Mail checkpoint status to addresses [null]
         -m <list>    Mail at checkpoints (SGE:{abens};
                      SLURM:{NONE, BEGIN, END, FAIL, REQUEUE, ALL}) [null]
         -n <str>     Job name (no spaces allowed) [batch.sh]
         -P <str>     Project\/account to charge job usage to [$DEFAULT_ACC]
         -p <uint>    Parallel Env <uint> number of slots per task [1]
         -Q <str>     Quality-of-service (SLURM only) [$DEFAULT_QOS]
         -q <str>     Target queue\/partition [default]
         -R <ufloat>  Allocate <uint> GB of RAM per core\/thread [1]
         -r           Resubmit failed tasks from previous run
         -S <uint>    Task step-size [1]
         -s           Spawn parallel sub-tasks (requires -S) [serial]
         -T <uint>    Run job on local machine using <uint> threads [0]
         -t <str>     Max runtime limit (HH:MM:SS format) [1:0:0]
         -W           After submitting tasks, wait for job to complete
	              before exiting
         -w           Warn\/notify upon failure (requires -M; SGE only)
         
Notes: 

  1. This script assumes the current machine is pre-configured to submit 
     jobs to the (UGE\/SGE or SLURM) cluster (or use -T instead)

  2. Option '-M' will accept a comma-separate list

  3. Available SGE PEs: slots (default), fill, rrobin.

/;
} 


sub submit {
    my %argv  = (
	'e' => $ENV{'SHELL'},
	'-' => 0, # bit array of boolean switches
	'i' => 1, 
	'j' => FALSE,	
	't' => '1:0:0', 
	'P' => $DEFAULT_ACC,
	'Q' => $DEFAULT_QOS, 
	'R' => 1, 
	'r' => FALSE,
	'S' => 0
    );
    getopts('C:cE:e:H:hIi:jM:m:n:P:p:Q:q:R:rS:sT:t:Ww', \%argv) && 
	(! $argv{'h'}) && (@ARGV == 1 || @ARGV == 2) || &SUBMIT_USAGE;
    for my $o (keys %argv) {
	if    ($o eq 'I') { $argv{'-'} |= INIT_BATCH_ONLY }
	elsif ($o eq 'W') { $argv{'-'} |= SUBMIT_AND_WAIT }
	elsif ($o eq 's') { $argv{'-'} |= SPAWN_PARALLEL }
	elsif ($o eq 'c') { $argv{'r'} = TRUE }
	elsif ($o eq 'T') { is_uint($argv{$o}, \&SUBMIT_USAGE) }
	elsif ($o eq 'e') { is_string($argv{$o}, \&SUBMIT_USAGE) }
    }
    my $inbatch = $ARGV[0];
    my $workdir = @ARGV == 2 ? $ARGV[1] : $inbatch eq '-' ? 'stdin.log' : $inbatch.'.log';
    my $pids = [];
    
    if (! File::Spec->file_name_is_absolute($workdir)) {
	$workdir = File::Spec->rel2abs($workdir);
    }
    if (! (Valkyr::Data::Type::Test::is_file($workdir, 'drwx') || eval { make_path($workdir) })) {
	throw("Cannot create working directory: $workdir");
    }
    if (Valkyr::Data::Type::Test::is_file(pidFile($workdir), 'frw')) {
	$pids = readPidFile($workdir);
    }
    if ($argv{'r'} && @$pids) {
	# The submission functions below will always check the
	# status of the first run's PID, as any subsequent 
	# successful re-run will also cause the original PID's
	# task .complete status file to be generated:
	$argv{'r'} = $pids->[0];
    } else {
	$argv{'r'} = FALSE;  # incase -r was set on an initial run
    }

    my $success = TRUE;
    if ($argv{'T'}) {
	$success = submitToLocalCore($inbatch, $workdir, \%argv);
    } else {
	$success = submitToGridCore($inbatch, $workdir, \%argv);
    }
    if ($success) {
	push(@$pids, $$);
	writePidFile($workdir, $pids);    
    }
    return TRUE;
}


sub CREATE_USAGE {
    die sprintf("Usage: %s create <file_name>\n", basename(__PROGRAM__));
}


sub create {
    @ARGV == 1 || &CREATE_USAGE;

    open($ARGV[0], WRITE)->close();

    return 1;
}

sub DELETE_USAGE {
    die sprintf("Usage: %s delete <file_name>\n", basename(__PROGRAM__));
}

sub delete {
    @ARGV == 1 || &DELETE_USAGE;

    unlink($ARGV[0]);

    return TRUE;
}

sub SENTINEL_USAGE {
    die sprintf("Usage: %s sentinel <taskfile> <taskstatus>\n", basename(__PROGRAM__));
}

sub sentinel {
    @ARGV == 2 || &SENTINEL_USAGE;

    my $username;
    my $hostname;
    my $taskfile = $ARGV[0];
    my $taskstatus = $ARGV[1];
    my $hostname_command = 'hostname';
    if (! File::Spec->file_name_is_absolute($taskfile)) {
	$taskfile = File::Spec->rel2abs($taskfile);
    }
    if ($^O =~ /linux/i) {  # The OS
	$hostname_command .= ' --fqdn';
    }
    $username = $ENV{'USER'};
    $hostname = `$hostname_command`;
    chomp($hostname);
    report("$username\@$hostname:$taskfile $taskstatus");

    return TRUE;
}

sub HELP_MESSAGE {
    my $PROGRAM = basename(__PROGRAM__);
    die qq/
Program: $PROGRAM ($PURPOSE)
Version: $PACKAGE $VERSION
Contact: $CONTACT

Usage:   $PROGRAM <command> [options]

Command: submit    Submit a job to the cluster
         sentinel  Report job execution env
         create    Create touch file
         delete    Delete touch file

/;
}


main: {
    my %FUNCTION = (
	'submit' => \&submit, 'create' => \&create,
	'delete' => \&delete, 'sentinel' => \&sentinel, 
	);
    
    if (!@ARGV || !exists($FUNCTION{$ARGV[0]})) {
	&HELP_MESSAGE;
    } else {
	exit(! &{$FUNCTION{shift(@ARGV)}} );
    }
}

