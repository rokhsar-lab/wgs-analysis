
PREFIX       := /usr/local

SRC_DIR      := src
SUB_DIR      := submodules
BUILD_DIR    := $(PWD)/build

SCRIPT_DIR    = $(BUILD_DIR)/scripts
BIN_DIR       = $(BUILD_DIR)/bin
LIB_DIR       = $(BUILD_DIR)/lib
SHR_DIR       = $(BUILD_DIR)/share
INC_DIR       = $(BUILD_DIR)/include

TOUCH         = touch
ECHO         := echo
AWK          := $(filter /%,$(shell /bin/sh -c 'type awk'))
CAT          := $(filter /%,$(shell /bin/sh -c 'type cat'))
CD           := cd
CP           := $(filter /%,$(shell /bin/sh -c 'type cp'))
SED          := $(filter /%,$(shell /bin/sh -c 'type sed'))
CHMOD        := $(filter /%,$(shell /bin/sh -c 'type chmod'))
CHMOD_DIR     = $(CHMOD) 755
PWD          := pwd

MKDIR        := $(filter /%,$(shell /bin/sh -c 'type mkdir'))
MKDIR_P       = $(MKDIR) -p

RM           := $(filter /%,$(shell /bin/sh -c 'type rm'))
RM_R          = $(RM) -r
RM_RF         = $(RM_R) -f 

#PYTHON       := $(filter /%,$(shell /bin/sh -c 'type python'))
#PYTHON_VER   := $(shell $(PYTHON) --version 2>&1 | awk '{if (/Python/) {split($$2,v,".");print "python"v[1]"."v[2]}}')

GIT          := $(filter /%,$(shell /bin/sh -c 'type git'))
GIT_SUBUPDATE = $(GIT) submodule update --init --recursive
GIT_CHECKOUT  = $(GIT) checkout

INSTALL      := $(filter /%,$(shell /bin/sh -c 'type install'))
INSTALL_LIB   = $(INSTALL) -p -m 644
INSTALL_EXE   = $(INSTALL) -p -m 755
INSTALL_DIR   = $(INSTALL) -p -m 755 -d

PACKAGE    := wgs-analysis
VERSION    := $(shell $(GIT) describe --long --tags --always)
CONTACT    := Jessen V. Bredeson (jessenbredeson\@berkeley.edu)
LICENSE    := LICENSE

TABIX_CONFIG_FLAGS = LIB=$(LIB_DIR) INSTALLSITEARCH=$(LIB_DIR) INSTALLARCHLIB=$(LIB_DIR) INSTALLVENDORARCH=$(LIB_DIR)


SCRIPT_TARGETS = \
	$(SCRIPT_DIR)/isize \
	$(SCRIPT_DIR)/plot-ab \
	$(SCRIPT_DIR)/plot-hapshare \
	$(SCRIPT_DIR)/plot-kmer-stats \
	$(SCRIPT_DIR)/plot-singleton-rate-profile \
	$(SCRIPT_DIR)/plot-snv-rate-hist \
	$(SCRIPT_DIR)/plot-snv-rate-profile \
	$(SCRIPT_DIR)/plot-allele-balance-scat \
	$(SCRIPT_DIR)/plot-ibd \
	$(SCRIPT_DIR)/plot-roh \
	$(SCRIPT_DIR)/GenomeAnalysisTK \
	$(SCRIPT_DIR)/IBD \
	$(SCRIPT_DIR)/balance-fasta \
	$(SCRIPT_DIR)/bedtk \
	$(SCRIPT_DIR)/bwatrim \
	$(SCRIPT_DIR)/ddup \
	$(SCRIPT_DIR)/fxatk \
	$(SCRIPT_DIR)/hapshare \
	$(SCRIPT_DIR)/linker-clipper \
	$(SCRIPT_DIR)/linker-filter \
	$(SCRIPT_DIR)/linker-splitter \
	$(SCRIPT_DIR)/pairwise \
	$(SCRIPT_DIR)/picard \
	$(SCRIPT_DIR)/propersam \
	$(SCRIPT_DIR)/qbatch \
	$(SCRIPT_DIR)/snvrate \
	$(SCRIPT_DIR)/vcftk \
	$(SCRIPT_DIR)/allele-balance-filter \
	$(SCRIPT_DIR)/repair \
	$(SCRIPT_DIR)/hist \
	$(SCRIPT_DIR)/run-wgs-preprocessing

BIN_TARGETS = \
	$(BIN_DIR)/bwa \
	$(SUB_DIR)/htslib/htsfile \
	$(SUB_DIR)/samtools/samtools \
	$(BIN_DIR)/sickle \
	$(BIN_DIR)/seqtk \
	$(BIN_DIR)/fastq-mcf

LIB_TARGETS = \
	$(LIB_DIR)/Valkyr/Error.pm \
	$(LIB_DIR)/Tabix.pm


.PHONY: htslib samtools tabix valkyr sickle fastqmcf fastq-mcf seqtk install clean distclean deps activate

.SUFFIXES: .pl .py .sh .R


all: scripts binaries libraries 

deps: dependencies

dependencies: $(SUB_DIR)/bwa/bwa
dependencies: $(SUB_DIR)/htslib/htsfile
dependencies: $(SUB_DIR)/samtools/samtools
dependencies: $(SUB_DIR)/sickle/sickle
dependencies: $(SUB_DIR)/seqtk/seqtk
dependencies: $(SUB_DIR)/ea-utils/clipper/fastq-mcf 



libs: libraries

libraries: $(LIB_DIR) $(LIB_TARGETS)


$(LIB_DIR):
	$(INSTALL_DIR) $(LIB_DIR)

$(BIN_DIR):
	$(INSTALL_DIR) $(BIN_DIR)

$(SCRIPT_DIR):
	$(INSTALL_DIR) $(SCRIPT_DIR)


scripts: $(SCRIPT_DIR) $(SCRIPT_TARGETS)

$(SCRIPT_DIR)/%: $(SRC_DIR)/%.py
	-$(ECHO) '#!/usr/bin/env python' | $(CAT) - $(LICENSE) $< | \
		$(SED) "s/__PACKAGE_NAME__/$(PACKAGE)/;s/__PACKAGE_VERSION__/$(VERSION)/;s/__PACKAGE_CONTACT__/$(CONTACT)/" >$@

$(SCRIPT_DIR)/%: $(SRC_DIR)/%.sh
	-$(ECHO) '#!/usr/bin/env bash' | $(CAT) - $(LICENSE) $< | \
		$(SED) "s/__PACKAGE_NAME__/$(PACKAGE)/;s/__PACKAGE_VERSION__/$(VERSION)/;s/__PACKAGE_CONTACT__/$(CONTACT)/" >$@

$(SCRIPT_DIR)/%: $(SRC_DIR)/%.R
	-$(ECHO) '#!/usr/bin/env Rscript' | $(CAT) - $(LICENSE) $< | \
		$(SED) "s/__PACKAGE_NAME__/$(PACKAGE)/;s/__PACKAGE_VERSION__/$(VERSION)/;s/__PACKAGE_CONTACT__/$(CONTACT)/" >$@

$(SCRIPT_DIR)/%: $(SRC_DIR)/%.pl
	-$(ECHO) '#!/usr/bin/env perl' | $(CAT) - $(LICENSE) $< | \
		$(SED) "s/__PACKAGE_NAME__/$(PACKAGE)/;s/__PACKAGE_VERSION__/$(VERSION)/;s/__PACKAGE_CONTACT__/$(CONTACT)/" >$@

$(LIB_DIR)/%: $(SRC_DIR)/%
	-$(CAT) $(LICENSE) $< | \
		$(SED) "s/__PACKAGE_NAME__/$(PACKAGE)/;s/__PACKAGE_VERSION__/$(VERSION)/;s/__PACKAGE_CONTACT__/$(CONTACT)/" >$@


bins: binaries
binaries: $(BIN_DIR) $(BIN_TARGETS)

bwa: $(BIN_DIR)/bwa

$(BIN_DIR)/bwa: $(BIN_DIR) $(SUB_DIR)/bwa/bwa
	$(INSTALL_EXE) $(SUB_DIR)/bwa/bwa $(BIN_DIR)/bwa

$(SUB_DIR)/bwa/bwa:
	$(GIT_SUBUPDATE) $(SUB_DIR)/bwa
	$(MAKE) -C $(SUB_DIR)/bwa



htslib: $(SUB_DIR)/htslib/htsfile

$(SUB_DIR)/htslib/htsfile: $(SUB_DIR)/htslib/Makefile
	$(MAKE) all -C $(SUB_DIR)/htslib

$(SUB_DIR)/htslib/Makefile $(SUB_DIR)/htslib/config.mk: $(SUB_DIR)/htslib/configure
	$(CD) $(SUB_DIR)/htslib && ./configure --prefix=$(PREFIX) && $(TOUCH) Makefile

$(SUB_DIR)/htslib/configure: $(SUB_DIR)/htslib/configure.ac 
	$(CD) $(SUB_DIR)/htslib && autoreconf -i

$(SUB_DIR)/htslib/configure.ac:
	$(GIT_SUBUPDATE) $(SUB_DIR)/htslib



samtools: $(SUB_DIR)/samtools/samtools

$(SUB_DIR)/samtools/samtools: $(SUB_DIR)/htslib/htsfile $(SUB_DIR)/samtools/Makefile
	$(MAKE) all -C $(SUB_DIR)/samtools

$(SUB_DIR)/samtools/Makefile $(SUB_DIR)/samtools/config.mk: $(SUB_DIR)/samtools/configure
	$(CD) $(SUB_DIR)/samtools && ./configure --prefix=$(PREFIX) && $(TOUCH) Makefile

$(SUB_DIR)/samtools/configure: $(SUB_DIR)/samtools/configure.ac
	$(CD) $(SUB_DIR)/samtools && autoheader && autoconf -Wno-syntax

$(SUB_DIR)/samtools/configure.ac:
	$(GIT_SUBUPDATE) $(SUB_DIR)/samtools



fastqmcf: fastq-mcf
fastq-mcf: $(BIN_DIR) $(BIN_DIR)/fastq-mcf

$(BIN_DIR)/fastq-mcf: $(SUB_DIR)/ea-utils/clipper/fastq-mcf 
	$(INSTALL_EXE) $(SUB_DIR)/ea-utils/clipper/fastq-mcf $(SUB_DIR)/ea-utils/clipper/determine-phred $(BIN_DIR)

$(SUB_DIR)/ea-utils/clipper/fastq-mcf:
	$(GIT_SUBUPDATE) $(SUB_DIR)/ea-utils
	$(MAKE) fastq-mcf -C $(SUB_DIR)/ea-utils/clipper



seqtk: $(BIN_DIR)/seqtk

$(BIN_DIR)/seqtk: $(BIN_DIR) $(SUB_DIR)/seqtk/seqtk
	$(INSTALL_EXE) $(SUB_DIR)/seqtk/seqtk $(BIN_DIR)/seqtk

$(SUB_DIR)/seqtk/seqtk:
	$(GIT_SUBUPDATE) $(SUB_DIR)/seqtk
	$(MAKE) -C $(SUB_DIR)/seqtk



sickle: $(BIN_DIR)/sickle

$(BIN_DIR)/sickle: $(BIN_DIR) $(SUB_DIR)/sickle/sickle
	$(INSTALL_EXE) $(SUB_DIR)/sickle/sickle $(BIN_DIR)/sickle

$(SUB_DIR)/sickle/sickle:
	$(GIT_SUBUPDATE) $(SUB_DIR)/sickle
	$(MAKE) -C $(SUB_DIR)/sickle



tabix: $(LIB_DIR)/Tabix.pm

$(LIB_DIR)/Tabix.pm: $(LIB_DIR) $(SUB_DIR)/tabix/perl/blib/lib/Tabix.pm
	$(MAKE) -C $(SUB_DIR)/tabix/perl install

$(SUB_DIR)/tabix/perl/blib/lib/Tabix.pm: 
	$(GIT_SUBUPDATE) $(SUB_DIR)/tabix
	cd $(SUB_DIR)/tabix; $(MAKE) -f Makefile
	cd $(SUB_DIR)/tabix/perl; perl Makefile.PL $(TABIX_CONFIG_FLAGS); $(MAKE) -f Makefile



valkyr: $(LIB_DIR)/Valkyr/Error.pm

$(LIB_DIR)/Valkyr/Error.pm: $(LIB_DIR) $(SUB_DIR)/Valkyr/Error.pm 
	$(shell cd $(SUB_DIR); find Valkyr -type d -exec $(INSTALL_DIR) $(LIB_DIR)/{} \;)
	$(shell cd $(SUB_DIR); find Valkyr -name "*.pm" -exec $(INSTALL_LIB) {} $(LIB_DIR)/{} \;)

$(SUB_DIR)/Valkyr/Error.pm:
	$(GIT_SUBUPDATE) $(SUB_DIR)/Valkyr


activate:
	@$(ECHO) 'export PATH="$(PREFIX)/bin:$$PATH";' >activate
	@$(ECHO) 'export MANPATH="$(PREFIX)/share/man:$$MANPATH";' >>activate
	@$(ECHO) 'export INCLUDE_PATH="$(PREFIX)/include:$$INCLUDE_PATH";' >>activate
	@$(ECHO) 'export LIBRARY_PATH="$(PREFIX)/lib:$$LIBRARY_PATH";' >>activate
	@$(ECHO) 'export LD_RUN_PATH="$(PREFIX)/lib:$$LD_RUN_PATH";' >>activate
	@$(ECHO) 'export LD_LIBRARY_PATH="$(PREFIX)/lib:$$LD_LIBRARY_PATH";' >>activate
	@$(ECHO) 'export PERL5LIB="$(PREFIX)/lib:$$PERL5LIB";' >>activate
	@$(ECHO) 'export PKG_CONFIG_PATH="$(PREFIX)/lib/pkgconfig:$$PKG_CONFIG_PATH";' >>activate



install: all install-scripts install-dependencies activate

install-deps: install-dependencies
install-dependencies: dependencies install-binaries install-libraries activate

install-scripts: scripts
	$(INSTALL_DIR) $(PREFIX)/bin
	$(INSTALL_EXE) $(SCRIPT_DIR)/* $(PREFIX)/bin

install-bins: install-binaries
install-binaries: binaries
	make install -C $(SUB_DIR)/htslib
	make install -C $(SUB_DIR)/samtools
	$(INSTALL_DIR) $(PREFIX)/bin
	$(INSTALL_EXE) $(BIN_DIR)/* $(PREFIX)/bin

install-libs: install-libraries
install-libraries: libraries
	$(shell cd $(BUILD_DIR); find lib -type d -exec $(INSTALL_DIR) $(PREFIX)/{} \;)
	$(shell cd $(BUILD_DIR); find lib -type f -exec $(INSTALL_LIB) {} $(PREFIX)/{} \;)

#install-include: binaries
#	$(shell cd $(BUILD_DIR); find include -type d -exec $(INSTALL_DIR) $(PREFIX)/{} \;)
#	$(shell cd $(BUILD_DIR); find include -type f -exec $(INSTALL_LIB) {} $(PREFIX)/{} \;)

#install-share: binaries
#	$(shell cd $(BUILD_DIR); find share -type d -exec $(INSTALL_DIR) $(PREFIX)/{} \;)
#	$(shell cd $(BUILD_DIR); find share -type f -exec $(INSTALL_LIB) {} $(PREFIX)/{} \;)



clean: clean-dependencies clean-libraries
	-$(RM_RF) $(BUILD_DIR)
	-$(RM) activate

clean-deps: clean-dependencies
clean-dependencies:
	-$(MAKE) clean -i -C $(SUB_DIR)/bwa
	-$(MAKE) clean -i -C $(SUB_DIR)/htslib
	-$(MAKE) clean -i -C $(SUB_DIR)/samtools
	-$(MAKE) clean -i -C $(SUB_DIR)/ea-utils/clipper
	-$(MAKE) clean -i -C $(SUB_DIR)/seqtk
	-$(MAKE) clean -i -C $(SUB_DIR)/sickle

clean-libs: clean-libraries
clean-libraries:
	-$(MAKE) clean -i -C $(SUB_DIR)/tabix/perl
	-$(MAKE) clean -i -C $(SUB_DIR)/tabix
